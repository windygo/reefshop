# Copyright (C) 2019 rocketgenius
# This file is distributed under the same license as the Gravity Forms SendGrid Add-On plugin.
msgid ""
msgstr ""
"Project-Id-Version: Gravity Forms SendGrid Add-On 1.2.1\n"
"Report-Msgid-Bugs-To: https://gravityforms.com/support\n"
"Last-Translator: Gravity Forms <support@gravityforms.com>\n"
"Language-Team: Gravity Forms <support@gravityforms.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2019-06-14T18:16:15+00:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.2.0\n"
"X-Domain: gravityformssendgrid\n"

#. Plugin Name of the plugin
msgid "Gravity Forms SendGrid Add-On"
msgstr ""

#. Plugin URI of the plugin
msgid "https://www.gravityforms.com"
msgstr ""

#. Description of the plugin
msgid "Integrates Gravity Forms with SendGrid, allowing Gravity Forms notifications to be sent from your SendGrid account."
msgstr ""

#. Author of the plugin
msgid "rocketgenius"
msgstr ""

#. Author URI of the plugin
msgid "https://www.rocketgenius.com"
msgstr ""

#: class-gf-sendgrid.php:209
msgid "SendGrid makes it easy to reliably send email notifications. If you don't have a SendGrid account, you can %1$ssign up for one here%2$s. Once you have signed up, you can %3$sfind your API keys here%4$s."
msgstr ""

#: class-gf-sendgrid.php:217
msgid "SendGrid API Key"
msgstr ""

#: class-gf-sendgrid.php:225
msgid "SendGrid settings have been updated."
msgstr ""

#: class-gf-sendgrid.php:262
msgid "SendGrid"
msgstr ""

#: class-gf-sendgrid.php:266
msgid "You must %sauthenticate with SendGrid%s before sending emails using their service."
msgstr ""
