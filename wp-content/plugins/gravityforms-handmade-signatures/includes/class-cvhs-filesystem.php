<?php
/********************************************************************
 * Copyright (C) 2019 CodeVerve (https://codeverve.com)
 *
 * This file is part of Gravity Forms Handmade Signature
 *
 * Gravity Forms Handmade Signature is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gravity Forms Handmade Signature is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gravity Forms Handmade Signature. If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

if ( ! class_exists( 'GFForms' ) ) {
	die();
}

class GF_CVHS_Filesystem {

	/**
	 * Access type
	 * @var string
	 */
	private $type;

	/**
	 * Stores the initialized WordPress filesystem.
	 *
	 * @access private
	 * @var WP_Filesystem_Base|WP_Error
	 */
	private $file_system = null;

	/**
	 * Class constructor.
	 *
	 * @global WP_Filesystem_Base $wp_filesystem
	 * @access public
	 */
	public function __construct() {

		if ( ! function_exists( 'get_filesystem_method' ) ) {
			require_once ABSPATH . 'wp-admin/includes/file.php';
		}
		$this->type = get_filesystem_method();
		$this->init();
	}

	/**
	 * Returns the filesystem
	 * @return WP_Filesystem_Base|WP_Error
	 */
	public function get() {
		return $this->file_system;
	}

	/**
	 * Initialize file system
	 */
	private function init() {
		global $wp_filesystem;
		// Step 1: Get filesystem credentials needed for WP_Filesystem.
		$creds = $this->get_credentials();
		// Step 2: Check if the obtained credentials are working.
		$wp_upload_dir = wp_upload_dir();
		if ( ! WP_Filesystem( $creds, $wp_upload_dir['basedir'] ) ) {
			if ( isset( $wp_filesystem->errors ) && $wp_filesystem->errors instanceof \WP_Error ) {
				$this->file_system = $wp_filesystem->errors;
			} else {
				$error_msg         = esc_attr__( sprintf( "GFHS: Unable to save signature. Your %s credentials are wrong! Please note that your site requires correct %s credentials in order to be able to write into the filesystem.", $this->type, $this->type ), "gravityforms-handmade-signatures" );
				$this->file_system = new \WP_Error( 'credentials_error', $error_msg );
			}
			return;
		}
		// Initialize class private members with what was obtained.
		$this->file_system = $wp_filesystem;
	}

	/**
	 * Stripepd down version of request_filesystem_credentials() located in /wp-admin/includes/file.php that:
	 *  - if the fs method is direct returns true
	 *  - if the fs method is other returns the creds
	 *  - if the fs method is other and the credentials aren't set correctly returns false.
	 *
	 * @return bool|mixed|void
	 */
	private function get_credentials() {
		if ( 'direct' === $this->type ) {
			return true;
		}
		$credentials = get_option(
			'ftp_credentials',
			array(
				'hostname' => '',
				'username' => '',
			)
		);
		// If defined, set it to that, Else, If POST'd, set it to that, If not, Set it to whatever it previously was(saved details in option)
		$credentials['hostname'] = defined( 'FTP_HOST' ) ? FTP_HOST : $credentials['hostname'];
		$credentials['username'] = defined( 'FTP_USER' ) ? FTP_USER : $credentials['username'];
		$credentials['password'] = defined( 'FTP_PASS' ) ? FTP_PASS : '';
		// Check to see if we are setting the public/private keys for ssh
		$credentials['public_key']  = defined( 'FTP_PUBKEY' ) ? FTP_PUBKEY : '';
		$credentials['private_key'] = defined( 'FTP_PRIKEY' ) ? FTP_PRIKEY : '';
		// Sanitize the hostname, Some people might pass in odd-data:
		$credentials['hostname'] = preg_replace( '|\w+://|', '', $credentials['hostname'] ); //Strip any schemes off
		if ( strpos( $credentials['hostname'], ':' ) ) {
			list( $credentials['hostname'], $credentials['port'] ) = explode( ':', $credentials['hostname'], 2 );
			if ( ! is_numeric( $credentials['port'] ) ) {
				unset( $credentials['port'] );
			}
		} else {
			unset( $credentials['port'] );
		}
		if ( ( defined( 'FTP_SSH' ) && FTP_SSH ) || ( defined( 'FS_METHOD' ) && 'ssh2' == FS_METHOD ) ) {
			$credentials['connection_type'] = 'ssh';
		} elseif ( ( defined( 'FTP_SSL' ) && FTP_SSL ) && 'ftpext' == $this->type ) { //Only the FTP Extension understands SSL
			$credentials['connection_type'] = 'ftps';
		} elseif ( ! isset( $credentials['connection_type'] ) ) { //All else fails (And it's not defaulted to something else saved), Default to FTP
			$credentials['connection_type'] = 'ftp';
		}
		return $credentials;
	}

}