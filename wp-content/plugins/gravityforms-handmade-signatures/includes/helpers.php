<?php
/********************************************************************
 * Copyright (C) 2019 CodeVerve (https://codeverve.com)
 *
 * This file is part of Gravity Forms Handmade Signature
 *
 * Gravity Forms Handmade Signature is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gravity Forms Handmade Signature is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gravity Forms Handmade Signature. If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

if ( ! class_exists( 'GFForms' ) ) {
	die();
}

/**
 * Convert base64 string to image
 *
 * @param $base64_string
 * @param $output_file
 *
 * @return bool
 */
function gf_cvhs_base64_to_image( $base64_string, $output_file ) {
	$fs_wrapper = new GF_CVHS_Filesystem();
	$wp_fs      = $fs_wrapper->get();

	if ( is_wp_error( $wp_fs ) ) {
		return false;
	}
	// Split the base64 string on commas
	$data = explode( ',', $base64_string );
	if ( count( $data ) > 1 ) {
		$dir = dirname( $output_file );
		if ( $wp_fs->is_writable( $dir ) ) {
			return $wp_fs->put_contents( $output_file, base64_decode( $data[1] ) );
		} else {
			error_log( esc_attr__( sprintf( 'GFHS: %s is not writable.', $dir ), 'gravityforms-handmade-signatures' ) );
		}
	} else {
		error_log( esc_attr__( 'GFHS: Could not decode the base64 signature data to convert into image.', 'gravityforms-handmade-signatures' ) );
	}

	return false;
}

/**
 * Create index file
 *
 * @param null $path
 */
function gf_cvhs_create_index_file( $path = null ) {
	if ( is_null( $path ) ) {
		$path = gf_cvhs_get_signatures_path();
	}
	GFCommon::recursive_add_index_file( $path );
}

/**
 * Generates signature name
 *
 * @param $identifier
 *
 * @return string
 */
function gf_cvhs_generate_signature_name( $identifier ) {
	return md5( $identifier ) . '.png';
}

/**
 * Generates signatures path
 *
 * @param null $filename
 *
 * @return string
 */
function gf_cvhs_get_signatures_path( $filename = null ) {
	$dir  = wp_upload_dir();
	$path = $dir['basedir'] . DIRECTORY_SEPARATOR . 'gravity_forms' . DIRECTORY_SEPARATOR . 'cvhs';
	if ( ! is_null( $filename ) ) {
		$path = $path . DIRECTORY_SEPARATOR . $filename;
	}

	return $path;
}

/**
 * Creates signature directory
 *
 * @param $path
 * @param bool $index
 *
 * @return bool
 */
function gf_cvhs_create_dir( $path, $index = true ) {
	if ( ! file_exists( $path ) ) {
		$is_created = wp_mkdir_p( $path );
		if ( $is_created ) {
			if ( $index ) {
				GFCommon::recursive_add_index_file( $path );
			}

			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}

/**
 * Create signatures dir
 * @return bool
 */
function gf_cvhs_create_signatures_dir() {
	$path = gf_cvhs_get_signatures_path();

	return gf_cvhs_create_dir( $path );
}

/**
 * Removes signature
 *
 * @param $lead_id
 * @param $field_id
 *
 * @return array|bool
 */
function gf_cvhs_delete_signature( $lead_id, $field_id ) {
	$lead = is_array( $lead_id ) ? $lead_id : RGFormsModel::get_lead( $lead_id );
	gf_cvhs_delete_signature_file( rgar( $lead, $field_id ) );

	return GFAPI::update_entry_field( $lead['id'], $field_id, '' );
}

/**
 * Removes signature file
 *
 * @param $filename
 */
function gf_cvhs_delete_signature_file( $filename ) {
	$path = gf_cvhs_get_signatures_path( $filename );
	if ( file_exists( $path ) ) {
		unlink( $path );
	}
}

/**
 * Returns filenames saved accross different submissions for specific field.
 *
 * @param $form_id
 * @param GF_Field $field
 * @param string $status
 *
 * @return array
 */
function gf_cvhs_get_signatures_for_field( $form_id, $field, $status = '' ) {
	global $wpdb;
	$input_id_min  = (float) $field->id - 0.0001;
	$input_id_max  = (float) $field->id + 0.0001;
	$status_filter = '';
	if ( ! empty( $status ) ) {
		$status_filter = $wpdb->prepare( ' AND status=%s', $status );
	}
	if ( version_compare( gf_cvhs_get_gravityforms_db_version(), '2.3-dev-1', '<' ) ) {
		$lead_details_table_name = GFFormsModel::get_lead_details_table_name();
		$lead_table_name         = GFFormsModel::get_lead_table_name();
		$filenames               = $wpdb->get_col( $wpdb->prepare( "SELECT ld.value FROM {$lead_details_table_name} ld INNER JOIN {$lead_table_name} l ON l.id = ld.lead_id WHERE ld.form_id=%d AND ld.field_number BETWEEN %s AND %s {$status_filter}", $form_id, $input_id_min, $input_id_max ) );
	} else {
		$entry_meta_table_name = GFFormsModel::get_entry_meta_table_name();
		$filenames             = $wpdb->get_col( $wpdb->prepare( "SELECT meta_value FROM {$entry_meta_table_name} em INNER JOIN {$wpdb->prefix}gf_entry e ON e.id = em.entry_id WHERE em.form_id=%d AND em.meta_key=%s {$status_filter}", $form_id, $field->id ) );
	}

	return $filenames;
}

/**
 * Returns the gravityforms version
 * @return string
 */
function gf_cvhs_get_gravityforms_db_version() {
	$version = method_exists( 'GFFormsModel', 'get_database_version' ) ? GFFormsModel::get_database_version() : GFForms::$version;

	return $version;
}

/**
 * Returns the signature url
 *
 * @param $name
 *
 * @return string
 */
function gf_cvhs_get_signature_url( $name ) {
	$filename = pathinfo( $name, PATHINFO_FILENAME );

	return site_url() . "?page=gf_cvhs_signatures&q={$filename}";
}

/**
 * Returns the resource name based on DEBUG.
 *
 * @param $name
 * @param $extension
 *
 * @return string
 */
function gf_cvhs_resource( $name, $extension ) {
	if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
		$name = $name . '.' . $extension;
	} else {
		$name = $name . '.min.' . $extension;
	}

	return $name;
}

/**
 * Build HTML Attributes
 *
 * @param $attributes
 *
 * @return string
 */
function gf_cvhs_build_attributes( $attributes ) {
	if ( ! is_array( $attributes ) || count( $attributes ) == 0 ) {
		return '';
	}
	$prepared = array();
	foreach ( $attributes as $key => $value ) {
		if ( is_array( $value ) ) {
			$sub_value = '';
			foreach ( $value as $c_key => $c_val ) {
				$sub_value .= "{$c_key}:{$c_val};";
			}
			$entry = "{$key}={$sub_value}";
		} else {
			$entry = "{$key}={$value}";
		}
		array_push( $prepared, $entry );
	}

	return implode( ' ', $prepared );
}