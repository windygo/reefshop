<?php
/********************************************************************
 * Copyright (C) 2019 CodeVerve (https://codeverve.com)
 *
 * This file is part of Gravity Forms Handmade Signature
 *
 * Gravity Forms Handmade Signature is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gravity Forms Handmade Signature is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gravity Forms Handmade Signature. If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

if ( ! class_exists( 'GFForms' ) ) {
	die();
}

class GF_CVHS_Field extends GF_Field {

	/**
	 * The type
	 */
	const TYPE = 'cv_signature';

	/**
	 * @var string $type The field type.
	 */
	public $type = self::TYPE;

	/**
	 * Return the field title, for use in the form editor.
	 *
	 * @return string
	 */
	public function get_form_editor_field_title() {
		return esc_attr__( 'CV Signature', 'gravityforms-handmade-signatures' );
	}

	/**
	 * Assign the field button to the Advanced Fields group.
	 *
	 * @return array
	 */
	public function get_form_editor_button() {
		return array(
			'group' => 'advanced_fields',
			'text'  => $this->get_form_editor_field_title(),
		);
	}

	/**
	 * The settings which should be available on the field in the form editor.
	 *
	 * @return array
	 */
	function get_form_editor_field_settings() {
		return array(
			'label_setting',
			'description_setting',
			'rules_setting',
			'css_class_setting',
			'admin_label_setting',
			'visibility_setting',
			'conditional_logic_field_setting',
			'sbox_reset_text',
			'sbox_border_width',
			'sbox_pen_color',
			'sbox_border_color',
			'sbox_border_style',
			'sbox_background_color',
			'sbox_box_width',
			'sbox_box_height'
		);
	}

	/**
	 * Enable this field for use with conditional logic.
	 *
	 * @return bool
	 */
	public function is_conditional_logic_supported() {
		return true;
	}

	/**
	 * The scripts to be included in the form editor.
	 *
	 * @return string
	 */
	public function get_form_editor_inline_script_on_page_render() {

		// set the default field label for the simple type field
		$script = sprintf( "function SetDefaultValues_cv_signature(field) {field.label = '%s';}", $this->get_form_editor_field_title() ) . PHP_EOL;

		$default_reset_text = esc_html__( 'Reset', 'gravityforms-handmade-signatures' );

		// Handle loading the values
		$script .=
			"function gf_cvhs_is_empty(field, key) {
				if(field.hasOwnProperty(key)){
					if(field[key] == undefined) {
						return true;
					} else if(field[key].trim() == '') {
						return true;
					} else {
						return false;
					}
				} else {
					return true;
				}
			}" .
			"jQuery(document).on('gform_load_field_settings', function (event, field, form) {" .

			"var resetText = gf_cvhs_is_empty(field, 'signatureAreaResetText') ? '" . $default_reset_text . "' : field.signatureAreaResetText;" .
			"jQuery('#sbox_reset_text').val(resetText);" .

			"var areaWidth = gf_cvhs_is_empty(field, 'signatureAreaWidth') ? '300' : field.signatureAreaWidth;" .
			"jQuery('#sbox_box_width').val(areaWidth);" .

			"var areaHeight = gf_cvhs_is_empty(field, 'signatureAreaHeight') ? '180' : field.signatureAreaHeight;" .
			"jQuery('#sbox_box_height').val(areaHeight);" .

			"var bgColor = gf_cvhs_is_empty(field, 'signatureAreaBackgroundColor') ? '#ffffff' : field.signatureAreaBackgroundColor;" .
			"jQuery('#sbox_background_color').val(bgColor);" .
			"SetColorPickerColor('sbox_background_color', bgColor);" .

			"var penColor = gf_cvhs_is_empty(field, 'signatureAreaPenColor') ? '#000000' : field.signatureAreaPenColor;" .
			"jQuery('#sbox_pen_color').val(penColor);" .
			"SetColorPickerColor('sbox_pen_color', penColor);" .

			"var borderColor = gf_cvhs_is_empty(field, 'signatureAreaBorderColor') ? '#cccccc' : field.signatureAreaBorderColor;" .
			"jQuery('#sbox_border_color').val(borderColor);" .
			"SetColorPickerColor('sbox_border_color', borderColor);" .

			"var borderWidth = gf_cvhs_is_empty(field, 'signatureAreaBorderWidth') ? '2' : field.signatureAreaBorderWidth;" .
			"jQuery('#sbox_border_width').val(borderWidth);" .
			"SetColorPickerColor('sbox_border_width', borderWidth);" .

			"var borderStyle = gf_cvhs_is_empty(field, 'signatureAreaBorderStyle') ? 'dashed' : field.signatureAreaBorderStyle;" .
			"jQuery('#sbox_border_style').find('option').each(function(i,opt){
				var current = jQuery(opt);
				if(current.val() == borderStyle) { 
				current.prop('selected', true); 
				}
			});" .
			"SetColorPickerColor('sbox_border_style', borderStyle);" .

			"});" . PHP_EOL;

		// Save the SetSignatureResetText property
		$script .= "function SetSignatureResetText(text) {SetFieldProperty('signatureAreaResetText', text);}" . PHP_EOL;
		// Save the SetSignatureBoxWidth property
		$script .= "function SetSignatureBoxWidth(size) {SetFieldProperty('signatureAreaWidth', size);}" . PHP_EOL;
		// Save the SetSignatureBoxHeight property
		$script .= "function SetSignatureBoxHeight(size) {SetFieldProperty('signatureAreaHeight', size);}" . PHP_EOL;
		// saving the SetSignatureBoxBackgroundColor property and updating the UI to match
		$script .= "function SetSignatureBoxBackgroundColor(color) {SetFieldProperty('signatureAreaBackgroundColor', color);jQuery('.field_selected .gf_cv_signature_container').css('background-color', color);}" . PHP_EOL;
		// saving the SetSignatureBoxPenColor property
		$script .= "function SetSignatureBoxPenColor(color) {SetFieldProperty('signatureAreaPenColor', color);}" . PHP_EOL;
		// saving the SetSignatureBoxBorderColor property and updating the UI to match
		$script .= "function SetSignatureBoxBorderColor(color) {SetFieldProperty('signatureAreaBorderColor', color);jQuery('.field_selected .gf_cv_signature_container').css('border-color', color);}" . PHP_EOL;
		// saving the SetSignatureBoxBorderWidth property and updating the UI to match
		$script .= "function SetSignatureBoxBorderWidth(width) {SetFieldProperty('signatureAreaBorderWidth', width);jQuery('.field_selected .gf_cv_signature_container').css('border-width', width);}" . PHP_EOL;
		// saving the SetSignatureBoxBorderStyle property and updating the UI to match
		$script .= "function SetSignatureBoxBorderStyle(style) {SetFieldProperty('signatureAreaBorderStyle', style);jQuery('.field_selected .gf_cv_signature_container').css('border-color', style);}" . PHP_EOL;


		return $script;
	}

	/**
	 * Define the fields inner markup.
	 *
	 * @param array $form The Form Object currently being processed.
	 * @param string|array $value The field value. From default/dynamic population, $_POST, or a resumed incomplete submission.
	 * @param null|array $entry Null or the Entry Object currently being edited.
	 *
	 * @return string
	 */
	public function get_field_input( $form, $value = '', $entry = null ) {
		$id              = absint( $this->id );
		$form_id         = absint( $form['id'] );
		$is_entry_detail = $this->is_entry_detail();
		$is_form_editor  = $this->is_form_editor();

		// Prepare the value of the input ID attribute.
		$field_id = $is_entry_detail || $is_form_editor || $form_id == 0 ? "input_$id" : 'input_' . $form_id . "_$id";
		$value    = esc_attr( $value );

		// General input attributes
		$logic_event        = ! $is_form_editor && ! $is_entry_detail ? $this->_get_conditional_logic_event( 'keyup' ) : '';
		$required_attribute = $this->isRequired ? 'aria-required="true"' : '';
		$invalid_attribute  = $this->failed_validation ? 'aria-invalid="true"' : 'aria-invalid="false"';
		$disabled_text      = $is_form_editor ? 'disabled="disabled"' : '';

		// Styling input attributes
		$signature_area_width        = $this->get_setting( 'signatureAreaWidth', '300' );
		$signature_area_height       = $this->get_setting( 'signatureAreaHeight', '180' );
		$signature_area_bg_color     = $this->get_setting( 'signatureAreaBackgroundColor', '#ffffff' );
		$signature_area_pen_color    = $this->get_setting( 'signatureAreaPenColor', '#000000' );
		$signature_area_border_color = $this->get_setting( 'signatureAreaBorderColor', '#cccccc' );
		$signature_area_border_style = $this->get_setting( 'signatureAreaBorderStyle', 'dashed' );
		$signature_area_border_width = $this->get_setting( 'signatureAreaBorderWidth', '2px' );

		// Other text
		$signature_reset_text = $this->get_setting( 'signatureAreaResetText', 'Reset' );

		// Prepare input attributes
		$html_attributes = gf_cvhs_build_attributes( array(
			'width'                 => $signature_area_width . 'px',
			'height'                => $signature_area_height . 'px',
			'data-target'           => $field_id,
			'data-pen-color'        => $signature_area_pen_color,
			'data-background-color' => $signature_area_bg_color,
			'style'                 => array(
				'border-color' => $signature_area_border_color,
				'border-style' => $signature_area_border_style,
				'border-width' => $signature_area_border_width . 'px',
			)
		) );

		// Prepare markup
		$element_canvas = "<canvas id='cvhs-signature-{$field_id}' class='cvhs-signature' {$html_attributes}></canvas>";
		$element_reset  = "<div class='cvhs-signature-reset'><a href='#' data-target='cvhs-signature-{$field_id}' class='cvhs-signature-reset'>{$signature_reset_text}</a></div>";
		$element_body   = "<div class='cvhs-signature-pad--body'>{$element_canvas}{$element_reset}</div>";
		$element_input  = "<input name='input_{$id}' id='{$field_id}' type='hidden' value='{$value}' {$logic_event} {$required_attribute} {$invalid_attribute} {$disabled_text}/>";
		$element        = "<div id='cvhs-signature-pad_{$field_id}' class='cvhs-signature-pad'>{$element_body}</div>";


		return sprintf( "<div class='ginput_container ginput_container_%s'>%s %s</div>", $this->type, $element, $element_input );
	}

	/**
	 * Return the option
	 *
	 * @param $option_name
	 * @param string $option_default_value
	 *
	 * @return bool|string
	 */
	private function get_setting( $option_name, $option_default_value = '' ) {
		if ( ! isset( $this->$option_name ) ) {
			return $option_default_value;
		}

		return $this->$option_name;
	}

	/**
	 *  Returns conditional logic event
	 *
	 * @param string $event
	 *
	 * @return string
	 */
	private function _get_conditional_logic_event( $event = '' ) {
		$logic_event = version_compare( GFForms::$version, '2.4-beta-1', '<' ) ? $this->get_conditional_logic_event( $event ) : '';

		return $logic_event;
	}


	/**
	 * Returns the value before saving
	 *
	 * @param string $value
	 * @param array $form
	 * @param string $input_name
	 * @param int $lead_id
	 * @param array $lead
	 *
	 * @return array|string
	 */
	public function get_value_save_entry( $value, $form, $input_name, $lead_id, $lead ) {
		gf_cvhs_create_signatures_dir();
		$signature_file_name = gf_cvhs_generate_signature_name( $lead_id );
		$signature_file_path = gf_cvhs_get_signatures_path( $signature_file_name );
		$result              = gf_cvhs_base64_to_image( $value, $signature_file_path );
		if ( $result ) {
			$value = $signature_file_name;
		} else {
			$value = '';
		}

		return parent::get_value_save_entry( $value, $form, $input_name, $lead_id, $lead );
	}

	/**
	 * Returns the signature url in the merge tag
	 *
	 * @param array|string $value
	 * @param string $input_id
	 * @param array $entry
	 * @param array $form
	 * @param string $modifier
	 * @param array|string $raw_value
	 * @param bool $url_encode
	 * @param bool $esc_html
	 * @param string $format
	 * @param bool $nl2br
	 *
	 * @return string
	 */
	public function get_value_merge_tag( $value, $input_id, $entry, $form, $modifier, $raw_value, $url_encode, $esc_html, $format, $nl2br ) {
		if ( ! empty( $value ) ) {
			$value = gf_cvhs_get_signature_url( $value );
		}

		return parent::get_value_merge_tag( $value, $input_id, $entry, $form, $modifier, $raw_value, $url_encode, $esc_html, $format, $nl2br );
	}

	/**
	 * Returns the image in the entry list
	 *
	 * @param array|string $value
	 * @param array $entry
	 * @param string $field_id
	 * @param array $columns
	 * @param array $form
	 *
	 * @return string
	 */
	public function get_value_entry_list( $value, $entry, $field_id, $columns, $form ) {
		if ( ! empty( $value ) ) {
			$url   = gf_cvhs_get_signature_url( $value );
			$value = sprintf( "<a href='%s' target='_blank' title='%s'>%s</a>",
				$url,
				esc_attr__( 'Click to view', 'gravityforms-handmade-signatures' ),
				esc_attr__( 'Click to view', 'gravityforms-handmade-signatures' )
			);

			return $value;
		}

		return parent::get_value_entry_list( $value, $entry, $field_id, $columns, $form ); // TODO: Change the autogenerated stub
	}

	/**
	 * Returns the value entry detail
	 *
	 * @param array|string $value
	 * @param string $currency
	 * @param bool $use_text
	 * @param string $format
	 * @param string $media
	 *
	 * @return string
	 */
	public function get_value_entry_detail( $value, $currency = '', $use_text = false, $format = 'html', $media = 'screen' ) {
		if ( ! empty( $value ) ) {
			$url = gf_cvhs_get_signature_url( $value );
			if ( $format === 'html' ) {
				$value = sprintf( "<a href='%s' target='_blank' title='%s'><img src='%s' width='100' /></a>",
					$url,
					esc_attr__( 'Click to view', 'gravityforms-handmade-signatures' ),
					$url
				);
			} else {
				$value = $url;
			}

			return $value;
		}

		return parent::get_value_entry_detail( $value, $currency, $use_text, $format, $media ); // TODO: Change the autogenerated stub
	}


	/**
	 * Return the url when exporting entries
	 *
	 * @param array $entry
	 * @param string $input_id
	 * @param bool $use_text
	 * @param bool $is_csv
	 *
	 * @return array|string
	 */
	public function get_value_export( $entry, $input_id = '', $use_text = false, $is_csv = false ) {
		if ( empty( $input_id ) ) {
			$input_id = $this->id;
		}
		$value = rgar( $entry, $input_id );

		return ! empty( $value ) ? gf_cvhs_get_signature_url( $value ) : '';
	}

	/**
	 * Validation rules
	 *
	 * @param array|string $value
	 * @param array $form
	 */
	public function validate( $value, $form ) {
		parent::validate( $value, $form );
		if ( $this->isRequired ) {
			if ( ! $this->failed_validation && empty( $value ) ) {
				$this->failed_validation  = true;
				$this->validation_message = esc_attr__( "This field is required.", 'gravityforms' );
			}
		}
		if ( ! $this->failed_validation && ! empty( $value ) ) {
			$fs_wrapper = new GF_CVHS_Filesystem();
			$wp_fs      = $fs_wrapper->get();
			if ( is_wp_error( $wp_fs ) ) {
				$this->failed_validation  = true;
				$this->validation_message = esc_attr__( "Unable to save signature because of internal server misconfiguration. Please contact site administrator.", 'gravityforms-handmade-signatures' );
				error_log( 'GFHS: ' . $wp_fs->get_error_message() );
			}
		}
	}
}

try {
	GF_Fields::register( new GF_CVHS_Field() );
} catch ( Exception $e ) {
	error_log( $e->getMessage() );
}
