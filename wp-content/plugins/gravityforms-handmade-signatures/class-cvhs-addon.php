<?php
/********************************************************************
 * Copyright (C) 2019 CodeVerve (https://codeverve.com)
 *
 * This file is part of Gravity Forms Handmade Signature
 *
 * Gravity Forms Handmade Signature is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gravity Forms Handmade Signature is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gravity Forms Handmade Signature. If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

GFForms::include_addon_framework();

class GF_CVHS_AddOn extends GFAddOn {

	const FIELD_SIGNATURE = 'cv_signature';
	protected $_version = GF_CVHS_VERSION;
	protected $_min_gravityforms_version = '1.9';
	protected $_slug = 'gravityforms-handmade-signatures';
	protected $_path = 'gravityforms-handmade-signatures/gravityforms-handmade-signatures.php';
	protected $_full_path = __FILE__;
	protected $_title = 'Gravity Forms Handmade Signature';
	protected $_short_title = 'Handmade Singature';
	protected $_enable_rg_autoupgrade = false;

	/**
	 * @var object $_instance If available, contains an instance of this class.
	 */
	private static $_instance = null;

	/**
	 * Returns an instance of this class, and stores it in the $_instance property.
	 *
	 * @return object $_instance An instance of this class.
	 */
	public static function get_instance() {
		if ( self::$_instance == null ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Initialize field early so it is available when entry exports are being performed.
	 */
	public function pre_init() {
		parent::pre_init();
		if ( $this->is_gravityforms_supported() && class_exists( 'GF_Field' ) ) {
			require_once( 'includes/class-cvhs-filesystem.php' );
			require_once( 'includes/helpers.php' );
			require_once( 'includes/class-cvhs-field.php' );
			add_action( 'parse_request', array( $this, 'render_signature' ) );
		}
	}


	/**
	 * General hooks and language files
	 */
	public function init() {
		parent::init();
		add_filter( 'gform_merge_tag_filter', array( $this, 'merge_tag_filter' ), 10, 5 );
		add_action( 'gform_delete_lead', array( $this, 'delete_lead' ), 10, 1 );
		add_action( 'gform_delete_entries', array( $this, 'delete_entries' ), 10, 2 );
		add_action( 'gravityforms_cron', array( $this, 'handle_cron' ) );
	}

	/**
	 * Initializes admin hooks
	 */
	public function init_admin() {
		parent::init_admin();
		add_filter( 'gform_tooltips', array( $this, 'tooltips' ) );
		add_action( 'gform_field_appearance_settings', array( $this, 'field_appearance_settings' ), 10, 2 );
	}


	// # SCRIPTS & STYLES -----------------------------------------------------------------------------------------------

	/**
	 * Include scripts
	 *
	 * @return array
	 */
	public function scripts() {
		$scripts = array(
			array(
				'handle'  => 'signature_pad_js',
				'src'     => $this->get_base_url() . '/assets/resources/signature_pad/signature_pad.umd.min.js',
				'version' => $this->_version,
				'deps'    => array( 'jquery' ),
				'enqueue' => array(
					array( 'field_types' => array( self::FIELD_SIGNATURE ) ),
				),
			),

			array(
				'handle'  => 'gf_cvhs_js',
				'src'     => $this->get_base_url() . '/assets/' . gf_cvhs_resource( 'cvhs', 'js' ),
				'version' => $this->_version,
				'deps'    => array( 'jquery' ),
				'enqueue' => array(
					array( 'field_types' => array( self::FIELD_SIGNATURE ) ),
				),
			),

		);

		return array_merge( parent::scripts(), $scripts );
	}

	/**
	 * Include my_styles.css when the form contains a 'simple' type field.
	 *
	 * @return array
	 */
	public function styles() {
		$styles = array(
			array(
				'handle'  => 'gf_cvhs_css',
				'src'     => $this->get_base_url() . '/assets/' . gf_cvhs_resource( 'cvhs', 'css' ),
				'version' => $this->_version,
				'enqueue' => array(
					array( 'field_types' => array( self::FIELD_SIGNATURE ) )
				)
			)
		);

		return array_merge( parent::styles(), $styles );
	}


	// # HOOKS ----------------------------------------------------------------------------------------------------------

	/**
	 * Renders the signatsure
	 * @hooked parse_request
	 */
	public function render_signature() {
		$is_signature = rgget( 'page' ) == 'gf_cvhs_signatures';
		if ( ! $is_signature ) {
			return;
		}
		$image_name = rgget( 'q' ) . '.png';
		$image_path = gf_cvhs_get_signatures_path( $image_name );
		if ( file_exists( $image_path ) ) {
			header( "Content-type: image/png" );
			header( 'Expires: 0' );
			header( 'Content-Length: ' . filesize( $image_path ) );
			readfile( "$image_path" );
		}
		exit;
	}

	/**
	 * Prevent signature image to be included in {all_fields}
	 *
	 * @param $value
	 * @param $merge_tag
	 * @param $options
	 * @param $field
	 * @param $raw_field_value
	 *
	 * @return mixed
	 */
	public function merge_tag_filter( $value, $merge_tag, $options, $field, $raw_field_value ) {
		if ( $merge_tag == 'all_fields' ) {
			if ( $field->type === self::FIELD_SIGNATURE ) {
				$show = apply_filters( 'gform_' . self::FIELD_SIGNATURE . '_show_in_all_fields', true, $field, $options, $value );
				if ( ! $show ) {
					$value = $raw_field_value;
				}
			}
		}

		return $value;
	}

	/**
	 * Clean up the signature file once the entry is deleted from gravity forms.
	 *
	 * @hooked delete_entry
	 *
	 * @param $lead_id - The form submission entry
	 */
	public function delete_lead( $lead_id ) {
		$lead = RGFormsModel::get_lead( $lead_id );
		$form = RGFormsModel::get_form_meta( $lead['form_id'] );
		if ( ! is_array( $form['fields'] ) ) {
			return;
		}
		foreach ( $form['fields'] as $field ) {
			if ( $field->type == self::FIELD_SIGNATURE ) {
				gf_cvhs_delete_signature( $lead, $field->id );
			}
		}
	}

	/**
	 * Deletes signatures when deleting entries
	 *
	 * @param $form_id
	 * @param $status
	 */
	public function delete_entries( $form_id, $status ) {
		$form             = RGFormsModel::get_form_meta( $form_id );
		$signature_fields = GFAPI::get_fields_by_type( $form, self::FIELD_SIGNATURE );
		if ( ! empty( $signature_fields ) ) {
			foreach ( $signature_fields as $field ) {
				$signature_files = gf_cvhs_get_signatures_for_field( $form_id, $field, $status );
				if ( ! is_array( $signature_files ) || empty( $signature_files ) ) {
					return;
				}
				foreach ( $signature_files as $filename ) {
					gf_cvhs_delete_signature_file( $filename );
				}
			}
		}
	}

	/**
	 * Handles cron task
	 * @hooked gravityforms_cron
	 */
	public function handle_cron() {
		gf_cvhs_create_index_file();
	}


	// # FIELD SETTINGS -------------------------------------------------------------------------------------------------

	/**
	 * Add the tooltips for the field.
	 *
	 * @param array $tooltips An associative array of tooltips where the key is the tooltip name and the value is the tooltip.
	 *
	 * @return array
	 */
	public function tooltips( $tooltips ) {
		$simple_tooltips = array(
			'box_width_setting' => sprintf( '<h6>%s</h6>%s', esc_html__( 'Signature Area Width', 'gravityforms-handmade-signatures' ), esc_html__( 'This is the width in px of the signature area used for drawing a signature.', 'gravityforms-handmade-signatures' ) ),
		);

		return array_merge( $tooltips, $simple_tooltips );
	}

	/**
	 * Add the custom setting for the Simple field to the Appearance tab.
	 *
	 * @param int $position The position the settings should be located at.
	 * @param int $form_id The ID of the form currently being edited.
	 */
	public function field_appearance_settings( $position, $form_id ) {
		if ( $position == 250 ) {
			?>

            <li class="sbox_reset_text field_setting">
                <label for="sbox_reset_text" class="section_label">
					<?php esc_html_e( 'Signature Reset Button Text', 'gravityforms-handmade-signatures' ); ?>
					<?php gform_tooltip( 'sbox_reset_text' ) ?>
                </label>
                <input id="sbox_reset_text" type="text" onkeyup="SetSignatureResetText(jQuery(this).val());"
                       onchange="SetSignatureResetText(jQuery(this).val());"/>
            </li>

            <li class="sbox_box_width field_setting">
                <label for="sbox_box_width" class="section_label">
					<?php esc_html_e( 'Signature Box Width', 'gravityforms-handmade-signatures' ); ?>
					<?php gform_tooltip( 'sbox_box_width' ) ?>
                </label>
                <input id="sbox_box_width" type="number" style="width:50px;"
                       onkeyup="SetSignatureBoxWidth(jQuery(this).val());"
                       onchange="SetSignatureBoxWidth(jQuery(this).val());"/> px
            </li>

            <li class="sbox_box_height field_setting">
                <label for="sbox_box_height" class="section_label">
					<?php esc_html_e( 'Signature Box Height', 'gravityforms-handmade-signatures' ); ?>
					<?php gform_tooltip( 'sbox_box_height' ) ?>
                </label>
                <input id="sbox_box_height" type="number" style="width:50px;"
                       onkeyup="SetSignatureBoxHeight(jQuery(this).val());"
                       onchange="SetSignatureBoxHeight(jQuery(this).val());"/> px
            </li>

            <li class="sbox_background_color field_setting">
                <label for="sbox_background_color" class="section_label">
					<?php esc_html_e( 'Signature Box Background Color', 'gravityforms-handmade-signatures' ); ?>
					<?php gform_tooltip( 'sbox_background_color' ) ?>
                </label>
				<?php GFFormDetail::color_picker( 'sbox_background_color', 'SetSignatureBoxBackgroundColor' ) ?>
            </li>


            <li class="sbox_pen_color field_setting">
                <label for="sbox_pen_color" class="section_label">
					<?php esc_html_e( 'Signature Box Pen Color', 'gravityforms-handmade-signatures' ); ?>
					<?php gform_tooltip( 'sbox_pen_color' ) ?>
                </label>
				<?php GFFormDetail::color_picker( 'sbox_pen_color', 'SetSignatureBoxPenColor' ) ?>
            </li>

            <li class="sbox_border_color field_setting">
                <label for="sbox_border_color" class="section_label">
					<?php esc_html_e( 'Signature Box Border Color', 'gravityforms-handmade-signatures' ); ?>
					<?php gform_tooltip( 'sbox_border_color' ) ?>
                </label>
				<?php GFFormDetail::color_picker( 'sbox_border_color', 'SetSignatureBoxBorderColor' ) ?>
            </li>

            <li class="sbox_border_style field_setting">
                <label for="sbox_border_style" class="section_label">
					<?php esc_html_e( 'Signature Box Border Style', 'gravityforms-handmade-signatures' ); ?>
					<?php gform_tooltip( 'sbox_border_style' ) ?>
                </label>
                <select id="sbox_border_style" onchange="SetSignatureBoxBorderStyle(jQuery(this).val());">
                    <option value="dotted"><?php esc_html_e( 'Dotted', 'gravityforms-handmade-signatures' ) ?></option>
                    <option value="dashed"><?php esc_html_e( 'Dashed', 'gravityforms-handmade-signatures' ) ?></option>
                    <option value="groove"><?php esc_html_e( 'Groove', 'gravityforms-handmade-signatures' ) ?></option>
                    <option value="ridge"><?php esc_html_e( 'Ridge', 'gravityforms-handmade-signatures' ) ?></option>
                    <option value="inset"><?php esc_html_e( 'Inset', 'gravityforms-handmade-signatures' ) ?></option>
                    <option value="outset"><?php esc_html_e( 'Outset', 'gravityforms-handmade-signatures' ) ?></option>
                    <option value="double"><?php esc_html_e( 'Double', 'gravityforms-handmade-signatures' ) ?></option>
                    <option value="solid"><?php esc_html_e( 'Solid', 'gravityforms-handmade-signatures' ) ?></option>
                </select>
            </li>

            <li class="sbox_border_width field_setting">
                <label for="sbox_border_width" class="section_label">
					<?php esc_html_e( 'Signature Box Border Width', 'gravityforms-handmade-signatures' ); ?>
					<?php gform_tooltip( 'sbox_border_width' ) ?>
                </label>
                <input id="sbox_border_width" type="number" style="width:50px;"
                       onkeyup="SetSignatureBoxBorderWidth(jQuery(this).val());"
                       onchange="SetSignatureBoxBorderWidth(jQuery(this).val());"/> px
            </li>

			<?php
		}
	}

}