<?php
/*
Plugin Name: Gravity Forms Handmade Signatures
Plugin URI: https://codeverve.com
Description: Signature field for GravityForms that allows users to sign forms using mouse or any touch device
Version: 1.0.1
Author: CodeVerve
Author URI: https://codeverve.com
License: GPLv2
Text Domain: gravityforms-handmade-signatures
Domain Path: /languages

------------------------------------------------------------------------
Copyright (C) 2019 CodeVerve (https://codeverve.com)

This file is part of Gravity Forms Handmade Signature

Gravity Forms Handmade Signature is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

Gravity Forms Handmade Signature is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Gravity Forms Handmade Signature. If not, see <https://www.gnu.org/licenses/>.

*/

define( 'GF_CVHS_VERSION', '1.0.1' );

add_action( 'gform_loaded', array( 'GF_CVHS_Bootstrap', 'load' ), 5 );

class GF_CVHS_Bootstrap {
	public static function load() {
		if ( ! method_exists( 'GFForms', 'include_addon_framework' ) ) {
			return;
		}
		require_once( 'class-cvhs-addon.php' );
		GFAddOn::register( 'GF_CVHS_AddOn' );
	}
}