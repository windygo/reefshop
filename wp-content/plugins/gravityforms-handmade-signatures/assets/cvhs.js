/*!
 * Gravity forms - Handmade signatures
 * (c) 2019 Darko Gjorgjijoski (CodeVerve.com) | Released under GPLv2
 */
(function ($) {
    "use strict";
    var CVHS = function () {
        this.instances = {};
        this.init = function () {
            var pads = document.getElementsByClassName('cvhs-signature');
            if (pads.length > 0) {
                for (var i = 0; i < pads.length; i++) {
                    var el = pads[i];
                    var backgroundColor = el.getAttribute('data-background-color');
                    var penColor = el.getAttribute('data-pen-color');
                    this.instances[el.id.toString()] = new SignaturePad(el, {
                        backgroundColor: backgroundColor,
                        penColor: penColor,
                        onEnd: function () {
                            var image = this.toDataURL('image/png');
                            var inst = $('#' + this.canvas.id);
                            var targetId = inst.data('target');
                            var target = document.getElementById(targetId);
                            target.value = image;
                        }
                    });
                }
            }
            this.attach_handlers();
        };
        this.reset = function (id) {
            if (this.instances.hasOwnProperty(id)) {
                this.instances[id.toString()].clear();
                var value_field_id = id.replace('cvhs-signature-', '');
                var value_field = document.getElementById(value_field_id);
                if (value_field) {
                    value_field.value = null;
                }
            }
        };
        this.attach_handlers = function () {
            var self = this;
            $(document).on('click', '.cvhs-signature-reset a', function (e) {
                e.preventDefault();
                var target = $(this).data('target');
                if (target) {
                    self.reset(target);
                }
            })
        };
    };
    $(document).on('gform_post_render', function () {
        var _CVHS = new CVHS();
        _CVHS.init();
    })
})(jQuery);