��    �      $      ,
      ,
     -
  	   1
     ;
  	   ?
     I
  	   M
     W
  	   [
     e
  	   i
     s
  	   w
     �
  	   �
     �
  	   �
     �
  X   �
               &  
   3     >  	   L  	   V     `     s     �     �     �     �     �     �     �     �  
   �     �     �                          !     -     A     Q     _     k     ~     �  #   �  !   �     �     �     �     �       g        �  &   �     �     �     �     �  
   �     �  	   �     �     �     �     	  
   #     .     K     P     U  	   j     t     {  	   �     �     �  	   �     �     �     �     �     �     �     �  
               
   *     5     J     Q     V     o  
   t          �     �  $   �  �   �  �   �     '     <  	   U     _  
   e  	   p  	   z     �  &   �     �     �  
   �     �     �             �   )     �     �      �     �  
     E        c     h  
   n     y       
   �     �     �     �     �     �     �     �     �               (     0  	   >     H  	   K     U     ^     d     i     q     y     �     �     �     �  �  �     u  	   y     �  	   �     �  	   �     �  
   �     �  
   �     �  
   �     �  
   �     �  
   �     �  q   �     k     �     �     �     �     �  
   �     �     �     �               *     >     C     ]     r  
        �     �     �     �     �     �     �     �     �          (     <     O     [  +   n  )   �     �     �     �     �     �  c        o  '   t     �     �     �     �     �     �     �     �     �     �          )  !   ;     ]  	   d     n     �     �     �     �     �     �  	   �     �     �     �  
   �     	               1     A     R     _     n     �     �     �     �  
   �     �     �     �  $      �   %  �   �     �     �  	   �     �  
   �  
   �  	   �  	   �  ,         -     C     V     b     t     �     �  �   �     R      i   ,   u   )   �   
   �   Z   �   
   2!     =!  
   C!     N!     V!  
   i!     t!     �!     �!     �!     �!  	   �!     �!     �!     �!     �!     "     "     '"     4"  	   7"     A"     I"     O"     T"     \"     d"     m"     t"     �"     �"   100 100italic 200 200italic 300 300italic 500 500italic 600 600italic 700 700italic 800 800italic 900 900italic Activate License Add Scripts (Google Analytics, etc.) here. Runs before the closing body tag (wp_footer). Advanced Settings After Footer After Header After Post After Sidebar Alignment Animation Animation Duration Background Color Before Footer Before Header Before Post Before Sidebar Blur Bold Text Color Border Radius Box Box Shadow Boxed Brand Colors Cart Checkout Color Company Name Company URL Content beside Logo Custom 404 Page Custom Footer Custom Menu Deactivate License Default Default: 320px Default: above 1024px for desktops. Default: above 768px for tablets. Delay Desktop Disable Embeds Disable Emojis Disable RSS Feed Display social media icons in your pre-header, footer or template file by using the [social] shortcode. Down Enter your Premium Add-On license key. Facebook Fade Fade In Filled Font Color Font Family Font Size Footer Footer Code Full Screen Full Screen Menu Settings Full Width Global Template Settings %1s Grey Grow Hamburger Icon Color Head Code Header Header Code Hide Logo Hover Hover Effect Instagram Large Letter Spacing License Key Line Height LinkedIn Logo Logo Area Background Color Logo Color Menu Alignment Menu Height Menu Width Mobile Menu Settings Modern Name Navigation Hover Effects None Off Canvas Off Canvas (left) Off Canvas (right) Off Canvas Settings Page Builder Framework Premium Addon Paste your shortcode to populate a saved row/template throughout your website. <br><br>Examples:<br>[elementor-template id="xxx"]<br>[fl_builder_insert_layout id="xxx"] Paste your shortcode to replace the default menu with your Custom Menu. <br><br>Example:<br>[elementor-template id="xxx"]<br>[fl_builder_insert_layout id="xxx"] Performance Settings Performance Settings %1s Pinterest Plain Pre Header Product/s Push Menu Quantity Recommended image size: 1200px x 900px Remove Feed Links Remove Generator Remove RSD Remove Shortlink Remove Title Remove jQuery Migrate Remove wlwmanifest Replace the default 404 page with your custom layout. <br><br>Example:<br>[elementor-template id="xxx"]<br>[fl_builder_insert_layout id="xxx"] Responsive Breakpoints Rounded Runs after the opening body tag. Runs inside the head tag. Screenshot Set a delay after the sticky navigation should appear. Default: 300px Size Slide Slide Down Small Social Media Icons Soundcloud Stacked (advanced) Sticky Footer Sticky Navigation Style Sub Menu Animation Subtotal Tags Text transform Theme Settings Transparent Header Twitter Typekit Fonts Underline Up Uppercase Variants Vimeo Yelp Youtube Zoom In Zoom Out active https://mapsteps.com italic regular PO-Revision-Date: 2019-11-12 14:32+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Loco https://localise.biz/
Language: it
Project-Id-Version: Page Builder Framework Premium Add-On
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-11-12 14:22+0000
Last-Translator: davidv <dev-email@flywheel.local>
Language-Team: Italian
X-Loco-Version: 2.3.1; wp-5.3-beta1 100 100italic 200 200italic 300 300italic 500 500corsivo 600 600corsivo 700 700corsivo 800 800corsivo 900 900corsivo Attiva Licenza Aggiungi qui gli Script (Google Analytics, etc.). Vengono eseguiti prima della chiusura del tag body (wp_footer). Impostazioni Avanzate Dopo il Footer Dopo l'Header Dopo il Post Dopo la Sidebar Allineamento Animazione Animation Duration Colore di Sfondo Prima del Footer Prima dell' Header Prima del Post Prima della Sidebar Blur Colore del Font Grassetto Arrotondamento Bordo Incorniciato Box Shadow Incorniciato Colori del Brand Carrello Checkout Colori Nome Società URL Società Contenuto di fianco al Logo Pagina 404 Personalizzata Footer Personalizzato Menu personalizzato Disattiva Licenza  Predefinito Predefinito: 320px Default: superiore ai 1024px per i desktop. Default: superiore ai 768px per i tablet. Ritardo Desktop Disabilita Embeds Disabilita Emoji Disabilita Feed RSS Mostra le icone dei social media nel tuo pre-header, footer o template usando la shortcode [social] Giù Inserisci la licenza del Premium Add-On Facebook Fade Fade In Riempito Colore Font Font Family Dimensione Font Footer Codice nel footer Schermo Intero Impostazioni Menu Full Screen Dimensione Intera Impostazioni Globali Template %1s Grigio Crescente Colore Icona Hamburger Codice nell'head Header Codice nell'header Nascondi Logo Hover Effetto Hover Instagram Grande Spaziatura Lettere Licenza Interlinea LinkedIn Logo Colore di Sfondo del Logo Colore del Logo Alineamento Menu Altezza Menu Larghezza Menu Impostazioni Menu Mobile Moderno Nome Effetti Hover del Menu Nessuno Off Canvas Off Canvas (sinistra) Off Canvas (destra) Impostazioni Off Canvas Page Builder Framework Premium Addon Incolla la tua shortcode per popolare tutto il sito con una riga/template già salvata. <br><br>Examples:<br>[elementor-template id="xxx"]<br>[fl_builder_insert_layout id="xxx"] Incolla la tua shortcode per sostituire il menu predefinito con il you Menu Personalizzato.  <br><br>Example:<br>[elementor-template id="xxx"]<br>[fl_builder_insert_layout id="xxx"] Impostazioni Performance Impostazioni Performance %1s Pinterest Semplice Pre Header Prodotto/i Menu Push Quantità Dimensioni immagine suggerite 1200px x 900px Rimuovi Link dei Feed Rimuovi Generatore Rimuovi RSD Rimuovi Shortlink Rimuovi Titolo Rimuovi jQuery Migrate Rimuovi wlwmanifest Sostituisce la pagina 404 di default con il tuo layout personalizzato. <br><br>Per Esempio:<br>[elementor-template id="xxx"]<br>[fl_builder_insert_layout id="xxx"] Responsive Breakpoints Arrotondato Viene eseguito dopo l'apertura del tag body. Viene eseguito all''interno del tag head. Screenshot Inserisci un ritardo dopo il quale apparirà la navigazione persistente. Predefinito:300px Dimensione Slide Slide Down Piccola Icone Social Media Soundcloud Sovrapposto (avanzato) Footer Persistente Menu Persistente Stile Animaizoni Sub Menu Subtotale Tags Trasformazione Testo Opzioni del Tema Header Trasparente Twitter Typekit Fonts Sottolineato Up Maiuscolo Vaianti Vimeo Yelp Youtube Zoom In Zoom Out attiva https://mapsteps.com corsivo regular 