#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-11-12 14:22+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/"

#: wpbf-premium.php:178
#, php-format
msgid ""
"Your License expires in <strong>%1$s</strong>. <a href=\"%2$s\" "
"target=\"_blank\">Renew your License</a> to keep getting Feature Updates & "
"Premium Support."
msgstr ""

#: wpbf-premium.php:198
#, php-format
msgid ""
"You need to install/activate the <strong>%1$s</strong> theme for <strong>"
"%2$s</strong> to work!"
msgstr ""

#. %1$s: Caution button, %2$s: WordPress repository (link).
#: wpbf-premium.php:211
#, php-format
msgid ""
"%1$s You are running an outdated version of Page Builder Framework that is "
"no longer supported! Please always use the version from the official %2$s."
msgstr ""

#. %1%s: Plugin name, %2$s: Renewal URL.
#: wpbf-premium.php:232
#, php-format
msgid ""
"Your License for <strong>%1$s</strong> has expired. <a href=\"%2$s\" "
"target=\"_blank\">Renew your License</a> to keep getting Feature Updates & "
"Premium Support."
msgstr ""

#: wpbf-premium.php:243
#, php-format
msgid ""
"Please <a href=\"%1$s\">activate your license key</a> to receive updates for "
"<strong>%2$s</strong>. <a href=\"%3$s\" target=\"_blank\">Help</a>"
msgstr ""

#: wpbf-premium.php:279
#, php-format
msgid ""
"%1$s Love using Page Builder Framework? - That's Awesome! Help us spread the "
"word and leave us a %2$s 5-star review %3$s in the WordPress repository."
msgstr ""

#: wpbf-premium.php:280
msgid "Sure! You deserve it!"
msgstr ""

#: inc/blog-layouts.php:54
msgid "Grid"
msgstr ""

#: inc/class-custom-sections.php:672
msgctxt "Post Type General Name"
msgid "Custom Sections"
msgstr ""

#: inc/class-custom-sections.php:673
msgctxt "Post Type Singular Name"
msgid "Section"
msgstr ""

#: inc/class-custom-sections.php:674 inc/class-custom-sections.php:1719
#: inc/class-custom-sections.php:1720
msgid "Custom Sections"
msgstr ""

#: inc/class-custom-sections.php:675
msgid "All Sections"
msgstr ""

#: inc/class-custom-sections.php:676
msgid "Add New Section"
msgstr ""

#: inc/class-custom-sections.php:677
msgid "New Section"
msgstr ""

#: inc/class-custom-sections.php:678
msgid "Edit Section"
msgstr ""

#: inc/class-custom-sections.php:679
msgid "Update Section"
msgstr ""

#: inc/class-custom-sections.php:680
msgid "Search Section"
msgstr ""

#: inc/class-custom-sections.php:709 inc/class-custom-sections.php:716
#: inc/class-custom-sections.php:789 inc/class-custom-sections.php:997
msgid "Location"
msgstr ""

#: inc/class-custom-sections.php:763 inc/class-custom-sections.php:766
msgid "Section updated."
msgstr ""

#: inc/class-custom-sections.php:767
#, php-format
msgid "Section restored to revision from %s"
msgstr ""

#: inc/class-custom-sections.php:768
msgid "Section published."
msgstr ""

#: inc/class-custom-sections.php:769
msgid "Section saved."
msgstr ""

#: inc/class-custom-sections.php:770
msgid "Section submitted."
msgstr ""

#: inc/class-custom-sections.php:772
#, php-format
msgid "Section scheduled for: <strong>%1$s</strong>."
msgstr ""

#: inc/class-custom-sections.php:773
msgid "M j, Y @ G:i"
msgstr ""

#: inc/class-custom-sections.php:775
msgid "Section draft updated."
msgstr ""

#: inc/class-custom-sections.php:796
msgid "Display Rules"
msgstr ""

#: inc/class-custom-sections.php:803
msgid "Advanced"
msgstr ""

#: inc/class-custom-sections.php:808
msgid "Theme Hooks"
msgstr ""

#: inc/class-custom-sections.php:818
msgid "General"
msgstr ""

#: inc/class-custom-sections.php:832 inc/integration/elementor-pro.php:39
msgid "Pre Header"
msgstr ""

#: inc/class-custom-sections.php:842 inc/customizer/wpbf-kirki.php:25
msgid "Header"
msgstr ""

#: inc/class-custom-sections.php:848
msgid "Navigation"
msgstr ""

#: inc/class-custom-sections.php:856
msgid "Mobile Navigation"
msgstr ""

#: inc/class-custom-sections.php:860
msgid "Sidebar"
msgstr ""

#: inc/class-custom-sections.php:866 inc/customizer/wpbf-kirki.php:32
msgid "Footer"
msgstr ""

#: inc/class-custom-sections.php:872 inc/class-custom-sections.php:1158
msgid "Posts"
msgstr ""

#: inc/class-custom-sections.php:950 inc/class-custom-sections.php:955
#: inc/class-custom-sections.php:1631
msgid "Display Theme Hooks"
msgstr ""

#: inc/class-custom-sections.php:963 inc/class-custom-sections.php:968
msgid "Hide Theme Hooks"
msgstr ""

#: inc/class-custom-sections.php:1010
msgid "Hooks"
msgstr ""

#: inc/class-custom-sections.php:1026
msgid "Priority"
msgstr ""

#: inc/class-custom-sections.php:1067
#, php-format
msgid "All %s"
msgstr ""

#: inc/class-custom-sections.php:1144
msgid "Select..."
msgstr ""

#: inc/class-custom-sections.php:1146
msgid "Entire Site"
msgstr ""

#: inc/class-custom-sections.php:1147 inc/settings/wpbf-global-settings.php:185
#: inc/settings/wpbf-global-settings.php:223
msgid "Blog Page"
msgstr ""

#: inc/class-custom-sections.php:1148
msgid "All Archive"
msgstr ""

#: inc/class-custom-sections.php:1149
msgid "Author Archive"
msgstr ""

#: inc/class-custom-sections.php:1150
msgid "Date Archive"
msgstr ""

#: inc/class-custom-sections.php:1151 inc/settings/wpbf-global-settings.php:186
#: inc/settings/wpbf-global-settings.php:224
msgid "Search Results"
msgstr ""

#: inc/class-custom-sections.php:1152 inc/settings/wpbf-global-settings.php:184
msgid "404 Page"
msgstr ""

#: inc/class-custom-sections.php:1155
msgid "Pages"
msgstr ""

#: inc/class-custom-sections.php:1159
msgid "Post Category"
msgstr ""

#: inc/class-custom-sections.php:1160
msgid "Post Tag"
msgstr ""

#: inc/class-custom-sections.php:1161
msgid "Post Archive"
msgstr ""

#: inc/class-custom-sections.php:1169
msgid "Downloads"
msgstr ""

#: inc/class-custom-sections.php:1170
msgid "Downloads Category"
msgstr ""

#: inc/class-custom-sections.php:1171
msgid "Downloads Tag"
msgstr ""

#: inc/class-custom-sections.php:1176
msgid "Products"
msgstr ""

#: inc/class-custom-sections.php:1177
msgid "Product Category"
msgstr ""

#: inc/class-custom-sections.php:1178
msgid "Product Tag"
msgstr ""

#: inc/class-custom-sections.php:1240
msgid "All Post Categories"
msgstr ""

#: inc/class-custom-sections.php:1251 inc/class-custom-sections.php:1367
msgid "All Post Tags"
msgstr ""

#: inc/class-custom-sections.php:1282
msgid "All Authors"
msgstr ""

#: inc/class-custom-sections.php:1313
msgid "All Downloads Categories"
msgstr ""

#: inc/class-custom-sections.php:1323
msgid "All Downloads Tags"
msgstr ""

#: inc/class-custom-sections.php:1356
msgid "All Product Categories"
msgstr ""

#: inc/class-custom-sections.php:1560
msgid "Include"
msgstr ""

#: inc/class-custom-sections.php:1565
msgid "Add Inclusion Rule"
msgstr ""

#: inc/class-custom-sections.php:1570
msgid "Exclude"
msgstr ""

#: inc/class-custom-sections.php:1575
msgid "Add Exclusion Rule"
msgstr ""

#: inc/class-custom-sections.php:1604
msgid "Restrict to Logged-In Users"
msgstr ""

#: inc/class-custom-sections.php:1628
msgid "Display available theme hooks on the frontend of your website."
msgstr ""

#: inc/helpers.php:146
#, php-format
msgid "Click the button below to load the video from %s."
msgstr ""

#. %1$s Docs url.
#: inc/helpers.php:149
#, php-format
msgid ""
"Something went wrong. Please make sure you enter the embed-url as the src "
"tag for the shortcode. <a href=\"%1$s\" target=\"_blank\">Help</a>"
msgstr ""

#: inc/init.php:14
msgid "Theme Settings"
msgstr ""

#: inc/theme-mods.php:139
msgid "Call to Action"
msgstr ""

#: inc/customizer/adobe-fonts.php:53 inc/customizer/adobe-fonts.php:98
msgid "Typekit Fonts"
msgstr ""

#: inc/customizer/custom-fonts.php:37 inc/customizer/custom-fonts.php:62
#: inc/customizer/wpbf-kirki.php:48 inc/customizer/wpbf-kirki.php:1125
msgid "Custom Fonts"
msgstr ""

#: inc/customizer/customizer-functions.php:38
msgid "Stacked (advanced)"
msgstr ""

#: inc/customizer/customizer-functions.php:39
msgid "Off Canvas (right)"
msgstr ""

#: inc/customizer/customizer-functions.php:40
msgid "Off Canvas (left)"
msgstr ""

#: inc/customizer/customizer-functions.php:41
msgid "Full Screen"
msgstr ""

#: inc/customizer/customizer-functions.php:42
#: inc/customizer/customizer-functions.php:58
#: inc/customizer/wpbf-kirki.php:2480
msgid "Custom Menu"
msgstr ""

#: inc/customizer/customizer-functions.php:57
msgid "Off Canvas"
msgstr ""

#: inc/customizer/wpbf-kirki.php:41 inc/customizer/wpbf-kirki.php:1051
msgid "Adobe Fonts"
msgstr ""

#: inc/customizer/wpbf-kirki.php:57
msgid "Social Media Icons"
msgstr ""

#: inc/customizer/wpbf-kirki.php:66 inc/customizer/wpbf-kirki.php:1196
#: inc/customizer/wpbf-kirki.php:1985
msgid "Sticky Navigation"
msgstr ""

#: inc/customizer/wpbf-kirki.php:73
msgid "Navigation Hover Effects"
msgstr ""

#: inc/customizer/wpbf-kirki.php:80
msgid "Call to Action Button"
msgstr ""

#: inc/customizer/wpbf-kirki.php:87 inc/customizer/wpbf-kirki.php:1928
#: inc/settings/options.php:35 inc/settings/options.php:67
#: inc/settings/wpbf-global-settings.php:91
msgid "Transparent Header"
msgstr ""

#: inc/customizer/wpbf-kirki.php:106
msgid "Custom 404 Page"
msgstr ""

#: inc/customizer/wpbf-kirki.php:107
msgid ""
"Replace the default 404 page with your custom layout. <br><br>Example:<br>"
"[elementor-template id=\"xxx\"]<br>[fl_builder_insert_layout id=\"xxx\"]"
msgstr ""

#: inc/customizer/wpbf-kirki.php:120
msgid "Icons"
msgstr ""

#: inc/customizer/wpbf-kirki.php:121
msgid ""
"Display social media icons in your pre-header, footer or template file by "
"using the [social] shortcode."
msgstr ""

#: inc/customizer/wpbf-kirki.php:125
msgid "Facebook"
msgstr ""

#: inc/customizer/wpbf-kirki.php:126
msgid "Twitter"
msgstr ""

#: inc/customizer/wpbf-kirki.php:127
msgid "Pinterest"
msgstr ""

#: inc/customizer/wpbf-kirki.php:128
msgid "Youtube"
msgstr ""

#: inc/customizer/wpbf-kirki.php:129
msgid "Instagram"
msgstr ""

#: inc/customizer/wpbf-kirki.php:130
msgid "Vimeo"
msgstr ""

#: inc/customizer/wpbf-kirki.php:131
msgid "Soundcloud"
msgstr ""

#: inc/customizer/wpbf-kirki.php:132
msgid "LinkedIn"
msgstr ""

#: inc/customizer/wpbf-kirki.php:133
msgid "Yelp"
msgstr ""

#: inc/customizer/wpbf-kirki.php:134
msgid "Behance"
msgstr ""

#: inc/customizer/wpbf-kirki.php:135
msgid "Spotify"
msgstr ""

#: inc/customizer/wpbf-kirki.php:136
msgid "Reddit"
msgstr ""

#: inc/customizer/wpbf-kirki.php:146
msgid "Facebook URL"
msgstr ""

#: inc/customizer/wpbf-kirki.php:163
msgid "Twitter URL"
msgstr ""

#: inc/customizer/wpbf-kirki.php:180
msgid "Pinterest URL"
msgstr ""

#: inc/customizer/wpbf-kirki.php:197
msgid "Youtube URL"
msgstr ""

#: inc/customizer/wpbf-kirki.php:214
msgid "Instagram URL"
msgstr ""

#: inc/customizer/wpbf-kirki.php:231
msgid "Vimeo URL"
msgstr ""

#: inc/customizer/wpbf-kirki.php:248
msgid "Soundcloud URL"
msgstr ""

#: inc/customizer/wpbf-kirki.php:265
msgid "LinkedIn URL"
msgstr ""

#: inc/customizer/wpbf-kirki.php:282
msgid "Yelp URL"
msgstr ""

#: inc/customizer/wpbf-kirki.php:299
msgid "Behance URL"
msgstr ""

#: inc/customizer/wpbf-kirki.php:316
msgid "Spotify URL"
msgstr ""

#: inc/customizer/wpbf-kirki.php:333
msgid "Reddit URL"
msgstr ""

#: inc/customizer/wpbf-kirki.php:358
msgid "Style"
msgstr ""

#: inc/customizer/wpbf-kirki.php:364
msgid "Plain"
msgstr ""

#: inc/customizer/wpbf-kirki.php:365
msgid "Rounded"
msgstr ""

#: inc/customizer/wpbf-kirki.php:366
msgid "Boxed"
msgstr ""

#: inc/customizer/wpbf-kirki.php:374 inc/customizer/wpbf-kirki.php:554
#: inc/customizer/wpbf-kirki.php:615 inc/customizer/wpbf-kirki.php:697
#: inc/customizer/wpbf-kirki.php:779 inc/customizer/wpbf-kirki.php:861
#: inc/customizer/wpbf-kirki.php:943 inc/customizer/wpbf-kirki.php:1497
#: inc/customizer/wpbf-kirki.php:2873
msgid "Color"
msgstr ""

#: inc/customizer/wpbf-kirki.php:380
msgid "Default"
msgstr ""

#: inc/customizer/wpbf-kirki.php:381
msgid "Grey"
msgstr ""

#: inc/customizer/wpbf-kirki.php:382
msgid "Brand Colors"
msgstr ""

#: inc/customizer/wpbf-kirki.php:383
msgid "Filled"
msgstr ""

#: inc/customizer/wpbf-kirki.php:391 inc/customizer/wpbf-kirki.php:2892
msgid "Size"
msgstr ""

#: inc/customizer/wpbf-kirki.php:397 inc/customizer/wpbf-kirki.php:450
msgid "Small"
msgstr ""

#: inc/customizer/wpbf-kirki.php:398 inc/customizer/wpbf-kirki.php:452
msgid "Large"
msgstr ""

#: inc/customizer/wpbf-kirki.php:406 inc/customizer/wpbf-kirki.php:3183
#: inc/customizer/wpbf-kirki.php:3194 inc/customizer/wpbf-kirki.php:3205
#: inc/customizer/wpbf-kirki.php:3231 inc/customizer/wpbf-kirki.php:3242
#: inc/customizer/wpbf-kirki.php:3253 inc/customizer/wpbf-kirki.php:3279
#: inc/customizer/wpbf-kirki.php:3290 inc/customizer/wpbf-kirki.php:3301
#: inc/customizer/wpbf-kirki.php:3327 inc/customizer/wpbf-kirki.php:3338
#: inc/customizer/wpbf-kirki.php:3349 inc/customizer/wpbf-kirki.php:3375
#: inc/customizer/wpbf-kirki.php:3386 inc/customizer/wpbf-kirki.php:3397
#: inc/customizer/wpbf-kirki.php:3423 inc/customizer/wpbf-kirki.php:3434
#: inc/customizer/wpbf-kirki.php:3445 inc/customizer/wpbf-kirki.php:3471
#: inc/customizer/wpbf-kirki.php:3482 inc/customizer/wpbf-kirki.php:3493
#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:278
msgid "Font Size"
msgstr ""

#: inc/customizer/wpbf-kirki.php:429
msgid "Grid Layout Settings"
msgstr ""

#: inc/customizer/wpbf-kirki.php:451
msgid "Medium"
msgstr ""

#: inc/customizer/wpbf-kirki.php:453
msgid "xLarge"
msgstr ""

#: inc/customizer/wpbf-kirki.php:454
msgid "Collapse"
msgstr ""

#: inc/customizer/wpbf-kirki.php:469
msgid "Masonry Effect"
msgstr ""

#: inc/customizer/wpbf-kirki.php:490
msgid "Bold Text Color"
msgstr ""

#: inc/customizer/wpbf-kirki.php:503 inc/customizer/wpbf-kirki.php:566
#: inc/customizer/wpbf-kirki.php:627 inc/customizer/wpbf-kirki.php:709
#: inc/customizer/wpbf-kirki.php:791 inc/customizer/wpbf-kirki.php:873
#: inc/customizer/wpbf-kirki.php:955
msgid "Line Height"
msgstr ""

#: inc/customizer/wpbf-kirki.php:521 inc/customizer/wpbf-kirki.php:582
#: inc/customizer/wpbf-kirki.php:650 inc/customizer/wpbf-kirki.php:732
#: inc/customizer/wpbf-kirki.php:814 inc/customizer/wpbf-kirki.php:896
#: inc/customizer/wpbf-kirki.php:978
msgid "Letter Spacing"
msgstr ""

#: inc/customizer/wpbf-kirki.php:537 inc/customizer/wpbf-kirki.php:598
#: inc/customizer/wpbf-kirki.php:673 inc/customizer/wpbf-kirki.php:755
#: inc/customizer/wpbf-kirki.php:837 inc/customizer/wpbf-kirki.php:919
#: inc/customizer/wpbf-kirki.php:1001
msgid "Text transform"
msgstr ""

#: inc/customizer/wpbf-kirki.php:543 inc/customizer/wpbf-kirki.php:604
#: inc/customizer/wpbf-kirki.php:679 inc/customizer/wpbf-kirki.php:761
#: inc/customizer/wpbf-kirki.php:843 inc/customizer/wpbf-kirki.php:925
#: inc/customizer/wpbf-kirki.php:1007 inc/customizer/wpbf-kirki.php:1561
#: inc/customizer/wpbf-kirki.php:2802
msgid "None"
msgstr ""

#: inc/customizer/wpbf-kirki.php:544 inc/customizer/wpbf-kirki.php:605
#: inc/customizer/wpbf-kirki.php:680 inc/customizer/wpbf-kirki.php:762
#: inc/customizer/wpbf-kirki.php:844 inc/customizer/wpbf-kirki.php:926
#: inc/customizer/wpbf-kirki.php:1008
msgid "Uppercase"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1025
msgid "Enable Adobe Fonts"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1035
msgid "Adobe Fonts ID"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1057
msgid "Adobe Font"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1069 inc/customizer/wpbf-kirki.php:1147
#: inc/settings/wpbf-global-settings.php:98
#: inc/settings/wpbf-global-settings.php:102
msgid "Name"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1073 inc/customizer/wpbf-kirki.php:1151
msgid "Font Family"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1077
msgid "Variants"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1080
msgid "100"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1081
msgid "100italic"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1082
msgid "200"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1083
msgid "200italic"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1084
msgid "300"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1085
msgid "300italic"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1086
msgid "regular"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1087
msgid "italic"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1088
msgid "500"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1089
msgid "500italic"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1090
msgid "600"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1091
msgid "600italic"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1092
msgid "700"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1093
msgid "700italic"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1094
msgid "800"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1095
msgid "800italic"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1096
msgid "900"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1097
msgid "900italic"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1116
msgid "Enable Custom Fonts"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1131
msgid "Custom Font"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1156
msgid "Woff"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1161
msgid "Woff2"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1166
msgid "TTF"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1171
msgid "SVG"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1176
msgid "EOT"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1206 inc/customizer/wpbf-kirki.php:2078
msgid "Logo"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1227
msgid "Hide Logo"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1228
msgid "Hides logo from sticky navigation."
msgstr ""

#: inc/customizer/wpbf-kirki.php:1249
msgid "Menu Height"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1272 inc/customizer/wpbf-kirki.php:2582
msgid "Logo Area Background Color"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1302 inc/customizer/wpbf-kirki.php:1879
#: inc/customizer/wpbf-kirki.php:1936 inc/customizer/wpbf-kirki.php:2000
#: inc/customizer/wpbf-kirki.php:2094 inc/customizer/wpbf-kirki.php:2442
#: inc/customizer/wpbf-kirki.php:2696 inc/customizer/wpbf-kirki.php:2768
#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:324
#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:470
msgid "Background Color"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1322 inc/customizer/wpbf-kirki.php:1903
#: inc/customizer/wpbf-kirki.php:1960 inc/customizer/wpbf-kirki.php:2038
#: inc/customizer/wpbf-kirki.php:2106
#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:300
#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:446
msgid "Font Color"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1341 inc/customizer/wpbf-kirki.php:1384
#: inc/customizer/wpbf-kirki.php:1891 inc/customizer/wpbf-kirki.php:1915
#: inc/customizer/wpbf-kirki.php:1948 inc/customizer/wpbf-kirki.php:1972
#: inc/customizer/wpbf-kirki.php:2019 inc/customizer/wpbf-kirki.php:2057
#: inc/customizer/wpbf-kirki.php:2118 inc/customizer/wpbf-kirki.php:2149
msgid "Hover"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1360 inc/customizer/wpbf-kirki.php:2130
msgid "Logo Color"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1408 inc/customizer/wpbf-kirki.php:2168
msgid "Tagline Color"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1453
msgid "Box Shadow"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1470
msgid "Blur"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1537
msgid "Delay"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1542
msgid "Set a delay after the sticky navigation should appear. Default: 300px"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1556 inc/customizer/wpbf-kirki.php:2813
msgid "Animation"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1562
msgid "Fade In"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1563
msgid "Slide Down"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1564
msgid "Hide on Scroll"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1578
msgid "Animation Duration"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1612 inc/customizer/wpbf-kirki.php:2193
#: inc/customizer/wpbf-kirki.php:2606
msgid "Off Canvas Settings"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1633 inc/customizer/wpbf-kirki.php:2209
#: inc/customizer/wpbf-kirki.php:2622
msgid "Full Screen Menu Settings"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1653 inc/customizer/wpbf-kirki.php:1724
#: inc/customizer/wpbf-kirki.php:2224 inc/customizer/wpbf-kirki.php:2280
#: inc/customizer/wpbf-kirki.php:2675
msgid "Icon Color"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1678 inc/customizer/wpbf-kirki.php:2244
msgid "Mobile Menu Settings"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1698 inc/customizer/wpbf-kirki.php:2259
msgid "Hamburger Icon Color"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1747
msgid "Sticky Pre Header"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1773
msgid "Display Call to Action Button"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1774
msgid ""
"You can declare the Call to Action Button manually by assigning the \"wpbf-"
"cta-menu-item\" class to your menu-item of choice. Ticking this setting will "
"display the Call to Action Button as the last element inside your main "
"navigation."
msgstr ""

#: inc/customizer/wpbf-kirki.php:1783
msgid "Display Call to Action Button (Mobile)"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1799
msgid "Text"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1815 inc/customizer/wpbf-kirki.php:2992
msgid "URL"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1831
msgid "Open in a new Tab"
msgstr ""

#: inc/customizer/wpbf-kirki.php:1847 inc/customizer/wpbf-kirki.php:2914
msgid "Border Radius"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2324
msgid "Sub Menu Animation"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2330 inc/customizer/wpbf-kirki.php:2819
msgid "Fade"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2331
msgid "Down"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2332
msgid "Up"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2333
msgid "Zoom In"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2334
msgid "Zoom Out"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2392 inc/customizer/wpbf-kirki.php:2652
msgid "Menu Width"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2393
msgid "Default: 320px"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2426 inc/customizer/wpbf-kirki.php:2752
msgid "Overlay"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2481
msgid ""
"Paste your shortcode to replace the default menu with your Custom Menu. <br>"
"<br>Example:<br>[elementor-template id=\"xxx\"]<br>[fl_builder_insert_layout "
"id=\"xxx\"]"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2499
msgid "Advanced Settings"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2514
msgid "Menu Alignment"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2537
msgid "Content beside Logo"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2637
msgid "Push Menu"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2716
msgid "Sub Menu Arrow Color"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2796
msgid "Hover Effect"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2803
msgid "Underline"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2804
msgid "Box"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2805
msgid "Modern"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2820
msgid "Slide"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2821
msgid "Grow"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2841
msgid "Alignment"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2943
msgid "Sticky Footer"
msgstr ""

#: inc/customizer/wpbf-kirki.php:2976
msgid "Theme Author"
msgstr ""

#: inc/customizer/wpbf-kirki.php:3032
msgid "Custom Footer"
msgstr ""

#: inc/customizer/wpbf-kirki.php:3033
msgid ""
"Paste your shortcode to populate a saved row/template throughout your "
"website. <br><br>Examples:<br>[elementor-template id=\"xxx\"]<br>"
"[fl_builder_insert_layout id=\"xxx\"]"
msgstr ""

#: inc/customizer/wpbf-kirki.php:3049
msgid "Head Code"
msgstr ""

#: inc/customizer/wpbf-kirki.php:3050
msgid "Runs inside the head tag."
msgstr ""

#: inc/customizer/wpbf-kirki.php:3062
msgid "Header Code"
msgstr ""

#: inc/customizer/wpbf-kirki.php:3063
msgid "Runs after the opening body tag."
msgstr ""

#: inc/customizer/wpbf-kirki.php:3075
msgid "Footer Code"
msgstr ""

#: inc/customizer/wpbf-kirki.php:3076
msgid ""
"Add Scripts (Google Analytics, etc.) here. Runs before the closing body tag "
"(wp_footer)."
msgstr ""

#: inc/customizer/wpbf-kirki.php:3110 inc/customizer/wpbf-kirki.php:3127
#: inc/customizer/wpbf-kirki.php:3144
msgid "Logo Width"
msgstr ""

#: inc/customizer/wpbf-kirki.php:3532 inc/customizer/wpbf-kirki.php:3544
#: inc/customizer/wpbf-kirki.php:3556
msgid "Posts per Row"
msgstr ""

#: inc/integration/elementor-pro.php:30
msgid "Before Header"
msgstr ""

#: inc/integration/elementor-pro.php:49
msgid "After Header"
msgstr ""

#: inc/integration/elementor-pro.php:67
msgid "Before Footer"
msgstr ""

#: inc/integration/elementor-pro.php:76
msgid "After Footer"
msgstr ""

#: inc/integration/elementor-pro.php:86
msgid "Before Post"
msgstr ""

#: inc/integration/elementor-pro.php:95
msgid "After Post"
msgstr ""

#: inc/integration/elementor-pro.php:105
msgid "Before Sidebar"
msgstr ""

#: inc/integration/elementor-pro.php:114
msgid "After Sidebar"
msgstr ""

#: inc/settings/wpbf-global-settings.php:53
msgid "Archives"
msgstr ""

#: inc/settings/wpbf-global-settings.php:79
#, php-format
msgid "Global Template Settings %1s"
msgstr ""

#: inc/settings/wpbf-global-settings.php:80
#, php-format
msgid "Blog Layouts %1s"
msgstr ""

#: inc/settings/wpbf-global-settings.php:81
#, php-format
msgid "Performance Settings %1s"
msgstr ""

#: inc/settings/wpbf-global-settings.php:82
msgid "Responsive Breakpoints"
msgstr ""

#: inc/settings/wpbf-global-settings.php:83
#, php-format
msgid "White Label %s"
msgstr ""

#: inc/settings/wpbf-global-settings.php:84
msgid "Plugin"
msgstr ""

#: inc/settings/wpbf-global-settings.php:85
msgid "Theme"
msgstr ""

#: inc/settings/wpbf-global-settings.php:88
msgid "Blog Layout Settings"
msgstr ""

#: inc/settings/wpbf-global-settings.php:88
msgid "Enables additional Blog Layout Settings in the Customizer."
msgstr ""

#: inc/settings/wpbf-global-settings.php:89
msgid "Full Width"
msgstr ""

#: inc/settings/wpbf-global-settings.php:90
msgid "Remove Title"
msgstr ""

#: inc/settings/wpbf-global-settings.php:92
msgid "Performance Settings"
msgstr ""

#: inc/settings/wpbf-global-settings.php:93
msgid "Desktop"
msgstr ""

#: inc/settings/wpbf-global-settings.php:94
msgid "Tablet"
msgstr ""

#: inc/settings/wpbf-global-settings.php:95
msgid "Mobile"
msgstr ""

#: inc/settings/wpbf-global-settings.php:96
msgid "Company Name"
msgstr ""

#: inc/settings/wpbf-global-settings.php:97
msgid "Company URL"
msgstr ""

#: inc/settings/wpbf-global-settings.php:99
#: inc/settings/wpbf-global-settings.php:103
msgid "Description"
msgstr ""

#: inc/settings/wpbf-global-settings.php:100
#: inc/settings/wpbf-global-settings.php:249
msgid "Tags"
msgstr ""

#: inc/settings/wpbf-global-settings.php:101
msgid "Screenshot"
msgstr ""

#: inc/settings/wpbf-global-settings.php:248
msgid "Categories"
msgstr ""

#: inc/settings/wpbf-global-settings.php:251
msgid "Date Archives"
msgstr ""

#: inc/settings/wpbf-global-settings.php:299
msgid "Compile inline CSS"
msgstr ""

#: inc/settings/wpbf-global-settings.php:301
msgid "Remove Feed Links"
msgstr ""

#: inc/settings/wpbf-global-settings.php:303
msgid "Remove RSD"
msgstr ""

#: inc/settings/wpbf-global-settings.php:305
msgid "Remove wlwmanifest"
msgstr ""

#: inc/settings/wpbf-global-settings.php:307
msgid "Remove Generator"
msgstr ""

#: inc/settings/wpbf-global-settings.php:309
msgid "Remove Shortlink"
msgstr ""

#: inc/settings/wpbf-global-settings.php:311
msgid "Disable Emojis"
msgstr ""

#: inc/settings/wpbf-global-settings.php:313
msgid "Disable Embeds"
msgstr ""

#: inc/settings/wpbf-global-settings.php:315
msgid "Remove jQuery Migrate"
msgstr ""

#: inc/settings/wpbf-global-settings.php:317
msgid "Disable RSS Feed"
msgstr ""

#: inc/settings/wpbf-global-settings.php:331
msgid "Default: until 480px for mobiles."
msgstr ""

#: inc/settings/wpbf-global-settings.php:343
msgid "Default: above 768px for tablets."
msgstr ""

#: inc/settings/wpbf-global-settings.php:355
msgid "Default: above 1024px for desktops."
msgstr ""

#: inc/settings/wpbf-global-settings.php:444
msgid "Recommended image size: 1200px x 900px"
msgstr ""

#: inc/settings/wpbf-settings-template.php:22
msgid "Template Settings"
msgstr ""

#: inc/settings/wpbf-settings-template.php:23
msgid "License"
msgstr ""

#: inc/settings/wpbf-settings-template.php:48
#, php-format
msgid "License Key Activation %1s"
msgstr ""

#: inc/settings/wpbf-settings-template.php:55
msgid "License Key"
msgstr ""

#: inc/settings/wpbf-settings-template.php:59
msgid "Enter your Premium Add-On license key."
msgstr ""

#: inc/settings/wpbf-settings-template.php:65
#: inc/settings/wpbf-settings-template.php:74
msgid "Activate License"
msgstr ""

#: inc/settings/wpbf-settings-template.php:69
msgid "active"
msgstr ""

#: inc/settings/wpbf-settings-template.php:71
msgid "Deactivate License"
msgstr ""

#: inc/integration/woocommerce/woocommerce-functions.php:111
#: inc/integration/woocommerce/woocommerce-functions.php:133
#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:30
msgid "Cart"
msgstr ""

#: inc/integration/woocommerce/woocommerce-functions.php:113
msgid "-"
msgstr ""

#: inc/integration/woocommerce/woocommerce-functions.php:151
msgid "Product/s"
msgstr ""

#: inc/integration/woocommerce/woocommerce-functions.php:152
msgid "Quantity"
msgstr ""

#: inc/integration/woocommerce/woocommerce-functions.php:192
msgid "Subtotal"
msgstr ""

#: inc/integration/woocommerce/woocommerce-functions.php:200
msgid "Checkout"
msgstr ""

#: inc/integration/woocommerce/woocommerce-functions.php:250
msgid "Your Off Canvas Sidebar Widgets will appear here."
msgstr ""

#: inc/integration/woocommerce/woocommerce-functions.php:255
msgid "Add Widgets"
msgstr ""

#: inc/integration/woocommerce/woocommerce-functions.php:271
msgid "Filter"
msgstr ""

#: inc/integration/woocommerce/woocommerce-quick-view.php:180
msgid "Placeholder"
msgstr ""

#: inc/integration/woocommerce/woocommerce-quick-view.php:227
#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:261
msgid "Quick View"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:24
#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:401
msgid "Icon"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:31
msgid "Basket"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:32
msgid "Bag"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:55
msgid "\"Cart\" Text"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:61
#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:79
#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:153
#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:178
msgid "Show"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:62
#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:80
#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:154
#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:179
msgid "Hide"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:73
msgid "Amount"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:102
msgid "Cart Dropdown"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:108
msgid "Enable"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:109
msgid "Disable"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:120
msgid "Cart Popup"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:121
msgid ""
"Displays the cart dropdown for a short period of time after a product was "
"added to the cart"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:147
msgid "Cart Button"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:172
msgid "Checkout Button"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:199
msgid "Image Flip"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:200
msgid ""
"Displays the first image of your product gallery (if available) when "
"hovering over the product thumbnail."
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:206
#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:225
#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:267
#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:389
msgid "Enabled"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:207
#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:224
#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:268
#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:390
msgid "Disabled"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:218
msgid "Infinite Scroll"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:348
#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:494
msgid "Overlay Background Color"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:383
msgid "Off Canvas Sidebar"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:407
msgid "Search"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:408
msgid "Hamburger"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:426
msgid "Label"
msgstr ""

#: inc/integration/woocommerce/wpbf-kirki-woocommerce.php:520
msgid "Distraction Free Checkout"
msgstr ""

#. Name of the plugin
#. Description of the plugin
msgid "Page Builder Framework Premium Addon"
msgstr ""

#. Description of the plugin
msgid "Premium Add-On for Page Builder Framework"
msgstr ""

#. URI of the plugin
msgid "https://wp-pagebuilderframework.com"
msgstr ""

#. Author of the plugin
msgid "David Vongries"
msgstr ""

#. Author URI of the plugin
msgid "https://mapsteps.com"
msgstr ""
