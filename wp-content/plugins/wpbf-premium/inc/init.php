<?php
/**
 * Init.
 *
 * @package Page Builder Framework Premium Add-On
 */

defined( 'ABSPATH' ) || die( "Can't access directly" );

/**
 * Theme settings page.
 */
function wpbf_premium_settings() {
	add_theme_page( __( 'Theme Settings', 'wpbfpremium' ), __( 'Theme Settings', 'wpbfpremium' ), 'manage_options', 'wpbf-premium', 'wpbf_premium_settings_callback' );
}
add_action( 'admin_menu', 'wpbf_premium_settings' );

/**
 * Theme settings page callback.
 */
function wpbf_premium_settings_callback() {
	require_once WPBF_PREMIUM_DIR . 'inc/settings/settings-page.php';
}

/**
 * Admin scripts & styles.
 */
function wpbf_premium_admin_scripts() {

	global $pagenow;

	if ( 'themes.php' === $pagenow && isset( $_GET['page'] ) ) {

		if ( 'wpbf-premium' !== $_GET['page'] ) {
			return;
		}

		wp_enqueue_style( 'settings-page', WPBF_PREMIUM_URI . 'assets/css/settings-page.css', array(), WPBF_PREMIUM_VERSION );
		wp_enqueue_style( 'setting-fields', WPBF_PREMIUM_URI . 'assets/css/setting-fields.css', array(), WPBF_PREMIUM_VERSION );
		wp_enqueue_style( 'wpbf-admin-page', WPBF_PREMIUM_URI . 'assets/css/admin-page.css', array(), WPBF_PREMIUM_VERSION );
		wp_enqueue_script( 'wpbf-premium-settings', WPBF_PREMIUM_URI . 'js/wpbf-premium-admin.js', array( 'jquery' ), WPBF_PREMIUM_VERSION, true );

	}

}
add_action( 'admin_enqueue_scripts', 'wpbf_premium_admin_scripts' );

/**
 * Change inline style location.
 *
 * @return string The stylesheet handle.
 */
function wpbf_premium_change_inline_style_location() {
	return 'wpbf-premium';
}
add_filter( 'wpbf_add_inline_style', 'wpbf_premium_change_inline_style_location' );

// Kirki.
require_once WPBF_PREMIUM_DIR . 'inc/customizer/wpbf-kirki.php';

// Custom fonts.
require_once WPBF_PREMIUM_DIR . 'inc/customizer/custom-fonts.php';

// Adobe Fonts integration.
require_once WPBF_PREMIUM_DIR . 'inc/customizer/adobe-fonts.php';

// Custom controls.
require_once WPBF_PREMIUM_DIR . 'inc/customizer/custom-controls.php';

// Customizer functions.
require_once WPBF_PREMIUM_DIR . 'inc/customizer/customizer-functions.php';

// Styles.
require_once WPBF_PREMIUM_DIR . 'inc/customizer/styles.php';

// Responsive styles.
require_once WPBF_PREMIUM_DIR . 'inc/customizer/responsive.php';

// Options.
require_once WPBF_PREMIUM_DIR . 'inc/settings/options.php';

// Premium settings.
require_once WPBF_PREMIUM_DIR . 'inc/settings/global-settings.php';

// Premium settings output.
require_once WPBF_PREMIUM_DIR . 'inc/settings/global-functions.php';

// Body classes.
require_once WPBF_PREMIUM_DIR . 'inc/body-classes.php';

// Blog Layouts.
require_once WPBF_PREMIUM_DIR . 'inc/blog-layouts.php';

// Helpers.
require_once WPBF_PREMIUM_DIR . 'inc/helpers.php';

// Theme mods.
require_once WPBF_PREMIUM_DIR . 'inc/theme-mods.php';

// Customizer Export/Import (https://de.wordpress.org/plugins/customizer-export-import/).
require_once WPBF_PREMIUM_DIR . 'inc/integration/customizer-import-export.php';

// Custom Sections.
require_once WPBF_PREMIUM_DIR . 'inc/class-custom-sections.php';

/**
 * Plugins loaded.
 *
 * Load specific integrations after plugins are loaded
 * to make sure they exist when we check for them.
 */
function wpbf_premium_plugins_loaded() {

	// Beaver Themer.
	if ( class_exists( 'FLThemeBuilderLoader' ) && class_exists( 'FLBuilderLoader' ) ) {
		require_once WPBF_PREMIUM_DIR . 'inc/integration/beaver-themer.php';
	}

	// WooCommerce.
	if ( class_exists( 'WooCommerce' ) ) {
		require_once WPBF_PREMIUM_DIR . '/inc/integration/woocommerce.php';
	}

}
add_action( 'plugins_loaded', 'wpbf_premium_plugins_loaded' );

/**
 * Elementor integration.
 */
function wpbf_elementor_integration() {
	require_once WPBF_PREMIUM_DIR . 'inc/integration/elementor.php';
}
add_action( 'elementor/init', 'wpbf_elementor_integration' );

/**
 * Elementor Pro integration.
 */
function wpbf_elementor_pro_integration() {
	require_once WPBF_PREMIUM_DIR . 'inc/integration/elementor-pro.php';
}
add_action( 'elementor_pro/init', 'wpbf_elementor_pro_integration' );


/**
 * Divi integration.
 */
function wpbf_divi_integration() {
	// Works both for Divi Builder and Divi Theme.
	if ( ! function_exists( 'et_pb_is_pagebuilder_used' ) ) {
		return;
	}

	require_once WPBF_PREMIUM_DIR . 'inc/integration/divi.php';
}
add_action( 'init', 'wpbf_divi_integration' );
