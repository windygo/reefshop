<?php
/**
 * Theme settings template.
 *
 * @package Page Builder Framework Premium Add-On
 * @subpackage Settings
 */

defined( 'ABSPATH' ) || die( "Can't access directly" );
?>

<div class="wrap settingstuff">
	<h1>
		<?php echo esc_html( get_admin_page_title() ); ?>
		<span class="version"><?php echo esc_html( WPBF_PREMIUM_VERSION ); ?></span>
	</h1>

	<?php $active_tab = isset( $_GET['tab'] ) ? sanitize_text_field( $_GET['tab'] ) : 'settings'; ?>

	<h2 class="nav-tab-wrapper">
		<a href="?page=wpbf-premium&tab=settings" class="nav-tab<?php echo 'settings' === $active_tab ? ' nav-tab-active' : ''; ?>"><?php _e( 'Theme Settings', 'wpbfpremium' ); ?></a>

		<a href="?page=wpbf-premium&tab=license" class="nav-tab<?php echo 'license' === $active_tab ? ' nav-tab-active' : ''; ?>"><?php _e( 'License', 'wpbfpremium' ); ?></a>
	</h2>

	<form method="post" action="options.php" class="wpbf-settings-form">

		<?php


		if ( 'settings' === $active_tab ) {

			settings_fields( 'wpbf-premium-group' );
			?>

			<div class="wpbf-admin-page">
				<div class="left-section">

					<?php
					require __DIR__ . '/metaboxes/customizer.php';
					require __DIR__ . '/metaboxes/premium.php';
					require __DIR__ . '/metaboxes/whitelabel.php';
					submit_button();
					?>

				</div><!-- .left-section -->

				<?php if ( ! wpbf_is_white_labeled() ) : ?>
					<div class="right-section">

						<?php
						require __DIR__ . '/metaboxes/documentation.php';
						require __DIR__ . '/metaboxes/community.php';
						require __DIR__ . '/metaboxes/additional.php';
						?>

					</div><!-- .right-section -->
				<?php endif; ?>
			</div>

			<?php
		} elseif ( 'license' === $active_tab ) {

			settings_fields( 'wpbf_premium_license' );
			require __DIR__ . '/metaboxes/license.php';
			submit_button();

		}

		?>

	</form>
</div><!-- .settingstuff -->
