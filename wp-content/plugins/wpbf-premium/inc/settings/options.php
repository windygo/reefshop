<?php
/**
 * Options.
 *
 * @package Page Builder Framework Premium Add-On
 * @subpackage Settings
 */

defined( 'ABSPATH' ) || die( "Can't access directly" );

/**
 * Load metaboxes.
 */
function wpbf_premium_metabox_setup() {

	add_action( 'add_meta_boxes', 'wpbf_premium_transparent_header_metabox', 20 );
	add_action( 'save_post', 'wpbf_premium_save_postmeta', 10, 2 );

}
add_action( 'load-post.php', 'wpbf_premium_metabox_setup' );
add_action( 'load-post-new.php', 'wpbf_premium_metabox_setup' );

/**
 * Transparent header metabox.
 */
function wpbf_premium_transparent_header_metabox() {

	// Get public post types.
	$post_types = get_post_types( array( 'public' => true ) );

	// Remove certain post types from array.
	unset( $post_types['wpbf_hooks'], $post_types['elementor_library'], $post_types['fl-builder-template'] );

	// Add transparent header metabox.
	add_meta_box( 'wpbf_header', __( 'Transparent Header', 'wpbfpremium' ), 'wpbf_transparent_header_metabox_callback', $post_types, 'side', 'default' );

}

/**
 * Transparent header metabox callback.
 *
 * @param object $post The post object.
 */
function wpbf_transparent_header_metabox_callback( $post ) {

	wp_nonce_field( basename( __FILE__ ), 'wpbf_premium_options_nonce' );

	$stored_meta = get_post_meta( $post->ID );

	// Set default value.
	if ( ! isset( $stored_meta['wpbf_premium_options'][0] ) ) {
		$stored_meta['wpbf_premium_options'][0] = false;
	}

	$stored_meta = $stored_meta['wpbf_premium_options'][0];

	if ( strpos( $stored_meta, 'transparent-header' ) !== false ) {
		$transparent_header = 'transparent-header';
	} else {
		$transparent_header = false;
	}

	?>

	<div>
		<input id="transparent-header" type="checkbox" name="wpbf_premium_options[]" value="transparent-header" <?php checked( $transparent_header, 'transparent-header' ); ?> />
		<label for="transparent-header"><?php _e( 'Transparent Header', 'wpbfpremium' ); ?></label>
	</div>

	<?php

}

/**
 * Save postmeta data.
 *
 * @param int $post_id The post ID.
 */
function wpbf_premium_save_postmeta( $post_id ) {

	$is_autosave    = wp_is_post_autosave( $post_id );
	$is_revision    = wp_is_post_revision( $post_id );
	$is_valid_nonce = ( isset( $_POST['wpbf_premium_options_nonce'] ) && wp_verify_nonce( $_POST['wpbf_premium_options_nonce'], basename( __FILE__ ) ) ) ? true : false;

	// Stop here if is autosave, revision or nonce is invalid.
	if ( $is_autosave || $is_revision || ! $is_valid_nonce ) {
		return;
	}

	// Stop here if current user can't edit posts.
	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return $post_id;
	}

	if ( isset( $_POST['wpbf_premium_options'] ) ) {

		$options = array();

		// Transparent header.
		if ( in_array( 'transparent-header', $_POST['wpbf_premium_options'] ) !== false ) {
			$options[] = 'transparent-header';
		}

	}

	update_post_meta( $post_id, 'wpbf_premium_options', $options );

}
