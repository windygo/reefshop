<?php
/**
 * Metabox template for displaying whitelabel settings.
 *
 * @package Page_Builder_Framework_Premium_Add_On
 */

defined( 'ABSPATH' ) || die( "Can't access directly" );
?>

<div class="neatbox whitelabel-box has-subboxes has-bigger-heading is-grouped is-smooth">

	<?php do_settings_sections( 'wpbf-whitelabel-sections' ); ?>

</div><!-- .whitelabel-box -->
