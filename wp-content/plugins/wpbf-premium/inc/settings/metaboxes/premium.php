<?php
/**
 * Metabox template for displaying premium settings.
 *
 * @package Page_Builder_Framework_Premium_Add_On
 */

defined( 'ABSPATH' ) || die( "Can't access directly" );
?>

<div class="neatbox premium-box has-subboxes has-bigger-heading is-smooth">

	<?php do_settings_sections( 'wpbf-premium-settings' ); ?>

</div><!-- .premium-box -->
