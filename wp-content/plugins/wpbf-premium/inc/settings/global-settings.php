<?php
/**
 * Theme settings.
 *
 * @package Page Builder Framework Premium Add-On
 * @subpackage Settings
 */

defined( 'ABSPATH' ) || die( "Can't access directly" );

/**
 * Get custom post types.
 *
 * @param boolean $third_party If we only want to get third party post types.
 * @param boolean $as_archives If post types should be output as archives.
 *
 * @return array The post types array.
 */
function wpbf_get_cpts( $third_party = false, $as_archives = false ) {

	// Get public post types.
	$post_types = get_post_types( array( 'public' => true ) );

	// Remove known post types from array.
	unset(
		$post_types['wpbf_hooks'],
		$post_types['elementor_library'],
		$post_types['fl-builder-template'],
		$post_types['attachment']
	);

	if ( $third_party ) {
		unset(
			$post_types['page'],
			$post_types['post']
		);
	}

	// Remove product custom post type if WooCommerce is active.
	if ( class_exists( 'WooCommerce' ) ) {
		unset( $post_types['product'] );
	}

	// Remove download custom post type if EDD is active.
	if ( class_exists( 'Easy_Digital_Downloads' ) ) {
		unset( $post_types['download'] );
	}

	if ( $as_archives ) {

		// Construct archives array from remaining custom post types.
		foreach ( $post_types as $post_type ) {
			$post_types[ $post_type . '-archive' ] = ucfirst( $post_type ) . ' ' . __( 'Archives', 'wpbfpremium' );
			unset( $post_types[ $post_type ] );
		}
	}

	return $post_types;

}

/**
 * Premium Add-On settings.
 */
function wpbf_premium() {

	// Vars.
	$template_settings_link     = '<a href="https://wp-pagebuilderframework.com/docs/global-template-settings/" target="_blank" class="dashicons dashicons-editor-help help-icon"></a>';
	$performance_settings_link  = '<a href="https://wp-pagebuilderframework.com/docs/performance-settings/" target="_blank" class="dashicons dashicons-editor-help help-icon"></a>';
	$white_label_settings_link  = '<a href="https://wp-pagebuilderframework.com/docs/white-label/" target="_blank" class="dashicons dashicons-editor-help help-icon"></a>';
	$blog_layout_settings       = '<a href="https://wp-pagebuilderframework.com/docs/advanced-blog-layouts/" target="_blank" class="dashicons dashicons-editor-help help-icon"></a>';
	$child_theme_generator_link = '<a href="https://wp-pagebuilderframework.com/child-theme-generator/" target="_blank">Child Theme Generator</a>';

	// Setting.
	register_setting( 'wpbf-premium-group', 'wpbf_settings' );

	// Sections.
	add_settings_section( 'wpbf-global-template-settings', sprintf( __( 'Global Template Settings %1s', 'wpbfpremium' ), $template_settings_link ), '', 'wpbf-premium-settings' );
	add_settings_section( 'wpbf-blog-layout-settings', sprintf( __( 'Blog Layouts %1s', 'wpbfpremium' ), $blog_layout_settings ), '', 'wpbf-premium-settings' );
	add_settings_section( 'wpbf-performance-settings', sprintf( __( 'Performance Settings %1s', 'wpbfpremium' ), $performance_settings_link ), '', 'wpbf-premium-settings' );
	add_settings_section( 'wpbf-responsive-breakpoints-settings', __( 'Responsive Breakpoints', 'wpbfpremium' ), '', 'wpbf-premium-settings' );

	// Whitelabel sections.
	add_settings_section( 'wpbf-whitelabel-company-section', sprintf( __( 'White Label %s', 'wpbfpremium' ), $white_label_settings_link ), '', 'wpbf-whitelabel-sections' );
	add_settings_section( 'wpbf-whitelabel-plugin-section', __( 'Plugin', 'wpbfpremium' ), '', 'wpbf-whitelabel-sections' );
	add_settings_section( 'wpbf-whitelabel-theme-section', __( 'Theme', 'wpbfpremium' ), '', 'wpbf-whitelabel-sections' );

	// Fields.
	add_settings_field( 'wpbf_blog_layouts', __( 'Blog Layout Settings', 'wpbfpremium' ) . '<p class="description" style="margin-top: 10px;">' . __( 'Enables additional Blog Layout Settings in the Customizer.', 'wpbfpremium' ) . '</p>', 'wpbf_blog_layouts_callback', 'wpbf-premium-settings', 'wpbf-blog-layout-settings' );
	add_settings_field( 'wpbf_fullwidth_global', __( 'Full Width', 'wpbfpremium' ), 'wpbf_fullwidth_global_callback', 'wpbf-premium-settings', 'wpbf-global-template-settings' );
	add_settings_field( 'wpbf_removetitle_global', __( 'Remove Title', 'wpbfpremium' ), 'wpbf_removetitle_global_callback', 'wpbf-premium-settings', 'wpbf-global-template-settings' );
	add_settings_field( 'wpbf_transparent_header_global', __( 'Transparent Header', 'wpbfpremium' ), 'wpbf_transparent_header_global_callback', 'wpbf-premium-settings', 'wpbf-global-template-settings' );
	add_settings_field( 'wpbf_clean_head', __( 'Performance Settings', 'wpbfpremium' ), 'wpbf_performance_callback', 'wpbf-premium-settings', 'wpbf-performance-settings' );
	add_settings_field( 'wpbf_breakpoint_desktop', __( 'Desktop', 'wpbfpremium' ), 'wpbf_breakpoint_desktop_callback', 'wpbf-premium-settings', 'wpbf-responsive-breakpoints-settings' );
	add_settings_field( 'wpbf_breakpoint_medium', __( 'Tablet', 'wpbfpremium' ), 'wpbf_breakpoint_medium_callback', 'wpbf-premium-settings', 'wpbf-responsive-breakpoints-settings' );
	add_settings_field( 'wpbf_breakpoint_mobile', __( 'Mobile', 'wpbfpremium' ), 'wpbf_breakpoint_mobile_callback', 'wpbf-premium-settings', 'wpbf-responsive-breakpoints-settings' );
	add_settings_field( 'wpbf_theme_company_name', __( 'Company Name', 'wpbfpremium' ), 'wpbf_theme_company_name_callback', 'wpbf-whitelabel-sections', 'wpbf-whitelabel-company-section' );
	add_settings_field( 'wpbf_theme_company_url', __( 'Company URL', 'wpbfpremium' ), 'wpbf_theme_company_url_callback', 'wpbf-whitelabel-sections', 'wpbf-whitelabel-company-section' );
	add_settings_field( 'wpbf_theme_name', __( 'Name', 'wpbfpremium' ), 'wpbf_theme_name_callback', 'wpbf-whitelabel-sections', 'wpbf-whitelabel-theme-section' );
	add_settings_field( 'wpbf_theme_description', __( 'Description', 'wpbfpremium' ), 'wpbf_theme_description_callback', 'wpbf-whitelabel-sections', 'wpbf-whitelabel-theme-section' );
	add_settings_field( 'wpbf_theme_tags', __( 'Tags', 'wpbfpremium' ), 'wpbf_theme_tags_callback', 'wpbf-whitelabel-sections', 'wpbf-whitelabel-theme-section' );
	add_settings_field( 'wpbf_theme_screenshot', __( 'Screenshot', 'wpbfpremium' ), 'wpbf_theme_screenshot_callback', 'wpbf-whitelabel-sections', 'wpbf-whitelabel-theme-section' );
	add_settings_field( 'wpbf_plugin_name', __( 'Name', 'wpbfpremium' ), 'wpbf_plugin_name_callback', 'wpbf-whitelabel-sections', 'wpbf-whitelabel-plugin-section' );
	add_settings_field( 'wpbf_plugin_description', __( 'Description', 'wpbfpremium' ), 'wpbf_plugin_description_callback', 'wpbf-whitelabel-sections', 'wpbf-whitelabel-plugin-section' );

}
add_action( 'admin_init', 'wpbf_premium' );

/**
 * Full width callback.
 */
function wpbf_fullwidth_global_callback() {

	// Vars.
	$post_types = wpbf_get_cpts();
	$settings   = get_option( 'wpbf_settings' );
	?>

	<div class="setting-fields is-gapless">

		<?php
		$number = 0;

		// Loop through post types.
		foreach ( $post_types as $post_type ) {

			$full_width_global = false;

			if ( isset( $settings['wpbf_fullwidth_global'] ) && in_array( $post_type, $settings['wpbf_fullwidth_global'], true ) ) {
				$full_width_global = $post_type;
			}
			?>

			<div class="field setting-field">
				<label for="wpbf_settings_wpbf_fullwidth_global_<?php echo esc_attr( $number ); ?>" class="label checkbox-label">
					<?php echo esc_html( ucfirst( $post_type ) ); ?>
					<input type="checkbox" name="wpbf_settings[wpbf_fullwidth_global][]" id="wpbf_settings_wpbf_fullwidth_global_<?php echo esc_attr( $number ); ?>" value="<?php echo esc_attr( $post_type ); ?>" <?php checked( $full_width_global, $post_type ); ?>>
					<div class="indicator"></div>
				</label>
			</div>

			<?php
			$number++;

		}
		?>

	</div>

	<?php

}

/**
 * Remove title callback.
 */
function wpbf_removetitle_global_callback() {

	// Vars.
	$post_types = wpbf_get_cpts();
	$settings   = get_option( 'wpbf_settings' );
	?>

	<div class="setting-fields is-gapless">

		<?php
		$number = 0;

		// Loop through post types.
		foreach ( $post_types as $post_type ) {

			$remove_title_global = false;

			if ( isset( $settings['wpbf_removetitle_global'] ) && in_array( $post_type, $settings['wpbf_removetitle_global'], true ) ) {
				$remove_title_global = $post_type;
			}
			?>

			<div class="field setting-field">
				<label for="wpbf_settings_wpbf_removetitle_global_<?php echo esc_attr( $number ); ?>" class="label checkbox-label">
					<?php echo esc_html( ucfirst( $post_type ) ); ?>
					<input type="checkbox" name="wpbf_settings[wpbf_removetitle_global][]" id="wpbf_settings_wpbf_removetitle_global_<?php echo esc_attr( $number ); ?>" value="<?php echo esc_attr( $post_type ); ?>" <?php checked( $remove_title_global, $post_type ); ?>>
					<div class="indicator"></div>
				</label>
			</div>

			<?php
			$number++;

		}
		?>

	</div>

	<?php

}

/**
 * Transparent header callback.
 */
function wpbf_transparent_header_global_callback() {

	$post_types = wpbf_get_cpts();
	$settings   = get_option( 'wpbf_settings' );
	?>

	<div class="setting-fields is-gapless">

		<?php
		$number = 0;

		// Loop through post types.
		foreach ( $post_types as $post_type ) {

			$transparent_header_global = false;

			if ( isset( $settings['wpbf_transparent_header_global'] ) && in_array( $post_type, $settings['wpbf_transparent_header_global'], true ) ) {
				$transparent_header_global = $post_type;
			}
			?>

			<div class="field setting-field">
				<label for="wpbf_settings_wpbf_transparent_header_global_<?php echo esc_attr( $number ); ?>" class="label checkbox-label">
					<?php echo esc_html( ucfirst( $post_type ) ); ?>
					<input type="checkbox" name="wpbf_settings[wpbf_transparent_header_global][]" id="wpbf_settings_wpbf_transparent_header_global_<?php echo esc_attr( $number ); ?>" value="<?php echo esc_attr( $post_type ); ?>" <?php checked( $transparent_header_global, $post_type ); ?>>
					<div class="indicator"></div>
				</label>
			</div>

			<?php
			$number++;

		}
		?>

	</div>

	<div class="setting-fields is-gapless wpbf-transparent-header-advanced-wrapper" style="display: none;">

		<?php
		$number = 100;

		// Advanced settings array.
		$advanced_settings = array(
			'404_page'      => __( '404 Page', 'wpbfpremium' ),
			'front_page'    => __( 'Blog Page', 'wpbfpremium' ),
			'search'        => __( 'Search Results', 'wpbfpremium' ),
			'archives'      => __( 'All Archives' ),
			'post_archives' => __( 'Post Archives' ),
		);

		// Merge advanced settings array with wpbf_get_cpts array.
		$advanced_settings = array_merge( $advanced_settings, wpbf_get_cpts( $third_party = true, $as_archives = true ) );

		// Loop through advanced settings array.
		foreach ( $advanced_settings as $advanced_setting => $value ) {

			$transparent_header_global = false;

			if ( isset( $settings['wpbf_transparent_header_global'] ) && in_array( $advanced_setting, $settings['wpbf_transparent_header_global'], true ) ) {
				$transparent_header_global = $advanced_setting;
			}
			?>

			<div class="field setting-field">
				<label for="wpbf_settings_wpbf_transparent_header_global_<?php echo esc_attr( $number ); ?>" class="label checkbox-label">
					<?php echo esc_html( ucfirst( $value ) ); ?>
					<input type="checkbox" name="wpbf_settings[wpbf_transparent_header_global][]" id="wpbf_settings_wpbf_transparent_header_global_<?php echo esc_attr( $number ); ?>" value="<?php echo esc_attr( $advanced_setting ); ?>" <?php checked( $transparent_header_global, $advanced_setting ); ?>>
					<div class="indicator"></div>
				</label>
			</div>

			<?php
			$number++;

		}
		?>

	</div>
	<a href="#" class="wpbf-transparent-header-advanced wpbf-show-advanced">+ Advanced</a>

	<?php

}

/**
 * Blog layouts callback.
 */
function wpbf_blog_layouts_callback() {

	$archives = wpbf_get_cpts( $third_party = true, $as_archives = true );
	$settings = get_option( 'wpbf_settings' );

	// Default archives.
	$default_archives = array(
		'blog'   => __( 'Blog Page', 'wpbfpremium' ),
		'search' => __( 'Search Results', 'wpbfpremium' ),
	);

	// Merge default archives array with wpbf_get_cpts array.
	$archives = array_merge( $default_archives, $archives );
	?>

	<div class="setting-fields is-gapless">

		<?php
		$number = 0;

		// Loop through archives.
		foreach ( $archives as $archive => $value ) {

			$blog_layouts = false;

			if ( isset( $settings['wpbf_blog_layouts'] ) && in_array( $archive, $settings['wpbf_blog_layouts'], true ) ) {
				$blog_layouts = $archive;
			}
			?>

			<div class="field setting-field">
				<label for="wpbf_settings_wpbf_blog_layouts_<?php echo esc_attr( $number ); ?>" class="label checkbox-label">
					<?php echo esc_html( ucfirst( $value ) ); ?>
					<input type="checkbox" name="wpbf_settings[wpbf_blog_layouts][]" id="wpbf_settings_wpbf_blog_layouts_<?php echo esc_attr( $number ); ?>" value="<?php echo esc_attr( $archive ); ?>" <?php checked( $blog_layouts, $archive ); ?>>
					<div class="indicator"></div>
				</label>
			</div>

			<?php
			$number++;

		}
		?>

	</div>

	<div class="setting-fields is-gapless wpbf-blog-layouts-advanced-wrapper" style="display: none;">

		<?php
		$number = 100;

		// Advanced settings array.
		$advanced_default_archives = array(
			'category' => __( 'Categories', 'wpbfpremium' ),
			'tag'      => __( 'Tags', 'wpbfpremium' ),
			'author'   => __( 'Author Archives' ),
			'date'     => __( 'Date Archives', 'wpbfpremium' ),
		);

		// Loop through advanced settings array.
		foreach ( $advanced_default_archives as $advanced_default_archive => $value ) {

			$blog_layouts = false;

			if ( isset( $settings['wpbf_blog_layouts'] ) && in_array( $advanced_default_archive, $settings['wpbf_blog_layouts'] ) ) {
				$blog_layouts = $advanced_default_archive;
			}
			?>

			<div class="field setting-field">
				<label for="wpbf_settings_wpbf_blog_layouts_<?php echo esc_attr( $number ); ?>" class="label checkbox-label">
					<?php echo esc_html( ucfirst( $value ) ); ?>
					<input type="checkbox" name="wpbf_settings[wpbf_blog_layouts][]" id="wpbf_settings_wpbf_blog_layouts_<?php echo esc_attr( $number ); ?>" value="<?php echo esc_attr( $advanced_default_archive ); ?>" <?php checked( $blog_layouts, $advanced_default_archive ); ?>>
					<div class="indicator"></div>
				</label>
			</div>

			<?php
			$number++;

		}
		?>

	</div>
	<a href="#" class="wpbf-blog-layouts-advanced wpbf-show-advanced">+ Advanced</a>

	<?php

}

/**
 * Performance callback.
 */
function wpbf_performance_callback() {

	$settings = get_option( 'wpbf_settings' );

	$removals = array(
		'css_file'              => __( 'Compile inline CSS', 'wpbfpremium' ),
		'remove_feed'           => __( 'Remove Feed Links', 'wpbfpremium' ),
		'remove_rsd'            => __( 'Remove RSD', 'wpbfpremium' ),
		'remove_wlwmanifest'    => __( 'Remove wlwmanifest', 'wpbfpremium' ),
		'remove_generator'      => __( 'Remove Generator', 'wpbfpremium' ),
		'remove_shortlink'      => __( 'Remove Shortlink', 'wpbfpremium' ),
		'disable_emojis'        => __( 'Disable Emojis', 'wpbfpremium' ),
		'disable_embeds'        => __( 'Disable Embeds', 'wpbfpremium' ),
		'remove_jquery_migrate' => __( 'Remove jQuery Migrate', 'wpbfpremium' ),
		'disable_rss_feed'      => __( 'Disable RSS Feed', 'wpbfpremium' ),
	);

	$removal_values = $removals;

	if ( isset( $settings['wpbf_clean_head'] ) ) {

		foreach ( $removals as $key => $value ) {
			$removal_values[ $key ] = in_array( $key, $settings['wpbf_clean_head'], true ) ? 1 : 0;
		}
	}
	?>

	<div class="setting-fields is-gapless">

		<?php
		$number = 0;

		foreach ( $removal_values as $key => $value ) {
			?>

			<div class="field setting-field">
				<label for="wpbf_settings_wpbf_clean_head_<?php echo esc_attr( $number ); ?>" class="label checkbox-label">
					<?php echo esc_html( ucfirst( $removals[ $key ] ) ); ?>
					<input type="checkbox" name="wpbf_settings[wpbf_clean_head][]" id="wpbf_settings_wpbf_clean_head_<?php echo esc_attr( $number ); ?>" value="<?php echo esc_attr( $key ); ?>" class="wpbf-performance-setting" <?php checked( $value, 1 ); ?>>
					<div class="indicator"></div>
				</label>
			</div>

			<?php
			$number++;

		}
		?>

	</div>
	<a href="#" class="wpbf-performance-select-all wpbf-show-advanced">Select All</a>

	<?php

}

/**
 * Mobile breakpoint callback.
 */
function wpbf_breakpoint_mobile_callback() {

	$wpbf_settings     = get_option( 'wpbf_settings' );
	$breakpoint_mobile = ! empty( $wpbf_settings['wpbf_breakpoint_mobile'] ) ? $wpbf_settings['wpbf_breakpoint_mobile'] : false;

	echo '<label><input type="text" name="wpbf_settings[wpbf_breakpoint_mobile]" value="' . esc_attr( $breakpoint_mobile ) . '" placeholder="480px" size="10" /> <span class="description">' . __( 'Default: until 480px for mobiles.', 'wpbfpremium' ) . '</span></label>';

}

/**
 * Medium breakpoint callback.
 */
function wpbf_breakpoint_medium_callback() {

	$wpbf_settings     = get_option( 'wpbf_settings' );
	$breakpoint_medium = ! empty( $wpbf_settings['wpbf_breakpoint_medium'] ) ? $wpbf_settings['wpbf_breakpoint_medium'] : false;

	echo '<label><input type="text" name="wpbf_settings[wpbf_breakpoint_medium]" value="' . esc_attr( $breakpoint_medium ) . '" placeholder="768px" size="10" /> <span class="description">' . __( 'Default: above 768px for tablets.', 'wpbfpremium' ) . '</span></label>';

}

/**
 * Desktop breakpoint callback.
 */
function wpbf_breakpoint_desktop_callback() {

	$wpbf_settings      = get_option( 'wpbf_settings' );
	$breakpoint_desktop = ! empty( $wpbf_settings['wpbf_breakpoint_desktop'] ) ? $wpbf_settings['wpbf_breakpoint_desktop'] : false;

	echo '<label><input type="text" name="wpbf_settings[wpbf_breakpoint_desktop]" value="' . esc_attr( $breakpoint_desktop ) . '" placeholder="1024px" size="10" /> <span class="description">' . __( 'Default: above 1024px for desktops.', 'wpbfpremium' ) . '</span></label>';

}

/**
 * Theme name callback.
 */
function wpbf_theme_name_callback() {

	$wpbf_settings = get_option( 'wpbf_settings' );
	$theme_name    = isset( $wpbf_settings['wpbf_theme_name'] ) ? $wpbf_settings['wpbf_theme_name'] : false;

	echo '<input class="all-options" type="text" name="wpbf_settings[wpbf_theme_name]" value="' . esc_html( $theme_name ) . '" />';

}

/**
 * Theme description callback.
 */
function wpbf_theme_description_callback() {

	$wpbf_settings     = get_option( 'wpbf_settings' );
	$theme_description = isset( $wpbf_settings['wpbf_theme_description'] ) ? $wpbf_settings['wpbf_theme_description'] : false;

	echo '<input class="regular-text" type="text" name="wpbf_settings[wpbf_theme_description]" value="' . esc_html( $theme_description ) . '" />';

}

/**
 * Theme tags callback.
 */
function wpbf_theme_tags_callback() {

	$wpbf_settings = get_option( 'wpbf_settings' );
	$theme_tags    = isset( $wpbf_settings['wpbf_theme_tags'] ) ? $wpbf_settings['wpbf_theme_tags'] : false;

	echo '<input class="regular-text" type="text" name="wpbf_settings[wpbf_theme_tags]" value="' . esc_html( $theme_tags ) . '" />';

}

/**
 * Theme company name callback.
 */
function wpbf_theme_company_name_callback() {

	$wpbf_settings      = get_option( 'wpbf_settings' );
	$theme_company_name = isset( $wpbf_settings['wpbf_theme_company_name'] ) ? $wpbf_settings['wpbf_theme_company_name'] : false;

	echo '<input class="all-options" type="text" name="wpbf_settings[wpbf_theme_company_name]" value="' . esc_html( $theme_company_name ) . '" />';

}

/**
 * Theme company url callback.
 */
function wpbf_theme_company_url_callback() {

	$wpbf_settings     = get_option( 'wpbf_settings' );
	$theme_company_url = isset( $wpbf_settings['wpbf_theme_company_url'] ) ? $wpbf_settings['wpbf_theme_company_url'] : false;

	echo '<input class="all-options" type="text" name="wpbf_settings[wpbf_theme_company_url]" value="' . esc_html( $theme_company_url ) . '" />';

}

/**
 * Theme screenshot callback.
 */
function wpbf_theme_screenshot_callback() {

	$wpbf_settings    = get_option( 'wpbf_settings' );
	$theme_screenshot = isset( $wpbf_settings['wpbf_theme_screenshot'] ) ? $wpbf_settings['wpbf_theme_screenshot'] : false;

	if ( function_exists( 'wp_enqueue_media' ) ) {

		wp_enqueue_media();

	} else {

		wp_enqueue_style( 'thickbox' );
		wp_enqueue_script( 'media-upload' );
		wp_enqueue_script( 'thickbox' );

	}

	?>

	<input id="wpbf-screenshot" class="wpbf-screenshot-url regular-text" type="text" name="wpbf_settings[wpbf_theme_screenshot]" value="<?php echo esc_url( $theme_screenshot ); ?>">
	<a href="#" class="wpbf-screenshot-upload button-secondary"><?php _e( 'Add or Upload File', 'wpbfpremum' ); ?></a>
	<a href="#" class="wpbf-screenshot-remove button-secondary">x</a><br>
	<label for="wpbf-screenshot" class="description"><span class="description"><?php _e( 'Recommended image size: 1200px x 900px', 'wpbfpremium' ); ?></span></label>

	</p>

	<?php

}

/**
 * Plugin name callback.
 */
function wpbf_plugin_name_callback() {

	$wpbf_settings = get_option( 'wpbf_settings' );
	$plugin_name   = isset( $wpbf_settings['wpbf_plugin_name'] ) ? $wpbf_settings['wpbf_plugin_name'] : false;

	echo '<input class="all-options" type="text" name="wpbf_settings[wpbf_plugin_name]" value="' . esc_html( $plugin_name ) . '" />';

}

/**
 * Plugin description callback.
 */
function wpbf_plugin_description_callback() {

	$wpbf_settings      = get_option( 'wpbf_settings' );
	$plugin_description = isset( $wpbf_settings['wpbf_plugin_description'] ) ? $wpbf_settings['wpbf_plugin_description'] : false;

	echo '<input class="regular-text" type="text" name="wpbf_settings[wpbf_plugin_description]" value="' . esc_html( $plugin_description ) . '" />';

}
