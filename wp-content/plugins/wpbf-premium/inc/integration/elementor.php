<?php
/**
 * Elementor integration.
 *
 * @package Page Builder Framework Premium Add-On
 * @subpackage Integration
 */

defined( 'ABSPATH' ) || die( "Can't access directly" );

/**
 * Fix Elementor line-height issue.
 *
 * https://github.com/pojome/elementor/issues/3197
 */
function wpbf_elementor_line_height_fix() {

	$line_height_h1 = get_theme_mod( 'page_h1_line_height' );
	$line_height_h2 = get_theme_mod( 'page_h2_line_height' );
	$line_height_h3 = get_theme_mod( 'page_h3_line_height' );
	$line_height_h4 = get_theme_mod( 'page_h4_line_height' );
	$line_height_h5 = get_theme_mod( 'page_h5_line_height' );
	$line_height_h6 = get_theme_mod( 'page_h6_line_height' );

	if ( $line_height_h1 ) {
		echo '.elementor-widget-heading h1.elementor-heading-title, .elementor-widget-heading h2.elementor-heading-title, .elementor-widget-heading h3.elementor-heading-title, .elementor-widget-heading h4.elementor-heading-title, .elementor-widget-heading h5.elementor-heading-title, .elementor-widget-heading h6.elementor-heading-title {';
		echo sprintf( 'line-height: %s;', esc_attr( $line_height_h1 ) );
		echo '}';
	}

	if ( $line_height_h2 ) {
		echo '.elementor-widget-heading h2.elementor-heading-title {';
		echo sprintf( 'line-height: %s;', esc_attr( $line_height_h2 ) );
		echo '}';
	}

	if ( $line_height_h3 ) {
		echo '.elementor-widget-heading h3.elementor-heading-title {';
		echo sprintf( 'line-height: %s;', esc_attr( $line_height_h3 ) );
		echo '}';
	}

	if ( $line_height_h4 ) {
		echo '.elementor-widget-heading h4.elementor-heading-title {';
		echo sprintf( 'line-height: %s;', esc_attr( $line_height_h4 ) );
		echo '}';
	}

	if ( $line_height_h5 ) {
		echo '.elementor-widget-heading h5.elementor-heading-title {';
		echo sprintf( 'line-height: %s;', esc_attr( $line_height_h5 ) );
		echo '}';
	}

	if ( $line_height_h6 ) {
		echo '.elementor-widget-heading h6.elementor-heading-title {';
		echo sprintf( 'line-height: %s;', esc_attr( $line_height_h6 ) );
		echo '}';
	}

}
add_action( 'wpbf_before_customizer_css', 'wpbf_elementor_line_height_fix', 20 );

/**
 * Auto add custom sections to Elementor cpt support.
 *
 * Caused issues with Beaver Builder, let's revert this just in case.
 */
function wpbf_elementor_cpt_support() {
	$post_types = get_option( 'elementor_cpt_support', array() );

	if ( ! in_array( 'wpbf_hooks', $post_types, true ) ) {
		array_push( $post_types, 'wpbf_hooks' );
		update_option( 'elementor_cpt_support', $post_types, true );
	}
}
// add_action( 'admin_init', 'wpbf_elementor_cpt_support' );
