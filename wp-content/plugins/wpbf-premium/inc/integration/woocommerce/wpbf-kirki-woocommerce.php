<?php
/**
 * Kirki WooCommerce.
 *
 * @package Page Builder Framework Premium Add-On
 * @subpackage Integration
 */

defined( 'ABSPATH' ) || die( "Can't access directly" );

/**
 * Extend the WordPress customizer.
 */
function wpbf_kirki_premium_woocommerce() {

	if ( ! class_exists( 'Kirki' ) ) {
		return;
	}

	/* Fields – menu item */

	// Menu item icon.
	Kirki::add_field(
		'wpbf',
		array(
			'type'     => 'select',
			'settings' => 'woocommerce_menu_item_icon',
			'label'    => __( 'Icon', 'wpbfpremium' ),
			'section'  => 'wpbf_woocommerce_menu_item_options',
			'default'  => 'cart',
			'priority' => 0,
			'multiple' => 1,
			'choices'  => array(
				'cart'   => __( 'Cart', 'wpbfpremium' ),
				'basket' => __( 'Basket', 'wpbfpremium' ),
				'bag'    => __( 'Bag', 'wpbfpremium' ),
			),
		)
	);

	// Separator.
	Kirki::add_field(
		'wpbf',
		array(
			'type'     => 'custom',
			'settings' => 'separator-56462',
			'section'  => 'wpbf_woocommerce_menu_item_options',
			'default'  => '<hr style="border-top: 1px solid #ccc; border-bottom: 1px solid #f8f8f8">',
			'priority' => 0,
		)
	);

	// Menu item text.
	Kirki::add_field(
		'wpbf',
		array(
			'type'     => 'select',
			'settings' => 'woocommerce_menu_item_label',
			'label'    => __( '"Cart" Text', 'wpbfpremium' ),
			'section'  => 'wpbf_woocommerce_menu_item_options',
			'default'  => 'show',
			'priority' => 20,
			'multiple' => 1,
			'choices'  => array(
				'show' => __( 'Show', 'wpbfpremium' ),
				'hide' => __( 'Hide', 'wpbfpremium' ),
			),
		)
	);

	// Menu item amount.
	Kirki::add_field(
		'wpbf',
		array(
			'type'     => 'select',
			'settings' => 'woocommerce_menu_item_amount',
			'label'    => __( 'Amount', 'wpbfpremium' ),
			'section'  => 'wpbf_woocommerce_menu_item_options',
			'default'  => 'show',
			'priority' => 21,
			'multiple' => 1,
			'choices'  => array(
				'show' => __( 'Show', 'wpbfpremium' ),
				'hide' => __( 'Hide', 'wpbfpremium' ),
			),
		)
	);

	Kirki::add_field(
		'wpbf',
		array(
			'type'     => 'custom',
			'settings' => 'separator-11214',
			'section'  => 'wpbf_woocommerce_menu_item_options',
			'default'  => '<hr style="border-top: 1px solid #ccc; border-bottom: 1px solid #f8f8f8">',
			'priority' => 22,
		)
	);

	// Menu item dropdown.
	Kirki::add_field(
		'wpbf',
		array(
			'type'     => 'select',
			'settings' => 'woocommerce_menu_item_dropdown',
			'label'    => __( 'Cart Dropdown', 'wpbfpremium' ),
			'section'  => 'wpbf_woocommerce_menu_item_options',
			'default'  => 'show',
			'priority' => 23,
			'multiple' => 1,
			'choices'  => array(
				'show' => __( 'Enable', 'wpbfpremium' ),
				'hide' => __( 'Disable', 'wpbfpremium' ),
			),
		)
	);

	// Display on add to cart.
	Kirki::add_field(
		'wpbf',
		array(
			'type'            => 'toggle',
			'settings'        => 'woocommerce_menu_item_dropdown_popup',
			'label'           => __( 'Cart Popup', 'wpbfpremium' ),
			'description'     => __( 'Displays the cart dropdown for a short period of time after a product was added to the cart', 'wpbfpremium' ),
			'section'         => 'wpbf_woocommerce_menu_item_options',
			'default'         => false,
			'priority'        => 23,
			'multiple'        => 1,
			'active_callback' => array(
				array(
					'setting'  => 'woocommerce_menu_item_dropdown',
					'operator' => '!=',
					'value'    => 'hide',
				),
				array(
					'setting'  => 'menu_sticky',
					'operator' => '==',
					'value'    => true,
				),
			),
		)
	);

	// Cart button.
	Kirki::add_field(
		'wpbf',
		array(
			'type'     => 'select',
			'settings' => 'woocommerce_menu_item_dropdown_cart_button',
			'label'    => __( 'Cart Button', 'wpbfpremium' ),
			'section'  => 'wpbf_woocommerce_menu_item_options',
			'default'  => 'show',
			'priority' => 24,
			'multiple' => 1,
			'choices'  => array(
				'show' => __( 'Show', 'wpbfpremium' ),
				'hide' => __( 'Hide', 'wpbfpremium' ),
			),
			'active_callback' => array(
				array(
					'setting'  => 'woocommerce_menu_item_dropdown',
					'operator' => '!=',
					'value'    => 'hide',
				),
			),
		)
	);

	// Checkout button.
	Kirki::add_field(
		'wpbf',
		array(
			'type'     => 'select',
			'settings' => 'woocommerce_menu_item_dropdown_checkout_button',
			'label'    => __( 'Checkout Button', 'wpbfpremium' ),
			'section'  => 'wpbf_woocommerce_menu_item_options',
			'default'  => 'show',
			'priority' => 25,
			'multiple' => 1,
			'choices'  => array(
				'show' => __( 'Show', 'wpbfpremium' ),
				'hide' => __( 'Hide', 'wpbfpremium' ),
			),
			'active_callback' => array(
				array(
					'setting'  => 'woocommerce_menu_item_dropdown',
					'operator' => '!=',
					'value'    => 'hide',
				),
			),
		)
	);

	/* Fields – shop & archive pages (loop) */

	// Image flip.
	Kirki::add_field(
		'wpbf',
		array(
			'type'        => 'select',
			'settings'    => 'woocommerce_loop_image_flip',
			'label'       => __( 'Image Flip', 'wpbfpremium' ),
			'description' => __( 'Displays the first image of your product gallery (if available) when hovering over the product thumbnail.', 'wpbfpremium' ),
			'section'     => 'woocommerce_product_catalog',
			'default'     => 'enabled',
			'priority'    => 25,
			'multiple'    => 1,
			'choices'     => array(
				'enabled'  => __( 'Enabled', 'wpbfpremium' ),
				'disabled' => __( 'Disabled', 'wpbfpremium' ),
			),
		)
	);

	// Infinite scroll.
	Kirki::add_field(
		'wpbf',
		array(
			'type'     => 'select',
			'settings' => 'woocommerce_loop_infinite_scroll',
			'label'    => __( 'Infinite Scroll', 'wpbfpremium' ),
			'section'  => 'woocommerce_product_catalog',
			'default'  => 'disabled',
			'priority' => 25,
			'multiple' => 1,
			'choices'  => array(
				'disabled' => __( 'Disabled', 'wpbfpremium' ),
				'enabled'  => __( 'Enabled', 'wpbfpremium' ),
				// 'button'   => __( 'Load More Button', 'wpbfpremium' ),
			),
		)
	);

	// Separator.
	Kirki::add_field(
		'wpbf',
		array(
			'type'     => 'custom',
			'settings' => 'separator-48556239',
			'section'  => 'woocommerce_product_catalog',
			'default'  => '<hr style="border-top: 1px solid #ccc; border-bottom: 1px solid #f8f8f8">',
			'priority' => 25,
		)
	);

	// Separator.
	Kirki::add_field(
		'wpbf',
		array(
			'type'     => 'custom',
			'settings' => 'separator-83905',
			'section'  => 'woocommerce_product_catalog',
			'default'  => '<hr style="border-top: 1px solid #ccc; border-bottom: 1px solid #f8f8f8">',
			'priority' => 30,
		)
	);

	// Quick view.
	Kirki::add_field(
		'wpbf',
		array(
			'type'     => 'select',
			'settings' => 'woocommerce_loop_quick_view',
			'label'    => __( 'Quick View', 'wpbfpremium' ),
			'section'  => 'woocommerce_product_catalog',
			'default'  => 'enabled',
			'priority' => 30,
			'multiple' => 1,
			'choices'  => array(
				'enabled'  => __( 'Enabled', 'wpbfpremium' ),
				'disabled' => __( 'Disabled', 'wpbfpremium' ),
			),
		)
	);

	// Quick view font size.
	Kirki::add_field(
		'wpbf',
		array(
			'type'      => 'dimension',
			'label'     => __( 'Font Size', 'wpbfpremium' ),
			'settings'  => 'woocommerce_loop_quick_view_font_size',
			'section'   => 'woocommerce_product_catalog',
			'transport' => 'postMessage',
			'priority'  => 30,
			'default'   => '14px',
			'active_callback' => array(
				array(
					'setting'  => 'woocommerce_loop_quick_view',
					'operator' => '!=',
					'value'    => 'disabled',
				),
			),
		)
	);

	// Quick view font color.
	Kirki::add_field(
		'wpbf',
		array(
			'type'      => 'color',
			'settings'  => 'woocommerce_loop_quick_view_font_color',
			'label'     => __( 'Font Color', 'wpbfpremium' ),
			'section'   => 'woocommerce_product_catalog',
			'transport' => 'postMessage',
			'default'   => '#ffffff',
			'priority'  => 30,
			'choices'   => array(
				'alpha' => true,
			),
			'active_callback' => array(
				array(
					'setting'  => 'woocommerce_loop_quick_view',
					'operator' => '!=',
					'value'    => 'disabled',
				),
			),
		)
	);

	// Quick view background color.
	Kirki::add_field(
		'wpbf',
		array(
			'type'      => 'color',
			'settings'  => 'woocommerce_loop_quick_view_background_color',
			'label'     => __( 'Background Color', 'wpbfpremium' ),
			'section'   => 'woocommerce_product_catalog',
			'transport' => 'postMessage',
			'default'   => 'rgba(0,0,0,.7)',
			'priority'  => 30,
			'choices'   => array(
				'alpha' => true,
			),
			'active_callback' => array(
				array(
					'setting'  => 'woocommerce_loop_quick_view',
					'operator' => '!=',
					'value'    => 'disabled',
				),
			),
		)
	);

	// Quick view overlay color.
	Kirki::add_field(
		'wpbf',
		array(
			'type'     => 'color',
			'settings' => 'woocommerce_loop_quick_view_overlay_color',
			'label'    => __( 'Overlay Background Color', 'wpbfpremium' ),
			'section'  => 'woocommerce_product_catalog',
			'default'  => 'rgba(0,0,0,.8)',
			'priority' => 30,
			'choices'  => array(
				'alpha' => true,
			),
			'active_callback' => array(
				array(
					'setting'  => 'woocommerce_loop_quick_view',
					'operator' => '!=',
					'value'    => 'disabled',
				),
			),
		)
	);

	// Separator.
	Kirki::add_field(
		'wpbf',
		array(
			'type'     => 'custom',
			'settings' => 'separator-10304214',
			'section'  => 'woocommerce_product_catalog',
			'default'  => '<hr style="border-top: 1px solid #ccc; border-bottom: 1px solid #f8f8f8">',
			'priority' => 30,
		)
	);

	// Off canvas sidebar.
	Kirki::add_field(
		'wpbf',
		array(
			'type'     => 'select',
			'settings' => 'woocommerce_loop_off_canvas_sidebar',
			'label'    => __( 'Off Canvas Sidebar', 'wpbfpremium' ),
			'section'  => 'woocommerce_product_catalog',
			'default'  => 'disabled',
			'priority' => 30,
			'multiple' => 1,
			'choices'  => array(
				'enabled'  => __( 'Enabled', 'wpbfpremium' ),
				'disabled' => __( 'Disabled', 'wpbfpremium' ),
			),
		)
	);

	// Off sidebar icon.
	Kirki::add_field(
		'wpbf',
		array(
			'type'     => 'select',
			'settings' => 'woocommerce_loop_off_canvas_sidebar_icon',
			'label'    => __( 'Icon', 'wpbfpremium' ),
			'section'  => 'woocommerce_product_catalog',
			'default'  => 'search',
			'priority' => 30,
			'multiple' => 1,
			'choices'  => array(
				'search'    => __( 'Search', 'wpbfpremium' ),
				'hamburger' => __( 'Hamburger', 'wpbfpremium' ),
			),
			'active_callback' => array(
				array(
					'setting'  => 'woocommerce_loop_off_canvas_sidebar',
					'operator' => '!=',
					'value'    => 'disabled',
				),
			),
		)
	);

	// Off canvas sidebar label.
	Kirki::add_field(
		'wpbf',
		array(
			'type'     => 'text',
			'settings' => 'woocommerce_loop_off_canvas_sidebar_label',
			'label'    => __( 'Label', 'wpbfpremium' ),
			'section'  => 'woocommerce_product_catalog',
			'default'  => 'Filter',
			'priority' => 30,
			'active_callback' => array(
				array(
					'setting'  => 'woocommerce_loop_off_canvas_sidebar',
					'operator' => '!=',
					'value'    => 'disabled',
				),
			),
		)
	);

	// Off canvas sidebar font color.
	Kirki::add_field(
		'wpbf',
		array(
			'type'      => 'color',
			'settings'  => 'woocommerce_loop_off_canvas_sidebar_font_color',
			'label'     => __( 'Font Color', 'wpbfpremium' ),
			'section'   => 'woocommerce_product_catalog',
			'transport' => 'postMessage',
			'default'   => '#ffffff',
			'priority'  => 30,
			'choices'   => array(
				'alpha' => true,
			),
			'active_callback' => array(
				array(
					'setting'  => 'woocommerce_loop_off_canvas_sidebar',
					'operator' => '!=',
					'value'    => 'disabled',
				),
			),
		)
	);

	// Off canvas sidebar background color.
	Kirki::add_field(
		'wpbf',
		array(
			'type'      => 'color',
			'settings'  => 'woocommerce_loop_off_canvas_sidebar_background_color',
			'label'     => __( 'Background Color', 'wpbfpremium' ),
			'section'   => 'woocommerce_product_catalog',
			'transport' => 'postMessage',
			'default'   => '',
			'priority'  => 30,
			'choices'   => array(
				'alpha' => true,
			),
			'active_callback' => array(
				array(
					'setting'  => 'woocommerce_loop_off_canvas_sidebar',
					'operator' => '!=',
					'value'    => 'disabled',
				),
			),
		)
	);

	// Off canvas sidebar overlay color.
	Kirki::add_field(
		'wpbf',
		array(
			'type'      => 'color',
			'settings'  => 'woocommerce_loop_off_canvas_sidebar_overlay_color',
			'label'     => __( 'Overlay Background Color', 'wpbfpremium' ),
			'section'   => 'woocommerce_product_catalog',
			'default'   => 'rgba(0,0,0,.2)',
			'priority'  => 30,
			'transport' => 'postMessage',
			'choices'   => array(
				'alpha' => true,
			),
			'active_callback' => array(
				array(
					'setting'  => 'woocommerce_loop_off_canvas_sidebar',
					'operator' => '!=',
					'value'    => 'disabled',
				),
			),
		)
	);

	/* Fields – checkout page */

	// Distraction free.
	Kirki::add_field(
		'wpbf',
		array(
			'type'     => 'toggle',
			'settings' => 'woocommerce_distraction_free_checkout',
			'label'    => __( 'Distraction Free Checkout', 'wpbfpremium' ),
			'section'  => 'woocommerce_checkout',
			'default'  => 0,
			'priority' => 1,
			'multiple' => 1,
		)
	);

}
add_action( 'after_setup_theme', 'wpbf_kirki_premium_woocommerce' );
