<?php
/**
 * Custom controls.
 *
 * @package Page Builder Framework Premium Add-On
 * @subpackage Customizer
 */

defined( 'ABSPATH' ) || die( "Can't access directly" );

/**
 * WYSIWYG control.
 */
add_action( 'customize_register', function ( $wp_customize ) {

	if ( class_exists( 'Kirki_Control_Base' ) ) {

		class Kirki_WPBF_WYSIWYG_Control extends Kirki_Control_Base {

			public $type = 'wysiwyg';
			public function render_content() {
				?>
				<label>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<?php
				$settings = array(
					'media_buttons' => false,
					'editor_height' => '150',
					'teeny'         => true,
				);
				$this->filter_editor_setting_link();
				wp_editor( $this->value(), $this->id, $settings );
				?>
				</label>
				<?php
				do_action( 'admin_footer' );
				do_action( 'admin_print_footer_scripts' );
			}

			private function filter_editor_setting_link() {
				add_filter( 'the_editor', function ( $output ) { return preg_replace( '/<textarea/', '<textarea ' . $this->get_link(), $output, 1 ); } );
			}

		}

	}

	/**
	 * Register WYSIWYG control with Kirki.
	 *
	 * @param array $controls The Kirki controls.
	 *
	 * @return array The updated Kirki controls.
	 */
	add_filter( 'kirki/control_types', function ( $controls ) {
		$controls['wysiwyg'] = 'Kirki_WPBF_WYSIWYG_Control';
		return $controls;
	} );

} );
