( function( $ ) {

	/* Social */

	// Background color.
	wp.customize( 'social_background_color', function( value ) {
		value.bind( function( newval ) {
			$('.wpbf-social-icons a').css('background', newval );
		} );
	} );

	// Icon color.
	wp.customize( 'social_color', function( value ) {
		value.bind( function( newval ) {
			$('.wpbf-social-icons a').css('color', newval );
		} );
	} );

	// Font size.
	wp.customize( 'social_font_size', function( value ) {
		value.bind( function( newval ) {
			$('.wpbf-social-icon').css('font-size', newval + 'px' );
		} );
	} );

	/* Text */

	// Line height.
	wp.customize( 'page_line_height', function( value ) {
		value.bind( function( newval ) {
			$('#content').css('line-height', newval );
		} );
	} );

	// Bold color.
	wp.customize( 'page_bold_color', function( value ) {
		value.bind( function( newval ) {
			$('b, strong').css('color', newval);
		} );
	} );

	/* Menu */

	// Letter spacing.
	wp.customize( 'menu_letter_spacing', function( value ) {
		value.bind( function( newval ) {
			$('.wpbf-menu').css('letter-spacing', newval + 'px' );
		} );
	} );

	/* H1 */

	// Line height.
	wp.customize( 'page_h1_line_height', function( value ) {
		value.bind( function( newval ) {
			$('h1, h2, h3, h4, h5, h6').css('cssText', 'line-height: ' + newval + ' !important;' );
		} );
	} );

	// Letter spacing.
	wp.customize( 'page_h1_letter_spacing', function( value ) {
		value.bind( function( newval ) {
			$('h1, h2, h3, h4, h5, h6').css('letter-spacing', newval + 'px' );
		} );
	} );

	/* H2 */

	// Line height.
	wp.customize( 'page_h2_line_height', function( value ) {
		value.bind( function( newval ) {
			$('h2').css('cssText', 'line-height: ' + newval + ' !important;' );
		} );
	} );

	// Letter spacing.
	wp.customize( 'page_h2_letter_spacing', function( value ) {
		value.bind( function( newval ) {
			$('h2').css('letter-spacing', newval + 'px' );
		} );
	} );

	/* H3 */

	// Line height.
	wp.customize( 'page_h3_line_height', function( value ) {
		value.bind( function( newval ) {
			$('h3').css('cssText', 'line-height: ' + newval + ' !important;' );
		} );
	} );

	// Letter spacing.
	wp.customize( 'page_h3_letter_spacing', function( value ) {
		value.bind( function( newval ) {
			$('h3').css('letter-spacing', newval + 'px' );
		} );
	} );

	/* H4 */

	// Line height.
	wp.customize( 'page_h4_line_height', function( value ) {
		value.bind( function( newval ) {
			$('h4').css('cssText', 'line-height: ' + newval + ' !important;' );
		} );
	} );

	// Letter spacing.
	wp.customize( 'page_h4_letter_spacing', function( value ) {
		value.bind( function( newval ) {
			$('h4').css('letter-spacing', newval + 'px' );
		} );
	} );

	/* H5 */

	// Line height.
	wp.customize( 'page_h5_line_height', function( value ) {
		value.bind( function( newval ) {
			$('h5').css('cssText', 'line-height: ' + newval + ' !important;' );
		} );
	} );

	// Letter spacing.
	wp.customize( 'page_h5_letter_spacing', function( value ) {
		value.bind( function( newval ) {
			$('h5').css('letter-spacing', newval + 'px' );
		} );
	} );

	/* H6 */

	// Line height.
	wp.customize( 'page_h6_line_height', function( value ) {
		value.bind( function( newval ) {
			$('h6').css('cssText', 'line-height: ' + newval + ' !important;' );
		} );
	} );

	// Letter spacing.
	wp.customize( 'page_h6_letter_spacing', function( value ) {
		value.bind( function( newval ) {
			$('h6').css('letter-spacing', newval + 'px' );
		} );
	} );

	/* Navigation */

	// Stacked advanced background color.
	wp.customize( 'menu_stacked_bg_color', function( value ) {
		value.bind( function( newval ) {
			$('.wpbf-menu-stacked-advanced-wrapper').css('background-color', newval );
		} );
	} );

	// Stacked advanced content.
	wp.customize( 'menu_stacked_wysiwyg', function( value ) {
		value.bind( function( newval ) {
			$('.wpbf-menu-stacked-advanced-wrapper .wpbf-3-4').html( newval );
		} );
	} );

	// Transparent header.
	wp.customize( 'menu_transparent_background_color', function( value ) {
		value.bind( function( newval ) {
			$('.wpbf-transparent-header .wpbf-navigation, .wpbf-transparent-header .wpbf-mobile-nav-wrapper').css('background-color', newval );
		} );
	} );

	// Off canvas hamburger icon color.
	wp.customize( 'menu_off_canvas_hamburger_color', function( value ) {
		value.bind( function( newval ) {
			$('.wpbf-nav-item, .wpbf-nav-item a').css('color', newval );
		} );
	} );

	// Off canvas sub menu arrow color.
	wp.customize( 'menu_off_canvas_submenu_arrow_color', function( value ) {
		value.bind( function( newval ) {
			$('.wpbf-menu-off-canvas .wpbf-submenu-toggle').css('color', newval );
		} );
	} );

	// Off canvas overlay color.
	wp.customize( 'menu_overlay_color', function( value ) {
		value.bind( function( newval ) {
			$('.wpbf-menu-overlay').css('background', newval );
		} );
	} );

	/* Mobile Navigation */

	// Mobile overlay color.
	wp.customize( 'mobile_menu_overlay_color', function( value ) {
		value.bind( function( newval ) {
			$('.wpbf-mobile-menu-overlay').css('background', newval );
		} );
	} );

	/* Call to Action */
	wp.customize( 'cta_button_border_radius', function( value ) {
		value.bind( function( newval ) {
			$('.wpbf-cta-menu-item a').css('border-radius', newval + 'px' );
		} );
	} );

	/* WooCommerce */

	/* Loop */

	// Quick view font size.
	wp.customize( 'woocommerce_loop_quick_view_font_size', function( value ) {
		value.bind( function( newval ) {
			$('.wpbf-woo-quick-view').css('font-size', newval );
		} );
	} );

	// Quick view font color.
	wp.customize( 'woocommerce_loop_quick_view_font_color', function( value ) {
		value.bind( function( newval ) {
			$('.wpbf-woo-quick-view').css('color', newval );
		} );
	} );

	// Quick view background color.
	wp.customize( 'woocommerce_loop_quick_view_background_color', function( value ) {
		value.bind( function( newval ) {
			$('.wpbf-woo-quick-view').css('background-color', newval );
		} );
	} );

	// Off canvas sidebar font color.
	wp.customize( 'woocommerce_loop_off_canvas_sidebar_font_color', function( value ) {
		value.bind( function( newval ) {
			$('.wpbf-woo-off-canvas-sidebar-button').css('color', newval );
		} );
	} );

	// Off canvas sidebar background color.
	wp.customize( 'woocommerce_loop_off_canvas_sidebar_background_color', function( value ) {
		value.bind( function( newval ) {
			$('.wpbf-woo-off-canvas-sidebar-button').css('background-color', newval );
		} );
	} );

	// Off canvas sidebar overlay color.
	wp.customize( 'woocommerce_loop_off_canvas_sidebar_overlay_color', function( value ) {
		value.bind( function( newval ) {
			$('.wpbf-woo-off-canvas-sidebar-overlay').css('background-color', newval );
		} );
	} );

} )( jQuery );