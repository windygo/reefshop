<?php
/**
 * The style "default" of the separate item of the shortcode 'Tours'
 *
 * @package WordPress
 * @subpackage BaseKit Addons
 * @since v1.0.0
 */

$args = get_query_var('trx_addons_args_sc_tours');

$link = get_permalink();

if (!empty($args['slider'])) {
	?><div class="slider-slide swiper-slide"><?php
} else if ($args['columns'] > 1) {
	?><div class="<?php echo esc_attr(trx_addons_get_column_class(1, $args['columns'])); ?>"><?php
}
?>
<div class="sc_tours_item<?php
	if (isset($args['hide_excerpt']) && $args['hide_excerpt']) echo ' without_content';
?>">
    <div class="sc_tour_wrapper">
	<?php
	// Featured image or icon
	if ( has_post_thumbnail()) {
		trx_addons_get_template_part('templates/tpl.featured.php',
									'trx_addons_args_featured',
									apply_filters('trx_addons_filter_args_featured', array(
																'class' => 'sc_tours_item_thumb',
																'hover' => 'zoomin',
																'thumb_size' => apply_filters('trx_addons_filter_thumb_size', trx_addons_get_thumb_size($args['columns'] > 2 ? 'medium' : 'tours'), 'tours-default')
																),
																'tours-default'
													)
									);
	}
	?>
        <div class="sc_tours_item_subtitle"><?php trx_addons_show_layout(trx_addons_get_post_terms(' ', get_the_ID(), BASEKIT_ADDONS_CPT_TOURS_TAXONOMY));?></div>
    </div>
	<div class="sc_tours_item_info">
		<div class="sc_tours_item_header">
			<h4 class="sc_tours_item_title"><a href="<?php echo esc_url($link); ?>"><?php the_title(); ?></a></h4>
		</div>
		<?php if (!isset($args['hide_excerpt']) || $args['hide_excerpt']==0) { ?>
			<div class="sc_tours_item_content"><?php the_excerpt(); ?></div>
		<?php } ?>
	</div>
</div>
<?php
if (!empty($args['slider']) || $args['columns'] > 1) {
	?></div><?php
}
?>