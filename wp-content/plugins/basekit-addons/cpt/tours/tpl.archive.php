<?php
/**
 * The template to display the Tours archive
 *
 * @package WordPress
 * @subpackage BaseKit Addons
 * @since v1.0.0
 */

get_header(); 

do_action('trx_addons_action_start_archive');

if (have_posts()) {

	?><div class="sc_tours sc_tours_default">
		
		<div class="sc_tours_columns_wrap <?php echo esc_attr(trx_addons_get_columns_wrap_class()); ?> columns_padding_bottom"><?php

			$sc_tours_style   = explode('_', trx_addons_get_option('tours_style'));
			$sc_tours_type    = $sc_tours_style[0];
			$sc_tours_columns = empty($sc_tours_style[1]) ? 1 : max(1, $sc_tours_style[1]);

			while ( have_posts() ) { the_post(); 
				basekit_addons_get_template_part(array(
												BASEKIT_ADDONS_PLUGIN_CPT . 'tours/tpl.'.trim($sc_tours_type).'-item.php',
												BASEKIT_ADDONS_PLUGIN_CPT . 'tours/tpl.default-item.php'
												),
												'trx_addons_args_sc_tours',
												array(
													'type' => $sc_tours_type,
													'columns' => $sc_tours_columns,
													'slider' => false
												)
											);
			}

		?></div><!-- .sc_tours_columns_wrap --><?php

    ?></div><!-- .sc_tours --><?php

	the_posts_pagination( array(
		'mid_size'  => 2,
		'prev_text' => esc_html__( '<', 'basekit-addons' ),
		'next_text' => esc_html__( '>', 'basekit-addons' ),
		'before_page_number' => '<span class="meta-nav screen-reader-text">' . esc_html__( 'Page', 'basekir_addons' ) . ' </span>',
	) );

} else {

	trx_addons_get_template_part('templates/tpl.posts-none.php');

}

do_action('trx_addons_action_end_archive');

get_footer();
?>