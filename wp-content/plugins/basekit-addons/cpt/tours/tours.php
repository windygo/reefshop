<?php
/**
 * BaseKit Addons Custom post type: Tours
 *
 * @package WordPress
 * @subpackage BaseKit Addons
 * @since v1.0.0
 */

// Don't load directly
if ( ! defined( 'ABSPATH' ) ) die( '-1' );


// -----------------------------------------------------------------
// -- Custom post type registration
// -----------------------------------------------------------------

// Define Custom post type and taxonomy constants
if ( ! defined('BASEKIT_ADDONS_CPT_TOURS_PT') )		define('BASEKIT_ADDONS_CPT_TOURS_PT', 'cpt_tours');
if ( ! defined('BASEKIT_ADDONS_CPT_TOURS_TAXONOMY') )	define('BASEKIT_ADDONS_CPT_TOURS_TAXONOMY', 'cpt_tours_taxonomy');

// Register post type and taxonomy
if (!function_exists('basekit_addons_cpt_tours_init')) {
	add_action( 'init', 'basekit_addons_cpt_tours_init' );
	function basekit_addons_cpt_tours_init() {

		register_post_type( BASEKIT_ADDONS_CPT_TOURS_PT, array(
			'label'               => esc_html__( 'Tours & Activities', 'basekit-addons' ),
			'description'         => esc_html__( 'Tours & Activities', 'basekit-addons' ),
			'labels'              => array(
				'name'                => esc_html__( 'Tours & Activities', 'basekit-addons' ),
				'singular_name'       => esc_html__( 'Tours & Activities', 'basekit-addons' ),
				'menu_name'           => esc_html__( 'Tours & Activities', 'basekit-addons' ),
				'parent_item_colon'   => esc_html__( 'Parent Item:', 'basekit-addons' ),
				'all_items'           => esc_html__( 'All Tours & Activities items', 'basekit-addons' ),
				'view_item'           => esc_html__( 'View Tours & Activities item', 'basekit-addons' ),
				'add_new_item'        => esc_html__( 'Add New Tours & Activities item', 'basekit-addons' ),
				'add_new'             => esc_html__( 'Add New', 'basekit-addons' ),
				'edit_item'           => esc_html__( 'Edit Tours & Activities item', 'basekit-addons' ),
				'update_item'         => esc_html__( 'Update Tours & Activities item', 'basekit-addons' ),
				'search_items'        => esc_html__( 'Search Tours & Activities items', 'basekit-addons' ),
				'not_found'           => esc_html__( 'Not found', 'basekit-addons' ),
				'not_found_in_trash'  => esc_html__( 'Not found in Trash', 'basekit-addons' ),
			),
			'taxonomies'          => array(BASEKIT_ADDONS_CPT_TOURS_TAXONOMY),
			'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments'),
			'public'              => true,
			'hierarchical'        => false,
			'has_archive'         => true,
			'can_export'          => true,
			'show_in_admin_bar'   => true,
			'show_in_menu'        => true,
			'menu_position'       => '55.1',
			'menu_icon'			  => 'dashicons-images-alt',
			'capability_type'     => 'post',
			'rewrite'             => array( 'slug' => 'tours' )
			)
		);

		register_taxonomy( BASEKIT_ADDONS_CPT_TOURS_TAXONOMY, BASEKIT_ADDONS_CPT_TOURS_PT, array(
			'post_type' 		=> BASEKIT_ADDONS_CPT_TOURS_PT,
			'hierarchical'      => true,
			'labels'            => array(
				'name'              => esc_html__( 'Tours & Activities Taxonomy', 'basekit-addons' ),
				'singular_name'     => esc_html__( 'Tours & Activities Taxonomy', 'basekit-addons' ),
				'search_items'      => esc_html__( 'Search Tours & Activities Taxonomy', 'basekit-addons' ),
				'all_items'         => esc_html__( 'All Tours & Activities Taxonomy', 'basekit-addons' ),
				'parent_item'       => esc_html__( 'Parent Tours & Activities Taxonomy', 'basekit-addons' ),
				'parent_item_colon' => esc_html__( 'Parent Tours & Activities Taxonomy:', 'basekit-addons' ),
				'edit_item'         => esc_html__( 'Edit Tours & Activities Taxonomy', 'basekit-addons' ),
				'update_item'       => esc_html__( 'Update Tours & Activities Taxonomy', 'basekit-addons' ),
				'add_new_item'      => esc_html__( 'Add New Tours & Activities Taxonomy', 'basekit-addons' ),
				'new_item_name'     => esc_html__( 'New Tours & Activities Taxonomy Name', 'basekit-addons' ),
				'menu_name'         => esc_html__( 'Tours & Activities Taxonomy', 'basekit-addons' ),
			),
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'cpt_tours' )
			)
		);
	}
}

// Add 'Dishes' parameters in the ThemeREX Addons Options
if (!function_exists('basekit_addons_cpt_tours_options')) {
	add_filter( 'trx_addons_filter_options', 'basekit_addons_cpt_tours_options');
	function basekit_addons_cpt_tours_options($options) {
		trx_addons_array_insert_after($options, 'cpt_section', basekit_addons_cpt_tours_get_list_options());
		return $options;
	}
}

// Return parameters list for plugin's options
if (!function_exists('basekit_addons_cpt_tours_get_list_options')) {
	function basekit_addons_cpt_tours_get_list_options($add_parameters=array()) {
		return apply_filters('trx_addons_cpt_list_options', array(
			'tours_info' => array(
				"title" => esc_html__('Tours & Activities', 'basekit-addons'),
				"desc" => wp_kses_data( __('Settings of the Tours & Activities archive', 'basekit-addons') ),
				"type" => "info"
			),
			'tours_style' => array(
				"title" => esc_html__('Style', 'basekit-addons'),
				"desc" => wp_kses_data( __('Style of the Tours & Activities archive', 'basekit-addons') ),
				"std" => 'default_2',
				"options" => apply_filters('trx_addons_filter_cpt_archive_styles',
											array(
												'default' => esc_html__('Default', 'basekit-addons'),
                                                'default_2' => esc_html__('Default 2', 'basekit-addons')
											),
											BASEKIT_ADDONS_CPT_TOURS_PT),
				"type" => "select"
			)
		), 'tours');
	}
}

	
// Enqueue CPT-spacific styles and scripts
if ( !function_exists( 'basekit_addons_cpt_tours_frontend_scripts' ) ) {
	add_action( 'wp_enqueue_scripts', 'basekit_addons_cpt_tours_frontend_scripts' );
	function basekit_addons_cpt_tours_frontend_scripts() {
		if (basekit_addons_get_file_dir(BASEKIT_ADDONS_PLUGIN_CPT . 'tours/tours.css')!='') {
			wp_enqueue_style( 'basekit_addons-'.BASEKIT_ADDONS_CPT_TOURS_PT,  basekit_addons_get_file_url(BASEKIT_ADDONS_PLUGIN_CPT . 'tours/tours.css'), array(), null );
		}
		if (basekit_addons_get_file_dir(BASEKIT_ADDONS_PLUGIN_CPT . 'tours/tours.js')!='') {
			wp_enqueue_script('basekit_addons-'.BASEKIT_ADDONS_CPT_TOURS_PT, basekit_addons_get_file_url(BASEKIT_ADDONS_PLUGIN_CPT . 'tours/tours.js'), array('jquery'), null, true );
		}
	}
}

// Return true if it's CPT Example page
if ( !function_exists( 'basekit_addons_is_tours_page' ) ) {
	function basekit_addons_is_tours_page() {
		return defined('BASEKIT_ADDONS_CPT_TOURS_PT')
					&& !is_search()
					&& (
						(is_single() && get_post_type()==BASEKIT_ADDONS_CPT_TOURS_PT)
						|| is_post_type_archive(BASEKIT_ADDONS_CPT_TOURS_PT)
						|| is_tax(BASEKIT_ADDONS_CPT_TOURS_TAXONOMY)
						);
	}
}


// Return taxonomy for the current post type
if ( !function_exists( 'basekit_addons_cpt_tours_post_type_taxonomy' ) ) {
	add_filter( 'trx_addons_filter_post_type_taxonomy',	'basekit_addons_cpt_tours_post_type_taxonomy', 10, 2 );
	function basekit_addons_cpt_tours_post_type_taxonomy($tax='', $post_type='') {
		if ( defined('BASEKIT_ADDONS_CPT_TOURS_PT') && $post_type == BASEKIT_ADDONS_CPT_TOURS_PT )
			$tax = BASEKIT_ADDONS_CPT_TOURS_TAXONOMY;
		return $tax;
	}
}


// Return link to the all posts for the breadcrumbs
if ( !function_exists( 'basekit_addons_cpt_tours_get_blog_all_posts_link' ) ) {
	add_filter('trx_addons_filter_get_blog_all_posts_link', 'basekit_addons_cpt_tours_get_blog_all_posts_link', 10, 2);
	function basekit_addons_cpt_tours_get_blog_all_posts_link($link='', $args=array()) {
		if ($link=='') {
			if (basekit_addons_is_tours_page() && !is_post_type_archive(BASEKIT_ADDONS_CPT_TOURS_PT))
				$link = '<a href="'.esc_url(get_post_type_archive_link( BASEKIT_ADDONS_CPT_TOURS_PT )).'">'.esc_html__('All Tours & Activities Items', 'basekit-addons').'</a>';
		}
		return $link;
	}
}


// Return current page title
if ( !function_exists( 'basekit_addons_cpt_tours_get_blog_title' ) ) {
	add_filter( 'trx_addons_filter_get_blog_title', 'basekit_addons_cpt_tours_get_blog_title');
	function basekit_addons_cpt_tours_get_blog_title($title='') {
		if ( defined('BASEKIT_ADDONS_CPT_TOURS_PT') ) {
			if ( is_post_type_archive(BASEKIT_ADDONS_CPT_TOURS_PT) )
				$title = esc_html__('All Tours & Activities Items', 'basekit-addons');

		}
		return $title;
	}
}


// Show related posts
if ( !function_exists( 'basekit_addons_cpt_tours_related_posts_after_article' ) ) {
    add_action('trx_addons_action_after_article', 'basekit_addons_cpt_tours_related_posts_after_article', 20, 1);
    function basekit_addons_cpt_tours_related_posts_after_article( $mode ) {
        if ($mode == 'tours.single' && apply_filters('trx_addons_filter_show_related_posts_after_article', true)) {
            do_action('trx_addons_action_related_posts', $mode);
        }
    }
}

if ( !function_exists( 'basekit_addons_cpt_tours_related_posts_show' ) ) {
    add_filter('trx_addons_filter_show_related_posts', 'basekit_addons_cpt_tours_related_posts_show');
    function basekit_addons_cpt_tours_related_posts_show( $show ) {
        if (!$show && is_single() && get_post_type() == BASEKIT_ADDONS_CPT_TOURS_PT) {
            do_action('trx_addons_action_related_posts', 'tours.single');
            $show = true;
        }
        return $show;
    }
}

if ( !function_exists( 'basekit_addons_cpt_tours_related_posts' ) ) {
    add_action('trx_addons_action_related_posts', 'basekit_addons_cpt_tours_related_posts', 10, 1);
    function basekit_addons_cpt_tours_related_posts( $mode ) {
        if ($mode == 'tours.single') {
            $trx_addons_related_style   = explode('_', trx_addons_get_option('tours_style'));
            $trx_addons_related_type    = $trx_addons_related_style[0];
            $trx_addons_related_columns = empty($trx_addons_related_style[1]) ? 1 : max(1, $trx_addons_related_style[1]);

            trx_addons_get_template_part(
                'templates/tpl.posts-related.php',
                'trx_addons_args_related',
                apply_filters('trx_addons_filter_args_related', array(
                        'class' => 'tours_page_related sc_tours sc_tours_'.esc_attr($trx_addons_related_type),
                        'posts_per_page' => $trx_addons_related_columns,
                        'columns' => $trx_addons_related_columns,
                        'template' => BASEKIT_ADDONS_PLUGIN_CPT . 'tours/tpl.'.trim($trx_addons_related_type).'-item.php',
                        'template_args_name' => 'trx_addons_args_sc_tours',
                        'template_get_file_dir' => 'basekit_addons_get_file_dir',
                        'post_type' => BASEKIT_ADDONS_CPT_TOURS_PT,
                        'taxonomies' => array(BASEKIT_ADDONS_CPT_TOURS_TAXONOMY),
                        'more_text' => __('Learn more', 'trx_addons')
                    )
                )
            );
        }
    }
}



// Replace standard theme templates
//-------------------------------------------------------------

// Change standard single template for services posts
if ( !function_exists( 'basekit_addons_cpt_tours_single_template' ) ) {
	add_filter('single_template', 'basekit_addons_cpt_tours_single_template');
	function basekit_addons_cpt_tours_single_template($template) {
		global $post;
		if (is_single() && $post->post_type == BASEKIT_ADDONS_CPT_TOURS_PT)
			$template = basekit_addons_get_file_dir(BASEKIT_ADDONS_PLUGIN_CPT . 'tours/tpl.single.php');
		return $template;
	}
}

// Change standard archive template for services posts
if ( !function_exists( 'basekit_addons_cpt_tours_archive_template' ) ) {
	add_filter('archive_template',	'basekit_addons_cpt_tours_archive_template');
	function basekit_addons_cpt_tours_archive_template( $template ) {
		if ( is_post_type_archive(BASEKIT_ADDONS_CPT_TOURS_PT) )
			$template = basekit_addons_get_file_dir(BASEKIT_ADDONS_PLUGIN_CPT . 'tours/tpl.archive.php');
		return $template;
	}	
}

// Change standard category template for services categories (groups)
if ( !function_exists( 'basekit_addons_cpt_tours_taxonomy_template' ) ) {
	add_filter('taxonomy_template',	'basekit_addons_cpt_tours_taxonomy_template');
	function basekit_addons_cpt_tours_taxonomy_template( $template ) {
		if ( is_tax(BASEKIT_ADDONS_CPT_TOURS_TAXONOMY) )
			$template = basekit_addons_get_file_dir(BASEKIT_ADDONS_PLUGIN_CPT . 'tours/tpl.archive.php');
		return $template;
	}	
}



// Admin utils
// -----------------------------------------------------------------

// Show <select> with Tours & Activities categories in the admin filters area
if (!function_exists('basekit_addons_cpt_tours_admin_filters')) {
	add_action( 'restrict_manage_posts', 'basekit_addons_cpt_tours_admin_filters' );
	function basekit_addons_cpt_tours_admin_filters() {
		trx_addons_admin_filters(BASEKIT_ADDONS_CPT_TOURS_PT, BASEKIT_ADDONS_CPT_TOURS_TAXONOMY);
	}
}
  
// Clear terms cache on the taxonomy save
if (!function_exists('basekit_addons_cpt_tours_admin_clear_cache')) {
	add_action( 'edited_'.BASEKIT_ADDONS_CPT_TOURS_TAXONOMY, 'basekit_addons_cpt_tours_admin_clear_cache', 10, 1 );
	add_action( 'delete_'.BASEKIT_ADDONS_CPT_TOURS_TAXONOMY, 'basekit_addons_cpt_tours_admin_clear_cache', 10, 1 );
	add_action( 'created_'.BASEKIT_ADDONS_CPT_TOURS_TAXONOMY, 'basekit_addons_cpt_tours_admin_clear_cache', 10, 1 );
	function basekit_addons_cpt_tours_admin_clear_cache( $term_id=0 ) {
		trx_addons_admin_clear_cache_terms(BASEKIT_ADDONS_CPT_TOURS_TAXONOMY);
	}
}


// trx_sc_tours
//-------------------------------------------------------------
/*
[trx_sc_tours id="unique_id" type="default" cat="category_slug or id" count="3" columns="3" slider="0|1"]
*/
if ( !function_exists( 'basekit_addons_sc_tours' ) ) {
	function basekit_addons_sc_tours($atts, $content=null) {
		$atts = trx_addons_sc_prepare_atts('trx_sc_tours', $atts, array(
			// Individual params
			"type" => "default",
			"columns" => "",
			"cat" => "",
			"count" => 3,
			"offset" => 0,
			"orderby" => '',
			"order" => '',
			"ids" => '',
			"slider" => 0,
			"slider_pagination" => "none",
			"slider_controls" => "none",
			"slides_space" => 0,
			"title" => "",
			"subtitle" => "",
			"description" => "",
			"link" => '',
			"link_image" => '',
			"link_text" => esc_html__('Learn more', 'basekit-addons'),
			"title_align" => "left",
			"title_style" => "default",
			"title_tag" => '',
			// Common params
			"id" => "",
			"class" => "",
			"css" => ""
			)
		);

		if (!empty($atts['ids'])) {
			$atts['ids'] = str_replace(array(';', ' '), array(',', ''), $atts['ids']);
			$atts['count'] = count(explode(',', $atts['ids']));
		}
		$atts['count'] = max(1, (int) $atts['count']);
		$atts['offset'] = max(0, (int) $atts['offset']);
		if (empty($atts['orderby'])) $atts['orderby'] = 'title';
		if (empty($atts['order'])) $atts['order'] = 'asc';
		$atts['slider'] = max(0, (int) $atts['slider']);
		if ($atts['slider'] > 0 && (int) $atts['slider_pagination'] > 0) $atts['slider_pagination'] = 'bottom';

		ob_start();
		basekit_addons_get_template_part(array(
										BASEKIT_ADDONS_PLUGIN_CPT . 'tours/tpl.'.trx_addons_esc($atts['type']).'.php',
										BASEKIT_ADDONS_PLUGIN_CPT . 'tours/tpl.default.php'
										), 
										'trx_addons_args_sc_tours',
										$atts
									);
		$output = ob_get_contents();
		ob_end_clean();
		
		return apply_filters('trx_addons_sc_output', $output, 'trx_sc_tours', $atts, $content);
	}
}

// Return params
if (!function_exists('basekit_addons_sc_tours_add_in_vc_params')) {
	function basekit_addons_sc_tours_add_in_vc_params() {
		return apply_filters('trx_addons_sc_map', array(
				"base" => "trx_sc_tours",
				"name" => esc_html__("SC_Example", 'basekit-addons'),
				"description" => wp_kses_data( __("Display Tours & Activities items from specified group", 'basekit-addons') ),
				"category" => esc_html__('ThemeREX', 'basekit-addons'),
				"icon" => 'icon_trx_sc_tours',
				"class" => "trx_sc_tours",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array_merge(
					array(
						array(
							"param_name" => "type",
							"heading" => esc_html__("Layout", 'basekit-addons'),
							"description" => wp_kses_data( __("Select shortcode's layout", 'basekit-addons') ),
							"admin_label" => true,
							"std" => "default",
							"value" => array_flip(apply_filters('trx_addons_sc_type', array(
												'default' => esc_html__('Default', 'basekit-addons')
												), 'trx_sc_tours')),
							"type" => "dropdown"
						),
						array(
							"param_name" => "cat",
							"heading" => esc_html__("Category", 'basekit-addons'),
							"description" => wp_kses_data( __("Select Tours & Activities category", 'basekit-addons') ),
							"value" => array_merge(array(esc_html__('- Select category -', 'basekit-addons') => 0), 
													array_flip(trx_addons_get_list_terms(false, BASEKIT_ADDONS_CPT_TOURS_TAXONOMY))),
							"std" => "0",
							"type" => "dropdown"
						)
					),
					trx_addons_vc_add_query_param(''),
					trx_addons_vc_add_slider_param(),
					trx_addons_vc_add_title_param(),
					trx_addons_vc_add_id_param()
				)
			), 'trx_sc_tours' );
	}
}




// SOW Widget
//------------------------------------------------------
if (class_exists('TRX_Addons_SOW_Widget')) {
	class TRX_Addons_SOW_Widget_Example extends TRX_Addons_SOW_Widget {
		
		function __construct() {
			parent::__construct(
				'trx_addons_sow_widget_tours',
				esc_html__('BaseKit Example Widget', 'basekit-addons'),
				array(
					'classname' => 'widget_tours',
					'description' => __('Display Tours & Activities posts', 'basekit-addons')
				),
				array(),
				false,
				BASEKIT_ADDONS_PLUGIN_DIR
			);
	
		}


		// Return array with all widget's fields
		function get_widget_form() {
			return apply_filters('trx_addons_sow_map', array_merge(
				array(
					'type' => array(
						'label' => __('Layout', 'basekit-addons'),
						"description" => wp_kses_data( __("Select shortcodes's layout", 'basekit-addons') ),
						'default' => 'default',
						'options' => apply_filters('trx_addons_sc_type', array(
											'default' => esc_html__('Default', 'basekit-addons')
										), $this->get_sc_name(), 'sow' ),
						'type' => 'select'
					),
					"cat" => array(
						"label" => esc_html__("Category", 'basekit-addons'),
						"description" => wp_kses_data( __("Select Tours & Activities category", 'basekit-addons') ),
						"default" => 0,
						"options" => trx_addons_array_merge(array(0 => esc_html__('- Select category -', 'basekit-addons')),
															trx_addons_get_list_terms(false, BASEKIT_ADDONS_CPT_TOURS_TAXONOMY)
															),
						"type" => "select"
					)
				),
				trx_addons_sow_add_query_param(''),
				trx_addons_sow_add_slider_param(),
				trx_addons_sow_add_title_param(),
				trx_addons_sow_add_id_param()
			), $this->get_sc_name());
		}

	}
	siteorigin_widget_register('trx_addons_sow_widget_tours', __FILE__, 'TRX_Addons_SOW_Widget_Example');
}
?>