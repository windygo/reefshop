<?php
/**
 * The template to display the Tours & Activities single page
 *
 * @package WordPress
 * @subpackage BaseKit Addons
 * @since v1.0.0
 */

get_header();

while ( have_posts() ) { the_post();
	do_action('trx_addons_action_before_article', 'tours.single');
	?>
	<article id="post-<?php the_ID(); ?>" <?php post_class( 'tours_page itemscope' ); trx_addons_seo_snippets('', 'Article'); ?>>

		<?php do_action('trx_addons_action_article_start', 'tours.single'); ?>
		
		<section class="tours_page_header">

			<?php
			
			// Title
			if ( !trx_addons_sc_layouts_showed('title') ) {
				?><h2 class="tours_page_title"><?php the_title(); ?></h2><?php
			}
			?>

		</section>
		<?php

		// Post content
		?><section class="tours_page_content entry-content"<?php trx_addons_seo_snippets('articleBody'); ?>><?php
			the_content();
		?></section><!-- .entry-content --><?php

		do_action('trx_addons_action_article_end', 'tours.single');

	?></article><?php

    do_action('trx_addons_action_after_article', 'tours.single');
}

get_footer();
?>