<?php
/**
 * ThemeREX Addons Custom post type: Properties (Gutenberg support)
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.6.22
 */

// Don't load directly
if ( ! defined( 'TRX_ADDONS_VERSION' ) ) {
	die( '-1' );
}



// Gutenberg Block
//------------------------------------------------------

// Add scripts and styles for the editor
if ( ! function_exists( 'trx_addons_gutenberg_sc_boats_editor_assets' ) ) {
	add_action( 'enqueue_block_editor_assets', 'trx_addons_gutenberg_sc_boats_editor_assets' );
	function trx_addons_gutenberg_sc_boats_editor_assets() {
		if ( trx_addons_exists_gutenberg() && trx_addons_get_setting( 'allow_gutenberg_blocks' ) ) {
			// Scripts
			wp_enqueue_script(
				'trx-addons-gutenberg-editor-block-boats',
				trx_addons_get_file_url( TRX_ADDONS_PLUGIN_CPT . 'boats/gutenberg/boats.gutenberg-editor.js' ),
				array( 'wp-blocks', 'wp-editor', 'wp-i18n', 'wp-element', 'trx_addons-admin', 'trx_addons-utils', 'trx_addons-gutenberg-blocks' ),
				filemtime( trx_addons_get_file_dir( TRX_ADDONS_PLUGIN_CPT . 'boats/gutenberg/boats.gutenberg-editor.js' ) ),
				true
			);
		}
	}
}

// Block register
if ( ! function_exists( 'trx_addons_sc_boats_add_in_gutenberg' ) ) {
	add_action( 'init', 'trx_addons_sc_boats_add_in_gutenberg' );
	function trx_addons_sc_boats_add_in_gutenberg() {
		if ( trx_addons_exists_gutenberg() && trx_addons_get_setting( 'allow_gutenberg_blocks' ) ) {
			register_block_type(
				'trx-addons/boats', array(
					'attributes'      => array_merge(
						array(
							'type'                    => array(
								'type'    => 'string',
								'default' => 'default',
							),
							'more_text'               => array(
								'type'    => 'string',
								'default' => esc_html__( 'Read more' ),
							),
							'pagination'              => array(
								'type'    => 'string',
								'default' => 'none',
							),
							'map_height'        => array(
								'type'    => 'number',
								'default' => 350,
							),
							'boats_type'         => array(
								'type'    => 'string',
								'default' => '0',
							),
							'boats_status'       => array(
								'type'    => 'string',
								'default' => '0',
							),
							'boats_labels'       => array(
								'type'    => 'string',
								'default' => '0',
							),
							'boats_country'      => array(
								'type'    => 'string',
								'default' => '0',
							),
							'boats_city'         => array(
								'type'    => 'string',
								'default' => '0',
							),
							'boats_neighborhood' => array(
								'type'    => 'string',
								'default' => '0',
							),
							'cars_transmission'       => array(
								'type'    => 'string',
								'default' => '',
							),
						),
						trx_addons_gutenberg_get_param_query(),
						trx_addons_gutenberg_get_param_slider(),
						trx_addons_gutenberg_get_param_title(),
						trx_addons_gutenberg_get_param_button(),
						trx_addons_gutenberg_get_param_id()
					),
					'render_callback' => 'trx_addons_gutenberg_sc_boats_render_block',
				)
			);
		}
	}
}

// Block render
if ( ! function_exists( 'trx_addons_gutenberg_sc_boats_render_block' ) ) {
	function trx_addons_gutenberg_sc_boats_render_block( $attributes = array() ) {
		return trx_addons_sc_boats( $attributes );
	}
}

// Return list of allowed layouts
if ( ! function_exists( 'trx_addons_gutenberg_sc_boats_get_layouts' ) ) {
	add_filter( 'trx_addons_filter_gutenberg_sc_layouts', 'trx_addons_gutenberg_sc_boats_get_layouts', 10, 1 );
	function trx_addons_gutenberg_sc_boats_get_layouts( $array = array() ) {
		$array['trx_sc_boats'] = apply_filters( 'trx_addons_sc_type', trx_addons_components_get_allowed_layouts( 'cpt', 'boats', 'sc' ), 'trx_sc_boats' );

		return $array;
	}
}

// Add shortcode's specific vars to the JS storage
if ( ! function_exists( 'trx_addons_gutenberg_sc_boats_params' ) ) {
	add_filter( 'trx_addons_filter_gutenberg_sc_params', 'trx_addons_gutenberg_sc_boats_params' );
	function trx_addons_gutenberg_sc_boats_params( $vars = array() ) {
		if ( trx_addons_exists_gutenberg() && trx_addons_get_setting( 'allow_gutenberg_blocks' ) ) {

			// Boat Location
			$vars['cpt_boat_location']    = trx_addons_get_list_terms( false, TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_LOCATION );
			$vars['cpt_boat_location'][0] = esc_html__( '- Boat Location -', 'trx_addons' );

			// Boat Amenities
			$vars['cpt_boat_amenities']    = trx_addons_get_list_terms( false, TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_AMENITIES );
			$vars['cpt_boat_amenities'][0] = esc_html__( '- Amenities -', 'trx_addons' );

			// Boat Type
			$vars['cpt_boat_type']    = trx_addons_get_list_terms( false, TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE );
			$vars['cpt_boat_type'][0] = esc_html__( '- Boat Type -', 'trx_addons' );

			// Boat Specification
			$vars['cpt_boat_specifications']     = trx_addons_get_list_terms( false, TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_SPECIFICATIONS );
			$vars['cpt_boat_specifications'][0] = esc_html__( '- Specification -', 'trx_addons' );

			// Crew
			$vars['cpt_boat_crew']    = trx_addons_get_list_terms( false, TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_CREW );
			$vars['cpt_boat_crew'][0] = esc_html__( '- Crew -', 'trx_addons' );


			return $vars;
		}
	}
}
