<?php
/**
 * The template's part to display the property's address
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.6.22
 */

$trx_addons_args = get_query_var('trx_addons_args_boats_address');
$trx_addons_meta = $trx_addons_args['meta'];

$title = __('Show all boats from %s', 'trx_addons');

?><span class="boats_address"><?php
	// Address
	if (!empty($trx_addons_meta['address'])) {
		?><span class="boats_address_item"><?php trx_addons_show_layout($trx_addons_meta['address']); ?></span><?php
	}
?></span>