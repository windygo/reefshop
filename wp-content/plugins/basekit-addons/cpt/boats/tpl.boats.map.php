<?php
/**
 * The style "map" of the Properties
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.6.22
 */

//$map_type = trx_addons_get_option('api_google') != '' && function_exists('trx_addons_sc_googlemap')
//				? 'google'
//				: (function_exists('trx_addons_sc_yandexmap')
//					? 'yandex'
//					: ''
//					);

$map_type = '';
if ( function_exists('trx_addons_sc_googlemap')) {
    $map_type ='google';
} elseif ( function_exists('trx_addons_sc_yandexmap') ) {
    $map_type ='yandex';
}




if (!empty($map_type)) {
	$on_property_post = is_single() && get_post_type()==BASEKIT_ADDONS_CPT_BOATS_PT;
	$args = get_query_var('trx_addons_args_sc_boats');
	$query_args = trx_addons_cpt_boats_query_params_to_args(
					isset($_GET['boats_type'])
					|| (is_single() && get_post_type()==BASEKIT_ADDONS_CPT_BOATS_PT)
					|| (is_single() && get_post_type()==TRX_ADDONS_CPT_AGENTS_PT)
					|| (is_post_type_archive(BASEKIT_ADDONS_CPT_BOATS_PT) && (int) trx_addons_get_value_gp('compare') == 1)
                    || is_tax(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_SPECIFICATIONS)
                    || is_tax(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_AMENITIES)
                    || is_tax(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE)
                    || is_tax(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_LOCATION)
                    || is_tax(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE)
                    || is_tax(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_LOCATION)
                    || is_tax(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_CREW)
						? array()
						: array(
								'boats_type' => $args['boats_type'],
								'boats_status' => $args['boats_status'],
								'boats_labels' => $args['boats_labels'],
								'boats_country' => $args['boats_country'],
								'boats_state' => $args['boats_state'],
								'boats_city' => $args['boats_city'],
								'boats_neighborhood' => $args['boats_neighborhood'],
								'boats_order' => $args['orderby'] . '_' . $args['order']
								),
					true);
	if (!isset($_GET['boats_type'])) {
		if (is_single() && get_post_type()==BASEKIT_ADDONS_CPT_BOATS_PT) {
			$args['ids'] = get_the_ID();
			$query_args['post_type'] = BASEKIT_ADDONS_CPT_BOATS_PT;
		} else if (is_post_type_archive(BASEKIT_ADDONS_CPT_BOATS_PT) && (int) trx_addons_get_value_gp('compare') == 1) {
			$posts = array();
			$list = urldecode(trx_addons_get_value_gpc('trx_addons_boats_compare_list', ''));
			$list = !empty($list) ? json_decode($list, true) : array();
			if (is_array($list)) {
				foreach ($list as $k=>$v) {
					$id = (int) str_replace('id_', '', $k);
					if ($id > 0) $posts[] = $id;
				}
			}
			if (count($posts) > 0) {
				$args['ids'] = join(',', $posts);
			}
		}
// Attention! Parameter 'suppress_filters' is damage WPML-queries!
//		$query_args['suppress_filters'] = true;
		$query_args['ignore_sticky_posts'] = true;
		if (empty($args['ids'])) {
			if (empty($query_args['posts_per_page'])) {
				$query_args['posts_per_page'] = $args['count'];
				$query_args['offset'] = $args['offset'];
			}
		} else
			$query_args = trx_addons_query_add_posts_and_cats($query_args, $args['ids']);
	}
	$query = new WP_Query( $query_args );
	if ($query->found_posts > 0) {
		if ($args['count'] > $query->found_posts) $args['count'] = $query->found_posts;
		?><div<?php if (!empty($args['id'])) echo ' id="'.esc_attr($args['id']).'"'; ?> class="sc_boats sc_boats_<?php
				echo esc_attr($args['type']);
				if (!empty($args['class'])) echo ' '.esc_attr($args['class']); 
				?>"<?php
			if (!empty($args['css'])) echo ' style="'.esc_attr($args['css']).'"';
			?>><?php
	
			trx_addons_sc_show_titles('sc_boats', $args);
			
			?><div class="sc_boats_content sc_item_content"><?php
	
			$markers = array();
			
			$default_icon = trx_addons_get_option('boats_marker');
			if (empty($default_icon)) $default_icon = trx_addons_remove_protocol(trx_addons_get_option("api_{$map_type}_marker"));
			if (empty($default_icon)) $default_icon = trx_addons_get_file_url(BASEKIT_ADDONS_PLUGIN_CPT . 'boats/boats.png');
			
			while ( $query->have_posts() ) { $query->the_post();
				$meta = get_post_meta(get_the_ID(), 'trx_addons_options', true);
				if (($on_property_post && empty($meta['show_map'])) || empty($meta['location'])) continue;
				if (empty($meta['marker'])) {
					$terms = get_the_terms(get_the_ID(), TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_LOCATION);
					if (is_array($terms) && count($terms)>0) {
						$term = trx_addons_array_get_first($terms, false);
						$icon = trx_addons_get_term_meta(array(
														'taxonomy'	=> TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_LOCATION,
														'term_id'	=> $term->term_id,
														'key'		=> 'image'
														));
					} else
						$icon = '';
				} else
					$icon = $meta['marker'];
				$latlng = explode(',', $meta['location']);
				$markers[] = array(
								'title' => get_the_title(),
								'description' => trx_addons_get_template_part_as_string(
													BASEKIT_ADDONS_PLUGIN_CPT . 'boats/tpl.boats.default-item.php',
													'trx_addons_args_boats',
													$args),
								'address' => '',
								'latlng' => trim($latlng[0]).','.trim($latlng[1]),
								'icon' => !empty($icon) ? $icon : $default_icon
								);
			}
	
			wp_reset_postdata();
	
			// Display map
			$map_args = array(
				"markers" => $markers,
				"zoom" => count($markers)>1 ? 0 : 10,
				"height" => max(100, $args['map_height']),
				"id" => !empty($args['id']) ? $args['id'].'_map' : "",
                "style" => 'dark'
			);

			trx_addons_show_layout(
				$map_type == 'google'
					? trx_addons_sc_googlemap($map_args)
					: trx_addons_sc_yandexmap($map_args)
				);
		
			?></div><?php		// .sc_boats_content
	
			trx_addons_sc_show_links('sc_boats', $args);
	
		?></div><!-- /.sc_boats --><?php
	}
}
