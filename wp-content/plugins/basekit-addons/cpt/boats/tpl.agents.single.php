<?php
/**
 * The template to display the agent's single page
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.6.22
 */

get_header();

while ( have_posts() ) { the_post();
	$trx_addons_meta = get_post_meta(get_the_ID(), 'trx_addons_options', true);
	do_action('trx_addons_action_before_article', 'boats.agents.single');
	?>
	<article id="post-<?php the_ID(); ?>" data-post-id="<?php the_ID(); ?>" <?php post_class( 'agents_page itemscope' ); trx_addons_seo_snippets('author', 'Person'); ?>>

		<?php do_action('trx_addons_action_article_start', 'boats.agents.single'); ?>
		
		<section class="boats_page_section boats_page_agent">
			<div class="boats_page_agent_wrap"><?php
				basekit_addons_get_template_part(BASEKIT_ADDONS_PLUGIN_CPT . 'boats/tpl.boats.parts.agent.php',
												'trx_addons_args_boats_agent',
												array(
													'meta' => array(
															'agent_type' => 'agent',
															'agent' => get_the_ID()
															)
												)
											);
			?></div>
		</section><!-- .boats_page_agent -->
		
		<?php
		$page_number = get_query_var('paged') 
							? get_query_var('paged') 
							: (get_query_var('page') 
								? get_query_var('page') 
								: 1);
		if ($page_number==1 && trim(get_the_content()) != '') {
			?><section class="boats_page_section boats_page_content entry-content"<?php trx_addons_seo_snippets('articleBody'); ?>><?php
				?><h4 class="boats_page_section_title"><?php esc_html_e('About me', 'trx_addons'); ?></h4><?php
				the_content( );
			?></section><!-- .entry-content --><?php
		}
		?>

		<section class="boats_page_section boats_page_offers_list">
			<h4 class="boats_page_section_title"><?php esc_html_e('All my offers', 'trx_addons'); ?></h4><?php
			?><div class="boats_page_offers_list_wrap"><?php
				basekit_addons_get_template_part(BASEKIT_ADDONS_PLUGIN_CPT . 'boats/tpl.boats.parts.loop.php',
												'trx_addons_args_boats_loop',
												array(
													'blog_style' => trx_addons_get_option('agents_boats_style'),
													'query_params' => array(
																		'boats_agent' => get_the_ID()
																		)
													)
											);
			?></div>
		</section><!-- .boats_page_list -->

		<?php do_action('trx_addons_action_article_end', 'boats.agents.single'); ?>

	</article>
	<?php

}

get_footer();
