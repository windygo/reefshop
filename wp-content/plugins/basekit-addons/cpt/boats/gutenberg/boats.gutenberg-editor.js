(function(blocks, editor, i18n, element) {
	// Set up variables
	var el = element.createElement;

	// Register Block - Properties
	blocks.registerBlockType(
		'trx-addons/boats', {
			title: i18n.__( 'Properties' ),
			icon: 'admin-multisite',
			category: 'trx-addons-cpt',
			attributes: trx_addons_object_merge(
				{
					type: {
						type: 'string',
						default: 'default'
					},
					more_text: {
						type: 'string',
						default: i18n.__( 'Read more' ),
					},
					pagination: {
						type: 'string',
						default: 'none'
					},
					map_height: {
						type: 'number',
						default: 350
					},
					boats_type: {
						type: 'string',
						default: '0'
					},
					boats_status: {
						type: 'string',
						default: '0'
					},
					boats_labels: {
						type: 'string',
						default: '0'
					},
					boats_country: {
						type: 'string',
						default: '0'
					},
					boats_city: {
						type: 'string',
						default: '0'
					},
					boats_neighborhood: {
						type: 'string',
						default: '0'
					},
				},
				trx_addons_gutenberg_get_param_query(),
				trx_addons_gutenberg_get_param_slider(),
				trx_addons_gutenberg_get_param_title(),
				trx_addons_gutenberg_get_param_button(),
				trx_addons_gutenberg_get_param_id()
			),
			edit: function(props) {
				return trx_addons_gutenberg_block_params(
					{
						'render': true,
						'general_params': el(
							'div', {},
							// Layout
							trx_addons_gutenberg_add_param(
								{
									'name': 'type',
									'title': i18n.__( 'Layout' ),
									'descr': i18n.__( "Select shortcodes's layout" ),
									'type': 'select',
									'options': trx_addons_gutenberg_get_lists( TRX_ADDONS_STORAGE['gutenberg_sc_params']['sc_layouts']['trx_sc_boats'] )
								}, props
							),
							// 'More' text
							trx_addons_gutenberg_add_param(
								{
									'name': 'more_text',
									'title': i18n.__( "'More' text" ),
									'descr': i18n.__( "Specify caption of the 'Read more' button. If empty - hide button" ),
									'type': 'text',
								}, props
							),
							// Pagination
							trx_addons_gutenberg_add_param(
								{
									'name': 'pagination',
									'title': i18n.__( 'Pagination' ),
									'descr': i18n.__( "Add pagination links after posts. Attention! If using slider - pagination not allowed!" ),
									'type': 'select',
									'options': trx_addons_gutenberg_get_lists( TRX_ADDONS_STORAGE['gutenberg_sc_params']['sc_paginations'] )
								}, props
							),
							//  Map height
							trx_addons_gutenberg_add_param(
								{
									'name': 'map_height',
									'title': i18n.__( "Map height" ),
									'descr': i18n.__( "Specify height of the map with boats" ),
									'type': 'number',
									'min': 100,
									'dependency': {
										'type': ['map']
									}
								}, props
							),
							// Type
							trx_addons_gutenberg_add_param(
								{
									'name': 'boats_type',
									'title': i18n.__( 'Type' ),
									'descr': i18n.__( "Select the type to show boats that have it!" ),
									'type': 'select',
									'options': trx_addons_gutenberg_get_lists( TRX_ADDONS_STORAGE['gutenberg_sc_params']['sc_boats_type'] )
								}, props
							),
							// Status
							trx_addons_gutenberg_add_param(
								{
									'name': 'boats_status',
									'title': i18n.__( 'Status' ),
									'descr': i18n.__( "Select the status to show boats that have it" ),
									'type': 'select',
									'options': trx_addons_gutenberg_get_lists( TRX_ADDONS_STORAGE['gutenberg_sc_params']['sc_boats_status'] )
								}, props
							),
							// Label
							trx_addons_gutenberg_add_param(
								{
									'name': 'boats_labels',
									'title': i18n.__( 'Label' ),
									'descr': i18n.__( "Select the label to show boats that have it" ),
									'type': 'select',
									'options': trx_addons_gutenberg_get_lists( TRX_ADDONS_STORAGE['gutenberg_sc_params']['sc_boats_labels'] )
								}, props
							),
							// Country
							trx_addons_gutenberg_add_param(
								{
									'name': 'boats_country',
									'title': i18n.__( 'Country' ),
									'descr': i18n.__( "Select the country to show boats from" ),
									'type': 'select',
									'options': trx_addons_gutenberg_get_lists( TRX_ADDONS_STORAGE['gutenberg_sc_params']['sc_boats_country'] )
								}, props
							),
							// State
							trx_addons_gutenberg_add_param(
								{
									'name': 'boats_state',
									'title': i18n.__( 'State' ),
									'descr': i18n.__( "Select the county/state to show boats from" ),
									'type': 'select',
									'options': trx_addons_gutenberg_get_lists( TRX_ADDONS_STORAGE['gutenberg_sc_params']['sc_boats_states'] )
								}, props
							),
							// City
							trx_addons_gutenberg_add_param(
								{
									'name': 'boats_city',
									'title': i18n.__( 'City' ),
									'descr': i18n.__( "Select the city to show boats from it" ),
									'type': 'select',
									'options': trx_addons_gutenberg_get_lists( TRX_ADDONS_STORAGE['gutenberg_sc_params']['sc_boats_cities'] )
								}, props
							),
							// Neighborhood
							trx_addons_gutenberg_add_param(
								{
									'name': 'boats_neighborhood',
									'title': i18n.__( 'Neighborhood' ),
									'descr': i18n.__( "Select the neighborhood to show boats from" ),
									'type': 'select',
									'options': trx_addons_gutenberg_get_lists( TRX_ADDONS_STORAGE['gutenberg_sc_params']['sc_boats_neighborhoods'] )
								}, props
							),					
						),
						'additional_params': el(
							'div', {},
							// Query params
							trx_addons_gutenberg_add_param_query( props ),
							// Title params
							trx_addons_gutenberg_add_param_title( props, true ),
							// Slider params
							trx_addons_gutenberg_add_param_slider( props ),
							// ID, Class, CSS params
							trx_addons_gutenberg_add_param_id( props )
						)
					}, props
				);
			},
			save: function(props) {
				return el( '', null );
			}
		}
	);
})( window.wp.blocks, window.wp.editor, window.wp.i18n, window.wp.element, );
