<?php
/**
 * ThemeREX Addons Custom post type: Boats (Elementor support)
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.6.22
 */

// Don't load directly
if ( ! defined( 'TRX_ADDONS_VERSION' ) ) {
	die( '-1' );
}



// Elementor Widget
//------------------------------------------------------
if (!function_exists('trx_addons_sc_boats_add_in_elementor')) {
	
	// Load required styles and scripts for Elementor Editor mode
	if ( !function_exists( 'trx_addons_sc_boats_elm_editor_load_scripts' ) ) {
		add_action("elementor/editor/before_enqueue_scripts", 'trx_addons_sc_boats_elm_editor_load_scripts');
		function trx_addons_sc_boats_elm_editor_load_scripts() {
			wp_enqueue_script( 'trx_addons-sc_boats-elementor-editor', trx_addons_get_file_url(TRX_ADDONS_PLUGIN_CPT . 'boats/boats.elementor.editor.js'), array('jquery'), null, true );
		}
	}
	
	// Register widgets
	add_action( 'elementor/widgets/widgets_registered', 'trx_addons_sc_boats_add_in_elementor' );
	function trx_addons_sc_boats_add_in_elementor() {
		
		if (!class_exists('TRX_Addons_Elementor_Widget')) return;	

		class TRX_Addons_Elementor_Widget_Boats extends TRX_Addons_Elementor_Widget {

			/**
			 * Widget base constructor.
			 *
			 * Initializing the widget base class.
			 *
			 * @since 1.6.41
			 * @access public
			 *
			 * @param array      $data Widget data. Default is an empty array.
			 * @param array|null $args Optional. Widget default arguments. Default is null.
			 */
			public function __construct( $data = [], $args = null ) {
				parent::__construct( $data, $args );
				$this->add_plain_params([
					'map_height' => 'size+unit'
				]);
			}

			/**
			 * Retrieve widget name.
			 *
			 * @since 1.6.41
			 * @access public
			 *
			 * @return string Widget name.
			 */
			public function get_name() {
				return 'trx_sc_boats';
			}

			/**
			 * Retrieve widget title.
			 *
			 * @since 1.6.41
			 * @access public
			 *
			 * @return string Widget title.
			 */
			public function get_title() {
				return __( 'Properties', 'trx_addons' );
			}

			/**
			 * Retrieve widget icon.
			 *
			 * @since 1.6.41
			 * @access public
			 *
			 * @return string Widget icon.
			 */
			public function get_icon() {
				return 'eicon-info-box';
			}

			/**
			 * Retrieve the list of categories the widget belongs to.
			 *
			 * Used to determine where to display the widget in the editor.
			 *
			 * @since 1.6.41
			 * @access public
			 *
			 * @return array Widget categories.
			 */
			public function get_categories() {
				return ['trx_addons-cpt'];
			}

			/**
			 * Register widget controls.
			 *
			 * Adds different input fields to allow the user to change and customize the widget settings.
			 *
			 * @since 1.6.41
			 * @access protected
			 */
			protected function _register_controls() {
				// If open params in Elementor Editor
				$this->start_controls_section(
					'section_sc_boats',
					[
						'label' => __( 'Boats', 'trx_addons' ),
					]
				);

				$this->add_control(
					'type',
					[
						'label' => __( 'Layout', 'trx_addons' ),
						'label_block' => false,
						'type' => \Elementor\Controls_Manager::SELECT,
						'options' => apply_filters('trx_addons_sc_type', trx_addons_components_get_allowed_layouts('cpt', 'boats', 'sc'), 'trx_sc_boats'),
						'default' => 'default'
					]
				);

				$this->add_control(
					'more_text',
					[
						'label' => __( "'More' text", 'trx_addons' ),
						'label_block' => false,
						'type' => \Elementor\Controls_Manager::TEXT,
						'default' => esc_html__('Read more', 'trx_addons'),
					]
				);

				$this->add_control(
					'pagination',
					[
						'label' => __( 'Pagination', 'trx_addons' ),
						'label_block' => false,
						'description' => wp_kses_data( __("Add pagination links after posts. Attention! If using slider - pagination not allowed!", 'trx_addons') ),
						'type' => \Elementor\Controls_Manager::SELECT,
						'options' => trx_addons_get_list_sc_paginations(),
						'default' => 'none'
					]
				);

				$this->add_control(
					'map_height',
					[
						'label' => __( 'Height', 'trx_addons' ),
						'type' => \Elementor\Controls_Manager::SLIDER,
						'default' => [
							'size' => 350,
							'unit' => 'px'
						],
						'range' => [
							'px' => [
								'min' => 0,
								'max' => 500
							],
							'em' => [
								'min' => 0,
								'max' => 50
							],
						],
						'size_units' => [ 'px', 'em' ],
						'condition' => [
							//'map' => '1'
							'type' => 'map'
						],
						'selectors' => [
							'{{WRAPPER}} .sc_googlemap,{{WRAPPER}} .sc_yandexmap' => 'height: {{SIZE}}{{UNIT}};',
						],
					]
				);

				$this->add_control(
					'boats_type',
					[
						'label' => __( 'Boat Type', 'trx_addons' ),
						'label_block' => false,
						'type' => \Elementor\Controls_Manager::SELECT,
						'options' => trx_addons_array_merge(array(0 => esc_html__('- Boat Type -', 'trx_addons')), trx_addons_get_list_terms(false, TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE)),
						'default' => '0'
					]
				);

				$this->add_control(
					'boats_location',
					[
						'label' => __( 'Boat Location', 'trx_addons' ),
						'label_block' => false,
						'type' => \Elementor\Controls_Manager::SELECT,
						'options' => trx_addons_array_merge(array(0 => esc_html__('- Boat Location -', 'trx_addons')), trx_addons_get_list_terms(false, TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_LOCATION)),
						'default' => '0'
					]
				);

				$this->add_control(
					'boats_crew',
					[
						'label' => __( 'Boat Crew', 'trx_addons' ),
						'label_block' => false,
						'type' => \Elementor\Controls_Manager::SELECT,
						'options' => trx_addons_array_merge(array(0 => esc_html__('- Boat Crew -', 'trx_addons')), trx_addons_get_list_terms(false, TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_CREW)),
						'default' => '0'
					]
				);

				$this->add_control(
					'boats_specification',
					[
						'label' => __( 'Boat Specification', 'trx_addons' ),
						'label_block' => false,
						'type' => \Elementor\Controls_Manager::SELECT,
						'options' => trx_addons_array_merge(array(0 => esc_html__('- Boat Specification -', 'trx_addons')), trx_addons_get_list_terms(false, TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_SPECIFICATIONS)),
						'default' => '0'
					]
				);

				$this->add_control(
					'boats_amenities',
					[
						'label' => __( 'Boat Amenities', 'trx_addons' ),
						'label_block' => false,
						'type' => \Elementor\Controls_Manager::SELECT,
						'options' => trx_addons_array_merge(array(0 => esc_html__('- Boat Amenities -', 'trx_addons')), trx_addons_get_list_terms(false, TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_AMENITIES)),
						'default' => '0'
					]
				);


				$this->end_controls_section();
				
				$this->add_query_param(false, [
					'orderby' => [
								'options' => trx_addons_get_list_sc_query_orderby('none', 'none,ID,post_date,price,title,rand')
					],
					'columns' => [
								'condition' => [
									'type' => ['default', 'slider']
								]
					]
				]);
				
				$this->add_slider_param(false, [
					'slider' => [
								'condition' => [
									'type' => ['default', 'slider']
								]
					],
					/*
					'slider_pagination' => [
								'options' => trx_addons_array_merge(trx_addons_get_list_sc_slider_paginations(), array(
																			'bottom_outside' => esc_html__('Bottom Outside', 'trx_addons')
																			))
					]
					*/
				]);
				
				$this->add_title_param();
			}
		}
		
		// Register widget
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new TRX_Addons_Elementor_Widget_Boats() );
	}
}


// Disable our widgets (shortcodes) to use in Elementor
// because we create special Elementor's widgets instead
if (!function_exists('trx_addons_sc_boats_black_list')) {
	add_action( 'elementor/widgets/black_list', 'trx_addons_sc_boats_black_list' );
	function trx_addons_sc_boats_black_list($list) {
		$list[] = 'TRX_Addons_SOW_Widget_Boats';
		$list[] = 'trx_addons_widget_boats_search';
		return $list;
	}
}
