<?php
/**
 * The template with loop of the boats archive
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.6.22
 */

$args = get_query_var('trx_addons_args_boats_loop');

$trx_addons_need_reset_query = false;
if (!empty($args['query_params'])) {
	$query_args = trx_addons_cpt_boats_query_params_to_args($args['query_params'], true);
	if (count($query_args) > 0) query_posts($query_args);
	$trx_addons_need_reset_query = true;
}

do_action('trx_addons_action_start_archive');

if (have_posts()) {

	$trx_addons_boats_style   = explode('_', !empty($args['blog_style'])
															? $args['blog_style']
															: trx_addons_get_option('boats_blog_style')
											);
	$trx_addons_boats_type    = $trx_addons_boats_style[0];
	$trx_addons_boats_columns = empty($trx_addons_boats_style[1]) ? 1 : max(1, $trx_addons_boats_style[1]);

	?><div class="sc_boats sc_boats_default">
		
		<div class="sc_boats_columns_wrap sc_boats_columns_<?php
			echo esc_attr($trx_addons_boats_columns);
			if ($trx_addons_boats_columns > 1) echo ' ' . esc_attr(trx_addons_get_columns_wrap_class()) . ' columns_padding_bottom';
		?>"><?php

			while ( have_posts() ) { the_post(); 
				basekit_addons_get_template_part(array(
												BASEKIT_ADDONS_PLUGIN_CPT . 'boats/tpl.boats.'.trim($trx_addons_boats_type).'-item.php',
												BASEKIT_ADDONS_PLUGIN_CPT . 'boats/tpl.boats.default-item.php'
												),
												'trx_addons_args_sc_boats',
												array(
													'type' => $trx_addons_boats_type,
													'columns' => $trx_addons_boats_columns,
													'slider' => false
												)
											);
			}
			wp_reset_postdata();

		?></div><!-- .sc_boats_columns_wrap --><?php

    ?></div><!-- .sc_boats --><?php

	the_posts_pagination( array(
		'mid_size'  => 2,
		'prev_text' => esc_html__( '<', 'trx_addons' ),
		'next_text' => esc_html__( '>', 'trx_addons' ),
		'before_page_number' => '<span class="meta-nav screen-reader-text">' . esc_html__( 'Page', 'trx_addons' ) . ' </span>',
	) );

} else {

	trx_addons_get_template_part('templates/tpl.posts-none.php');

}

do_action('trx_addons_action_end_archive');

if ($trx_addons_need_reset_query) wp_reset_query();
