<?php
/**
 * The template's part to display the property's price
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.6.22
 */

$trx_addons_args = get_query_var('trx_addons_args_boats_price');
$trx_addons_meta = $trx_addons_args['meta'];

?><span class="boats_price"><?php
	if (!empty($trx_addons_meta['price'])) {
		?><span class="boats_price_data boats_price1"><?php
			echo esc_html(trx_addons_format_price($trx_addons_meta['price']));
		?></span><?php
	}
    if (!empty($trx_addons_meta['before_price'])) {
        ?><span class="boats_price_label boats_price_before"><?php
        trx_addons_show_layout(trx_addons_prepare_macros($trx_addons_meta['before_price']));
        ?></span><?php
    }
?></span>