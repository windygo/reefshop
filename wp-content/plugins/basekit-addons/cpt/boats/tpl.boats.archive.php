<?php
/**
 * The template to display the boats archive
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.6.22
 */

get_header();

basekit_addons_get_template_part(BASEKIT_ADDONS_PLUGIN_CPT . 'boats/tpl.boats.parts.loop.php');

get_footer();
