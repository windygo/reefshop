<?php
/**
 * ThemeREX Addons Custom post type: Boats
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.6.22
 */

// Don't load directly
if ( ! defined( 'TRX_ADDONS_VERSION' ) ) {
	die( '-1' );
}


// -----------------------------------------------------------------
// -- Custom post type registration
// -----------------------------------------------------------------

// Define Custom post type and taxonomy constants for 'Boats'
if ( ! defined('BASEKIT_ADDONS_CPT_BOATS_PT') )
		define('BASEKIT_ADDONS_CPT_BOATS_PT', 'cpt_boats');

// Register post type and taxonomy
if (!function_exists('trx_addons_cpt_boats_init')) {
	add_action( 'init', 'trx_addons_cpt_boats_init' );
	function trx_addons_cpt_boats_init() {
		
		trx_addons_meta_box_register(BASEKIT_ADDONS_CPT_BOATS_PT, array(
			"basic_section" => array(
				"title" => esc_html__('Basic information', 'trx_addons'),
				"desc" => wp_kses_data( __('Basic information about the boats', 'trx_addons') ),
				"type" => "section"
			),
			"price" => array(
				"title" => esc_html__("Rent price", 'trx_addons'),
				"desc" => wp_kses_data( __('Specify main price for this boat (only digits)', 'trx_addons') ),
				"class" => "trx_addons_column-1_4",
				"std" => "",
				"type" => "text"
			),
            "before_price" => array(
                "title" => esc_html__("After price", 'trx_addons'),
                "desc" => wp_kses_data( __('Specify any text to display it after the price', 'trx_addons') ),
                "class" => "trx_addons_column-1_4",
                "std" => "",
                "type" => "text"
            ),
			"length" => array(
				"title" => esc_html__("Boat Size", 'trx_addons'),
				"desc" => wp_kses_data( __('Boat Size (only digits)', 'trx_addons') ),
				"class" => "trx_addons_column-1_4",
				"std" => "",
				"type" => "text"
			),
			"length_prefix" => array(
				"title" => esc_html__("Boat Length prefix", 'trx_addons'),
				"desc" => wp_kses_data( __('Boat Length prefix (unit of measurement)', 'trx_addons') ),
				"class" => "trx_addons_column-1_4",
				"std" => "",
				"type" => "text"
			),
			"cruising_speed" => array(
				"title" => esc_html__("Cruising speed", 'trx_addons'),
				"desc" => wp_kses_data( __('Boat Cruising Speed (only digits)', 'trx_addons') ),
				"class" => "trx_addons_column-1_4",
				"std" => "",
				"type" => "text"
			),
			"cruising_speed_prefix" => array(
				"title" => esc_html__("Cruising speed prefix", 'trx_addons'),
				"desc" => wp_kses_data( __('Boat Cruising speed prefix (unit of measurement).', 'trx_addons') ),
				"class" => "trx_addons_column-1_4",
				"std" => "",
				"type" => "text"
			),
			"cabins" => array(
				"title" => esc_html__("Cabins", 'trx_addons'),
				"desc" => wp_kses_data( __('Cabins number (only digits)', 'trx_addons') ),
				"class" => "trx_addons_column-1_4",
				"std" => 0,
				"options" => trx_addons_get_list_range(0, 10),
				"type" => "select"
			),
			"charter_guest" => array(
				"title" => esc_html__("Charter guest: ", 'trx_addons'),
				"desc" => wp_kses_data( __('Charter guest: (only digits)', 'trx_addons') ),
				"class" => "trx_addons_column-1_4",
				"std" => 0,
				"options" => trx_addons_get_list_range(0, 15),
				"type" => "select"
			),
			"built" => array(
				"title" => esc_html__("Year built", 'trx_addons'),
				"desc" => wp_kses_data( __('Specify or select the year when the boat is built (only digits)', 'trx_addons') ),
				"class" => "trx_addons_column-1_4",
				"std" => "",
				"type" => "text"
			),
			"model" => array(
				"title" => esc_html__("Model", 'trx_addons'),
				"desc" => wp_kses_data( __('Boat Model - it will help you search this boat directly', 'trx_addons') ),
				"class" => "trx_addons_column-1_4",
				"std" => "",
				"type" => "text"
			),
            "manufacturer" => array(
				"title" => esc_html__("Manufacturer", 'trx_addons'),
				"desc" => wp_kses_data( __('Boat Manufacturer:', 'trx_addons') ),
				"class" => "trx_addons_column-1_4",
				"std" => "",
				"type" => "text"
			),

			"map_section" => array(
				"title" => esc_html__('Map and Location', 'trx_addons'),
				"desc" => wp_kses_data( __('Address and location on the map', 'trx_addons') ),
				"type" => "section"
			),
			"address" => array(
				"title" => esc_html__("Location Boat", 'trx_addons'),
				"desc" => wp_kses_data( __('Place where the boat is located', 'trx_addons') ),
				"class" => "trx_addons_column-1_2",
				"std" => "",
				"type" => "text"
			),
			"zip" => array(
				"title" => esc_html__("Zip", 'trx_addons'),
				"desc" => wp_kses_data( __('Zip code', 'trx_addons') ),
				"class" => "trx_addons_column-1_2",
				"std" => "",
				"type" => "text"
			),
			"show_map" => array(
				"title" => esc_html__("Show map", 'trx_addons'),
				"desc" => wp_kses_data( __("Show map on the boat's single page", 'trx_addons') ),
				"std" => "0",
				"type" => "checkbox"
			),
			"marker" => array(
				"title" => esc_html__("Map marker", 'trx_addons'),
				"desc" => wp_kses_data( __("Select image to represent this property on the map. If empty - use marker from 'Property type' or default marker", 'trx_addons') ),
				"std" => "",
				"dependency" => array(
					"show_map" => array(1)
				),
				"type" => "image"
			),
			"location" => array(
				"title" => esc_html__("Map location", 'trx_addons'),
				"desc" => wp_kses_data( __('Click on the map or drag marker or find location by address', 'trx_addons') ),
				"std" => "",
				"type" => "map"
			),

			"gallery_section" => array(
				"title" => esc_html__('Gallery', 'trx_addons'),
				"desc" => wp_kses_data( __('Images gallery for this property', 'trx_addons') ),
				"type" => "section"
			),
			"gallery" => array(
				"title" => esc_html__("Images gallery", 'trx_addons'),
				"desc" => wp_kses_data( __("Select images to create gallery on the single page of this property", 'trx_addons') ),
				"std" => "",
				"multiple" => true,
				"type" => "image"
			),
			"video" => array(
				"title" => esc_html__("Video", 'trx_addons'),
				"desc" => wp_kses_data( __('Specify URL with boats video from popular video hosting (Youtube, Vimeo)', 'trx_addons') ),
				"std" => "",
				"type" => "text"
			),
			"video_description" => array(
				"title" => esc_html__("Description", 'trx_addons'),
				"desc" => wp_kses_data( __('Specify short description to the video above', 'trx_addons') ),
				"dependency" => array(
					"video" => array("not_empty")
				),
				"std" => "",
				"type" => "textarea"
			),
			"virtual_tour" => array(
				"title" => esc_html__("Virtual Tour", 'trx_addons'),
				"desc" => wp_kses_data( __('Enter virtual tour embeded code', 'trx_addons') ),
				"std" => "",
				"type" => "textarea"
			),
			"virtual_tour_description" => array(
				"title" => esc_html__("Description", 'trx_addons'),
				"desc" => wp_kses_data( __('Specify short description to the virtual tour above', 'trx_addons') ),
				"dependency" => array(
					"virtual_tour" => array("not_empty")
				),
				"std" => "",
				"type" => "textarea"
			),
			"attachments" => array(
				"title" => esc_html__("Attachments", 'trx_addons'),
				"desc" => wp_kses_data( __("Select additional files to attach its to this property", 'trx_addons') ),
				"std" => "",
				"multiple" => true,
				"type" => "media"
			),
			"attachments_description" => array(
				"title" => esc_html__("Description", 'trx_addons'),
				"desc" => wp_kses_data( __('Specify short description to the attachments above', 'trx_addons') ),
				"dependency" => array(
					"attachments" => array("not_empty")
				),
				"std" => "",
				"type" => "textarea"
			),

			"agent_section" => array(
				"title" => esc_html__('Agent', 'trx_addons'),
				"desc" => wp_kses_data( __('What display in the Agent information block?', 'trx_addons') ),
				"type" => "section"
			),
			"agent_type" => array(
				"title" => esc_html__("Agent type", 'trx_addons'),
				"desc" => wp_kses_data( __("What display in the Agent information block?", 'trx_addons') ),
				"std" => "agent",
				"options" => array(
					"agent" => esc_html__('Agent', 'trx_addons'),
					"author" => esc_html__('Author', 'trx_addons'),
					"none" => esc_html__('Hide block', 'trx_addons')
				),
				"type" => "radio"
			),
			"agent" => array(
				"title" => esc_html__("Select agent", 'trx_addons'),
				"desc" => wp_kses_data( __("Select agent", 'trx_addons') ),
				"std" => "0",
				"options" => array(),
				"dependency" => array(
					"agent_type" => array("agent")
				),
				"type" => "select"
			)
		));
		
		// Register post type and taxonomy
		register_post_type(
			BASEKIT_ADDONS_CPT_BOATS_PT,
			apply_filters('trx_addons_filter_register_post_type',
				array(
					'label'               => esc_html__( 'Boats', 'trx_addons' ),
					'description'         => esc_html__( 'Boat Description', 'trx_addons' ),
					'labels'              => array(
						'name'                => esc_html__( 'Boats', 'trx_addons' ),
						'singular_name'       => esc_html__( 'Boat', 'trx_addons' ),
						'menu_name'           => esc_html__( 'Boats', 'trx_addons' ),
						'parent_item_colon'   => esc_html__( 'Parent Item:', 'trx_addons' ),
						'all_items'           => esc_html__( 'All Boats', 'trx_addons' ),
						'view_item'           => esc_html__( 'View Boat', 'trx_addons' ),
						'add_new_item'        => esc_html__( 'Add New Boat', 'trx_addons' ),
						'add_new'             => esc_html__( 'Add New', 'trx_addons' ),
						'edit_item'           => esc_html__( 'Edit Boat', 'trx_addons' ),
						'update_item'         => esc_html__( 'Update Boat', 'trx_addons' ),
						'search_items'        => esc_html__( 'Search Boat', 'trx_addons' ),
						'not_found'           => esc_html__( 'Not found', 'trx_addons' ),
						'not_found_in_trash'  => esc_html__( 'Not found in Trash', 'trx_addons' ),
					),
					'taxonomies'          => array(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_AMENITIES,
                                                   TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_SPECIFICATIONS,
                                                   TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_CREW,
                                                   TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE,
                                                   TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_LOCATION,
												   ),
                    'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments'),
					'public'              => true,
					'hierarchical'        => false,
					'has_archive'         => true,
					'can_export'          => true,
					'show_in_admin_bar'   => true,
					'show_in_menu'        => true,
					'menu_position'       => '53.3',
					'menu_icon'			  => 'dashicons-palmtree',
					'capability_type'     => 'post',
					'rewrite'             => array( 'slug' => 'boats' )
				),
				BASEKIT_ADDONS_CPT_BOATS_PT
			)
		);
	}
}

// Add 'Boats' parameters in the ThemeREX Addons Options
if (!function_exists('basekit_addons_cpt_boats_options')) {
	add_filter( 'trx_addons_filter_options', 'basekit_addons_cpt_boats_options');
	function basekit_addons_cpt_boats_options($options) {

		trx_addons_array_insert_after($options, 'cpt_section', basekit_addons_cpt_boats_get_list_options());
		return $options;
	}
}


// Return parameters list for plugin's options
if (!function_exists('basekit_addons_cpt_boats_get_list_options')) {
	function basekit_addons_cpt_boats_get_list_options($add_parameters=array()) {
		return apply_filters('trx_addons_cpt_list_options', array(
			'boats_info' => array(
				"title" => esc_html__('Boats', 'trx_addons'),
				"desc" => wp_kses_data( __('Settings of the boats page', 'trx_addons') ),
				"type" => "info"
			),
			'boats_blog_style' => array(
				"title" => esc_html__('Blog archive style', 'trx_addons'),
				"desc" => wp_kses_data( __('Style of the boats archive', 'trx_addons') ),
				"std" => 'default',
				"options" => apply_filters('trx_addons_filter_cpt_archive_styles',
											trx_addons_components_get_allowed_layouts('cpt', 'boats', 'arh'),
											BASEKIT_ADDONS_CPT_BOATS_PT),
                'type'       => 'hidden',
			),
			'boats_single_style' => array(
				"title" => esc_html__('Single boats style', 'trx_addons'),
				"desc" => wp_kses_data( __("Style of the single boats's page", 'trx_addons') ),
				"std" => 'default',
				"options" => apply_filters('trx_addons_filter_cpt_single_styles', array(
					'default' => esc_html__('Default', 'trx_addons'),
				), BASEKIT_ADDONS_CPT_BOATS_PT),
				"type" => "select"
			),
			'boats_marker' => array(
				"title" => esc_html__('Default marker', 'trx_addons'),
				"desc" => wp_kses_data( __('Default marker to show boats on the map', 'trx_addons') ),
				"std" => '',
				"type" => "image"
			)
		), 'boats');
	}
}


// Fill 'options' arrays when its need in the admin mode
if (!function_exists('trx_addons_cpt_boats_before_show_options')) {
	add_filter('trx_addons_filter_before_show_options', 'trx_addons_cpt_boats_before_show_options', 10, 2);
	function trx_addons_cpt_boats_before_show_options($options, $post_type, $group='') {
		if ($post_type == BASEKIT_ADDONS_CPT_BOATS_PT) {
			foreach ($options as $id=>$field) {
				 if ($id == 'agent') {
					$options[$id]['options'] = trx_addons_get_list_posts(false, array(
																'post_type' => TRX_ADDONS_CPT_AGENTS_PT,
																'orderby' => 'title',
																'order' => 'ASC'
																)
														);
				}
			}
		}
		return $options;
	}
}


// Save some parameters (like 'price', 'agent', 'id', 'cabins', etc.) for search and sorting
// and store 'country', 'state', 'city' and 'neighborhood' as post's terms
if ( !function_exists( 'trx_addons_cpt_boats_save_post_options' ) ) {
	add_filter('trx_addons_filter_save_post_options', 'trx_addons_cpt_boats_save_post_options', 10, 3);
	function trx_addons_cpt_boats_save_post_options($options, $post_id, $post_type) {
		if ($post_type == BASEKIT_ADDONS_CPT_BOATS_PT) {
			global $post;
			// Update post meta and post terms for search and sort
			update_post_meta($post_id, 'trx_addons_boats_price', $options['price']);
			update_post_meta($post_id, 'trx_addons_boats_length', $options['length']);
			update_post_meta($post_id, 'trx_addons_boats_charter_guest', $options['charter_guest']);
			update_post_meta($post_id, 'trx_addons_boats_cabins', $options['cabins']);
			update_post_meta($post_id, 'trx_addons_boats_number_of_crew', $options['number_of_crew']);
			update_post_meta($post_id, 'trx_addons_boats_id', $options['model']);
			update_post_meta($post_id, 'trx_addons_boats_zip', $options['zip']);
			update_post_meta($post_id, 'trx_addons_boats_address', $options['address']);
			update_post_meta($post_id, 'trx_addons_boats_agent', $options['agent_type']=='none'
																		? 0 
																		: ($options['agent_type']=='agent'
																			? $options['agent']
																			: -get_the_author_meta('ID', !empty($post->ID) && $post->ID==$post_id
																				? $post->post_author
																				: false)
																			)
							);
			// Update min and max values of the cabins, charter_guest, area, price, etc.
			trx_addons_cpt_boats_update_min_max();
		}
		return $options;
	}
}


// Update min and max values of the cabins, charter_guest, area, price, etc.
if ( !function_exists( 'trx_addons_cpt_boats_update_min_max' ) ) {
	function trx_addons_cpt_boats_update_min_max() {
		global $wpdb;
		$rez = $wpdb->get_results( "SELECT min(cabins.meta_value+0) as bed_min, max(cabins.meta_value+0) as bed_max,
										 min(charter_guest.meta_value+0) as bath_min, max(charter_guest.meta_value+0) as bath_max,
										 min(area.meta_value+0.0) as area_min, max(area.meta_value+0.0) as area_max,
										 min(price.meta_value+0.0) as price_min, max(price.meta_value+0.0) as price_max
									FROM {$wpdb->posts}
										INNER JOIN {$wpdb->postmeta} AS cabins ON {$wpdb->posts}.ID = cabins.post_id
										INNER JOIN {$wpdb->postmeta} AS charter_guest ON {$wpdb->posts}.ID = charter_guest.post_id
										INNER JOIN {$wpdb->postmeta} AS area ON {$wpdb->posts}.ID = area.post_id
										INNER JOIN {$wpdb->postmeta} AS price ON {$wpdb->posts}.ID = price.post_id
									WHERE 1=1
										AND ({$wpdb->posts}.post_status='publish')
										AND cabins.meta_key='trx_addons_boats_cabins'
										AND charter_guest.meta_key='trx_addons_boats_charter_guest'
										AND area.meta_key='trx_addons_boats_length'
										AND price.meta_key='trx_addons_boats_price'",
									ARRAY_A
									);
		update_option('trx_addons_boats_min_max', $rez[0]);
	}
}


// Return min and max values of the cabins, charter_guest, area, price, etc.
if ( !function_exists( 'trx_addons_cpt_boats_get_min_max' ) ) {
	function trx_addons_cpt_boats_get_min_max($key='') {
		static $min_max=false;
		if ($min_max === false)
			$min_max = array_merge(array(
									'area_min' => 0,
									'area_max' => 1000,
									'price_min' => 0,
									'price_max' => 1000000
									),
								get_option('trx_addons_boats_min_max', array())
								);
		return empty($key) ? $min_max : $min_max[$key];
	}
}

	
// Load required styles and scripts for the frontend
if ( !function_exists( 'trx_addons_cpt_boats_load_scripts_front' ) ) {
	add_action("wp_enqueue_scripts", 'trx_addons_cpt_boats_load_scripts_front');
	function trx_addons_cpt_boats_load_scripts_front() {
		wp_enqueue_style( 'trx_addons-cpt_boats', basekit_addons_get_file_url(BASEKIT_ADDONS_PLUGIN_CPT . 'boats/boats.css'), array(), null );
		wp_enqueue_script('trx_addons-cpt_boats', basekit_addons_get_file_url(BASEKIT_ADDONS_PLUGIN_CPT . 'boats/boats.js'), array('jquery'), null, true );
	}
}

// Load responsive styles for the frontend
if ( !function_exists( 'trx_addons_cpt_boats_load_responsive_styles' ) ) {
	add_action("wp_enqueue_scripts", 'trx_addons_cpt_boats_load_responsive_styles', 2000);
	function trx_addons_cpt_boats_load_responsive_styles() {
		wp_enqueue_style( 'trx_addons-cpt_boats-responsive', basekit_addons_get_file_url(BASEKIT_ADDONS_PLUGIN_CPT . 'boats/boats.responsive.css'), array(), null );
	}
}


// Load required styles and scripts for the backend
if ( !function_exists( 'trx_addons_cpt_boats_load_scripts_admin' ) ) {
	add_action("admin_enqueue_scripts", 'trx_addons_cpt_boats_load_scripts_admin');
	function trx_addons_cpt_boats_load_scripts_admin() {
		wp_enqueue_script('trx_addons-cpt_boats', trx_addons_get_file_url(BASEKIT_ADDONS_PLUGIN_CPT . 'boats/boats.admin.js'), array('jquery'), null, true );
	}
}


// Return true if it's boats page
if ( !function_exists( 'trx_addons_is_boats_page' ) ) {
	function trx_addons_is_boats_page() {
		return defined('BASEKIT_ADDONS_CPT_BOATS_PT')
					&& !is_search()
					&& (
						(is_single() && get_post_type()==BASEKIT_ADDONS_CPT_BOATS_PT)
						|| is_post_type_archive(BASEKIT_ADDONS_CPT_BOATS_PT)
						|| is_tax(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_CREW)
						|| is_tax(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_SPECIFICATIONS)
						|| is_tax(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_AMENITIES)
						|| is_tax(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE)
						|| is_tax(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_LOCATION)
						);
	}
}

// Return taxonomy for the current post type
if ( !function_exists( 'trx_addons_cpt_boats_post_type_taxonomy' ) ) {
	add_filter( 'trx_addons_filter_post_type_taxonomy',	'trx_addons_cpt_boats_post_type_taxonomy', 10, 2 );
	function trx_addons_cpt_boats_post_type_taxonomy($tax='', $post_type='') {
		if ( defined('BASEKIT_ADDONS_CPT_BOATS_PT') && $post_type == BASEKIT_ADDONS_CPT_BOATS_PT )
			$tax = TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE;
		return $tax;
	}
}


// Return link to the all posts for the breadcrumbs
if ( !function_exists( 'trx_addons_cpt_boats_get_blog_all_posts_link' ) ) {
	add_filter('trx_addons_filter_get_blog_all_posts_link', 'trx_addons_cpt_boats_get_blog_all_posts_link', 10, 2);
	function trx_addons_cpt_boats_get_blog_all_posts_link($link='', $args=array()) {
		if ($link=='') {
			if (trx_addons_is_boats_page()
				&& (!is_post_type_archive(BASEKIT_ADDONS_CPT_BOATS_PT) || (int) trx_addons_get_value_gp('compare') == 1)) {
				if (($url = get_post_type_archive_link( BASEKIT_ADDONS_CPT_BOATS_PT )) != '') {
					$obj = get_post_type_object(BASEKIT_ADDONS_CPT_BOATS_PT);
					$link = '<a href="'.esc_url($url).'">' . esc_html($obj->labels->all_items) . '</a>';
				}
			}
		}
		return $link;
	}
}


// Return current page title
if ( !function_exists( 'trx_addons_cpt_boats_get_blog_title' ) ) {
	add_filter( 'trx_addons_filter_get_blog_title', 'trx_addons_cpt_boats_get_blog_title');
	function trx_addons_cpt_boats_get_blog_title($title='') {
		if ( defined('BASEKIT_ADDONS_CPT_BOATS_PT') && is_post_type_archive(BASEKIT_ADDONS_CPT_BOATS_PT) && (int) trx_addons_get_value_gp('compare') == 1) {
			$title = esc_html__('Compare Boats', 'trx_addons');
		}
		return $title;
	}
}


// Parse query params from GET/POST and wp_query_parameters
if ( !function_exists( 'trx_addons_cpt_boats_query_params' ) ) {
	function trx_addons_cpt_boats_query_params($params=array()) {
		$q_obj = get_queried_object();
		if ( ($value = trx_addons_get_value_gp('boats_keyword')) != '' )	$params['boats_keyword'] = sanitize_text_field($value);

		if ( is_tax(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE))					$params['boat_type'] = (int) $q_obj->term_id;
		else if ( ($value = trx_addons_get_value_gp('boat_type')) > 0 )	$params['boat_type'] = (int) $value;


		if ( is_tax(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_LOCATION))					$params['boats_location'] = (int) $q_obj->term_id;
		else if ( ($value = trx_addons_get_value_gp('boats_location')) > 0 )	$params['boats_location'] = (int) $value;



        if ( is_tax(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_CREW))					$params['boats_crew'] = (int) $q_obj->term_id;
        else if ( ($value = trx_addons_get_value_gp('boats_crew')) > 0 )	$params['boats_crew'] = (int) $value;


		if ( ($value = trx_addons_get_value_gp('boats_length')) != '' )		$params['boats_length'] = sanitize_text_field($value);
		if ( ($value = trx_addons_get_value_gp('boats_price')) != '' )		$params['boats_price'] = sanitize_text_field($value);
		// Collect boats_features_xxx to the single param
		foreach ($_GET as $k=>$v) {
			if ( strpos($k, 'boats_amenities') === 0 ) {
				if (!isset($params['boats_amenities'])) $params['boats_amenities'] = array();
				$params['boats_amenities'][] = (int) $v;
			}
			if ( strpos($k, 'boats_specification') === 0 ) {
				if (!isset($params['boats_specification'])) $params['boats_specification'] = array();
				$params['boats_specification'][] = (int) $v;
			}

		}
		return $params;
	}
}


// Make new query to search boats or return $wp_query object if haven't search parameters
if ( !function_exists( 'trx_addons_cpt_boats_query_params_to_args' ) ) {
	function trx_addons_cpt_boats_query_params_to_args($params=array(), $new_query=false) {
		$params = trx_addons_cpt_boats_query_params($params);

		$args = $keywords = array();


		// Other params
		if (!empty($params['boats_agent']))
			$args = trx_addons_query_add_meta($args, 'trx_addons_boats_agent', $params['boats_agent']);


		if (!empty($params['boats_location']))
			$args = trx_addons_query_add_taxonomy($args, TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_LOCATION, $params['boats_location']);

		if (!empty($params['boats_type']))
			$args = trx_addons_query_add_taxonomy($args, TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE, $params['boats_type']);

		if (!empty($params['boats_crew']))
			$args = trx_addons_query_add_taxonomy($args, TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_CREW, $params['boats_crew']);

		if (!empty($params['boats_amenities']))
			foreach ($params['boats_amenities'] as $v)
				$args = trx_addons_query_add_taxonomy($args, TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_AMENITIES, $v);

		if (!empty($params['boats_specification']))
			foreach ($params['boats_specification'] as $v)
				$args = trx_addons_query_add_taxonomy($args, TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_SPECIFICATIONS, $v);
		if (!empty($params['boats_length']))
			if ($params['boats_length']!=trx_addons_cpt_boats_get_min_max('area_min').','.trx_addons_cpt_boats_get_min_max('area_max'))
				$args = trx_addons_query_add_meta($args, 'trx_addons_boats_length', $params['boats_length']);
		if (!empty($params['boats_price']))
			if ($params['boats_price']!=trx_addons_cpt_boats_get_min_max('price_min').','.trx_addons_cpt_boats_get_min_max('price_max'))
				$args = trx_addons_query_add_meta($args, 'trx_addons_boats_price', $params['boats_price']);
		if (!empty($params['boats_keyword'])) {
			// Search by any string
			 $args['s'] = $params['boats_keyword'];
		}
		if (!empty($params['boats_order'])) {
			$args['order'] = strpos($params['boats_order'], '_desc') !== false ? 'desc' : 'asc';
			$params['boats_order'] = str_replace(array('_asc', '_desc'), '' , $params['boats_order']);
			if ($params['boats_order'] == 'price') {
				$args['meta_key'] = 'trx_addons_boats_price';
				$args['orderby'] = 'meta_value_num';
			} else if (in_array($params['boats_order'], array('title', 'post_title')))
				$args['orderby'] = 'title';
			else if (in_array($params['boats_order'], array('date', 'post_date')))
				$args['orderby'] = 'date';
			else if ($params['boats_order'] == 'rand')
				$args['orderby'] = 'rand';
		}

		// Add keywords
		if (!empty($keywords)) {
			if (empty($args['meta_query']))
				$args['meta_query'] = $keywords;
			else
				$args['meta_query'] = array(
											'relation' => 'AND',
											$keywords,
											array(
												$args['meta_query']
											)
										);
		}

		// Prepare args for new query (not in 'pre_query')
		if ($new_query) {	// && count($args) > 0) {
			$args = array_merge(array(
						'post_type' => BASEKIT_ADDONS_CPT_BOATS_PT,
						'post_status' => current_user_can('read_private_pages') && current_user_can('read_private_posts') 
											? array('publish', 'private') 
											: 'publish'
					), $args);
			$page_number = get_query_var('paged') 
								? get_query_var('paged') 
								: (get_query_var('page') 
									? get_query_var('page') 
									: 1);
			if ($page_number > 1) {
				$args['paged'] = $page_number;
				$args['ignore_sticky_posts'] = true;
			}
			$ppp = get_option('posts_per_page');
			if ((int) $ppp == 0) $ppp = 10;
			$args['posts_per_page'] = (int) $ppp;
		}
		return $args;
	}
}


// Add query vars to filter posts
if (!function_exists('trx_addons_cpt_boats_pre_get_posts')) {
	add_action( 'pre_get_posts', 'trx_addons_cpt_boats_pre_get_posts' );
	function trx_addons_cpt_boats_pre_get_posts($query) {
		if (!$query->is_main_query()) return;

		if ($query->get('post_type') == BASEKIT_ADDONS_CPT_BOATS_PT) {
			
			// Filters and sort for the admin lists
			if (is_admin()) {
				$agent = trx_addons_get_value_gp('agent');
				if ((int) $agent < 0)
					$query->set('post_author', -$agent);
				else if ((int) $agent > 0) {
					$query->set('meta_key', 'trx_addons_boats_agent');
					$query->set('meta_value', $agent);
				}

			// Filters and sort for the foreground lists
			} else {
				$args = trx_addons_cpt_boats_query_params_to_args(array(), (int) trx_addons_get_value_gp('boats_query'));
				if (is_array($args) && count($args) > 0) {
					foreach ($args as $k=>$v)
						$query->set($k, $v);
				} else if ((int) trx_addons_get_value_gp('compare') == 1) {
					$posts = array();
					$list = urldecode(trx_addons_get_value_gpc('trx_addons_boats_compare_list', ''));
					$list = !empty($list) ? json_decode($list, true) : array();
					if (is_array($list)) {
						foreach ($list as $k=>$v) {
							$id = (int) str_replace('id_', '', $k);
							if ($id > 0) $posts[] = $id;
						}
					}
					if (count($posts) > 0) {
						$query->set('post__in', $posts);
					}
				}
			}
		}
	}
}


// Replace standard theme templates
//-------------------------------------------------------------

// Change standard single template for boats posts
if ( !function_exists( 'trx_addons_cpt_boats_single_template' ) ) {
	add_filter('single_template', 'trx_addons_cpt_boats_single_template');
	function trx_addons_cpt_boats_single_template($template) {
		global $post;
		if (is_single() && $post->post_type == BASEKIT_ADDONS_CPT_BOATS_PT)
			$template = basekit_addons_get_file_dir(BASEKIT_ADDONS_PLUGIN_CPT . 'boats/tpl.boats.single.php');
		return $template;
	}
}

// Change standard archive template for boats posts
if ( !function_exists( 'trx_addons_cpt_boats_archive_template' ) ) {
	add_filter('archive_template',	'trx_addons_cpt_boats_archive_template');
	function trx_addons_cpt_boats_archive_template( $template ) {
		if ( is_post_type_archive(BASEKIT_ADDONS_CPT_BOATS_PT) ) {
			if ((int) trx_addons_get_value_gp('compare') == 1)
				$template = basekit_addons_get_file_dir(BASEKIT_ADDONS_PLUGIN_CPT . 'boats/tpl.boats.compare.php');
			else
				$template = basekit_addons_get_file_dir(BASEKIT_ADDONS_PLUGIN_CPT . 'boats/tpl.boats.archive.php');
		}
		return $template;
	}	
}

// Change standard taxonomy template for boats posts
if ( !function_exists( 'trx_addons_cpt_boats_taxonomy_template' ) ) {
	add_filter('taxonomy_template',	'trx_addons_cpt_boats_taxonomy_template');
	function trx_addons_cpt_boats_taxonomy_template( $template ) {
		if ( is_tax(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_CREW)
            || is_tax(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_SPECIFICATIONS)
            || is_tax(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_AMENITIES)
            || is_tax(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE)
            || is_tax(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_LOCATION)
		) {
			$template = basekit_addons_get_file_dir(TRX_ADDONS_PLUGIN_CPT . 'boats/tpl.boats.archive.php');
		}
		return $template;
	}	
}

// Show related posts
if ( !function_exists( 'trx_addons_cpt_boats_related_posts_after_article' ) ) {
	add_action('trx_addons_action_after_article', 'trx_addons_cpt_boats_related_posts_after_article', 20, 1);
	function trx_addons_cpt_boats_related_posts_after_article( $mode ) {
		if ($mode == 'boats.single' && apply_filters('trx_addons_filter_show_related_posts_after_article', true)) {
			do_action('trx_addons_action_related_posts', $mode);
		}
	}
}

if ( !function_exists( 'trx_addons_cpt_boats_related_posts_show' ) ) {
	add_filter('trx_addons_filter_show_related_posts', 'trx_addons_cpt_boats_related_posts_show');
	function trx_addons_cpt_boats_related_posts_show( $show ) {
		if (!$show && is_single() && get_post_type() == BASEKIT_ADDONS_CPT_BOATS_PT) {
			do_action('trx_addons_action_related_posts', 'boats.single');
			$show = true;
		}
		return $show;
	}
}
if ( !function_exists( 'trx_addons_cpt_boats_related_posts' ) ) {
    add_action('trx_addons_action_related_posts', 'trx_addons_cpt_boats_related_posts', 10, 1);
    function trx_addons_cpt_boats_related_posts( $mode ) {
        if ($mode == 'boats.single') {
            $trx_addons_related_style   = explode('_', trx_addons_get_option('boats_blog_style'));
            $trx_addons_related_type    = $trx_addons_related_style[0];
            $trx_addons_related_columns = empty($trx_addons_related_style[1]) ? 1 : max(1, $trx_addons_related_style[1]);

            trx_addons_get_template_part('templates/tpl.posts-related.php',
                'trx_addons_args_related',
                apply_filters('trx_addons_filter_args_related', array(
                        'class' => 'boats_page_related sc_boats sc_boats_'.esc_attr($trx_addons_related_type),
                        'posts_per_page' => $trx_addons_related_columns,
                        'columns' => $trx_addons_related_columns,
                        'template' => BASEKIT_ADDONS_PLUGIN_CPT . 'boats/tpl.boats.'.trim($trx_addons_related_type).'-item.php',
                        'template_args_name' => 'trx_addons_args_sc_boats',
                        'template_get_file_dir' => 'basekit_addons_get_file_dir',
                        'post_type' => BASEKIT_ADDONS_CPT_BOATS_PT,
                        'taxonomies' => array(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE)
                    )
                )
            );
        }
    }
}

// Fill custom columns in the posts list
if (!function_exists('trx_addons_cpt_boats_fill_custom_column')) {
	add_action('manage_'.trx_addons_cpt_param('boats', 'post_type').'_posts_custom_column', 'trx_addons_cpt_boats_fill_custom_column', 9, 2);
	function trx_addons_cpt_boats_fill_custom_column($column_name='', $post_id=0) {
		static $meta_buffer = array();
		if (empty($meta_buffer[$post_id])) $meta_buffer[$post_id] = get_post_meta($post_id, 'trx_addons_options', true);
		$meta = $meta_buffer[$post_id];
		if ($column_name == 'cpt_boats_image') {
			$image = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), trx_addons_get_thumb_size('masonry') );
			if (!empty($image[0])) {
				?><img class="trx_addons_cpt_column_image_preview trx_addons_cpt_boats_image_preview"
						src="<?php echo esc_url($image[0]); ?>" 
						alt="<?php esc_attr_e('Boat preview', 'trx_addons'); ?>"<?php
						if (!empty($image[1])) echo ' width="'.intval($image[1]).'"';
						if (!empty($image[2])) echo ' height="'.intval($image[2]).'"';
				?>><?php
			}
		} else if ($column_name == 'cpt_boats_id') {
			if (!empty($meta['model'])) {
				?><div class="trx_addons_meta_row">
					<span class="trx_addons_meta_data"><?php echo esc_html($meta['model']); ?></span>
				</div><?php
			}
		} else if ($column_name == 'cpt_boats_price') {
			if (!empty($meta['price'])) {
				?><div class="trx_addons_meta_row">
					<span class="trx_addons_meta_label"><?php echo wp_kses_post(trx_addons_prepare_macros($meta['before_price'])); ?></span>
					<span class="trx_addons_meta_data"><?php echo esc_html(trx_addons_format_price($meta['price'])); ?></span>
				</div><?php
			}
		} else if ($column_name == 'cpt_boats_details') {
			?><div class="trx_addons_meta_row">
				<span class="trx_addons_meta_label"><?php esc_html_e('Published', 'trx_addons'); ?></span>
				<span class="trx_addons_meta_label"><?php echo esc_html(get_the_date()); ?></span>
			</div><?php
			?><div class="trx_addons_meta_row">
				<span class="trx_addons_meta_label"><?php esc_html_e('by', 'trx_addons'); ?></span>
				<span class="trx_addons_meta_label"><?php the_author(); ?></span>
			</div><?php
			if ($meta['agent_type']!='none' && ($meta['agent_type']=='author' || $meta['agent']!=0)) {
				?><div class="trx_addons_meta_row">
					<span class="trx_addons_meta_label"><?php esc_html_e('Agent', 'trx_addons'); ?></span>
					<span class="trx_addons_meta_label"><a href="<?php
							echo esc_url(get_admin_url(null, 'edit.php?post_type='.BASEKIT_ADDONS_CPT_BOATS_PT
										.'&agent='.intval($meta['agent']))); 
							?>"><?php
							if ($meta['agent_type']=='author')
								the_author();
							else
								echo esc_html(get_the_title($meta['agent']));
						?></a>
					</span>
				</div><?php
			}
		}
	}
}

// Return social icons links from array
// $show = icons|names|icons_names
if (!function_exists('basekit_addons_get_socials_links_custom')) {
    function basekit_addons_get_socials_links_custom($icons, $style='', $show='icons') {
        if (empty($style)) {
            $socials_type = trx_addons_get_setting('socials_type');
            $style = $socials_type=='images' ? 'bg' : ($socials_type == 'svg' ? 'svg' : 'icons');
        }
        $output = '';
        if (is_string($icons)) {
            $tmp = explode("\n", $icons);
            $icons = array();
            foreach ($tmp as $str) {
                $tmp2 = explode("=", trim(chop($str)));
                if (count($tmp2)==2) {
                    $icons[] = array(
                        'name' => (strpos($tmp2[0], 'icon-')===false ? 'trx_addons_icon-' : '') . trim($tmp2[0]),
                        'url' => trim($tmp2[1])
                    );
                }
            }
        }
        $show_icons = strpos($show, 'icons') !== false;
        $show_names = strpos($show, 'names') !== false;
        if (is_array($icons) && !empty($icons[0])) {
            foreach ($icons as $social) {
                $sn = $social['name'];
                $fn = $style=='icons' ? trx_addons_clear_icon_name($sn) : trx_addons_get_file_name($sn);
                $title = !empty($social['title']) ? $social['title'] : ucfirst(str_replace('-circled', '', $fn));
                $url = $social['url'];
                if (empty($url) || strtolower($sn)=='none') continue;
                $output .= '<a target="_blank" href="'.(strpos($url, 'mailto:')!==false || strpos($url, 'skype:')!==false
                        ? esc_attr($url)
                        : esc_url($url)
                    ).'"'
                    . ' class="social_item social_item_style_'.esc_attr($style).' sc_icon_type_'.esc_attr($style).' social_item_type_'.esc_attr($show).'">'
                    . ($show_icons
                        ? '<span class="social_icon social_icon_'.esc_attr($fn).'"'
                        . ($style=='bg' ? ' style="background-image: url('.esc_url($sn).');"' : '')
                        . '>'
                        . ($style=='icons'
                            ? '<span class="' . esc_attr($sn) . '"></span>'
                            : ($style=='svg'
                                ? trx_addons_get_svg_from_file($sn)
                                : ($style=='images'
                                    ? '<img src="'.esc_url($sn).'" alt="'.esc_attr($title).'" />'
                                    : '<span class="social_hover" style="background-image: url('.esc_url($sn).');"></span>'
                                )
                            )
                        )
                        . '</span>'
                        : '')
                    . ($show_names
                        ? '<span class="social_name social_'.esc_attr($fn).'">' . esc_html($title) . '</span>'
                        : '')
                    . '</a>';
            }
        }
        return $output;
    }
}


// AJAX handlers of the 'send_form' action
//-----------------------------------------------------------------
if ( !function_exists( 'trx_addons_cpt_boats_ajax_send_sc_form' ) ) {
	// Use 9 priority to early handling action (before standard handler from shortcode 'sc_form')
	add_action('wp_ajax_send_sc_form',			'trx_addons_cpt_boats_ajax_send_sc_form', 9);
	add_action('wp_ajax_nopriv_send_sc_form',	'trx_addons_cpt_boats_ajax_send_sc_form', 9);
	function trx_addons_cpt_boats_ajax_send_sc_form() {

		if ( !wp_verify_nonce( trx_addons_get_value_gp('nonce'), admin_url('admin-ajax.php') ) )
			die();
	
		parse_str($_POST['data'], $post_data);
		
		if (empty($post_data['property_agent'])) return;
		$agent_id = (int) $post_data['property_agent'];
		$agent_email = '';
		if ($agent_id > 0) {			// Agent
			$meta = get_post_meta($agent_id, 'trx_addons_options', true);
			$agent_email = $meta['email'];
		} else if ($agent_id < 0) {		// Author
			$user_id = abs($agent_id);
			$user_data = get_userdata($user_id);
			$agent_email = $user_data->user_email;
		}
		if (empty($agent_email)) return;
		
		$property_id = !empty($post_data['property_id']) ? (int) $post_data['property_id'] : 0;
		$property_title = !empty($property_id) ? get_the_title($property_id) : '';

		$response = array('error'=>'');
		
		$user_name	= !empty($post_data['name']) ? stripslashes($post_data['name']) : '';
		$user_email	= !empty($post_data['email']) ? stripslashes($post_data['email']) : '';
		$user_phone	= !empty($post_data['phone']) ? stripslashes($post_data['phone']) : '';
		$user_msg	= !empty($post_data['message']) ? stripslashes($post_data['message']) : '';
		
		// Attention! Strings below not need html-escaping, because mail is a plain text
		$subj = $property_id > 0
					? sprintf(__('Query on property "%s" from "%s"', 'trx_addons'), $property_title, $user_name)
					: sprintf(__('Query on help from "%s"', 'trx_addons'), $user_name);
		$msg = (!empty($user_name)	? "\n".sprintf(__('Name: %s', 'trx_addons'), $user_name) : '')
			.  (!empty($user_email) ? "\n".sprintf(__('E-mail: %s', 'trx_addons'), $user_email) : '')
			.  (!empty($user_phone) ? "\n".sprintf(__('Phone:', 'trx_addons'), $user_phone) : '')
			.  (!empty($user_msg)	? "\n\n".trim($user_msg) : '')
			.  "\n\n............. " . get_bloginfo('site_name') . " (" . esc_url(home_url('/')) . ") ............";

		if (is_email($agent_email) && !wp_mail($agent_email, $subj, $msg)) {
			$response['error'] = esc_html__('Error send message!', 'trx_addons');
		}
	
		echo json_encode($response);
		die();
	}
}


// Include additional files
// Attention! Must be included after the post type 'Boats' registration
//----------------------------------------------------------------------------
if ( ($fdir = basekit_addons_get_file_dir(BASEKIT_ADDONS_PLUGIN_CPT . "boats/boats.taxonomy_type.php")) != '') { include_once $fdir; }
if ( ($fdir = basekit_addons_get_file_dir(BASEKIT_ADDONS_PLUGIN_CPT . "boats/boats.taxonomy_boat_type.php")) != '') { include_once $fdir; }
if ( ($fdir = basekit_addons_get_file_dir(BASEKIT_ADDONS_PLUGIN_CPT . "boats/boats.taxonomy_specifications.php")) != '') { include_once $fdir; }
if ( ($fdir = basekit_addons_get_file_dir(BASEKIT_ADDONS_PLUGIN_CPT . "boats/boats.taxonomy_amenities.php")) != '') { include_once $fdir; }
if ( ($fdir = basekit_addons_get_file_dir(BASEKIT_ADDONS_PLUGIN_CPT . "boats/boats.taxonomy_crew.php")) != '') { include_once $fdir; }
if ( ($fdir = basekit_addons_get_file_dir(BASEKIT_ADDONS_PLUGIN_CPT . "boats/boats.taxonomy_boat_location.php")) != '') { include_once $fdir; }
if ( ($fdir = basekit_addons_get_file_dir(BASEKIT_ADDONS_PLUGIN_CPT . "boats/boats.agents.php")) != '') { include_once $fdir; }
if ( ($fdir = basekit_addons_get_file_dir(BASEKIT_ADDONS_PLUGIN_CPT . "boats/widget.boats_search.php")) != '') { include_once $fdir; }




// Add shortcodes
//----------------------------------------------------------------------------
require_once BASEKIT_ADDONS_PLUGIN_DIR . BASEKIT_ADDONS_PLUGIN_CPT . 'boats/boats-sc.php';

// Add shortcodes to Elementor
if ( trx_addons_exists_elementor() && function_exists('trx_addons_elm_init') ) {
	require_once BASEKIT_ADDONS_PLUGIN_DIR . BASEKIT_ADDONS_PLUGIN_CPT . 'boats/boats-sc-elementor.php';
}

// Add shortcodes to Gutenberg
if ( trx_addons_exists_gutenberg() && function_exists( 'trx_addons_gutenberg_get_param_id' ) ) {
	require_once BASEKIT_ADDONS_PLUGIN_DIR . BASEKIT_ADDONS_PLUGIN_CPT . 'boats/boats-sc-gutenberg.php';
}

// Add shortcodes to VC
if ( trx_addons_exists_vc() && function_exists( 'trx_addons_vc_add_id_param' ) ) {
	require_once BASEKIT_ADDONS_PLUGIN_DIR . BASEKIT_ADDONS_PLUGIN_CPT . 'boats/boats-sc-vc.php';
}
