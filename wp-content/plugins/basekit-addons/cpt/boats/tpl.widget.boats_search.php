<?php
/**
 * The style "default" of the Widget "Boats Search"
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.6.22
 */

$trx_addons_args = get_query_var('trx_addons_args_widget_boats_search');
extract($trx_addons_args);

$params = array_merge(array(
						'boats_keyword' => '',
						'boats_type' => '',
                        'boats_location' => '',
						'boats_crew' => '',
						'boats_length' => '',
						'boats_price' => '',
						'boats_amenities' => array(),
                        'boats_specification' => array(),
						), trx_addons_cpt_boats_query_params());

// Before widget (defined by themes)
trx_addons_show_layout($before_widget);
			
// Widget title if one was input (before and after defined by themes)
trx_addons_show_layout($title, $before_title, $after_title);
	
// Widget body
$form_style = $trx_addons_args['style'] = empty($trx_addons_args['style']) || trx_addons_is_inherit($trx_addons_args['style']) 
			? trx_addons_get_option('input_hover') 
			: $trx_addons_args['style'];
?><div
	class="sc_form boats_search boats_search_opened boats_search_<?php
		echo esc_attr($trx_addons_args['type']);
		if (!empty($trx_addons_args['class'])) echo ' '.esc_attr($trx_addons_args['class']);
		if (!empty($trx_addons_args['align']) && !trx_addons_is_off($trx_addons_args['align'])) echo ' sc_align_'.esc_attr($trx_addons_args['align']);
		?>"<?php
	if (!empty($trx_addons_args['css'])) echo ' style="'.esc_attr($trx_addons_args['css']).'"'; 
?>>
	<form class="boats_search_form sc_form_form sc_form_custom <?php if ($form_style != 'default') echo 'sc_input_hover_'.esc_attr($form_style); ?>" action="<?php echo esc_url(get_post_type_archive_link( BASEKIT_ADDONS_CPT_BOATS_PT )); ?>" method="get">

		<div class="boats_search_basic"><?php

			// If current page is not boats archive - make new query to show results
			?><input type="hidden" name="boats_query" value="<?php echo esc_attr(trx_addons_is_boats_page() && !is_single() ? '0' : '1');	?>"><?php
				
			// Keywords
			trx_addons_get_template_part(TRX_ADDONS_PLUGIN_SHORTCODES . 'form/tpl.form-field.php',
											'trx_addons_args_sc_form_field',
											array_merge($trx_addons_args, array(
														'labels'      => false,
														'field_name'  => 'boats_keyword',
														'field_type'  => 'text',
														'field_value' => $params['boats_keyword'],
														'field_req'   => false,
														'field_icon'  => 'trx_addons_icon-search',
														'field_title' => __('Search for', 'trx_addons'),
														'field_placeholder' => __('Search ...', 'trx_addons')
														))
										);
		
			// Show Advanced Search
			?><div class="boats_search_show_advanced trx_addons_icon-sliders" title="<?php esc_attr_e('Show advanced search', 'trx_addons'); ?>"></div><?php

			// Basic Submit button
			?><button class="boats_search_button trx_addons_icon-search" title="<?php esc_attr_e('Start search', 'trx_addons'); ?>"></button><?php
			
			
		?></div><!-- /.boats_search_basic -->

		<div class="boats_search_advanced"><?php
			
			// Boat Type
			$tax_obj = get_taxonomy(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE);
			$list = trx_addons_array_merge(array(0 => esc_html(sprintf(__(' %s ', 'trx_addons'), $tax_obj->label))),
											trx_addons_get_list_terms(false, TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE, array('hide_empty' => 1)));
			trx_addons_get_template_part(TRX_ADDONS_PLUGIN_SHORTCODES . 'form/tpl.form-field.php',
											'trx_addons_args_sc_form_field',
											array_merge($trx_addons_args, array(
														'labels'      => false,
														'field_name'  => 'boats_type',
														'field_type'  => 'select',
														'field_value' => $params['boats_type'],
														'field_req'   => false,
														'field_options'  => $list,
														'field_class' => 'trx_addons_boats_type',
														'field_data'  => array('not-selected' => 'true')
														))
										);
			// Boar Location
			$tax_obj = get_taxonomy(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_LOCATION);
			$list = trx_addons_array_merge(array(0 => esc_html(sprintf(__(' %s ', 'trx_addons'), $tax_obj->label))),
											trx_addons_get_list_terms(false, TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_LOCATION, array('hide_empty' => 1)));
			trx_addons_get_template_part(TRX_ADDONS_PLUGIN_SHORTCODES . 'form/tpl.form-field.php',
											'trx_addons_args_sc_form_field',
											array_merge($trx_addons_args, array(
														'labels'      => false,
														'field_name'  => 'boats_location',
														'field_type'  => 'select',
														'field_value' => $params['boats_location'],
														'field_req'   => false,
														'field_options'  => $list,
														'field_class' => 'trx_addons_boats_location',
														'field_data'  => array('not-selected' => 'true')
														))
										);
			// Boar CREW
			$tax_obj = get_taxonomy(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_CREW);
			$list = trx_addons_array_merge(array(0 => esc_html(sprintf(__(' %s ', 'trx_addons'), $tax_obj->label))),
											trx_addons_get_list_terms(false, TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_CREW, array('hide_empty' => 1)));
			trx_addons_get_template_part(TRX_ADDONS_PLUGIN_SHORTCODES . 'form/tpl.form-field.php',
											'trx_addons_args_sc_form_field',
											array_merge($trx_addons_args, array(
														'labels'      => false,
														'field_name'  => 'boats_crew',
														'field_type'  => 'select',
														'field_value' => $params['boats_crew'],
														'field_req'   => false,
														'field_options'  => $list,
														'field_class' => 'trx_addons_boats_crew',
														'field_data'  => array('not-selected' => 'true')
														))
										);


			// Boats Length
			trx_addons_get_template_part(TRX_ADDONS_PLUGIN_SHORTCODES . 'form/tpl.form-field.php',
											'trx_addons_args_sc_form_field',
											array_merge($trx_addons_args, array(
														'labels'      => true,
														'field_title' => __('Length', 'trx_addons'),
														'field_name'  => 'boats_length',
														'field_type'  => 'range',
														'field_value' => $params['boats_length'],
														'field_req'   => false,
														'field_min'   => trx_addons_cpt_boats_get_min_max('area_min'),
														'field_max'   => trx_addons_cpt_boats_get_min_max('area_max'),
														'field_step'  => (trx_addons_cpt_boats_get_min_max('area_max') - trx_addons_cpt_boats_get_min_max('area_min')) / 20
														))
										);

			// Price
			trx_addons_get_template_part(TRX_ADDONS_PLUGIN_SHORTCODES . 'form/tpl.form-field.php',
											'trx_addons_args_sc_form_field',
											array_merge($trx_addons_args, array(
														'labels'      => true,
														'field_title' => __('Price', 'trx_addons'),
														'field_name'  => 'boats_price',
														'field_type'  => 'range',
														'field_value' => $params['boats_price'],
														'field_req'   => false,
														'field_min'   => trx_addons_cpt_boats_get_min_max('price_min'),
														'field_max'   => trx_addons_cpt_boats_get_min_max('price_max'),
														'field_step'  => (trx_addons_cpt_boats_get_min_max('price_max') - trx_addons_cpt_boats_get_min_max('price_min')) / 20
														))



            );
			?>
            <div class="boats_amenities_search">
                <h6 class="search_title_section">AMENITIES</h6>
                <?php
                // AMENITIES
                $list = trx_addons_get_list_terms(false, TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_AMENITIES, array('hide_empty' => 1));
                if (is_array($list)) {
                    foreach ($list as $id=>$title) {
                        trx_addons_get_template_part(TRX_ADDONS_PLUGIN_SHORTCODES . 'form/tpl.form-field.php',
                                                'trx_addons_args_sc_form_field',
                                                array_merge($trx_addons_args, array(
                                                            'labels'      => true,
                                                            'field_placeholder' => $title,
                                                            'field_name'  => "boats_amenities_{$id}",
                                                            'field_type'  => 'checkbox',
                                                            'field_value' => $id,
                                                            'field_checked' => in_array($id, $params['boats_amenities']),
                                                            'field_req'   => false
                                                            ))
                                                );
                    }
                }
                ?>
            </div >
            <div class="boats_specification_search">
                <h6 class="search_title_section offset-top">SPECIFICATIONS</h6>
                <?php
                // SPECIFICATIONS
                $list = trx_addons_get_list_terms(false, TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_SPECIFICATIONS, array('hide_empty' => 1));
                if (is_array($list)) {
                    foreach ($list as $id=>$title) {
                        trx_addons_get_template_part(TRX_ADDONS_PLUGIN_SHORTCODES . 'form/tpl.form-field.php',
                                                'trx_addons_args_sc_form_field',
                                                array_merge($trx_addons_args, array(
                                                            'labels'      => true,
                                                            'field_placeholder' => $title,
                                                            'field_name'  => "boats_specification_{$id}",
                                                            'field_type'  => 'checkbox',
                                                            'field_value' => $id,
                                                            'field_checked' => in_array($id, $params['boats_specification']),
                                                            'field_req'   => false
                                                            ))
                                                );
                    }
                }

		    ?></div><!-- /.boats_search_advanced -->
        </div>
	</form>
</div><!-- /.sc_form --><?php

// After widget (defined by themes)
trx_addons_show_layout($after_widget);
