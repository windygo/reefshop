<?php
/**
 * Template of the custom slide content with Property's data for the Swiper
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.6.23
 */

$args = get_query_var('trx_addons_args_boats_slider_slide');

$meta = get_post_meta(get_the_ID(), 'trx_addons_options', true);
$link = get_permalink();

?><div class="sc_boats_slider_columns <?php echo esc_attr(trx_addons_get_columns_wrap_class()); ?>"><?php
	
	// First column with data
	?><div class="sc_boats_slider_column <?php echo esc_attr(trx_addons_get_column_class(2, 5)); ?>">
		<h4 class="sc_boats_slider_title"><?php the_title(); ?></h4>
		<ul class="sc_boats_slider_data trx_addons_list trx_addons_list_parameters"><?php
			if ( !empty($meta['model']) ) {
				?>
				<li class="sc_boats_slider_data_id">
					<strong><?php esc_html_e('Model:', 'trx_addons'); ?></strong>
					<em><?php echo esc_html($meta['model']); ?></em>
				</li>
				<?php
			}
			if ( !empty($meta['cabins']) ) {
				?>
				<li class="sc_boats_slider_data_beds">
					<strong><?php esc_html_e('Bedrooms', 'trx_addons'); ?></strong>
					<em><?php echo esc_html($meta['cabins']); ?></em>
				</li>
				<?php
			}
			if ( !empty($meta['charter_guest']) ) {
				?>
				<li class="sc_boats_slider_data_baths">
					<strong><?php esc_html_e('Bathrooms', 'trx_addons'); ?></strong>
					<em><?php echo esc_html($meta['charter_guest']); ?></em>
				</li>
				<?php
			}
			if ( !empty($meta['length']) ) {
				?>
				<li class="sc_boats_slider_data_area">
					<strong><?php esc_html_e('Area size', 'trx_addons'); ?></strong>
					<em><?php trx_addons_show_layout($meta['length']
													. ($meta['length_prefix']
															? ' ' . trx_addons_prepare_macros($meta['length_prefix'])
															: ''
														));
					?></em>
				</li>
				<?php
			}
			if ( !empty($meta['number_of_crew']) ) {
				?>
				<li class="sc_boats_slider_data_number_of_crew">
					<strong><?php esc_html_e('Garages', 'trx_addons'); ?></strong>
					<em><?php trx_addons_show_layout($meta['number_of_crew']);
					?></em>
				</li>
				<?php
			}
			if ( !empty($meta['price']) ) {
				?>
				<li class="sc_boats_slider_data_price">
					<strong><?php
						if (!empty($meta['before_price']))
							trx_addons_show_layout(trx_addons_prepare_macros($meta['before_price']));
						else
							esc_html_e('Price', 'trx_addons');
					?></strong>
					<em><?php
						if (!empty($meta['price']))
							echo esc_html(trx_addons_format_price($meta['price']));
					?></em>
				</li>
				<?php
			}
			?>
		</ul>
		<?php
		trx_addons_show_layout(trx_addons_sc_button(array(
										'link' => $link,
										'title' => esc_html__('Learn more', 'trx_addons'),
										'class' => esc_attr(apply_filters('trx_addons_filter_sc_item_link_classes', 'sc_boats_slider_data_button', 'sc_boats', $args))
										)
								));
		?>
	</div><?php

	// Second column with image
	?><div class="sc_boats_slider_column <?php echo esc_attr(trx_addons_get_column_class(3, 5)); ?>">
		<figure class="sc_boats_slider_image">
			<?php the_post_thumbnail(trx_addons_get_thumb_size('big'), array('alt' => get_the_title())); ?>
		</figure>
	</div>
</div>