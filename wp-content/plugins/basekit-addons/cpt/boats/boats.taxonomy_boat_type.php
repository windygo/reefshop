<?php
/**
 * ThemeREX Addons Custom post type: Boats (Taxonomy 'Boat Type' support)
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.6.22
 */

// Don't load directly
if ( ! defined( 'TRX_ADDONS_VERSION' ) ) {
	die( '-1' );
}

// -----------------------------------------------------------------
// -- Custom post type registration
// -----------------------------------------------------------------

// Define Custom post type and taxonomy constants for 'Boats'
if ( ! defined('TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE') )
		define('TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE', 'cpt_boat_type');

// Register post type and taxonomy
if (!function_exists('trx_addons_cpt_boats_taxonomy_boat_type_init')) {
	add_action( 'init', 'trx_addons_cpt_boats_taxonomy_boat_type_init' );
	function trx_addons_cpt_boats_taxonomy_boat_type_init() {

		register_taxonomy(
			TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE,
			BASEKIT_ADDONS_CPT_BOATS_PT,
			apply_filters('trx_addons_filter_register_taxonomy', 
				array(
					'post_type' 		=> BASEKIT_ADDONS_CPT_BOATS_PT,
					'hierarchical'      => true,
					'labels'            => array(
						'name'              => esc_html__( 'Boat Type', 'trx_addons' ),
						'singular_name'     => esc_html__( 'Boat Type', 'trx_addons' ),
						'search_items'      => esc_html__( 'Search Boats Types', 'trx_addons' ),
						'all_items'         => esc_html__( 'All Boats Types', 'trx_addons' ),
						'parent_item'       => esc_html__( 'Parent Boat Type', 'trx_addons' ),
						'parent_item_colon' => esc_html__( 'Parent Boat Type:', 'trx_addons' ),
						'edit_item'         => esc_html__( 'Edit Boat Type', 'trx_addons' ),
						'update_item'       => esc_html__( 'Update Boat Type', 'trx_addons' ),
						'add_new_item'      => esc_html__( 'Add New Boat Type', 'trx_addons' ),
						'new_item_name'     => esc_html__( 'New Type Name', 'trx_addons' ),
						'menu_name'         => esc_html__( 'Boat Type', 'trx_addons' ),
					),
					'show_ui'           => true,
					'show_admin_column' => true,
                    'meta_box_cb'		=> true,
					'query_var'         => true,
					'rewrite'           => array( 'slug' => 'boats_type-slug' )
				),
				BASEKIT_ADDONS_CPT_BOATS_PT,
				TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE
			)
		);
	}
}



// Replace standard theme templates
//-------------------------------------------------------------

// Change standard category template for boats categories (groups)
if ( !function_exists( 'trx_addons_cpt_boats_taxonomy_type_taxonomy_template' ) ) {
	add_filter('taxonomy_template',	'trx_addons_cpt_boats_taxonomy_type_taxonomy_template');
	function trx_addons_cpt_boats_taxonomy_type_taxonomy_template( $template ) {
		if ( is_tax(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE) ) {
			if (($template = basekit_addons_get_file_dir(BASEKIT_ADDONS_PLUGIN_CPT . 'boats/tpl.boats.archive_type.php')) == '')
				$template = basekit_addons_get_file_dir(BASEKIT_ADDONS_PLUGIN_CPT . 'boats/tpl.boats.archive.php');
		}
		return $template;
	}	
}

// Admin utils
// -----------------------------------------------------------------

// Show <select> with boats categories in the admin filters area
if (!function_exists('trx_addons_cpt_boats_taxonomy_type_admin_filters')) {
	add_action( 'restrict_manage_posts', 'trx_addons_cpt_boats_taxonomy_type_admin_filters' );
	function trx_addons_cpt_boats_taxonomy_type_admin_filters() {
		trx_addons_admin_filters(BASEKIT_ADDONS_CPT_BOATS_PT, TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE);
	}
}
  
// Clear terms cache on the taxonomy 'type' save
if (!function_exists('trx_addons_cpt_boats_taxonomy_type_admin_clear_cache_type')) {
	add_action( 'edited_'.TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE, 'trx_addons_cpt_boats_taxonomy_type_admin_clear_cache_type', 10, 1 );
	add_action( 'delete_'.TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE, 'trx_addons_cpt_boats_taxonomy_type_admin_clear_cache_type', 10, 1 );
	add_action( 'created_'.TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE, 'trx_addons_cpt_boats_taxonomy_type_admin_clear_cache_type', 10, 1 );
	function trx_addons_cpt_boats_taxonomy_type_admin_clear_cache_type( $term_id=0 ) {
		trx_addons_admin_clear_cache_terms(TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE);
	}
}