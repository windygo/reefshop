<?php
/**
 * ThemeREX Addons Custom post type: Boats (Shortcodes)
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.6.22
 */

// Don't load directly
if ( ! defined( 'TRX_ADDONS_VERSION' ) ) {
	die( '-1' );
}


// Prepare slides with Properties data
//----------------------------------------------------------------------------
if (!function_exists('trx_addons_cpt_boats_slider_content')) {
	add_filter('trx_addons_filter_slider_content', 'trx_addons_cpt_boats_slider_content', 10, 2);
	function trx_addons_cpt_boats_slider_content($image, $args) {
		if (get_post_type() == BASEKIT_ADDONS_CPT_BOATS_PT) {
			$image['content'] = trx_addons_get_template_part_as_string(BASEKIT_ADDONS_PLUGIN_CPT . 'boats/tpl.boats.slider-slide.php',
											'trx_addons_args_boats_slider_slide',
											compact('image', 'args')
										);
			$image['image'] = $image['link'] = $image['url'] = '';
		}
		return $image;
	}
}


// trx_sc_boats
//-------------------------------------------------------------
/*
[trx_sc_boats id="unique_id" type="default" cat="category_slug or id" count="3" columns="3" slider="0|1"]
*/
if ( !function_exists( 'trx_addons_sc_boats' ) ) {
	function trx_addons_sc_boats($atts, $content=null) {

		// Exit to prevent recursion
		if (trx_addons_sc_stack_check('trx_sc_boats')) return '';

		$atts = trx_addons_sc_prepare_atts('trx_sc_boats', $atts, trx_addons_sc_common_atts('id,title,slider,query', array(
			// Individual params
			"type" => "default",
			"boats_type" => '',
			"boats_status" => '',
			"boats_labels" => '',
			"boats_country" => '',
			"boats_state" => '',
			"boats_city" => '',
			"boats_neighborhood" => '',
			"map_height" => 350,
			"pagination" => "none",
			"page" => 1,
			"more_text" => esc_html__('Read more', 'trx_addons'),
			))
		);

		if (!empty($atts['ids'])) {
			$atts['ids'] = str_replace(array(';', ' '), array(',', ''), $atts['ids']);
			$atts['count'] = count(explode(',', $atts['ids']));
		}
		$atts['count'] = max(1, (int) $atts['count']);
		$atts['offset'] = max(0, (int) $atts['offset']);
		if (empty($atts['orderby'])) $atts['orderby'] = 'title';
		if (empty($atts['order'])) $atts['order'] = 'asc';
		$atts['slider'] = max(0, (int) $atts['slider']);
		if ($atts['slider'] > 0 && (int) $atts['slider_pagination'] > 0) $atts['slider_pagination'] = 'bottom';
		if ($atts['slider'] > 0) $atts['pagination'] = 'none';

		ob_start();
		basekit_addons_get_template_part(array(
										BASEKIT_ADDONS_PLUGIN_CPT . 'boats/tpl.boats.'.trx_addons_esc($atts['type']).'.php',
										BASEKIT_ADDONS_PLUGIN_CPT . 'boats/tpl.boats.default.php'
										),
                                        'trx_addons_args_sc_boats',
                                        $atts
                                    );
		$output = ob_get_contents();
		ob_end_clean();
		
		return apply_filters('trx_addons_sc_output', $output, 'trx_sc_boats', $atts, $content);
	}
}


// Add shortcode [trx_sc_boats]
if (!function_exists('trx_addons_sc_boats_add_shortcode')) {
	function trx_addons_sc_boats_add_shortcode() {
		add_shortcode("trx_sc_boats", "trx_addons_sc_boats");
	}
	add_action('init', 'trx_addons_sc_boats_add_shortcode', 20);
}
