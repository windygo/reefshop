<?php
/**
 * The style "default" of the Properties
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.6.22
 */


$args = get_query_var('trx_addons_args_sc_boats');
if (empty($args['slider'])) $args['slider'] = 0;

$meta = get_post_meta(get_the_ID(), 'trx_addons_options', true);
$link = get_permalink();

if ($args['slider']) {
	?><div class="slider-slide swiper-slide"><?php
} else if ($args['columns'] > 1) {
	?><div class="<?php echo esc_attr(trx_addons_get_column_class(1, $args['columns'])); ?>"><?php
}
?><div data-post-id="<?php the_ID(); ?>" class="sc_boats_item with_image<?php
	echo isset($args['hide_excerpt']) && $args['hide_excerpt'] ? ' without_content' : ' with_content';
?>">
	<?php
	// Featured image
	if ( has_post_thumbnail() ) {
		trx_addons_get_template_part('templates/tpl.featured.php',
										'trx_addons_args_featured',
										apply_filters('trx_addons_filter_args_featured', array(
															'class' => 'sc_boats_item_thumb',
															'hover' => 'zoomin',
															'thumb_size' => apply_filters('trx_addons_filter_thumb_size',
																							trx_addons_get_thumb_size($args['columns'] > 2 ? 'medium' : 'big'),
																							'boats-default'),
															), 'boats-default'
														)
									);
	}
	?><div class="sc_boats_item_info">
		<div class="sc_boats_item_header"><?php
			// Title
			?><h5 class="sc_boats_item_title entry-title"><a href="<?php echo esc_url($link); ?>"><?php the_title(); ?></a></h5><?php
			?>
		</div>
		<div class="sc_boats_item_options">
			<div class="sc_boats_item_row sc_boats_item_row_info"><?php
				// Model
				if (!empty($meta['model'])) {
					?><span class="sc_boats_item_option sc_boats_item_id" title="<?php esc_html_e('model', 'trx_addons'); ?>">
						<span class="sc_boats_item_option_label">
							<span class="sc_boats_item_option_label_icon icon-ship"></span>
							<span class="sc_boats_item_option_label_text"><?php esc_html_e('Model:', 'trx_addons'); ?></span>
						</span>
						<span class="sc_boats_item_option_data"><?php trx_addons_show_layout($meta['model']); ?></span>
					</span><?php
				}
				// Year built
				if (!empty($meta['built'])) {
					?><span class="sc_boats_item_option sc_boats_item_built" title="<?php esc_html_e('Year built', 'trx_addons'); ?>">
						<span class="sc_boats_item_option_label">
							<span class="sc_boats_item_option_label_icon icon-tools-2"></span>
							<span class="sc_boats_item_option_label_text"><?php esc_html_e('Built:', 'trx_addons'); ?></span>
						</span>
						<span class="sc_boats_item_option_data"><?php trx_addons_show_layout($meta['built']); ?></span>
					</span><?php
				}
				// Length
				if (!empty($meta['length'])) {
					?><span class="sc_boats_item_option sc_boats_item_area" title="<?php esc_html_e('Length', 'trx_addons'); ?>">
						<span class="sc_boats_item_option_label">
							<span class="sc_boats_item_option_label_icon icon-ruler"></span>
							<span class="sc_boats_item_option_label_text"><?php esc_html_e('Length:', 'trx_addons'); ?></span>
						</span>
						<span class="sc_boats_item_option_data"><?php
							trx_addons_show_layout($meta['length']
													. ($meta['length_prefix']
															? ' ' . trx_addons_prepare_macros($meta['length_prefix'])
															: ''
														)
													);
						?></span>
					</span><?php
				}
			?></div>
			<div class="sc_boats_item_row sc_boats_item_row_info"><?php
				// Cabins
				if (!empty($meta['cabins'])) {
					?><span class="sc_boats_item_option sc_boats_item_cabins" title="<?php esc_html_e('Cabins number', 'trx_addons'); ?>">
						<span class="sc_boats_item_option_label">
							<span class="sc_boats_item_option_label_icon icon-bed"></span>
							<span class="sc_boats_item_option_label_text"><?php esc_html_e('Cabins:', 'trx_addons'); ?></span>
						</span>
						<span class="sc_boats_item_option_data"><?php echo esc_html($meta['cabins']); ?></span>
					</span><?php
				}
				// Charter guest
				if (!empty($meta['charter_guest'])) {
					?><span class="sc_boats_item_option sc_boats_item_charter_guest" title="<?php esc_html_e('Charter guest number', 'trx_addons'); ?>">
						<span class="sc_boats_item_option_label">
							<span class="sc_boats_item_option_label_icon icon-users-group"></span>
							<span class="sc_boats_item_option_label_text"><?php esc_html_e('Charter guest:', 'trx_addons'); ?></span>
						</span>
						<span class="sc_boats_item_option_data"><?php echo esc_html($meta['charter_guest']); ?></span>
					</span><?php
				}
			?></div>
			<div class="sc_boats_item_row sc_boats_item_row_address"><?php
				// Address
				?><span class="sc_boats_item_option sc_boats_item_address" title="<?php esc_html_e('Address', 'trx_addons'); ?>">
					<span class="sc_boats_item_option_label">
						<span class="sc_boats_item_option_label_icon icon-placeholder"></span>
						<span class="sc_boats_item_option_label_text"><?php esc_html_e('Address:', 'trx_addons'); ?></span>
					</span>
					<span class="sc_boats_item_option_data"><?php
						basekit_addons_get_template_part(BASEKIT_ADDONS_PLUGIN_CPT . 'boats/tpl.boats.parts.address.php',
													'trx_addons_args_boats_address',
													array(
														'meta' => array(
															'address' => $meta['address'],
															)
													));
					?></span>
				</span><?php
			?></div>
			<div class="sc_boats_item_row sc_boats_item_row_meta"><?php
				// Agent
				if ($meta['agent_type']!='none' && ($meta['agent_type']=='author' || $meta['agent']!=0)) {
					?><span class="sc_boats_item_option sc_boats_item_author" title="<?php esc_html_e('Agent or post author', 'trx_addons'); ?>">
						<span class="sc_boats_item_option_label">
							<span class="sc_boats_item_option_label_icon trx_addons_icon-user-alt"></span>
							<span class="sc_boats_item_option_label_text"><?php esc_html_e('Agent:', 'trx_addons'); ?></span>
						</span>
						<span class="sc_boats_item_option_data"><?php
							$trx_addons_agent = trx_addons_boats_get_agent_data($meta);
							echo (!empty($trx_addons_agent['posts_link'])
									? '<a href="'.esc_url($trx_addons_agent['posts_link']).'">'
									: ''
									)
								. esc_html($trx_addons_agent['name'])
								. (!empty($trx_addons_agent['posts_link'])
									? '</a>'
									: ''
									);
						?></span>
					</span><?php
				}
				
				// Publish date
				?><span class="sc_boats_item_option sc_boats_item_date" title="<?php esc_html_e('Publish date', 'trx_addons'); ?>">
					<span class="sc_boats_item_option_label">
						<span class="sc_boats_item_option_label_icon trx_addons_icon-calendar"></span>
						<span class="sc_boats_item_option_label_text"><?php esc_html_e('Added:', 'trx_addons'); ?></span>
					</span>
					<span class="sc_boats_item_option_data"><?php
						echo wp_kses_data(apply_filters('trx_addons_filter_get_post_date', date('d.m.y', get_the_date('U'))));
					?></span>
				</span><?php
			?></div><?php
			if (!empty($args['more_text'])) {
				?><div class="sc_boats_item_button sc_item_button"><a href="<?php echo esc_url($link); ?>" class="<?php echo esc_attr(apply_filters('trx_addons_filter_sc_item_link_classes', 'sc_button', 'sc_boats', $args)); ?>"><?php echo esc_html($args['more_text']); ?></a></div><?php
			}
		?></div>
	</div>
</div>
<?php
if ($args['slider'] || $args['columns'] > 1) {
	?></div><?php
}
