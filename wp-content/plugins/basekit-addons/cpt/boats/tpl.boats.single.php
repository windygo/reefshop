<?php
/**
 * The template to display the property's single page
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.6.22
 */

wp_enqueue_script('jquery-ui-accordion', false, array('jquery', 'jquery-ui-core'), null, true);

get_header();

while ( have_posts() ) { the_post();
	$trx_addons_meta = get_post_meta(get_the_ID(), 'trx_addons_options', true);
	do_action('trx_addons_action_before_article', 'boats.single');
	?>
	<article id="post-<?php the_ID(); ?>" data-post-id="<?php the_ID(); ?>" <?php post_class( 'boats_page itemscope' ); trx_addons_seo_snippets('', 'Article'); ?>>

		<?php do_action('trx_addons_action_article_start', 'boats.single'); ?>
		
		<section class="boats_page_section boats_page_header"><?php
			// Image
			if ( !trx_addons_sc_layouts_showed('featured') && has_post_thumbnail() ) {
				?><div class="boats_page_featured"><?php
					the_post_thumbnail(
                        apply_filters('trx_addons_filter_thumb_size', trx_addons_get_thumb_size('huge'), 'boats-single'),
										trx_addons_seo_image_params(array(
																		'alt' => get_the_title()
																		))
										);
                        // Price
                        if (!empty($trx_addons_meta['price'])) {
                            ?><div class="boats_page_title_price"><?php
                            basekit_addons_get_template_part(BASEKIT_ADDONS_PLUGIN_CPT . 'boats/tpl.boats.parts.price.php',
                                'trx_addons_args_boats_price',
                                array('meta' => $trx_addons_meta)
                            );
                            ?></div><?php
                        }
				?></div><?php
				if (!empty($trx_addons_meta['gallery'])) {
					$trx_addons_gallery = explode('|', $trx_addons_meta['gallery']);
					if (is_array($trx_addons_gallery)) {
						?><div class="boats_page_gallery"><?php
							array_unshift($trx_addons_gallery, get_post_thumbnail_id($id));
							$i = 0;
							foreach($trx_addons_gallery as $trx_addons_image) {
								$i++;
								if ($trx_addons_image != '') {
									$trx_addons_thumb = trx_addons_get_attachment_url($trx_addons_image, trx_addons_get_thumb_size('tiny'));
									$trx_addons_image = trx_addons_get_attachment_url($trx_addons_image, apply_filters('trx_addons_filter_thumb_size', trx_addons_get_thumb_size('huge'), 'boats-single'));
									if (!empty($trx_addons_thumb)) {
										$attr = trx_addons_getimagesize($trx_addons_thumb);
										?><span class="boats_page_gallery_item<?php if ($i==1) echo " boats_page_gallery_item_active"; ?>" data-image="<?php echo esc_url($trx_addons_image); ?>"><?php
											?><img src="<?php echo esc_url($trx_addons_thumb); ?>" alt="<?php esc_attr_e('Gallery item', 'trx_addons'); ?>"<?php
												if (!empty($attr[3])) echo ' '.trim($attr[3]);
											?>><?php
										?></span><?php
									}
								}
							}
						?></div><?php
					}
				}
			}
			
			// Title
			if ( true || !trx_addons_sc_layouts_showed('title') ) {
				?><div class="boats_page_title_wrap">
					<h2 class="boats_page_title">
						<?php the_title(); ?>
					</h2>
					<?php
                $boat_location = trx_addons_get_post_terms('', get_the_ID(), TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_LOCATION);
                ?>
                <div class="boats_page_title_address icon-placeholder">
                                     <?php if (!empty($boat_location)) {
                                         trx_addons_show_layout($boat_location);
                                     }
                                     ?>
                                </div>
				</div><?php
			}?>
            <div class="boats_page_meta_wrap">
                <?php
                if (!empty($trx_addons_meta['length'])) {
                ?><span class="boats_page_section_item">
                <span class="boats_page_label icon-ruler"></span>
                        <span class="boats_page_data"><?php
                            trx_addons_show_layout($trx_addons_meta['length']
                                . ($trx_addons_meta['length_prefix']
                                    ? ' ' . trx_addons_prepare_macros($trx_addons_meta['length_prefix'])
                                    : ''
                                )
                            );
                        ?></span>
                </span><?php
            }
            $boat_crew = strip_tags(trx_addons_get_post_terms('', get_the_ID(), TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_CREW));
            if (!empty($boat_crew)) {
            ?><span class="boats_page_section_item">
                <span class="boats_page_label icon-profile"></span>
                    <span class="boats_page_data"><?php
                        trx_addons_show_layout($boat_crew);
                        ?></span>
            </span><?php
            }
            if (!empty($trx_addons_meta['cabins'])) {
				?><span class="boats_page_section_item ">
					<span class="boats_page_label icon-bed"></span>
					<span class="boats_page_data"><?php echo esc_html($trx_addons_meta['cabins']); ?></span>
				</span><?php
			}
            if (!empty($trx_addons_meta['cruising_speed'])) {
                ?><span class="boats_page_section_item">
                <span class="boats_page_label icon-speed"></span>
                <span class="boats_page_data"><?php
                    trx_addons_show_layout($trx_addons_meta['cruising_speed']
                        . ($trx_addons_meta['cruising_speed_prefix']
                            ? ' ' . trx_addons_prepare_macros($trx_addons_meta['cruising_speed_prefix'])
                            : ''
                        )
                    );
                    ?></span>
                </span><?php
            }

			?>
            </div>

        <?php
		?></section><?php

		
		// Section's titles
		$trx_addons_section_titles = array(
			'description' => __('Description', 'trx_addons'),
			'details' => __('Details', 'trx_addons'),
			'features' => __('Features', 'trx_addons'),
			'floor_plans' => __('Floor plans', 'trx_addons'),
			'attachments' => __('Attachments', 'trx_addons'),
			'video' => __('Video', 'trx_addons'),
			'virtual_tour' => __('Virtual tour', 'trx_addons'),
			'contacts' => __('Contact Us', 'trx_addons'),
			'map' => __('Boat Location', 'trx_addons')
		);
		$trx_addons_tabs_id = 'boats_page_tabs';


		// Post content
		?><section id="<?php echo esc_attr($trx_addons_tabs_id.'_description'); ?>_content" class="boats_page_section boats_page_content entry-content"<?php trx_addons_seo_snippets('articleBody'); ?>><?php
			the_content( );
		?></section><!-- .entry-content --><?php


		// Details
		?><section id="<?php echo esc_attr($trx_addons_tabs_id.'_details'); ?>_content" class="boats_page_section boats_page_details">

            <div class="boats_page_details_wrapper">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <span class="boats_page_label"><?php esc_html_e('Boat type:', 'trx_addons'); ?></span>
                            </td>
                            <td>
                                <?php
                                $boat_type = trx_addons_get_post_terms('', get_the_ID(), TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_TYPE);
                                ?>
                                <span class="boats_page_data">
                                     <?php if (!empty($boat_type)) {
                                            trx_addons_show_layout($boat_type);
                                        }
                                        else{
                                            echo( ' - ' );
                                     }
                                      ?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <span class="boats_page_label"><?php esc_html_e('Manufacturer:', 'trx_addons'); ?></span>
                            </td>
                            <td>
                                <?php
                                 if (!empty($trx_addons_meta['manufacturer'])) {
                                     ?>
                                     <span class="boats_page_data"><?php trx_addons_show_layout($trx_addons_meta['manufacturer']); ?></span>
                                     <?php
                                 }
                                 else{
                                     echo( ' - ' );
                                 }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="boats_page_label"><?php esc_html_e('Model:', 'trx_addons'); ?></span>
                            </td>
                            <td>
                                <?php
                                 if (!empty($trx_addons_meta['model'])) {
                                     ?>
                                     <span class="boats_page_data"><?php trx_addons_show_layout($trx_addons_meta['model']); ?></span>
                                     <?php
                                 }
                                 else{
                                     echo( ' - ' );
                                 }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="boats_page_label"><?php esc_html_e('Year:', 'trx_addons'); ?></span>
                            </td>
                            <td>
                            <?php
                            if (!empty($trx_addons_meta['built'])) {
                                ?>
                                <span class="boats_page_data"><?php trx_addons_show_layout($trx_addons_meta['built']); ?></span>
                               <?php
                            }
                            else{
                                echo( ' - ' );
                            }
                            ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="boats_page_label"><?php esc_html_e('Length:', 'trx_addons'); ?></span>
                            </td>
                            <td>
                                <?php
                                if (!empty($trx_addons_meta['length'])) {
                                    ?>
                                    <span class="boats_page_data"><?php
                                        trx_addons_show_layout($trx_addons_meta['length']
                                            . ($trx_addons_meta['length_prefix']
                                                ? ' ' . trx_addons_prepare_macros($trx_addons_meta['length_prefix'])
                                                : ''
                                            )
                                        );
                                        ?></span>
                                   <?php
                                }
                                else{
                                    echo( ' - ' );
                                }
                                ?>
                            </td>
                        </tr>
                    </tbody>

                </table>
            </div>

            <div class="boats_page_details_wrapper">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <span class="boats_page_label"><?php esc_html_e('Cruising speed:', 'trx_addons'); ?></span>
                            </td>
                            <td>
                                <?php
                                if (!empty($trx_addons_meta['cruising_speed'])) {
                                    ?>
                                    <span class="boats_page_data"><?php
                                        trx_addons_show_layout($trx_addons_meta['cruising_speed']
                                            . ($trx_addons_meta['cruising_speed_prefix']
                                                ? ' ' . trx_addons_prepare_macros($trx_addons_meta['cruising_speed_prefix'])
                                                : ''
                                            )
                                        );
                                        ?>
                                    </span>
                                  <?php
                                }
                                else{
                                    echo( ' - ' );
                                }

                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="boats_page_label"><?php esc_html_e('Boat captain:', 'trx_addons'); ?></span>
                            </td>
                            <td>
                                <?php
                                $boat_agent = get_the_title($trx_addons_meta['agent']);
                                if (!empty($boat_agent)) {
                                    ?>
                                    <span class="boats_page_data"><?php trx_addons_show_layout($boat_agent); ?></span>
                                   <?php
                                }
                                else{
                                    echo( ' - ' );
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                $boat_crew = strip_tags(trx_addons_get_post_terms('', get_the_ID(), TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_CREW));
                                ?>
                                <span class="boats_page_label"><?php esc_html_e('Number of crew:', 'trx_addons'); ?></span>
                            </td>
                            <td>
                                <?php
                                if (!empty($boat_crew)) {
                                    ?><span class="boats_page_data">
                                    <?php
                                        trx_addons_show_layout($boat_crew);
                                        ?></span>
                                    <?php
                                }
                                else{
                                    echo( ' - ' );
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="boats_page_label"><?php esc_html_e('Charter guest:', 'trx_addons'); ?></span>
                            </td>
                            <td>
                                <?php
                                if (!empty($trx_addons_meta['charter_guest'])) {
                                    ?>
                                    <span class="boats_page_data"><?php echo esc_html($trx_addons_meta['charter_guest']); ?></span>
                                   <?php
                                }
                                else{
                                    echo( ' - ' );
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="boats_page_label"><?php esc_html_e('Cabins:', 'trx_addons'); ?></span>
                            </td>
                            <td>
                                <?php
                                if (!empty($trx_addons_meta['cabins'])) {
                                    ?>
                                    <span class="boats_page_data"><?php echo esc_html($trx_addons_meta['cabins']); ?></span>
                                   <?php
                                }
                                else{
                                    echo( ' - ' );
                                }
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

         <?php
			// Additional details
			if (!empty($trx_addons_meta['details_enable']) && !empty($trx_addons_meta['details']) && is_array($trx_addons_meta['details'])) {
				foreach ($trx_addons_meta['details'] as $detail) {
					if (!empty($detail['title'])) {
						?><span class="boats_page_section_item">
							<span class="boats_page_label"><?php
								trx_addons_show_layout(trx_addons_prepare_macros($detail['title'])); 
							?>:</span>
							<span class="boats_page_data"><?php
								trx_addons_show_layout(trx_addons_prepare_macros($detail['value'])); 
							?></span>
						</span><?php
					}
				}
			}
		?></section><!-- .boats_page_details -->
        <?php
        // Specifications & Amenities
        ?>
        <section class="boat_specification">
            <?php
            $boat_amenities = trx_addons_get_post_terms('', get_the_ID(), TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_AMENITIES);
            if(!empty($boat_amenities)) {
                ?>
                <div class="boat_specification_wrapper">
                    <h4 class="boats_page_amenities_title"><?php esc_html_e('Boat Amenities', 'trx_addons'); ?></h4>
                    <div class="boat_amenities_list">
                        <?php
                        trx_addons_show_layout($boat_amenities);
                        ?>
                    </div>
                </div>
                <?php
            }
            $boat_specification = trx_addons_get_post_terms('', get_the_ID(), TRX_ADDONS_CPT_BOATS_TAXONOMY_BOAT_SPECIFICATIONS);
            if(!empty($boat_specification)) {
                ?>
                <div class="boat_specification_wrapper">
                <h4 class="boats_page_specification_title"><?php esc_html_e('Specifications', 'trx_addons'); ?></h4>
                    <div class="boat_specification_list">
                        <?php
                            trx_addons_show_layout($boat_specification);
                        ?>
                    </div>
                </div>
                <?php
            }

            ?>

        </section>
    <?php
		// Attachments
		if (!empty($trx_addons_meta['attachments'])) {
			$trx_addons_meta['attachments'] = explode('|', $trx_addons_meta['attachments']);
			if (is_array($trx_addons_meta['attachments']) && count($trx_addons_meta['attachments'])>0) {
				?><section id="<?php echo esc_attr($trx_addons_tabs_id.'_attachments'); ?>_content" class="boats_page_attachments">
					<h4 class="boats_page_section_title"><?php echo esc_html($trx_addons_section_titles['attachments']); ?></h4><?php
					if (!empty($trx_addons_meta['attachments_description'])) {
						?><div class="boats_page_section_description"><?php
							echo wp_kses_post(nl2br($trx_addons_meta['attachments_description']));
						?></div><?php
					}
					?><div class="boats_page_attachments_list"><?php
						foreach ($trx_addons_meta['attachments'] as $file) {
							?><a href="<?php echo esc_url($file); ?>" target="_blank" download="<?php echo(esc_attr(basename($file))); ?>"><?php echo esc_html(basename($file));	?></a><?php
						}
					?></div>
				</section><!-- .boats_page_attachments --><?php
			}
		}


		// Video promo
		if (!empty($trx_addons_meta['video'])) {
			?><section id="<?php echo esc_attr($trx_addons_tabs_id.'_video'); ?>_content" class="boats_page_section boats_page_video">
				<h4 class="boats_page_section_title"><?php echo esc_html($trx_addons_section_titles['video']); ?></h4><?php
					if (!empty($trx_addons_meta['video_description'])) {
						?><div class="boats_page_section_description"><?php
							echo wp_kses_post(nl2br($trx_addons_meta['video_description']));
						?></div><?php
					}
				?><div class="boats_page_video_wrap"><?php
					trx_addons_show_layout(trx_addons_get_video_layout(array(
																			'link' => $trx_addons_meta['video']
																		)));
				?></div>
			</section><!-- .boats_page_video --><?php
		}


		// Virtual tour
		if (!empty($trx_addons_meta['virtual_tour'])) {
            ?>
            <section id="<?php echo esc_attr($trx_addons_tabs_id . '_virtual_tour'); ?>_content"
                     class="boats_page_section boats_page_virtual_tour">
            <h4 class="boats_page_section_title"><?php echo esc_html($trx_addons_section_titles['virtual_tour']); ?></h4><?php
            if (!empty($trx_addons_meta['virtual_tour_description'])) {
                ?>
                <div class="boats_page_section_description"><?php
                echo wp_kses_post(nl2br($trx_addons_meta['virtual_tour_description']));
                ?></div><?php
            }
            ?>
            <div class="boats_page_virtual_tour_wrap"><?php
                if (strpos($trx_addons_meta['virtual_tour'], '<') === false) {
                    ?>
                    <iframe src="<?php
                    echo esc_url($trx_addons_meta['virtual_tour']);
                    ?>" width="1170" height="658"></iframe><?php
                } else
                    trx_addons_show_layout($trx_addons_meta['virtual_tour']);
                ?></div>
            </section><!-- .boats_page_virtual_tour --><?php
        }


		// Agent info
		if (!empty($trx_addons_meta['agent_type']) 
			&& $trx_addons_meta['agent_type']!='none' 
			&& ($trx_addons_meta['agent_type']=='author' || $trx_addons_meta['agent']!=0)) {
			?><section id="<?php echo esc_attr($trx_addons_tabs_id.'_contacts'); ?>_content" class="boats_page_section boats_page_agent">
				<h4 class="boats_page_section_title"><?php echo esc_html($trx_addons_section_titles['contacts']); ?></h4>
				<div class="boats_page_agent_wrap scheme_dark"<?php trx_addons_seo_snippets('author', 'Person'); ?>><?php
					basekit_addons_get_template_part(BASEKIT_ADDONS_PLUGIN_CPT . 'boats/tpl.boats.parts.agent.php',
													'trx_addons_args_boats_agent',
													array('meta' => $trx_addons_meta)
												);
				?></div>
			</section><!-- .boats_page_agent --><?php
		}

		do_action('trx_addons_action_article_end', 'boats.single');

        // Map
        if (!empty($trx_addons_meta['show_map']) && !empty($trx_addons_meta['location'])) {
            ?><section id="<?php echo esc_attr($trx_addons_tabs_id.'_attachments'); ?>_content" class="boats_page_attachments">
            <h4 class="boats_page_section_title"><?php echo esc_html($trx_addons_section_titles['map']); ?></h4><?php
            ?><div class="boats_page_map"><?php
                trx_addons_show_layout(trx_addons_sc_boats(array(
                    'type' => 'map',
                    'ids' => get_the_ID()
                )));
                ?></div>
            </section><!-- .boats_page_map --><?php
        }

	?></article><?php

    do_action('trx_addons_action_after_article', 'boats.single');

}

get_footer();
