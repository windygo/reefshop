<?php
namespace icss_eme;

    class CodeAutoUpdate
         {
             //# URL to check for updates, this is where the index.php script goes
             public static $api_url;
             private static $slug;
             public static $plugin;
             
             public function __construct($api_url, $slug, $plugin)
                 {
                     self::$api_url = $api_url;
                     self::$slug    = $slug;
                     self::$plugin  = $plugin;
                 }
             
             
             public static function check_for_plugin_update($checked_data){
                    //echo var_dump($checked_data).'-- checked_data --<br>';

                     if (empty($checked_data->checked) || !isset($checked_data->checked[self::$plugin]))
                        return $checked_data;
                     
                     $request_string = self::prepare_request('plugin_update');
                     if($request_string === FALSE)
                        return $checked_data;
                     
                     // Start checking for an update
                     $request_uri = self::$api_url . '?' . http_build_query( $request_string , '', '&');
                     $data = wp_remote_get( $request_uri );

                     if(is_wp_error( $data ) || $data['response']['code'] != 200)
                        return $checked_data;
                     
                     $response_block = json_decode($data['body']);
                      
                     if(!is_array($response_block) || count($response_block) < 1)
                          {
                                     return $checked_data;
                          }
                     
                     //retrieve the last message within the $response_block
                     $response_block = $response_block[count($response_block) - 1];
                     $response = isset($response_block->message) ? $response_block->message : '';
                     
                     if (is_object($response) && !empty($response)) // Feed the update data into WP updater
                         {
                             //include slug and plugin data
                             $response->slug = self::$slug;
                             $response->plugin = self::$plugin;
                             $checked_data->response[self::$plugin] = $response;
                         }
                     
                     return $checked_data;
                 }  
             
             public static function plugins_api_call($def, $action, $args)
                 {
                     if (!is_object($args) || !isset($args->slug) || $args->slug != self::$slug){
                        return false;
                    }
                     
                     //$args->package_type = self::package_type;
                     
                     $request_string = self::prepare_request($action, $args);
                     if($request_string === FALSE){
                            return new WordPress/WP_Error('plugins_api_failed', __('An error occour when try to identify the plugin.' , 'icss_eme') . '&lt;/p> &lt;p>&lt;a href=&quot;?&quot; onclick=&quot;document.location.reload(); return false;&quot;>'. __( 'Try again', 'icss_eme' ) .'&lt;/a>');
                    }
                     
                     $request_uri = self::$api_url . '?' . http_build_query( $request_string , '', '&');
                     $data = wp_remote_get( $request_uri );
                     
                     if(is_wp_error( $data ) || $data['response']['code'] != 200)
                        return new WordPress/WP_Error('plugins_api_failed', __('An Unexpected HTTP Error occurred during the API request.' , 'icss_eme') . '&lt;/p> &lt;p>&lt;a href=&quot;?&quot; onclick=&quot;document.location.reload(); return false;&quot;>'. __( 'Try again', 'icss_eme' ) .'&lt;/a>', $data->get_error_message());
                     
                     $response_block = json_decode($data['body']);
                     //retrieve the last message within the $response_block
                     $response_block = $response_block[count($response_block) - 1];
                     $response = $response_block->message;
                     
                     if (is_object($response) && !empty($response)) // Feed the update data into WP updater
                         {
                             //include slug and plugin data
                             $response->slug = self::$slug;
                             $response->plugin = self::$plugin;
                             
                             $response->sections = (array)$response->sections;
                             $response->banners = (array)$response->banners;

                             return $response;
                         }
                 }
             
             public function prepare_request($action, $args = array())
                 {
                     global $wp_version;
                     
                     $license_data = get_site_option('icss_eme_license'); 
                 
                    $timeout = rand(1,20);
                     
                     return array(
                         'woo_sl_action'        => $action,
                         'version'              => ICSS_EME_VERSION,
                         'product_unique_id'    => ICSS_EME_PRODUCT_ID,
                         'licence_key'          => $license_data['key'],
                         'domain'               => ICSS_EME_INSTANCE,
                         'wp-version'           => $wp_version,
                        'timeout' => $timeout
                     );
                 }
         }

    namespace icss_eme\updater;

    class CodeAutoUpdate_Run {   
        public static function run_updater()
             {
                $wp_plugin_auto_update = new \icss_eme\CodeAutoUpdate(ICSS_EME_APP_API_URL, ICSS_EME_SLUG, ICSS_EME_PLUGIN);
                
                 // Take over the update check
                 add_filter('pre_set_site_transient_update_plugins', array($wp_plugin_auto_update, 'icss_eme\CodeAutoUpdate::check_for_plugin_update'));
            
                if ( isset($_GET['plugin']) && $_GET['plugin'] == ICSS_EME_SLUG  ) {
                     // Take over the Plugin info screen
                    add_filter('plugins_api', array($wp_plugin_auto_update, 'icss_eme\CodeAutoUpdate::plugins_api_call'), 10, 3);
                }

             }
        

    }

    global $CodeAutoUpdate_Run;
    $CodeAutoUpdate_Run = new \icss_eme\updater\CodeAutoUpdate_Run;
    add_action( 'after_setup_theme', 'icss_eme\updater\CodeAutoUpdate_Run::run_updater' );

?>