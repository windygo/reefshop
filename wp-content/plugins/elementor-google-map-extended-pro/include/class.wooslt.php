<?php
    namespace icss_eme;

    if ( ! defined( 'ABSPATH' ) ) { exit;}
    
    class icss_eme
        {
            var $licence;
            var $interface;
            
            /**
            * 
            * Run on class construct
            * 
            */
            function __construct( ) 
                {
                    $this->licence              =   new \icss_eme\licence; 
                    $this->interface            =   new \icss_eme\options_interface;
                     
                }
                
              
        } 
    
    
    
?>