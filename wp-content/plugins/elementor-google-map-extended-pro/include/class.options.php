<?php
//namespace icss_eme\options;
namespace icss_eme;
use \Elementor\Settings;

    class options_interface
        {
         
            var $licence;
         
            function __construct()
                {
                    
                    $this->licence          =   new licence();
                    
                    if (isset($_GET['page']) && ($_GET['page'] == 'icss-ms-options'  ||  $_GET['page'] == 'icss_eb_extended'))
                        {
                            add_action( 'init', array($this, 'eme_options_update'), 1 );
                        }
                        
                    //if ( empty ( $GLOBALS['admin_page_hooks']['icss-ms-options'] ) && empty ( $GLOBALS['admin_page_hooks']['icss-options'] ) ){
                        add_action( 'admin_menu', array($this, 'admin_menu') , 509 );
                        add_action( 'network_admin_menu', array($this, 'network_admin_menu') );
                    //}
                                        
                    if(!$this->licence->licence_key_verify())
                        {
                            add_action('admin_notices', array($this, 'admin_no_key_notices'));
                            add_action('network_admin_notices', array($this, 'admin_no_key_notices'));
                        }
                    
                }
                
            function __destruct()
                {
                
                }
            
            /* does not support network */
            function network_admin_menu()
                {
                    if(!$this->licence->licence_key_verify())
                        $hookID   = add_submenu_page('settings.php', ICSS_EME_NAME, ICSS_EME_NAME, 'manage_options', 'icss-ms-options', array($this, 'licence_form'));
                        else
                        $hookID   = add_submenu_page('settings.php', ICSS_EME_NAME, ICSS_EME_NAME, 'manage_options', 'icss-ms-options', array($this, 'licence_deactivate_form'));
                        
                    add_action('load-' . $hookID , array($this, 'load_dependencies'));
                    add_action('load-' . $hookID , array($this, 'admin_notices'));
                    
                    add_action('admin_print_styles-' . $hookID , array($this, 'admin_print_styles'));
                    add_action('admin_print_scripts-' . $hookID , array($this, 'admin_print_scripts'));
                }
                
            function admin_menu(){
                    global $submenu;
                    
                    $ele_menu = \Elementor\Settings::PAGE_ID;
                    if ( isset( $submenu[ $ele_menu ] ) && in_array( 'icss_eb_extended', wp_list_pluck( $submenu[ $ele_menu ], 2 ) ) ) {
                        // Submenu exists.
                    } else {
                        // Submenu doesn't exist.
                        add_submenu_page( \Elementor\Settings::PAGE_ID , __( 'Elementor Extended', 'icss_ete' ), __( 'Elementor Extended', 'icss_ete' ), 'manage_options', 'icss_eb_extended', array($this, 'icss_pageheader' ) );
                        add_action( 'elementor_page_icss_eb_extended', array($this, 'admin_notices'));
                    }

                    if(!$this->licence->licence_key_verify()){
                        add_action( 'elementor_page_icss_eb_extended', array($this, 'licence_form'));
                    }else{
                        add_action( 'elementor_page_icss_eb_extended', array($this, 'licence_deactivate_form'));
                    }
                    add_action( 'elementor_page_icss_eb_extended', array($this, 'google_map_api_form'), 520);
                    add_action( 'elementor_page_icss_eb_extended', array($this, 'google_map_languages'), 520);
                    add_action( 'admin_print_styles-elementor_page_icss_eb_extended', array($this, 'admin_print_styles'));
                    add_action( 'admin_print_scripts-elementor_page_icss_eb_extended', array($this, 'admin_print_scripts'));

                }   
                
            function options_interface()
                {
                    
                    if(!$this->licence->licence_key_verify() && !is_multisite())
                        {
                            $this->licence_form();
                            return;
                        }
                    
                    /* not supported */  
                    if(!$this->licence->licence_key_verify() && is_multisite())
                        {
                            $this->licence_multisite_require_nottice();
                            return;
                        }
                }
            
            function eme_options_update()
                {
                    
                    if (isset($_POST['icss_eme_license_form_submit']))
                        {
                            $this->eme_licence_form_submit();
                            return;
                        }
                        else if (isset($_POST['icss_eme_google_map_api_key_form_submit'])) {
                            $this->eme_api_key_form_submit();
                            return;
                        }
                        else if (isset($_POST['icss_eme_google_map_lang_form_submit'])) {
                            $this->eme_google_map_form_submit();
                            return;
                        }
            
                }

            function load_dependencies()
                {

                }
                
            function admin_notices()
                {
                    global $slt_form_submit_messages;
            
                    if($slt_form_submit_messages == '')
                        return;
                    
                    $messages = $slt_form_submit_messages;
 
                          
                    if(count($messages) > 0)
                        {
                            echo "<div id='notice' class='updated fade'><p>". implode("</p><p>", $messages )  ."</p></div>";
                        }

                }
                  
            function admin_print_styles()
                {
                    wp_register_style( 'icss_eme_admin', ICSS_EME_URL . '/css/admin.css' );
                    wp_enqueue_style( 'icss_eme_admin' ); 
                }
                
            function admin_print_scripts()
                {

                }
            
            
            function admin_no_key_notices()
                {
                    if ( !current_user_can('manage_options'))
                        return;
                    
                    $screen = get_current_screen();
                        
                    if(is_multisite())
                        {
                            if(isset($screen->id) && $screen->id    ==  'settings_page_icss-ms-options-network')
                                return;
                            ?><div class="updated fade"><p><?php _e( ICSS_EME_NAME." is inactive, please enter your", 'icss_eme' ) ?> <a href="<?php echo network_admin_url() ?>settings.php?page=icss-ms-options"><?php _e( "Licence Key", 'icss_eme' ) ?></a></p></div><?php
                        }
                        else
                        {
                            if(isset($screen->id) && $screen->id == 'elementor_page_icss_eb_extended')
                                return;
                            
                            ?><div class="updated fade"><p><?php _e( ICSS_EME_NAME." is inactive, please enter your", 'icss_eme' ) ?> <a href="admin.php?page=icss_eb_extended"><?php _e( "Licence Key", 'icss_eme' ) ?></a></p></div><?php
                        }
                }

            function eme_licence_form_submit()
                {
                    global $slt_form_submit_messages; 
                    
                    //check for de-activation
                    if (isset($_POST['icss_eme_license_form_submit']) && isset($_POST['icss_eme_license_deactivate']) && wp_verify_nonce($_POST['icss_eme_license_nonce'],'icss_eme_license')) {
                            
                            $license_data = get_site_option('icss_eme_license');                        
                            $license_key = $license_data['key'];

                            $timeout = rand(1,20);

                            //build the request query
                            $args = array(
                                                'woo_sl_action'         => 'deactivate',
                                                'licence_key'           => $license_key,
                                                'product_unique_id'     => ICSS_EME_PRODUCT_ID,
                                                'domain'                => ICSS_EME_INSTANCE,
                                                'timeout' => $timeout
                                            );
                            $request_uri    = ICSS_EME_APP_API_URL . '?' . http_build_query( $args , '', '&');
                            $data           = wp_remote_get( $request_uri );

                            if(is_wp_error( $data ) || $data['response']['code'] != 200)
                                {
                                    $slt_form_submit_messages[] .= __('There was a problem connecting to ', 'icss_eme') . ICSS_EME_APP_API_URL;
                                    return;  
                                }
                                
                            $response_block = json_decode($data['body']);
                            //retrieve the last message within the $response_block
                            $response_block = $response_block[count($response_block) - 1];
                            $response = $response_block->message;
                            
                            if(isset($response_block->status))
                                {
                                    if($response_block->status == 'success' && $response_block->status_code == 's201')
                                        {
                                            //the license is active and the software is active
                                            $slt_form_submit_messages[] = ICSS_EME_NAME. " " . $response_block->message;
                                            
                                            $license_data = get_site_option('icss_eme_license');
                                            
                                            //save the license
                                            $license_data['key']          = '';
                                            $license_data['last_check']   = time();
                                            
                                            update_site_option('icss_eme_license', $license_data);
                                        }
                                        
                                    else //if message code is e104  force de-activation
                                            if ($response_block->status_code == 'e002' || $response_block->status_code == 'e104')
                                                {
                                                    $license_data = get_site_option('icss_eme_license');
                                            
                                                    //save the license
                                                    $license_data['key']          = '';
                                                    $license_data['last_check']   = time();
                                                    
                                                    update_site_option('icss_eme_license', $license_data);
                                                }
                                        else
                                        {
                                            $slt_form_submit_messages[] = __('There was a problem deactivating the licence: ', 'icss_eme') . $response_block->message;
                                     
                                            return;
                                        }   
                                }
                                else
                                {
                                    $slt_form_submit_messages[] = __('There was a problem with the data block received from ' . ICSS_EME_APP_API_URL, 'icss_eme');
                                    return;
                                }
                                
                            //redirect
                            $current_url    =   'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                            
                            wp_redirect($current_url);
                            die();
                            
                        }  else if (isset($_POST['icss_eme_license_form_submit']) && wp_verify_nonce($_POST['icss_eme_license_nonce'],'icss_eme_license')) {
                            
                            $license_key = isset($_POST['license_key'])? sanitize_key(trim($_POST['license_key'])) : '';

                            if($license_key == '')
                                {
                                    $slt_form_submit_messages[] = __("Licence Key can't be empty", 'icss_eme');
                                    return;
                                }
                                
                            $timeout = rand(1,20);

                            //build the request query
                            $args = array(
                                                'woo_sl_action'         => 'activate',
                                                'licence_key'           => $license_key,
                                                'product_unique_id'     => ICSS_EME_PRODUCT_ID,
                                                'domain'                => ICSS_EME_INSTANCE,
                                                'timeout' => $timeout
                                            );
                            $request_uri    = ICSS_EME_APP_API_URL . '?' . http_build_query( $args , '', '&');
                            $data           = wp_remote_get( $request_uri );

                            if(is_wp_error( $data ) || $data['response']['code'] != 200)
                                {
                                    $slt_form_submit_messages[] .= __('There was a problem connecting to ', 'icss_eme') . ICSS_EME_APP_API_URL;
                                    return;  
                                }
                                
                            $response_block = json_decode($data['body']);
                            //retrieve the last message within the $response_block
                            $response_block = $response_block[count($response_block) - 1];
                            $response = $response_block->message;
                            
                            if(isset($response_block->status))
                                {
                                    if($response_block->status == 'success' && $response_block->status_code == 's100')
                                        {
                                            //the license is active and the software is active
                                            $slt_form_submit_messages[] = ICSS_EME_NAME." " . $response_block->message;
                                            
                                            $license_data = get_site_option('icss_eme_license');
                                            
                                            //save the license
                                            $license_data['key']          = $license_key;
                                            $license_data['last_check']   = time();
                                            
                                            update_site_option('icss_eme_license', $license_data);

                                        }
                                        else
                                        {
                                            $slt_form_submit_messages[] = __('There was a problem activating the licence: ', 'icss_eme') . $response_block->message;
                                            return;
                                        }   
                                }
                                else
                                {
                                    $slt_form_submit_messages[] = __('There was a problem with the data block received from ' . ICSS_EME_APP_API_URL, 'icss_eme');
                                    return;
                                }
                                
                            //redirect
                            $current_url    =   'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                            
                            wp_redirect($current_url);
                            die();
                        }   
                    
                }

            function icss_pageheader() { ?>
                <div class="wrap"> 
                    <h1><?php _e( "Elementor Extended Settings", 'icss_eb_extended' ) ?></h1>
                    <div class="sectionheader">License Keys</div>
                    <br>
                    <div class="explain" style="padding-bottom: 10px;">
                        <?php _e( "Manage your product license keys at", 'icss_eb_extended' ) ?> <a href="https://internetcss.com/my-account/" target="_blank">My Account</a> 
                    </div>
                </div> 
             <?php }
                
            function licence_form() { ?>
                <div class="wrap"> 
                    <form id="form_data" name="form" method="post">
                        <div>
                            <?php wp_nonce_field('icss_eme_license','icss_eme_license_nonce'); ?>
                            <input type="hidden" name="icss_eme_license_form_submit" value="true" />
                            <div class="section section-text">
                                <div class="option">
                                    <div class="controls">
                                        <span style="font-weight:bold;width:200px;display: inline-block;"><?php _e( 'Elementor Google Map Pro', 'icss_eme' ) ?></span>&nbsp;&nbsp;&nbsp;
                                        <input type="text" value="" name="license_key" class="text-input" style="width: 320px;" > &nbsp;&nbsp;&nbsp;
                                        <input type="submit" name="Submit" class="button-primary" value="<?php _e('Activate', 'icss_eme') ?>">
                                    </div>
                                </div> 
                            </div>
                        </div>                        
                    </form> 
                </div> 
             <?php }
            
            function licence_deactivate_form() {
                    $license_data = get_site_option('icss_eme_license');
                    ?>
                    <div class="wrap"> 
                        <div id="form_data">
                            <div>
                                <form id="form_data" name="form" method="post">    
                                    <?php wp_nonce_field('icss_eme_license','icss_eme_license_nonce'); ?>
                                    <input type="hidden" name="icss_eme_license_form_submit" value="true" />
                                    <input type="hidden" name="icss_eme_license_deactivate" value="true" />

                                     <div class="section section-text ">
                                        <div class="option">
                                            <div class="controls">
                                                <span style="font-weight:bold;width:200px;display: inline-block;"><?php _e( 'Elementor Google Map Pro', 'icss_eme' ) ?></span>&nbsp;&nbsp;&nbsp;
                                                <?php  
                                                    if($this->licence->is_local_instance())
                                                    {
                                                    ?>
                                                    <p>Local instance, no key applied.</p>
                                                    <?php   
                                                    }
                                                    else {
                                                    ?>
                                                <input disabled style="width: 320px;" value="<?php echo substr($license_data['key'], 0, 20) ?>-xxxxxxxx-xxxxxxxx" > &nbsp;&nbsp;&nbsp;<a class="button-secondary" title="Deactivate" href="javascript: void(0)" onclick="jQuery(this).closest('form').submit();">Deactivate</a></p>
                                                <?php } ?>
                                            </div>
                                        </div> 
                                    </div>
                                 </form>
                            </div>
                        </div> 
                </div>
            <?php }
                
            function licence_multisite_require_nottice()
                {
                    ?>
                        <div class="wrap"> 
                            <div id="icon-settings" class="icon32"></div>
                            <h2><?php _e( "General Settings", 'icss_eme' ) ?></h2>

                            <h2 class="subtitle"><?php _e( ICSS_EME_NAME." License Page", 'icss_eme' ) ?></h2>
                            <div id="form_data">
                                <div class="postbox">
                                    <div class="section section-text ">
                                        <h4 class="heading"><?php _e( "License Key Required", 'icss_eme' ) ?>!</h4>
                                        <div class="option">
                                            <div class="explain"><?php _e( "Enter the License Key you got when bought this product. If you lost the key, you can always retrieve it from", 'icss_eme' ) ?> <a href="https://internetcss.com/my-account/" target="_blank"><?php _e( "My Account", 'icss_eme' ) ?></a><br />
                                            <?php _e( "More keys can be generate from", 'icss_eme' ) ?> <a href="https://internetcss.com/my-account/" target="_blank"><?php _e( "My Account", 'icss_eme' ) ?></a> 
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div> 
                    <?php
                
                }    

            function google_map_api_form() { ?>
                <br>
                <div class="wrap"> 
                    <div class="sectionheader">API Settings</div>
                </div> 
                <div class="wrap"> 
                    <form id="form_data" name="form" method="post">
                        <div>
                            <?php wp_nonce_field('google_map_api_key','google_map_api_key_nonce'); ?>
                            <input type="hidden" name="icss_eme_google_map_api_key_form_submit" value="true" />
                            <div class="section section-text">
                                <div class="option">
                                    <div class="controls">
                                        <span style="font-weight:bold;width:200px;display: inline-block;"><?php _e( 'Google Map API', 'icss_eme' ) ?></span>&nbsp;&nbsp;&nbsp;
                                        <input type="text" value="<?php echo eb_map_get_option( 'eb_google_map_api_key', 'eb_map_general_settings' ); ?>" name="eb_google_map_api_key" class="text-input" style="width: 320px;" placeholder="Enter your Google Map API here"> &nbsp;&nbsp;&nbsp;
                                        <input type="submit" name="Submit" class="button-primary" value="<?php _e('Update', 'icss_eme') ?>">
                                        <br><br>
                                        <div class='explain italic'>A Google Map API key is required in order to use Elementor Google Map Extended Pro. Generate it <a href='https://developers.google.com/maps/documentation/javascript/get-api-key' target='_blank'>here</a>.</div>
                                    </div>
                                </div> 
                            </div>
                        </div>                        
                    </form> 
                </div> 
             <?php }

             function google_map_languages() { ?>
                <br>
                <div class="wrap"> 
                    <div class="sectionheader">Google Map Language</div>
                </div> 
                <div class="wrap"> 
                    <form id="form_data" name="form" method="post">
                        <div>
                            <?php wp_nonce_field('google_map_lang','google_map_lang_nonce'); ?>
                            <input type="hidden" name="icss_eme_google_map_lang_form_submit" value="true" />
                            <div class="section section-text">
                                <div class="option">
                                    <div class="controls">
                                        <span style="font-weight:bold;width:200px;display: inline-block;"><?php _e( 'Language', 'icss_eme' ) ?></span>
                                        <select name="eb_google_map_lang">
                                        <option value="af" lang="af">Afrikaans</option>
                                        <option value="ar" lang="ar">العربية</option>
                                        <option value="ary" lang="ar">العربية المغربية</option>
                                        <option value="as" lang="as">অসমীয়া</option>
                                        <option value="az" lang="az">Azərbaycan dili</option>
                                        <option value="azb" lang="az">گؤنئی آذربایجان</option>
                                        <option value="bel" lang="be">Беларуская мова</option>
                                        <option value="bg_BG" lang="bg">Български</option>
                                        <option value="bn_BD" lang="bn">বাংলা</option>
                                        <option value="bo" lang="bo">བོད་ཡིག</option>
                                        <option value="bs_BA" lang="bs">Bosanski</option>
                                        <option value="ca" lang="ca">Català</option>
                                        <option value="ceb" lang="ceb">Cebuano</option>
                                        <option value="cs_CZ" lang="cs">Čeština</option>
                                        <option value="cy" lang="cy">Cymraeg</option>
                                        <option value="da_DK" lang="da">Dansk</option>
                                        <option value="de_DE_formal" lang="de">Deutsch (Sie)</option>
                                        <option value="de_DE" lang="de">Deutsch</option>
                                        <option value="de_CH" lang="de">Deutsch (Schweiz)</option>
                                        <option value="de_CH_informal" lang="de">Deutsch (Schweiz, Du)</option>
                                        <option value="dzo" lang="dz">རྫོང་ཁ</option>
                                        <option value="el" lang="el">Ελληνικά</option>
                                        <option value="en_GB" lang="en">English (UK)</option>
                                        <option value="en_ZA" lang="en">English (South Africa)</option>
                                        <option value="en_NZ" lang="en">English (New Zealand)</option>
                                        <option value="en_AU" lang="en">English (Australia)</option>
                                        <option value="en_CA" lang="en">English (Canada)</option>
                                        <option value="eo" lang="eo">Esperanto</option>
                                        <option value="es_AR" lang="es">Español de Argentina</option>
                                        <option value="es_ES" lang="es">Español</option>
                                        <option value="es_CL" lang="es">Español de Chile</option>
                                        <option value="es_GT" lang="es">Español de Guatemala</option>
                                        <option value="es_CO" lang="es">Español de Colombia</option>
                                        <option value="es_MX" lang="es">Español de México</option>
                                        <option value="es_CR" lang="es">Español de Costa Rica</option>
                                        <option value="es_PE" lang="es">Español de Perú</option>
                                        <option value="es_VE" lang="es">Español de Venezuela</option>
                                        <option value="et" lang="et">Eesti</option>
                                        <option value="eu" lang="eu">Euskara</option>
                                        <option value="fa_IR" lang="fa">فارسی</option>
                                        <option value="fi" lang="fi">Suomi</option>
                                        <option value="fr_CA" lang="fr">Français du Canada</option>
                                        <option value="fr_BE" lang="fr">Français de Belgique</option>
                                        <option value="fr_FR" lang="fr">Français</option>
                                        <option value="fur" lang="fur">Friulian</option>
                                        <option value="gd" lang="gd">Gàidhlig</option>
                                        <option value="gl_ES" lang="gl">Galego</option>
                                        <option value="gu" lang="gu">ગુજરાતી</option>
                                        <option value="haz" lang="haz">هزاره گی</option>
                                        <option value="he_IL" lang="he">עִבְרִית</option>
                                        <option value="hi_IN" lang="hi">हिन्दी</option>
                                        <option value="hr" lang="hr">Hrvatski</option>
                                        <option value="hu_HU" lang="hu">Magyar</option>
                                        <option value="hy" lang="hy">Հայերեն</option>
                                        <option value="id_ID" lang="id">Bahasa Indonesia</option>
                                        <option value="is_IS" lang="is">Íslenska</option>
                                        <option value="it_IT" lang="it">Italiano</option>
                                        <option value="ja" lang="ja">日本語</option>
                                        <option value="jv_ID" lang="jv">Basa Jawa</option>
                                        <option value="ka_GE" lang="ka">ქართული</option>
                                        <option value="kab" lang="kab">Taqbaylit</option>
                                        <option value="kk" lang="kk">Қазақ тілі</option>
                                        <option value="km" lang="km">ភាសាខ្មែរ</option>
                                        <option value="ko_KR" lang="ko">한국어</option>
                                        <option value="ckb" lang="ku">كوردی&lrm;</option>
                                        <option value="lo" lang="lo">ພາສາລາວ</option>
                                        <option value="lt_LT" lang="lt">Lietuvių kalba</option>
                                        <option value="lv" lang="lv">Latviešu valoda</option>
                                        <option value="mk_MK" lang="mk">Македонски јазик</option>
                                        <option value="ml_IN" lang="ml">മലയാളം</option>
                                        <option value="mn" lang="mn">Монгол</option>
                                        <option value="mr" lang="mr">मराठी</option>
                                        <option value="ms_MY" lang="ms">Bahasa Melayu</option>
                                        <option value="my_MM" lang="my">ဗမာစာ</option>
                                        <option value="nb_NO" lang="nb">Norsk bokmål</option>
                                        <option value="ne_NP" lang="ne">नेपाली</option>
                                        <option value="nl_BE" lang="nl">Nederlands (België)</option>
                                        <option value="nl_NL" lang="nl">Nederlands</option>
                                        <option value="nl_NL_formal" lang="nl">Nederlands (Formeel)</option>
                                        <option value="nn_NO" lang="nn">Norsk nynorsk</option>
                                        <option value="oci" lang="oc">Occitan</option>
                                        <option value="pa_IN" lang="pa">ਪੰਜਾਬੀ</option>
                                        <option value="pl_PL" lang="pl">Polski</option>
                                        <option value="ps" lang="ps">پښتو</option>
                                        <option value="pt_PT" lang="pt">Português</option>
                                        <option value="pt_BR" lang="pt">Português do Brasil</option>
                                        <option value="pt_PT_ao90" lang="pt">Português (AO90)</option>
                                        <option value="rhg" lang="rhg">Ruáinga</option>
                                        <option value="ro_RO" lang="ro">Română</option>
                                        <option value="ru_RU" lang="ru">Русский</option>
                                        <option value="sah" lang="sah">Сахалыы</option>
                                        <option value="si_LK" lang="si">සිංහල</option>
                                        <option value="sk_SK" lang="sk">Slovenčina</option>
                                        <option value="sl_SI" lang="sl">Slovenščina</option>
                                        <option value="sq" lang="sq">Shqip</option>
                                        <option value="sr_RS" lang="sr">Српски језик</option>
                                        <option value="sv_SE" lang="sv" <?php if ( eb_map_get_option( 'eb_google_map_lang', 'eb_map_general_settings' ) == 'sv_SE' ) echo 'selected="selected"'; ?>>Svenska</option>
                                        <option value="szl" lang="szl">Ślōnskŏ gŏdka</option>
                                        <option value="ta_IN" lang="ta">தமிழ்</option>
                                        <option value="te" lang="te">తెలుగు</option>
                                        <option value="th" lang="th">ไทย</option>
                                        <option value="tl" lang="tl">Tagalog</option>
                                        <option value="tr_TR" lang="tr">Türkçe</option>
                                        <option value="tt_RU" lang="tt">Татар теле</option>
                                        <option value="tah" lang="ty">Reo Tahiti</option>
                                        <option value="ug_CN" lang="ug">ئۇيغۇرچە</option>
                                        <option value="uk" lang="uk">Українська</option>
                                        <option value="ur" lang="ur">اردو</option>
                                        <option value="uz_UZ" lang="uz">O‘zbekcha</option>
                                        <option value="vi" lang="vi">Tiếng Việt</option>
                                        <option value="zh_HK" lang="zh">香港中文版   </option>
                                        <option value="zh_TW" lang="zh">繁體中文</option>
                                        <option value="zh_CN" lang="zh" <?php if ( eb_map_get_option( 'eb_google_map_lang', 'eb_map_general_settings' ) == 'zh_CN' ) echo 'selected="selected"'; ?>>简体中文</option>
                                        </select>
                                        <input type="submit" name="Submit" class="button-primary" value="<?php _e('Save Changes', 'icss_eme') ?>">
                                        
                                    </div>
                                </div> 
                            </div>
                        </div>                        
                    </form> 
                </div> 
             <?php }

             function eme_api_key_form_submit()
                {
                    global $slt_form_submit_messages; 
                    
                    //check for de-activation
                    if (isset($_POST['icss_eme_google_map_api_key_form_submit']) && isset($_POST['eb_google_map_api_key']) && wp_verify_nonce($_POST['google_map_api_key_nonce'],'google_map_api_key')) {
                            $mapsettings = array(
                                'eb_google_map_api_key' => $_POST['eb_google_map_api_key']
                            );
                            update_option('eb_map_general_settings', $mapsettings);
                            //update_site_option('eb_google_map_api_key', $_POST['eb_google_map_api_key']);
                    }
                    
                }
                function eme_google_map_form_submit()
                {
                    global $slt_form_submit_messages; 
                    
                    //check for de-activation
                    if (isset($_POST['icss_eme_google_map_lang_form_submit']) && isset($_POST['eb_google_map_lang']) && wp_verify_nonce($_POST['google_map_lang_nonce'],'google_map_lang')) {
                            $mapsettingslang = array(
                                'eb_google_map_lang' => $_POST['eb_google_map_lang']
                            );
                            update_option('eb_map_general_settings', $mapsettingslang);
                            //update_site_option('eb_google_map_api_key', $_POST['eb_google_map_api_key']);
                    }
                    
                }


                
        }

                                   

?>