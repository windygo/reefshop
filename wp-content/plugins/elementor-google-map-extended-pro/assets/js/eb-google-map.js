(function($) {
    var EBGoogleMapHandler = function($scope, $) {
        var mapid = $scope.find('.eb-map'),
            maptype = $(mapid).data("eb-map-type"),
            marker_center = $(mapid).data("eb-map-marker-center"),
            zoom = $(mapid).data("eb-map-zoom"),
            map_lat = $(mapid).data("eb-map-lat"),
            map_lng = $(mapid).data("eb-map-lng"),
            gesture_handling = $(mapid).data("eb-map-gesture-handling"),
            defaultui = $(mapid).data("eb-map-defaultui"),
            zoomcontrol = $(mapid).data("eb-map-zoom-control"),
            zoomcontrolposition = $(mapid).data("eb-map-zoom-control-position"),
            maptypecontrol = $(mapid).data("eb-map-type-control"),
            maptypecontrolstyle = $(mapid).data("eb-map-type-control-style"),
            maptypecontrolposition = $(mapid).data("eb-map-type-control-position"),
            streetview = $(mapid).data("eb-map-streetview-control"),
            streetviewposition = $(mapid).data("eb-map-streetview-position"),
            styles = ($(mapid).data("eb-map-style") != '') ? $(mapid).data("eb-map-style") : '',
            pin_trigger_method = $(mapid).data("eb-map-trigger-method"),
            pin_animation_bounce = $(mapid).data("eb-map-animation-bounce"),
            pin_bounce_duration = $(mapid).data("eb-map-animation-bounce-duration"),
            pin_animation_drop = $(mapid).data("eb-map-animation-drop"),
            infowindow_onload = $(mapid).data("eb-map-infowindow-load"),
            infowindow_max_width = $(mapid).data("eb-map-infowindow-width"),
            marker_clustering = $(mapid).data("eb-map-marker-clustering"),
            infowindow_styles = $(mapid).data("eb-map-infowindow-styles"),
            infowindow_close_icon = $(mapid).data("eb-map-infowindow-close-icon"),
            infowindow,
            infowindow_active,
            marker,
            map;

        if (/Mobi/.test(navigator.userAgent)) {
            pin_trigger_method = 'click';
        }

        if (maptypecontrolstyle == 'DROPDOWN_MENU') {
            maptypecontrolstyle = google.maps.MapTypeControlStyle.DROPDOWN_MENU;
        } else if (maptypecontrolstyle == 'HORIZONTAL_BAR') {
            maptypecontrolstyle = google.maps.MapTypeControlStyle.HORIZONTAL_BAR;
        } else {
            maptypecontrolstyle = google.maps.MapTypeControlStyle.DEFAULT;
        }

        if (maptypecontrolposition == 'TOP_CENTER') {
            maptypecontrolposition = google.maps.ControlPosition.TOP_CENTER;
        } else if (maptypecontrolposition == 'TOP_RIGHT') {
            maptypecontrolposition = google.maps.ControlPosition.TOP_RIGHT;
        } else if (maptypecontrolposition == 'LEFT_CENTER') {
            maptypecontrolposition = google.maps.ControlPosition.LEFT_CENTER;
        } else if (maptypecontrolposition == 'RIGHT_CENTER') {
            maptypecontrolposition = google.maps.ControlPosition.RIGHT_CENTER;
        } else if (maptypecontrolposition == 'BOTTOM_CENTER') {
            maptypecontrolposition = google.maps.ControlPosition.BOTTOM_CENTER;
        } else if (maptypecontrolposition == 'BOTTOM_RIGHT') {
            maptypecontrolposition = google.maps.ControlPosition.BOTTOM_RIGHT;
        } else if (maptypecontrolposition == 'BOTTOM_LEFT') {
            maptypecontrolposition = google.maps.ControlPosition.BOTTOM_LEFT;
        } else {
            maptypecontrolposition = google.maps.ControlPosition.TOP_LEFT;
        }

        if (zoomcontrolposition == 'TOP_CENTER') {
            zoomcontrolposition = google.maps.ControlPosition.TOP_CENTER;
        } else if (zoomcontrolposition == 'TOP_RIGHT') {
            zoomcontrolposition = google.maps.ControlPosition.TOP_RIGHT;
        } else if (zoomcontrolposition == 'LEFT_CENTER') {
            zoomcontrolposition = google.maps.ControlPosition.LEFT_CENTER;
        } else if (zoomcontrolposition == 'RIGHT_CENTER') {
            zoomcontrolposition = google.maps.ControlPosition.RIGHT_CENTER;
        } else if (zoomcontrolposition == 'BOTTOM_CENTER') {
            zoomcontrolposition = google.maps.ControlPosition.BOTTOM_CENTER;
        } else if (zoomcontrolposition == 'BOTTOM_RIGHT') {
            zoomcontrolposition = google.maps.ControlPosition.BOTTOM_RIGHT;
        } else if (zoomcontrolposition == 'BOTTOM_LEFT') {
            zoomcontrolposition = google.maps.ControlPosition.BOTTOM_LEFT;
        } else if (zoomcontrolposition == 'TOP_LEFT') {
            zoomcontrolposition = google.maps.ControlPosition.TOP_LEFT;
        } else {
            zoomcontrolposition = google.maps.ControlPosition.RIGHT_BOTTOM;
        }

        if (streetviewposition == 'TOP_CENTER') {
            streetviewposition = google.maps.ControlPosition.TOP_CENTER;
        } else if (streetviewposition == 'TOP_RIGHT') {
            streetviewposition = google.maps.ControlPosition.TOP_RIGHT;
        } else if (streetviewposition == 'LEFT_CENTER') {
            streetviewposition = google.maps.ControlPosition.LEFT_CENTER;
        } else if (streetviewposition == 'RIGHT_CENTER') {
            streetviewposition = google.maps.ControlPosition.RIGHT_CENTER;
        } else if (streetviewposition == 'BOTTOM_CENTER') {
            streetviewposition = google.maps.ControlPosition.BOTTOM_CENTER;
        } else if (streetviewposition == 'BOTTOM_RIGHT') {
            streetviewposition = google.maps.ControlPosition.BOTTOM_RIGHT;
        } else if (streetviewposition == 'BOTTOM_LEFT') {
            streetviewposition = google.maps.ControlPosition.BOTTOM_LEFT;
        } else if (streetviewposition == 'TOP_LEFT') {
            streetviewposition = google.maps.ControlPosition.TOP_LEFT;
        } else {
            streetviewposition = google.maps.ControlPosition.RIGHT_BOTTOM;
        }

        function initMap() {
            var geocoder = new google.maps.Geocoder();

            //geocoder.geocode({ 'address': address }, function(results, status) {

            var myLatLng = { lat: parseFloat(map_lat), lng: parseFloat(map_lng) };
            var map = new google.maps.Map(mapid[0], {
                center: myLatLng,
                zoom: zoom,
                disableDefaultUI: defaultui,
                zoomControl: zoomcontrol,
                zoomControlOptions: {
                    position: zoomcontrolposition
                },
                mapTypeId: maptype,
                mapTypeControl: maptypecontrol,
                mapTypeControlOptions: {
                    style: maptypecontrolstyle,
                    position: maptypecontrolposition
                },
                streetViewControl: streetview,
                streetViewControlOptions: {
                    position: streetviewposition
                },
                styles: styles,
                gestureHandling: gesture_handling
            });

            var directionsService = new google.maps.DirectionsService();
            var directionsDisplay = new google.maps.DirectionsRenderer({
                suppressMarkers: true,
                map: map,
            });

            // Get the data and set the marker bounds
            var markersLocations = $(mapid).data('eb-locations'),
                markerBounds = new google.maps.LatLngBounds();

            var markerarray = [];
            var markersarray = [];

            // Loop through the data getting the address
            $.each(markersLocations, function(index, Element, content) {
                var content;
                if (Element.pin_marker_featured_image !== '') {
                    content = '<img src="' + Element.pin_marker_featured_image + '" class="eb-map-marker-featured-img" /><div class="eb-map-container"><h6>' + Element.title + '</h6><div class="eb-map-content">' + Element.content + '</div></div>';
                } else {
                    content = '<div class="eb-map-container"><h6>' + Element.title + '</h6><div class="eb-map-content">' + Element.content + '</div></div>';
                }
                var icon = '';
                if (Element.pin_icon !== '' ) {
                    if (Element.pin_icon == 'red') {
                        icon = EB_WP_URL.plugin_url + 'assets/images/marker_red.png';
                    } else if (Element.pin_icon == 'yellow') {
                        icon = EB_WP_URL.plugin_url + 'assets/images/marker_yellow.png';
                    } else if (Element.pin_icon == 'blue') {
                        icon = EB_WP_URL.plugin_url + 'assets/images/marker_blue.png';
                    } else if (Element.pin_icon == 'black') {
                        icon = EB_WP_URL.plugin_url + 'assets/images/marker_black.png';
                    } else if (Element.pin_icon == 'white') {
                        icon = EB_WP_URL.plugin_url + 'assets/images/marker_white.png';
                    } else if (Element.pin_icon == 'purple') {
                        icon = EB_WP_URL.plugin_url + 'assets/images/marker_purple.png';
                    } else if (Element.pin_icon == 'green') {
                        icon = EB_WP_URL.plugin_url + 'assets/images/marker_green.png';
                    } else if (Element.pin_icon == 'orange') {
                        icon = EB_WP_URL.plugin_url + 'assets/images/marker_orange.png';
                    } else if (Element.pin_icon == 'grey') {
                        icon = EB_WP_URL.plugin_url + 'assets/images/marker_grey.png';
                    } else {
                        icon = '';
                    }
                }
                if (Element.marker !== '') {
                    var icon_height = Element.icon_height.size,
                        icon_width = Element.icon_width.size,
                        icon_img = {
                            url: Element.marker,
                            origin: new google.maps.Point(0, 0),
                            size: new google.maps.Size(icon_width, icon_height),
                            scaledSize: new google.maps.Size(icon_width, icon_height)
                        };
                } else {
                    icon_img = '';
                }

                if (Element.pin_marker_use_url == 'yes') {
                    var pin_url = Element.pin_marker_url;
                } else {
                    var pin_url = '';
                }

                var labelobj = {};

                if (Element.label == 'yes') {
                    labelobj = {
                        text: Element.label_text,
                        color: Element.label_text_color,
                        fontSize: Element.label_text_size.size + Element.label_text_size.unit,
                        fontWeight: Element.label_text_weight
                    };
                } else {
                    labelobj = '';
                }

                // Add the markers
                marker = new google.maps.Marker({
                    map: map,
                    position: new google.maps.LatLng(parseFloat(Element.lat), parseFloat(Element.lng)),
                    icon: Element.marker !== '' ? icon_img : icon,
                    animation: pin_animation_drop == true ? google.maps.Animation.DROP : '',
                    label: labelobj,
                    url: pin_url,
                });

                var markerobj = {
                    lng: parseFloat(Element.lng),
                    lat: parseFloat(Element.lat),
                    marker: marker
                };

                markerarray.push(markerobj);

                markersarray.push(marker);

                if (marker_center == true) {
                    //center the map to the geometric center of all markers
                    //map.setCenter(markerBounds.getCenter());
                    markerBounds.extend(marker.getPosition());
                    google.maps.event.addListener(map, 'zoom_changed', function() {
                        zoomChangeBoundsListener =
                            google.maps.event.addListenerOnce(map, 'bounds_changed', function(event) {
                                if (this.initialZoom == true) {
                                    this.setZoom(zoom);
                                    this.initialZoom = false;
                                }
                                google.maps.event.removeListener(zoomChangeBoundsListener);
                            });
                    });
                    map.initialZoom = true;
                    map.fitBounds(markerBounds);
                }

                if (Element.load_infowindow == 'yes') {
                    LoadInfoWindow(map, marker, content, icon_height)
                }

                if (Element.title !== '') {
                    addInfoWindow(map, marker, content, icon_height)
                } else if (Element.content !== '') {
                    addInfoWindow(map, marker, content, icon_height)
                }

                if (infowindow_onload == true) {
                    if (Element.title !== '') {
                        LoadInfoWindow(map, marker, content, icon_height)
                    } else if (Element.content !== '') {
                        LoadInfoWindow(map, marker, content, icon_height)
                    }
                }
                if (pin_url !== '') {
                    google.maps.event.addListener(marker, Element.pin_url_trigger_method, function() {
                        if (Element.pin_marker_url_target == 'on') {
                            window.open(this.url);
                        } else {
                            window.location.href = this.url;
                        }
                    });
                }
                //});
            });
            
            if (marker_clustering == true) {
                var options = {
                    imagePath: EB_WP_URL.plugin_url + 'assets/images/m'
                };
                var markerCluster = new MarkerClusterer(map, markersarray, options);
            }

            function RenderDirection(map, response, directionsDisplay) {
                directionsDisplay.setDirections(response);
                directionsDisplay.setMap(map);
            }

            function calcRoute() {
                var selected = $scope.find(".eb-map-routeTo").find('option:selected');
                var latt = selected.data('eb-pin-lat');
                var lngg = selected.data('eb-pin-lng');
                var destination = { lat: latt, lng: lngg };
                var setpanel = $scope.find(".eb-map-panel");
                var request = {
                    origin: $(".eb-map-routeFrom").val(),
                    destination: destination,
                    travelMode: google.maps.TravelMode[$(".eb-map-routeMode").val()]
                };
                directionsService.route(request, function(response, status) {
                    /* Use Geocoder to get address */
                    var google_maps_geocoder = new google.maps.Geocoder();
                    google_maps_geocoder.geocode( { 'address': $(".eb-map-routeFrom").val()}, function(results, status) {
                        if (status == 'OK') {
                            var pos = results[0].geometry.location;
                            processAddress(results[0].formatted_address);
                            thisMarker.setPosition(pos);
                            thisInfoWindow.setContent('<div class="eb-map-container"><h6>Location</h6>' + results[0].formatted_address + '</div>');
                            thisInfoWindow.setPosition(pos);
                            thisInfoWindow.open(map, thisMarker);
                            map.setCenter(pos);
                            map.setZoom(14);
                        } else {
                            alert('Geocode was not successful for the following reason: ' + status);
                        }
                    });
                    if (status == google.maps.DirectionsStatus.OK) {
                        mapsatwork = false;
                        RenderDirection(map, response, directionsDisplay);
                        directionsDisplay.setPanel(setpanel[0]);
                    }
                });
            }

            $scope.find(".eb-map-routeGo").on("click", function() {
                calcRoute();
            });

            var mapsatwork = false;
            var thisInfoWindow = new google.maps.InfoWindow({
                maxWidth: infowindow_max_width
            });
            var thisMarker = new google.maps.Marker({
                map: map
            });

            $scope.find('.eb-map-get-address').click(function(e) {
                e.preventDefault();

                var is_chrome = /chrom(e|ium)/.test(navigator.userAgent.toLowerCase());
                var is_ssl = 'https:' == document.location.protocol;
                if (is_chrome && !is_ssl) {
                    return false;
                }
                if (navigator.geolocation) {

                    navigator.geolocation.getCurrentPosition(function(position) {
                        /* Current Coordinate */
                        var lat = position.coords.latitude;
                        var lng = position.coords.longitude;
                        var google_map_pos = new google.maps.LatLng(lat, lng);

                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };

                        /* Use Geocoder to get address */
                        var google_maps_geocoder = new google.maps.Geocoder();
                        google_maps_geocoder.geocode({ 'latLng': google_map_pos },
                            function(results, status) {
                                if (status == google.maps.GeocoderStatus.OK && results[0]) {
                                    processAddress(results[0].formatted_address);
                                    thisMarker.setPosition(pos);
                                    thisInfoWindow.setContent('<div class="eb-map-container"><h6>Current location</h6>' + results[0].formatted_address + '</div>');
                                    thisInfoWindow.setPosition(pos);
                                    thisInfoWindow.open(map, thisMarker);
                                    map.setCenter(pos);
                                    map.setZoom(14);
                                }
                            }
                        );
                    }, function() {
                        handleLocationError(true, infoWindow, map.getCenter());
                    });
                } else {
                    // Browser doesn't support Geolocation
                    handleLocationError(false, infoWindow, map.getCenter());
                }
            });

            $scope.find('.eb-map-marker-listing-widget').each(function(i) {
                var findmap = $(mapid).attr('id'),
                    zoom = $(this).data('eb-pin-listing-zoom'),
                    listing = $(this).find('.eb-map-marker-listing'),
                    click = $(this).find('.eb-map-marker-listing-click'),
                    windowopen = $(this).data('eb-pin-listing-infowindow');

                $(click).on('click', function(event) {
                    window.location.href = '#' + findmap;
                    latLng = new google.maps.LatLng(parseFloat($(this).closest('.eb-map-marker-listing').data('eb-pin-lat')), parseFloat($(this).closest('.eb-map-marker-listing').data('eb-pin-lng')));
                    map.panTo(latLng);
                    map.setZoom(zoom);
                    if (windowopen == true) {
                        for (i = 0; i < markerarray.length; i++) {
                            if (markerarray[i].lat == parseFloat($(this).closest('.eb-map-marker-listing').data('eb-pin-lat')) && markerarray[i].lng == parseFloat($(this).closest('.eb-map-marker-listing').data('eb-pin-lng'))) {
                                marker = markerarray[i].marker;
                            }
                        }
                        google.maps.event.trigger(marker, 'click');
                    }
                });

                $(click).hover(function() {
                    $(this).css('cursor', 'pointer');
                });

                //direction click
                var direction = $(this).find('.eb-map-marker-listing-direction');

                $(direction).on('click', function(event, v) {
                    if (mapsatwork) {
                        return;
                    }
                    mapsatwork = true;
                    /* Chrome need SSL! */
                    var is_chrome = /chrom(e|ium)/.test(navigator.userAgent.toLowerCase());
                    var is_ssl = 'https:' == document.location.protocol;
                    if (is_chrome && !is_ssl) {
                        return false;
                    }
                    if (navigator.geolocation) {

                        navigator.geolocation.getCurrentPosition(function(position) {
                            /* Current Coordinate */
                            var lat = position.coords.latitude;
                            var lng = position.coords.longitude;
                            var google_map_pos = new google.maps.LatLng(lat, lng);

                            var pos = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                            };

                            /* Use Geocoder to get address */
                            var google_maps_geocoder = new google.maps.Geocoder();
                            google_maps_geocoder.geocode({ 'latLng': google_map_pos },
                                function(results, status) {
                                    if (status == google.maps.GeocoderStatus.OK && results[0]) {
                                        processAddress(results[0].formatted_address);
                                        thisMarker.setPosition(pos);
                                        thisInfoWindow.setContent('<div class="eb-map-container"><h6>Current location</h6>' + results[0].formatted_address + '</div>');
                                        thisInfoWindow.setPosition(pos);
                                        thisInfoWindow.open(map, thisMarker);
                                        map.setCenter(pos);
                                        map.setZoom(14);
                                    }

                                    var latt = parseFloat($(event.currentTarget).parent().data('eb-pin-lat'));
                                    var lngg = parseFloat($(event.currentTarget).parent().data('eb-pin-lng'));
                                    var destination = { lat: latt, lng: lngg };
                                    var request = {
                                        origin: pos,
                                        destination: destination,
                                        travelMode: google.maps.TravelMode['DRIVING']
                                    };
                                    directionsService.route(request, function(response, status) {
                                        if (status == google.maps.DirectionsStatus.OK) {
                                            RenderDirection(map, response, directionsDisplay);
                                            //directionsDisplay.setPanel(setpanel[0]);
                                            mapsatwork = false;
                                        }
                                    });

                                }
                            );
                        }, function() {
                            handleLocationError(true, thisInfoWindow, map.getCenter());
                        });
                    } else {
                        handleLocationError(false, thisInfoWindow, map.getCenter());
                    }
                    $(direction).hover(function() {
                        $(this).css('cursor', 'pointer');
                    });
                });

            });
        }

        function processAddress(address) {
            $(".eb-map-routeFrom").val(address);
        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
        }

        function LoadInfoWindow(map, marker, content, icon_height) {
            
            var markerWidthSize = '-' + parseFloat(infowindow_max_width / 2),
                markerHeight;
            if (Element.marker_pin_on_off === 'on') {
                markerHeight = '-' + (parseFloat(icon_height) + 14);
            } else {
                markerHeight = '-58';
            }

            var myOptions = {
                    boxClass: 'eb-infowindow',
                    pixelOffset: new google.maps.Size(parseFloat(markerWidthSize), parseFloat(markerHeight)),
                    maxWidth: infowindow_max_width,
                    alignBottom: true,
                    zIndex: null,
                    boxStyle: {
                        width: infowindow_max_width + "px"
                    },
                    closeBoxURL: infowindow_close_icon,
                    infoBoxClearance: new google.maps.Size(1, 1),
                    isHidden: false,
                    pane: "floatPane",
                    enableEventPropagation: false
                };

            if (infowindow_styles === 'custom') {
                var infowindow = new InfoBox(myOptions);
            } else {
                var infowindow = new google.maps.InfoWindow({
                    maxWidth: infowindow_max_width
                });
            }

            infowindow_active = infowindow;

            if (infowindow_active) {
                infowindow_active.close();
            }

            infowindow.setContent(content);
            infowindow.open(map, marker);

            google.maps.event.addListener(map, 'click', function() {
                infowindow.close();
            });
        }
        

        function addInfoWindow(map, marker, content, icon_height) {
            var markerWidthSize = '-' + parseFloat(infowindow_max_width / 2),
                markerHeight;
            if (Element.marker_pin_on_off === 'on') {
                markerHeight = '-' + (parseFloat(icon_height) + 14);
            } else {
                markerHeight = '-58';
            }

            var myOptions = {
                    boxClass: 'eb-infowindow',
                    pixelOffset: new google.maps.Size(parseFloat(markerWidthSize), parseFloat(markerHeight)),
                    maxWidth: infowindow_max_width,
                    alignBottom: true,
                    zIndex: null,
                    boxStyle: {
                        width: infowindow_max_width + "px"
                    },
                    closeBoxURL: infowindow_close_icon,
                    infoBoxClearance: new google.maps.Size(1, 1),
                    isHidden: false,
                    pane: "floatPane",
                    enableEventPropagation: false
                };

            if (infowindow_styles === 'custom') {
                var infowindow = new InfoBox(myOptions);
            } else {
                var infowindow = new google.maps.InfoWindow({
                    maxWidth: infowindow_max_width
                });
            }

            google.maps.event.addListener(marker, pin_trigger_method, function() {
                if (infowindow_active) {
                    infowindow_active.close();
                }

                infowindow_active = infowindow;

                infowindow.setContent(content);
                infowindow.open(map, this);
                if (pin_animation_bounce == true) {
                    marker.setAnimation(google.maps.Animation.BOUNCE);
                    setTimeout(function() { marker.setAnimation(null); }, pin_bounce_duration);
                }
                if (pin_animation_drop == true) {
                    setTimeout(function() { marker.setAnimation(google.maps.Animation.DROP); }, 200);
                }
            });

            google.maps.event.addListener(map, 'click', function() {
                infowindow.close();
            });

        }

        initMap();

    };
    $(window).on('elementor/frontend/init', function() {
        elementorFrontend.hooks.addAction('frontend/element_ready/eb-google-map-extended.default', EBGoogleMapHandler);
    });
})(jQuery);