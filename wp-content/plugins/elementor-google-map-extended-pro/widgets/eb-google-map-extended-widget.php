<?php
namespace Elementor;
use icss_eme;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class EB_Google_Map_Extended_Pro extends Widget_Base {

	/**
	 * Retrieve heading widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'eb-google-map-extended';
	}

	/**
	 * Retrieve heading widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Google Map Extended', 'elementor' );
	}

	/**
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-google-maps';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'eb-elementor-extended' ];
	}

	public function get_script_depends() {
		return [ 'eb-google-maps-markerclusterer', 'eb-google-maps-infobox', 'eb-google-maps-api', 'eb-google-map' ];
	}

	/**
	 * Get button sizes.
	 *
	 * Retrieve an array of button sizes for the button widget.
	 *
	 * @since 1.0.0
	 * @access public
	 * @static
	 *
	 * @return array An array containing button sizes.
	 */
	public static function get_button_sizes() {
		return [
			'xs' => __( 'Extra Small', 'elementor' ),
			'sm' => __( 'Small', 'elementor' ),
			'md' => __( 'Medium', 'elementor' ),
			'lg' => __( 'Large', 'elementor' ),
			'xl' => __( 'Extra Large', 'elementor' ),
		];
	}


	/**
	 * Register google maps widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		$this->start_controls_section(
			'section_map',
			[
				'label' => __( 'Map', 'elementor' ),
			]
		);

		if (empty( icss_eme\eb_map_get_option( 'eb_google_map_api_key', 'eb_map_general_settings' ))) {
			$this->add_control(
			    'api_notice',
			    [
				    'type'  => Controls_Manager::RAW_HTML,
				    'raw'   => 'Please enter your Google Map API <a target="_blank" href="'.admin_url('admin.php?page=eb_google_map_setting').'">here</a>',
				    'label_block' => true,
			    ]
		    );
		}

		$this->add_control(
		    'map_notice',
		    [
			    'label' => __( 'Find Latitude & Longitude', 'elementor' ),
			    'type'  => Controls_Manager::RAW_HTML,
			    'raw'   => '<form onsubmit="ebMapFindAddress(this);" action="javascript:void(0);"><input type="text" id="eb-map-find-address" class="eb-map-find-address" style="margin-top:10px; margin-bottom:10px;"><input type="submit" value="Search" class="elementor-button elementor-button-default" onclick="ebMapFindAddress(this)"></form><div id="eb-output-result" class="eb-output-result" style="margin-top:10px; line-height: 1.3; font-size: 12px;"></div>',
			    'label_block' => true,
		    ]
	    );

		$this->add_control(
			'map_lat',
			[
				'label' => __( 'Latitude', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => '1.2833754',
				'default' => '1.2833754',
				'dynamic' => [ 'active' => true ]
			]
		);

		$this->add_control(
			'map_lng',
			[
				'label' => __( 'Longitude', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => '103.86072639999998',
				'default' => '103.86072639999998',
				'separator' => true,
				'dynamic' => [ 'active' => true ]
			]
		);

		$this->add_control(
			'marker_center',
			[
				'label' => __( 'Center Map', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'description' => __('If you have multiple markers, this will get all markers on the map and center them. Make use of the zoom level below to adjust to your liking.'),
			]
		);

		$this->add_control(
			'zoom',
			[
				'label' => __( 'Zoom Level', 'elementor' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 15,
				],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 25,
					],
				],
			]
		);

		$this->add_responsive_control(
			'height',
			[
				'label' => __( 'Height', 'elementor' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', 'vh' ],
				'default'   => [
				 	'size' => 300,
				],
				'range' => [
					'vh' => [
						'min' => 10,
						'max' => 100,
					],
					'px' => [
						'min' => 40,
						'max' => 1440,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .eb-map' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'map_type',
			[
				'label' => __( 'Map Type', 'elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'roadmap' => __( 'Road Map', 'elementor' ),
					'satellite' => __( 'Satellite', 'elementor' ),
					'hybrid' => __( 'Hybrid', 'elementor' ),
					'terrain' => __( 'Terrain', 'elementor' ),
				],
				'default' => 'roadmap',
			]
		);

		$this->add_control(
			'gesture_handling',
			[
				'label' => __( 'Gesture Handling', 'elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'auto' => __( 'Auto (Default)', 'elementor' ),
					'cooperative' => __( 'Cooperative', 'elementor' ),
					'greedy' => __( 'Greedy', 'elementor' ),
					'none' => __( 'None', 'elementor' ),
				],
				'default' => 'auto',
				'description' => __('Understand more about Gesture Handling by reading it <a href="https://developers.google.com/maps/documentation/javascript/reference/3/#MapOptions" target="_blank">here.</a> Basically it control how it handles gestures on the map. Used to be draggable and scroll wheel function which is deprecated.'),
			]
		);

		$this->add_control(
			'zoom_control',
			[
				'label' => __( 'Show Zoom Control', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes'
			]
		);

		$this->add_control(
			'zoom_control_position',
			[
				'label' => __( 'Control Position', 'elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'RIGHT_BOTTOM' => __( 'Bottom Right (Default)', 'elementor' ),
					'TOP_LEFT' => __( 'Top Left', 'elementor' ),
					'TOP_CENTER' => __( 'Top Center', 'elementor' ),
					'TOP_RIGHT' => __( 'Top Right', 'elementor' ),
					'LEFT_CENTER' => __( 'Left Center', 'elementor' ),
					'RIGHT_CENTER' => __( 'Right Center', 'elementor' ),
					'BOTTOM_LEFT' => __( 'Bottom Left', 'elementor' ),
					'BOTTOM_CENTER' => __( 'Bottom Center', 'elementor' ),
					'BOTTOM_RIGHT' => __( 'Bottom Right', 'elementor' ),
				],
				'default' => 'RIGHT_BOTTOM',
				'condition' => [
					'zoom_control' => 'yes',
				],
				'separator' => false,
			]
		);

		$this->add_control(
			'default_ui',
			[
				'label' => __( 'Show Default UI', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes'
			]
		);

		$this->add_control(
			'map_type_control',
			[
				'label' => __( 'Map Type Control', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes'
			]
		);

		$this->add_control(
			'map_type_control_style',
			[
				'label' => __( 'Control Styles', 'elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'DEFAULT' => __( 'Default', 'elementor' ),
					'HORIZONTAL_BAR' => __( 'Horizontal Bar', 'elementor' ),
					'DROPDOWN_MENU' => __( 'Dropdown Menu', 'elementor' ),
				],
				'default' => 'DEFAULT',
				'condition' => [
					'map_type_control' => 'yes',
				],
				'separator' => false,
			]
		);

		$this->add_control(
			'map_type_control_position',
			[
				'label' => __( 'Control Position', 'elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'TOP_LEFT' => __( 'Top Left (Default)', 'elementor' ),
					'TOP_CENTER' => __( 'Top Center', 'elementor' ),
					'TOP_RIGHT' => __( 'Top Right', 'elementor' ),
					'LEFT_CENTER' => __( 'Left Center', 'elementor' ),
					'RIGHT_CENTER' => __( 'Right Center', 'elementor' ),
					'BOTTOM_LEFT' => __( 'Bottom Left', 'elementor' ),
					'BOTTOM_CENTER' => __( 'Bottom Center', 'elementor' ),
					'BOTTOM_RIGHT' => __( 'Bottom Right', 'elementor' ),
				],
				'default' => 'TOP_LEFT',
				'condition' => [
					'map_type_control' => 'yes',
				],
				'separator' => false,
			]
		);

		$this->add_control(
			'streetview_control',
			[
				'label' => __( 'Show Streetview Control', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no'
			]
		);

		$this->add_control(
			'streetview_control_position',
			[
				'label' => __( 'Streetview Position', 'elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'RIGHT_BOTTOM' => __( 'Bottom Right (Default)', 'elementor' ),
					'TOP_LEFT' => __( 'Top Left', 'elementor' ),
					'TOP_CENTER' => __( 'Top Center', 'elementor' ),
					'TOP_RIGHT' => __( 'Top Right', 'elementor' ),
					'LEFT_CENTER' => __( 'Left Center', 'elementor' ),
					'RIGHT_CENTER' => __( 'Right Center', 'elementor' ),
					'BOTTOM_LEFT' => __( 'Bottom Left', 'elementor' ),
					'BOTTOM_CENTER' => __( 'Bottom Center', 'elementor' ),
					'BOTTOM_RIGHT' => __( 'Bottom Right', 'elementor' ),
				],
				'default' => 'RIGHT_BOTTOM',
				'condition' => [
					'streetview_control' => 'yes',
				],
				'separator' => false,
			]
		);

		$this->add_control(
			'custom_map_style',
			[
				'label' => __( 'Custom Map Style', 'elementor' ),
				'type' => Controls_Manager::TEXTAREA,
				'description' => __('Add style from <a href="https://mapstyle.withgoogle.com/" target="_blank">Google Map Styling Wizard</a> or <a href="https://snazzymaps.com/explore" target="_blank">Snazzy Maps</a>. Copy and Paste the style in the textarea.'),
				'condition' => [
					'map_type' => 'roadmap',
				],
			]
		);

		$this->add_control(
			'view',
			[
				'label' => __( 'View', 'elementor' ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$this->end_controls_section();

		/*Pins Option*/
		$this->start_controls_section(
			'map_marker_pin',
			[
				'label' => __( 'Marker Pins', 'elementor' ),
			]
		);

		$this->add_control(
			'pin_trigger_method',
			[
				'label' => __( 'Trigger Method for InfoWindow', 'elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'click' => __( 'On Click', 'elementor' ),
					'mouseover' => __( 'On Hover', 'elementor' ),
				],
				'default' => 'click',
			]
		);

		$this->add_control(
			'pin_animation_bounce',
			[
				'label' => __( 'Bounce Animation', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no'
			]
		);

		$this->add_control(
			'pin_animation_bounce_duration',
			[
				'label' => __( 'Animation Duration', 'elementor' ) . ' (ms)',
				'type' => Controls_Manager::NUMBER,
				'default' => '750',
				'min' => 0,
				'step' => 10000,
				'condition' => [
					'pin_animation_bounce' => 'yes',
				],
				'render_type' => 'none',
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'pin_animation_drop',
			[
				'label' => __( 'Drop Animation on load', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no'
			]
		);

		$this->add_control(
			'infowindow_onload',
			[
				'label' => __( 'Show All InfoWindow on load', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no'
			]
		);

		$this->add_control(
			'infowindow_max_width',
			[
				'label' => __( 'InfoWindow Max Width', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => '250',
				'default' => '250',
			]
		);

		$this->add_control(
			'marker_clustering',
			[
				'label' => __( 'Marker Clustering', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
			]
		);

		$repeater = new Repeater();

		$repeater->add_control(
			'pin_notice',
			[
				'label' => __( 'Find Latitude & Longitude', 'elementor' ),
			    'type'  => Controls_Manager::RAW_HTML,
			    'raw'   => '<form onsubmit="ebMapFindPinAddress(this);" action="javascript:void(0);"><input type="text" id="eb-map-find-address" class="eb-map-find-address" style="margin-top:10px; margin-bottom:10px;"><input type="submit" value="Search" class="elementor-button elementor-button-default" onclick="ebMapFindPinAddress(this)"></form><div id="eb-output-result" class="eb-output-result" style="margin-top:10px; line-height: 1.3; font-size: 12px;"></div>',
			    'label_block' => true,
		    ]
		);

		$repeater->add_control(
			'pin_lat',
			[
				'label' => __( 'Latitude', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [ 'active' => true ]
		    ]
		);

		$repeater->add_control(
			'pin_lng',
			[
				'label' => __( 'Longitude', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [ 'active' => true ]
			]
		);

		$repeater->add_control(
			'infowindow_this_onload',
			[
				'label' => __( 'Show this InfoWindow on load', 'elementor' ),
				'description' => __( 'Note: Make sure "Show All InfoWindow on load" is not enabled.', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
			]
		);

		$repeater->add_control(
			'pin_icon',
			[
				'label' => __( 'Marker Icon', 'elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'' => __( 'Default (Google)', 'elementor' ),
					'red' => __( 'Red', 'elementor' ),
					'blue' => __( 'Blue', 'elementor' ),
					'yellow' => __( 'Yellow', 'elementor' ),
					'purple' => __( 'Purple', 'elementor' ),
					'green' => __( 'Green', 'elementor' ),
					'orange' => __( 'Orange', 'elementor' ),
					'grey' => __( 'Grey', 'elementor' ),
					'white' => __( 'White', 'elementor' ),
					'black' => __( 'Black', 'elementor' ),
				],
				'default' => '',
				'separator' => 'before',
			]
		);

		$repeater->add_control(
			'pin_marker',
			[
				'label' => __( 'Choose Pin Marker Image/Icon', 'elementor' ),
				'type' => Controls_Manager::MEDIA,
			]
		);

		$repeater->add_control(
			'pin_marker_height',
			[
				'label' => __( 'Icon Height', 'elementor' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 100,
					],
				],
				'condition' => [
					'pin_marker!' => '',
				],
			]
		);

		$repeater->add_control(
			'pin_marker_width',
			[
				'label' => __( 'Icon Width', 'elementor' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 100,
					],
				],
				'condition' => [
					'pin_marker!' => '',
				],
			]
		);

		$repeater->add_control(
			'pin_marker_label',
			[
				'label' => __( 'Use Label Text', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'separator' => 'before',
			]
		);

		$repeater->add_control(
			'pin_marker_use_url',
			[
				'label' => __( 'Open URL Link', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no'
			]
		);

		$repeater->add_control(
			'pin_url_trigger_method',
			[
				'label' => __( 'Trigger Method', 'elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'click' => __( 'On Click', 'elementor' ),
					'mouseover' => __( 'On Hover', 'elementor' ),
				],
				'default' => 'click',
				'condition' => [
					'pin_marker_use_url' => 'yes',
				],
			]
		);

		$repeater->add_control(
			'pin_marker_url',
			[
				'label' => __( 'Link', 'elementor' ),
				'type' => Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'elementor' ),
				'show_external' => true,
				'condition' => [
					'pin_marker_use_url' => 'yes',
				],
			]
		);

		$repeater->add_control(
			'pin_marker_label_text',
			[
				'label' => __( 'Label Text', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Text' , 'elementor' ),
				'label_block' => true,
				'condition' => [
					'pin_marker_label' => 'yes',
				],
			]
		);

		$repeater->add_control(
			'pin_marker_label_text_color',
			[
				'label' => __( 'Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'condition' => [
					'pin_marker_label' => 'yes',
				],
			]
		);

		$repeater->add_control(
			'pin_marker_label_text_size',
			[
				'label' => __( 'Font size', 'elementor' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 200,
					],
				],
				'condition' => [
					'pin_marker_label' => 'yes',
				],
			]
		);

		$repeater->add_control(
			'pin_marker_label_text_weight',
			[
				'label' => __( 'Font Weight', 'elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'100' => __( '100', 'elementor' ),
					'200' => __( '200', 'elementor' ),
					'300' => __( '300', 'elementor' ),
					'400' => __( '400', 'elementor' ),
					'500' => __( '500', 'elementor' ),
					'600' => __( '600', 'elementor' ),
					'700' => __( '700', 'elementor' ),
					'800' => __( '800', 'elementor' ),
					'900' => __( '900', 'elementor' ),
					'default' => __( 'Default', 'elementor' ),
					'normal' => __( 'Normal', 'elementor' ),
					'bold' => __( 'Bold', 'elementor' ),
				],
				'default' => 'default',
				'condition' => [
					'pin_marker_label' => 'yes',
				],
			]
		);

		$repeater->add_control(
			'pin_marker_featured_image',
			[
				'label' => __( 'Choose a Featured Image for this marker', 'elementor' ),
				'type' => Controls_Manager::MEDIA,
				'description' => __( 'Note: This will show a featured image on top of the infowindow when you are using Custom Styling for InfoWindow.', 'elementor' ),
				'separator' => 'before',
			]
		);

		$repeater->add_control(
			'pin_title',
			[
				'label' => __( 'Title', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Pin Title' , 'elementor' ),
				'label_block' => true,
				'dynamic' => [ 'active' => true ]
			]
		);

		$repeater->add_control(
			'pin_content',
			[
				'label' => __( 'Content', 'elementor' ),
				'type' => Controls_Manager::WYSIWYG,
				'default' => __( 'Pin Content', 'elementor' ),
				'dynamic' => [ 'active' => true ]
			]
		);

		$this->add_control(
			'tabs',
			[
				'label' => __( 'Pin Item', 'elementor' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'pin_marker' => __( 'Pin Marker', 'elementor' ),
						'pin_marker_height' => __( 'Icon Height', 'elementor' ),
						'pin_marker_width' => __( 'Icon Width', 'elementor' ),
						'pin_title' => __( 'Pin #1', 'elementor' ),
						'pin_notice' => __( 'Find Latitude & Longitude', 'elementor' ),
						'pin_lat' => __( '1.2833754', 'elementor' ),
						'pin_lng' => __( '103.86072639999998', 'elementor' ),
						'pin_content' => __( 'I am item content. Click edit button to change this text. Lorem ipsum dolor sit amet.', 'elementor' ),
					],
				],
				'title_field' => '{{{ pin_title }}}',
				'separator' => 'before',
			]
		);


		$this->end_controls_section();

		/*Marker Listing Option*/
		$this->start_controls_section(
			'marker_listing_section',
			[
				'label' => __( 'Marker Listing', 'elementor' ),
			]
		);

		$this->add_control(
			'map_marker_listing',
			[
				'label' => __( 'Marker Listing', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'description' => __('This will display your markers information either on the top or the bottom of the map. Clicking on relevant marker will show up on the map.'),
			]
		);

		$this->add_control(
			'map_marker_listing_placement',
			[
				'label' => __( 'Listing Placement', 'elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'top' => __( 'Top of the Map', 'elementor' ),
					'bottom' => __( 'Bottom of the Map', 'elementor' ),
				],
				'default' => 'bottom',
				'condition' => [
					'map_marker_listing' => 'yes',
				],
			]
		);

		$this->add_control(
			'marker_listing_zoom',
			[
				'label' => __( 'Zoom', 'elementor' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 15,
				],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 25,
					],
				],
				'condition' => [
					'map_marker_listing' => 'yes',
				],
			]
		);

		$this->add_control(
			'map_marker_listing_infowindow',
			[
				'label' => __( 'Show InfoWindow?', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'description' => __('This will display InfoWindow on your markers when clicking on the listing.'),
				'condition' => [
					'map_marker_listing' => 'yes',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_map_direction',
			[
				'label' => __( 'Map Direction', 'elementor' ),
			]
		);

		$this->add_control(
			'map_direction',
			[
				'label' => __( 'Show Direction', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'description' => __('This will allow your visitors to search from their current location to the selected marker.'),
			]
		);

		$this->add_control(
			'map_text_direction',
			[
				'label' => __( 'Show Text Direction', 'elementor' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'description' => __('This will show the step by step directions in text.'),
			]
		);

		$this->add_control(
			'map_direction_input_placeholder',
			[
				'label' => __( 'Input Placeholder', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => 'Search current location..',
				'default' => 'Search current location..',
				'description' => __('Change the placeholder of the Address Input.'),
				'label_block' => true,
				'separator' => 'before',
				'condition' => [
					'map_direction' => 'yes',
				],
			]
		);

		$this->add_control(
			'btn_location_icon',
			[
				'label' => __( 'Location Button Icon', 'elementor' ),
				'type' => Controls_Manager::ICON,
				'label_block' => true,
				'default' => 'fa fa-compass',
			]
		);
	
		$this->add_control(
			'map_direction_route_btn',
			[
				'label' => __( 'Route Button', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => 'Route',
				'default' => 'Route',
				'description' => __('Change the text of the Route Button.'),
				'label_block' => true,
				'separator' => 'before',
				'condition' => [
					'map_direction' => 'yes',
				],
			]
		);

		$this->add_control(
			'btn_route_size',
			[
				'label' => __( 'Route Button Size', 'elementor' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'xs',
				'options' => self::get_button_sizes(),
			]
		);

		$this->add_control(
			'btn_route_icon',
			[
				'label' => __( 'Route Button Icon', 'elementor' ),
				'type' => Controls_Manager::ICON,
				'label_block' => true,
				'default' => '',
			]
		);

		$this->add_control(
			'btn_route_icon_align',
			[
				'label' => __( 'Route Button Icon Position', 'elementor' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'left',
				'options' => [
					'left' => __( 'Before', 'elementor' ),
					'right' => __( 'After', 'elementor' ),
				],
				'condition' => [
					'btn_route_icon!' => '',
				],
			]
		);

		$this->add_control(
			'btn_route_icon_indent',
			[
				'label' => __( 'Route Button Icon Spacing', 'elementor' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'max' => 50,
					],
				],
				'condition' => [
					'btn_route_icon!' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-button.eb-map-routeGo .elementor-align-icon-right' => 'margin-left: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .elementor-button.eb-map-routeGo .elementor-align-icon-left' => 'margin-right: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'map_direction_listing_text',
			[
				'label' => __( 'Direction Text', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => 'Direction to here',
				'default' => 'Direction to here',
				'description' => __('Change the text of the Direction on the Marker Listing.'),
				'label_block' => true,
				'separator' => 'before',
				'condition' => [
					'map_marker_listing' => 'yes',
				],
			]
		);

		$this->end_controls_section();

		/*Main Style*/
		$this->start_controls_section(
			'section_main_style',
			[
				'label' => __( 'Pin Global Styles', 'elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'pin_header_color',
			[
				'label' => __( 'Title Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eb-map-container h6' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => __( 'Title Typography', 'elementor' ),
				'name' => 'pin_header_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .eb-map-container h6',
			]
		);

		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'label' => __( 'Title Text Shadow', 'elementor' ),
				'name' => 'pin_header_text_shadow',
				'selector' => '{{WRAPPER}} .eb-map-container h6',
				'separator' => true,
			]
		);


		$this->add_control(
			'pin_content_color',
			[
				'label' => __( 'Content Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eb-map-container .eb-map-content' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => __( 'Content Typography', 'elementor' ),
				'name' => 'pin_content_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .eb-map-container .eb-map-content',
			]
		);

		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'label' => __( 'Content Text Shadow', 'elementor' ),
				'name' => 'pin_content_text_shadow',
				'selector' => '{{WRAPPER}} .eb-map-container .eb-map-content',
				'separator' => true,
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', 'elementor' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elementor' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'elementor' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'elementor' ),
						'icon' => 'fa fa-align-right',
					],
					'justify' => [
						'title' => __( 'Justified', 'elementor' ),
						'icon' => 'fa fa-align-justify',
					],
				],
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eb-map-container' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_infowindow_style',
			[
				'label' => __( 'InfoWindow Styles', 'elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'infowindow_styles',
			[
				'label' => __( 'Style', 'elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'default' => __( 'Google (Default)', 'elementor' ),
					'custom' => __( 'Custom Styles', 'elementor' ),
				],
				'default' => 'default',
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'infowindow_background',
				'label' => __( 'Background Color', 'elementor' ),
				'types' => [ 'classic' ],
				'selector' => '{{WRAPPER}} .eb-infowindow',
				'separator' => 'before',
				'condition' => [
					'infowindow_styles' => 'custom',
				],
			]
		);

		
		$this->add_control(
			'infowindow_close_icon',
			[
				'label' => __( 'Close Icon', 'elementor' ),
				'type' => Controls_Manager::ICON,
				'default' => 'fa fa-close',
				'condition' => [
					'infowindow_styles' => 'custom',
				],
			]
		);

		$this->add_control(
			'infowindow_close_icon_color',
			[
				'label' => __( 'Close Icon Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#ffffff',
				'selectors' => [
					'{{WRAPPER}} .eb-infowindow .eb-map-close' => 'color: {{VALUE}};',
				],
				'condition' => [
					'infowindow_styles' => 'custom',
				],
			]
		);

		$this->add_control(
			'infowindow_close_margin_gap',
			[
				'label' => __( 'Close Icon Margin', 'elementor' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'selectors' => [
					'{{WRAPPER}} .eb-map-close' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
				'condition' => [
					'infowindow_styles' => 'custom',
				],
			]
		);

		$this->add_control(
			'infowindow_arrow_color',
			[
				'label' => __( 'Arrow Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#ffffff',
				'selectors' => [
					'{{WRAPPER}} .eb-infowindow:after' => 'border-top-color: {{VALUE}};',
				],
				'condition' => [
					'infowindow_styles' => 'custom',
				],
				'separator' => 'before',
			]
		);


		$this->add_control(
			'infowindow_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .eb-infowindow' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					'{{WRAPPER}} .eb-infowindow .eb-map-marker-featured-img' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} 0 0;',
				],
				'separator' => 'before',
				'condition' => [
					'infowindow_styles' => 'custom',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'infowindow_box_shadow',
				'selector' => '{{WRAPPER}} .eb-infowindow',
				'condition' => [
					'infowindow_styles' => 'custom',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_marker_listing_style',
			[
				'label' => __( 'Marker Listing Styles', 'elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'map_marker_listing' => 'yes',
				],
			]
		);

		$this->add_control(
		    'map_marker_listing_header_notice',
		    [
			    'type'  => Controls_Manager::RAW_HTML,
			    'raw'   => '<div class="elementor-panel-heading-title elementor-section-title">Header Styles</div>',
		    ]
	    );

		$this->add_control(
			'map_marker_listing_header_color',
			[
				'label' => __( 'Header Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eb-map-marker-listing-widget tr th' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'map_marker_listing_header_bg_color',
			[
				'label' => __( 'Header Background Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eb-map-marker-listing-widget tr th' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => __( 'Header Typography', 'elementor' ),
				'name' => 'map_marker_listing_header_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .eb-map-marker-listing-widget tr th',
			]
		);

		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'label' => __( 'Header Text Shadow', 'elementor' ),
				'name' => 'map_marker_listing_header_text_shadow',
				'selector' => '{{WRAPPER}} .eb-map-marker-listing-widget tr th',
			]
		);

		$this->add_responsive_control(
			'map_marker_listing_header_align',
			[
				'label' => __( 'Header Alignment', 'elementor' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elementor' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'elementor' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'elementor' ),
						'icon' => 'fa fa-align-right',
					],
					'justify' => [
						'title' => __( 'Justified', 'elementor' ),
						'icon' => 'fa fa-align-justify',
					],
				],
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eb-map-marker-listing-widget tr th' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'map_marker_listing_header_text_padding',
			[
				'label' => __( 'Header Padding', 'elementor' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .eb-map-marker-listing-widget tr th' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'after',
			]
		);

		$this->add_control(
		    'map_marker_listing_row_notice',
		    [
			    'type'  => Controls_Manager::RAW_HTML,
			    'raw'   => '<div class="elementor-panel-heading-title elementor-section-title">Row Styles</div>',
		    ]
	    );

	    $this->add_control(
			'map_marker_listing_row_bg_color_even',
			[
				'label' => __( 'Row Background Color (Even)', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eb-map-marker-listing-widget tr.eb-map-marker-listing:nth-child(even)' => 'background-color: {{VALUE}};',
				],
				'default' => '#f7f7f7',
			]
		);

		$this->add_control(
			'map_marker_listing_row_color_even',
			[
				'label' => __( 'Row Color (Even)', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eb-map-marker-listing-widget tr.eb-map-marker-listing:nth-child(even)' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'map_marker_listing_row_bg_color_odd',
			[
				'label' => __( 'Row Background Color (Odd)', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eb-map-marker-listing-widget tr.eb-map-marker-listing:nth-child(odd)' => 'background-color: {{VALUE}};',
				],
				'default' => '#ffffff',
			]
		);

		$this->add_control(
			'map_marker_listing_row_color_odd',
			[
				'label' => __( 'Row Color (Odd)', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eb-map-marker-listing-widget tr.eb-map-marker-listing:nth-child(odd)' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => __( 'Row Typography', 'elementor' ),
				'name' => 'map_marker_listing_row_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .eb-map-marker-listing-widget tr.eb-map-marker-listing',
			]
		);

		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'label' => __( 'Row Text Shadow', 'elementor' ),
				'name' => 'map_marker_listing_row_text_shadow',
				'selector' => '{{WRAPPER}} .eb-map-marker-listing-widget tr.eb-map-marker-listing',
			]
		);

		$this->add_control(
			'map_marker_listing_row_text_padding',
			[
				'label' => __( 'Row Padding', 'elementor' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .eb-map-marker-listing-widget tr.eb-map-marker-listing td' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'after',
			]
		);

		$this->end_controls_section();

		//Direction Styles
		$this->start_controls_section(
			'section_map_direction_style',
			[
				'label' => __( 'Map Direction Styles', 'elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'map_direction' => 'yes',
				],
			]
		);

		$this->add_control(
		    'btn_location_button_notice',
		    [
			    'type'  => Controls_Manager::RAW_HTML,
			    'raw'   => '<div class="elementor-panel-heading-title elementor-section-title">Location Button</div>',
		    ]
	    );

		$this->start_controls_tabs( 'tabs_btn_location_button_style' );

		$this->start_controls_tab(
			'tab_btn_location_button_normal',
			[
				'label' => __( 'Normal', 'elementor' ),
			]
		);

		$this->add_control(
			'btn_location_text_color',
			[
				'label' => __( 'Icon Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .eb-map-get-address' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'btn_locatione_background_color',
			[
				'label' => __( 'Background Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'scheme' => [
					'type' => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_2,
				],
				'selectors' => [
					'{{WRAPPER}} .eb-map-get-address' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_btn_location_button_hover',
			[
				'label' => __( 'Hover', 'elementor' ),
			]
		);

		$this->add_control(
			'btn_location_hover_color',
			[
				'label' => __( 'Text Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eb-map-get-address:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'btn_location_background_hover_color',
			[
				'label' => __( 'Background Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .eb-map-get-address:hover' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'btn_location_hover_border_color',
			[
				'label' => __( 'Border Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'condition' => [
					'border_border!' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .eb-map-get-address:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'btn_location_hover_animation',
			[
				'label' => __( 'Hover Animation', 'elementor' ),
				'type' => Controls_Manager::HOVER_ANIMATION,
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->add_control(
		    'btn_route_button_notice',
		    [
			    'type'  => Controls_Manager::RAW_HTML,
			    'raw'   => '<div class="elementor-panel-heading-title elementor-section-title">Route Button</div>',
			    'separator' => 'before',
		    ]
	    );

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'btn_route_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .elementor-button.eb-map-routeGo',
				'separator' => 'before',
			]
		);

		$this->start_controls_tabs( 'tabs_btn_route_button_style' );

		$this->start_controls_tab(
			'tab_btn_route_button_normal',
			[
				'label' => __( 'Normal', 'elementor' ),
			]
		);

		$this->add_control(
			'btn_route_text_color',
			[
				'label' => __( 'Text Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .elementor-button.eb-map-routeGo' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'btn_route_background_color',
			[
				'label' => __( 'Background Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'scheme' => [
					'type' => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_4,
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-button.eb-map-routeGo' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_btn_route_button_hover',
			[
				'label' => __( 'Hover', 'elementor' ),
			]
		);

		$this->add_control(
			'btn_route_hover_color',
			[
				'label' => __( 'Text Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elementor-button.eb-map-routeGo:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'btn_route_background_hover_color',
			[
				'label' => __( 'Background Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elementor-button.eb-map-routeGo:hover' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'btn_route_hover_border_color',
			[
				'label' => __( 'Border Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'condition' => [
					'border_border!' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-button.eb-map-routeGo:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'btn_route_hover_animation',
			[
				'label' => __( 'Hover Animation', 'elementor' ),
				'type' => Controls_Manager::HOVER_ANIMATION,
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'btn_route_border',
				'placeholder' => '1px',
				'default' => '1px',
				'selector' => '{{WRAPPER}} .elementor-button.eb-map-routeGo',
			]
		);

		$this->add_control(
			'btn_route_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .elementor-button.eb-map-routeGo' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'btn_route__box_shadow',
				'selector' => '{{WRAPPER}} .elementor-button.eb-map-routeGo',
			]
		);

		$this->add_control(
			'btn_route_text_padding',
			[
				'label' => __( 'Padding', 'elementor' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .elementor-button.eb-map-routeGo' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render google maps widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _content_template() {
	}

	/**
	 * Render google maps widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$eb_map_styles = $settings['custom_map_style'];
		$eb_replace_code_content = strip_tags($eb_map_styles);
        $eb_new_replace_code_content = preg_replace('/\s/', '', $eb_replace_code_content);

		if ( 0 === absint( $settings['zoom']['size'] ) ) {
			$settings['zoom']['size'] = 10;
		}

		$this->add_render_attribute('eb-google-map-extended', 'data-eb-map-style', $eb_new_replace_code_content);

		$mapmarkers = array();

		foreach ( $settings['tabs'] as $index => $item ) :
			if ('' !== $item['pin_marker']['url']) {
				$marker_pin_on_off = 'on';
			} else {
				$marker_pin_on_off = 'off';
			}

			$mapmarkers[] = array('lat' => $item['pin_lat'], 'lng' => $item['pin_lng'],'title' => $item['pin_title'], 'content' => htmlspecialchars($item['pin_content'], ENT_QUOTES & ~ENT_COMPAT ), 'pin_icon' => $item['pin_icon'], 'marker_pin_on_off' => $marker_pin_on_off, 'marker' => $item['pin_marker']['url'], 'icon_width' => $item['pin_marker_width'], 'icon_height' => $item['pin_marker_height'], 'load_infowindow' => $item['infowindow_this_onload'], 'label' => $item['pin_marker_label'], 'label_text' => $item['pin_marker_label_text'], 'label_text_color' => $item['pin_marker_label_text_color'], 'label_text_size' => $item['pin_marker_label_text_size'], 'label_text_weight' => $item['pin_marker_label_text_weight'], 'pin_marker_use_url' => $item['pin_marker_use_url'], 'pin_url_trigger_method' => $item['pin_url_trigger_method'], 'pin_marker_url' => $item['pin_marker_url']['url'], 'pin_marker_url_target' => $item['pin_marker_url']['is_external'], 'pin_marker_featured_image' => $item['pin_marker_featured_image']['url'] );
		endforeach; 
		?>
		<?php if ( 'yes' == $settings['map_marker_listing'] && 'top' == $settings['map_marker_listing_placement'] ) {?>
		<div class="eb-map-marker-listing-widget eb-map-marker-listing-top elementor-elementor elementor-widget" data-eb-pin-listing-zoom="<?php echo $settings['marker_listing_zoom']['size']; ?>" data-eb-pin-listing-infowindow="<?php if ( 'yes' == $settings['map_marker_listing_infowindow'] ) { ?>true<?php } else { ?>false<?php } ?>">
			<div class="elementor-widget-container">
				<div class="elementor-clearfix">
					<table>
						<thead>
							<tr>
								<th><?php echo __('Title', 'eb-google-map-extended'); ?></th>
								<th><?php echo __('Description', 'eb-google-map-extended'); ?></th>
								<?php if ( $settings['map_direction'] == 'yes' ) {?>
								<th><?php echo __('Direction', 'eb-google-map-extended'); ?></th>
								<?php } ?>
							</tr>
						</thead>
				<?php
					foreach ( $settings['tabs'] as $index => $item ) :
					if ('' !== $item['pin_title'] || '' !== $item['pin_content'] ) {
				?>
				<tbody>
					<tr class="eb-map-marker-listing" data-eb-pin-lat="<?php echo $item['pin_lat']; ?>" data-eb-pin-lng="<?php echo $item['pin_lng']; ?>">
						<td class="eb-map-marker-listing-title-content eb-map-marker-listing-click">
							<div class="eb-marker-listing-icon-wrapper">
							<?php if ('' !== $item['pin_marker']['url']) {?>
								<div class="eb-marker-listing-icon">
									<img src="<?php echo $item['pin_marker']['url']; ?>" height="<?php echo $item['pin_marker_height']['size']; ?>px" width="<?php echo $item['pin_marker_width']['size']; ?>px">
								</div>
							<?php } else {?>
								<div class="eb-marker-listing-icon eb-marker-listing-icon-google">
									<?php
										if ($item['pin_icon'] == 'red') {?>
					                        <img src="<?php echo ICSS_EME_URL; ?>/assets/images/marker_red.png">
					                    <?php
					                    } else if ($item['pin_icon'] == 'yellow') {
					                        ?> <img src="<?php echo ICSS_EME_URL; ?>/assets/images/marker_yellow.png"> <?php
					                    } else if ($item['pin_icon'] == 'blue') {
					                        ?> <img src="<?php echo ICSS_EME_URL; ?>/assets/images/marker_blue.png"> <?php
					                    } else if ($item['pin_icon'] == 'black') {
					                        ?> <img src="<?php echo ICSS_EME_URL; ?>/assets/images/marker_black.png"> <?php
					                    } else if ($item['pin_icon'] == 'white') {
					                        ?> <img src="<?php echo ICSS_EME_URL; ?>/assets/images/marker_white.png"> <?php
					                    } else if ($item['pin_icon'] == 'purple') {
					                        ?> <img src="<?php echo ICSS_EME_URL; ?>/assets/images/marker_purple.png"> <?php
					                    } else if ($item['pin_icon'] == 'green') {
					                        ?> <img src="<?php echo ICSS_EME_URL; ?>/assets/images/marker_green.png"> <?php
					                    } else if ($item['pin_icon'] == 'orange') {
					                        ?> <img src="<?php echo ICSS_EME_URL; ?>/assets/images/marker_orange.png"> <?php
					                    } else if ($item['pin_icon'] == 'grey') {
					                        ?> <img src="<?php echo ICSS_EME_URL; ?>/assets/images/marker_grey.png"> <?php
					                    } else {?>
					                        <img src="https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi2_hdpi.png">
					                    <?php }
									?>
								</div>
								<?php } ?>
								<div class="eb-map-marker-listing-title"><?php echo $item['pin_title']; ?></div>
							</div>
						</td>
						<td class="eb-map-marker-listing-content eb-map-marker-listing-click">
							<?php echo $item['pin_content']; ?>
						</td>
						<?php if ( $settings['map_direction'] == 'yes' ) {?>
						<td class="eb-map-marker-listing-direction"><?php echo $settings['map_direction_listing_text'];?></td>
						<?php } ?>
					</tr>
				</tbody>
				<?php
				}
					endforeach; 
				?>
					
					</table>
				</div>
			</div>
		</div>
		<?php } ?>
		<div id="eb-map-<?php echo esc_attr($this->get_id()); ?>" class="eb-map" data-eb-map-marker-center="<?php if ( 'yes' == $settings['marker_center'] ) { ?>true<?php } else { ?>false<?php } ?>" data-eb-map-gesture-handling="<?php echo $settings['gesture_handling'] ?>" <?php if ( 'yes' == $settings['zoom_control'] ) { ?> data-eb-map-zoom-control="true" data-eb-map-zoom-control-position="<?php echo $settings['zoom_control_position']; ?>" <?php } else { ?> data-eb-map-zoom-control="false"<?php } ?> data-eb-map-defaultui="<?php if ( 'yes' == $settings['default_ui'] ) { ?>false<?php } else { ?>true<?php } ?>" <?php echo $this->get_render_attribute_string('eb-google-map-extended'); ?> data-eb-map-type="<?php echo $settings['map_type']; ?>" <?php if ( 'yes' == $settings['map_type_control'] ) { ?> data-eb-map-type-control="true" data-eb-map-type-control-style="<?php echo $settings['map_type_control_style']; ?>" data-eb-map-type-control-position="<?php echo $settings['map_type_control_position']; ?>"<?php } else { ?> data-eb-map-type-control="false"<?php } ?> <?php if ( 'yes' == $settings['streetview_control'] ) { ?> data-eb-map-streetview-control="true" data-eb-map-streetview-position="<?php echo $settings['streetview_control_position']; ?>"<?php } else {?> data-eb-map-streetview-control="false"<?php } ?> data-eb-map-lat="<?php echo $settings['map_lat']; ?>" data-eb-map-lng="<?php echo $settings['map_lng']; ?>" data-eb-map-zoom="<?php echo $settings['zoom']['size']; ?>" data-eb-map-infowindow-width="<?php echo $settings['infowindow_max_width']; ?>" data-eb-map-trigger-method="<?php echo $settings['pin_trigger_method']; ?>" data-eb-map-animation-bounce="<?php if ( 'yes' == $settings['pin_animation_bounce'] ) { ?>true<?php } else { ?>false<?php } ?>" <?php if ( 'yes' == $settings['pin_animation_bounce'] ) { ?> data-eb-map-animation-bounce-duration="<?php echo $settings['pin_animation_bounce_duration']; ?>"<?php } ?> data-eb-map-animation-drop="<?php if ( 'yes' == $settings['pin_animation_drop'] ) { ?>true<?php } else { ?>false<?php } ?>" data-eb-map-infowindow-load="<?php if ( 'yes' == $settings['infowindow_onload'] ) { ?>true<?php } else { ?>false<?php } ?>" data-eb-map-marker-clustering="<?php if ( 'yes' == $settings['marker_clustering'] ) { ?>true<?php } else { ?>false<?php } ?>" data-eb-map-infowindow-styles="<?php echo $settings['infowindow_styles']; ?>" data-eb-map-infowindow-close-icon="<?php echo $settings['infowindow_close_icon']; ?>" data-eb-locations='<?php echo json_encode($mapmarkers);?>'></div>
		<?php if ( $settings['map_direction'] == 'yes' ) {?>
		<div class="eb-map-direction-wrapper">
			<div class="eb-map-column eb-map-column-40">
				<div class="eb-map-direction-input">
					<label for="eb-map-routeFrom">
						<span class="screen-reader-text">Search for:</span>
					</label>
					<input type="input" class="eb-map-routeFrom" placeholder="<?php echo $settings['map_direction_input_placeholder'];?>" value="" name="eb-map-routeFrom">
					<button type="submit" class="eb-map-get-address <?php if ( $settings['btn_location_hover_animation'] ) {?>elementor-animation-<?php echo $settings['btn_location_hover_animation']; } ?>"><i class="<?php echo $settings['btn_location_icon']; ?>" aria-hidden="true"></i><span class="screen-reader-text">Search</span></button>
				</div>
			</div>
			<div class="eb-map-column">
				<div class="eb-map-direction-select">
					<select class="eb-map-routeTo" name="eb-map-routeTo">
						<?php
							foreach ( $settings['tabs'] as $index => $item ) :
								if ('' !== $item['pin_title'] || '' !== $item['pin_content'] ) {
							?>
								<option value="Jurong Bird Park Singapore" data-eb-pin-lat="<?php echo $item['pin_lat']; ?>" data-eb-pin-lng="<?php echo $item['pin_lng']; ?>">To: <?php echo $item['pin_title']; ?></option>
							<?php 
								}
							endforeach;
						?>
					</select>
				</div>
			</div>
			<div class="eb-map-column">
				<div class="eb-map-direction-select">
					<select class="eb-map-routeMode" name="eb-map-routeMode">
					    <option value="DRIVING"><?php echo __('Mode: Driving', 'eb-google-map-extended'); ?></option>
					    <option value="WALKING"><?php echo __('Mode: Walking', 'eb-google-map-extended'); ?></option>
					    <option value="BICYCLING"><?php echo __('Mode: Bicycling', 'eb-google-map-extended'); ?></option>
					    <option value="TRANSIT"><?php echo __('Mode: Transit', 'eb-google-map-extended'); ?></option>
					</select>
				</div>
			</div>
			<div class="eb-map-column">
				<div class="elementor-button-wrapper eb-map-direction-button">
					<button class="elementor-button-link elementor-button <?php if ( ! empty( $settings['btn_route_size'] ) ) {?>elementor-size-<?php echo $settings['btn_route_size'];?><?php } ?> eb-map-routeGo <?php if ( $settings['btn_route_hover_animation'] ) {?>elementor-animation-<?php echo $settings['btn_route_hover_animation']; } ?>">
						<span class="elementor-button-content-wrapper">
							<?php if ( $settings['btn_route_icon'] !== '' ) {?>
								<span class="elementor-button-icon elementor-align-icon-<?php echo $settings['btn_route_icon_align']; ?>">
								<i class="<?php echo $settings['btn_route_icon']; ?>" aria-hidden="true"></i>
							</span>
							<?php } ?>
							<span class="elementor-button-text"><?php echo $settings['map_direction_route_btn']; ?></span>
						</span>
					</button>
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if ( $settings['map_text_direction'] == 'yes' ) {?>
		<div class="eb-map-panel"></div>
		<?php } ?>
		<?php if ( 'yes' == $settings['map_marker_listing'] && 'bottom' == $settings['map_marker_listing_placement'] ) {?>
		<div class="eb-map-marker-listing-widget elementor-elementor elementor-widget" data-eb-pin-listing-zoom="<?php echo $settings['marker_listing_zoom']['size']; ?>" data-eb-pin-listing-infowindow="<?php if ( 'yes' == $settings['map_marker_listing_infowindow'] ) { ?>true<?php } else { ?>false<?php } ?>">
			<div class="elementor-widget-container">
				<div class="elementor-clearfix">
					<table>
						<thead>
							<tr>
								<th><?php echo __('Title', 'eb-google-map-extended'); ?></th>
								<th><?php echo __('Description', 'eb-google-map-extended'); ?></th>
								<?php if ( $settings['map_direction'] == 'yes' ) {?>
								<th><?php echo __('Direction', 'eb-google-map-extended'); ?></th>
								<?php } ?>
							</tr>
						</thead>
				<?php
					foreach ( $settings['tabs'] as $index => $item ) :
					if ('' !== $item['pin_title'] || '' !== $item['pin_content'] ) {
				?>
				<tbody>
					<tr class="eb-map-marker-listing" data-eb-pin-lat="<?php echo $item['pin_lat']; ?>" data-eb-pin-lng="<?php echo $item['pin_lng']; ?>">
						<td class="eb-map-marker-listing-title-content eb-map-marker-listing-click">
							<div class="eb-marker-listing-icon-wrapper">
							<?php if ('' !== $item['pin_marker']['url']) {?>
								<div class="eb-marker-listing-icon">
									<img src="<?php echo $item['pin_marker']['url']; ?>" height="<?php echo $item['pin_marker_height']['size']; ?>px" width="<?php echo $item['pin_marker_width']['size']; ?>px">
								</div>
							<?php } else {?>
								<div class="eb-marker-listing-icon eb-marker-listing-icon-google">
									<?php
										if ($item['pin_icon'] == 'red') {?>
					                        <img src="<?php echo ICSS_EME_URL; ?>/assets/images/marker_red.png">
					                    <?php
					                    } else if ($item['pin_icon'] == 'yellow') {
					                        ?> <img src="<?php echo ICSS_EME_URL; ?>/assets/images/marker_yellow.png"> <?php
					                    } else if ($item['pin_icon'] == 'blue') {
					                        ?> <img src="<?php echo ICSS_EME_URL; ?>/assets/images/marker_blue.png"> <?php
					                    } else if ($item['pin_icon'] == 'black') {
					                        ?> <img src="<?php echo ICSS_EME_URL; ?>/assets/images/marker_black.png"> <?php
					                    } else if ($item['pin_icon'] == 'white') {
					                        ?> <img src="<?php echo ICSS_EME_URL; ?>/assets/images/marker_white.png"> <?php
					                    } else if ($item['pin_icon'] == 'purple') {
					                        ?> <img src="<?php echo ICSS_EME_URL; ?>/assets/images/marker_purple.png"> <?php
					                    } else if ($item['pin_icon'] == 'green') {
					                        ?> <img src="<?php echo ICSS_EME_URL; ?>/assets/images/marker_green.png"> <?php
					                    } else if ($item['pin_icon'] == 'orange') {
					                        ?> <img src="<?php echo ICSS_EME_URL; ?>/assets/images/marker_orange.png"> <?php
					                    } else if ($item['pin_icon'] == 'grey') {
					                        ?> <img src="<?php echo ICSS_EME_URL; ?>/assets/images/marker_grey.png"> <?php
					                    } else {?>
					                        <img src="https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi2_hdpi.png">
					                    <?php }
									?>
								</div>
								<?php } ?>
								<div class="eb-map-marker-listing-title"><?php echo $item['pin_title']; ?></div>
							</div>
						</td>
						<td class="eb-map-marker-listing-content eb-map-marker-listing-click">
							<?php echo $item['pin_content']; ?>
						</td>
						<?php if ( $settings['map_direction'] == 'yes' ) {?>
						<td class="eb-map-marker-listing-direction"><?php echo $settings['map_direction_listing_text'];?></td>
						<?php } ?>
					</tr>
				</tbody>
				<?php
				}
					endforeach; 
				?>
					
					</table>
				</div>
			</div>
		</div>
		<?php } ?>
	<?php }
}

Plugin::instance()->widgets_manager->register_widget_type( new EB_Google_Map_Extended_Pro() );