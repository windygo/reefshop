<?php
/*
Plugin Name: Gravity Forms Booking Datepicker
Plugin URI: https://bitbucket.org/gchokeen/gravify-forms-booking-datepicker/
Description: Add extra functionality to GF datepicker
Version: 1.0.2
Author: Zvi
License: GPL2
Text Domain: gravity-forms-booking-datepicker
*/



define('GFBDATE_PLUGIN_NAME', plugin_basename(__FILE__));
define('GFBDATE_PLUGIN_DIR', dirname(__FILE__) . DIRECTORY_SEPARATOR);
define('GFBDATE_PLUGIN_VERSION','1.0.2');


define('GFBDATE_PLUGIN_URL',plugins_url('', __FILE__));



/*
 * Help http://www.gravityhelp.com/documentation/gravity-forms/extending-gravity-forms/ .. 
 */





if (!class_exists('GF_Booking_Datepicker')) {

	class GF_Booking_Datepicker{


		/**
		 * @var GF_Booking_Datepicker
		 */
		static private $_instance = null;

		/**
		 * Get GF_Booking_Datepicker object
		 *
		 * @return GF_Booking_Datepicker
		 */
		static public function getInstance()
		{
			if (self::$_instance == null) {
				self::$_instance = new GF_Booking_Datepicker();
			}

			return self::$_instance;
		}


		private function __construct()
		{

			register_activation_hook(GFBDATE_PLUGIN_NAME, array(&$this, 'pluginActivate'));
			register_deactivation_hook(GFBDATE_PLUGIN_NAME, array(&$this, 'pluginDeactivate'));
			register_uninstall_hook(GFBDATE_PLUGIN_NAME, array('gravify-forms-booking-datepicker', 'pluginUninstall'));

			## Register plugin widgets
			add_action('init', array($this, 'load_axip_transl'));
			add_action('plugins_loaded', array(&$this, 'pluginLoad'));

			
			add_filter("gform_noconflict_scripts", array(&$this, 'gravify_register_script'));
			
			if (is_admin()) {
				add_action('admin_enqueue_scripts', array(&$this, 'adminLoadScripts'));
				add_action('admin_head', array(&$this, 'adminLoadStyles'));
			}
			else{
				add_action('wp_enqueue_scripts', array(&$this, 'siteLoadScripts'),20);
				add_action( 'gform_enqueue_scripts',array(&$this, 'gravify_enqueue_scripts'), 10, 2 );
				add_action('wp_enqueue_style', array(&$this, 'siteLoadStyles'));
			}
    
                        
			add_action("gform_field_advanced_settings",array(&$this, 'gravify_advanced_settings'), 10, 2);
			add_action("gform_editor_js",array(&$this, 'editor_script') );
			
			add_filter('gform_tooltips',array(&$this, 'form_field_apply_dateblock_value') );
			add_filter('gform_tooltips',array(&$this, 'form_field_disable_pastdays_value') );
			add_filter('gform_tooltips',array(&$this, 'form_field_weekstart_value') );
			add_filter('gform_tooltips',array(&$this, 'form_field_disable_days_weekly_pattern_value') );
			
			add_filter('gform_tooltips',array(&$this, 'form_field_apply_timeblock_value') );
			add_filter('gform_tooltips',array(&$this, 'form_field_global_timeblock_value') );
			add_filter('gform_tooltips',array(&$this, 'form_field_apply_starttime_value') );
			add_filter('gform_tooltips',array(&$this, 'form_field_apply_endtime_value') );
			
			
			add_filter('wp_footer',array(&$this, 'gravify_datepicker'),20);
			
			add_filter( 'gform_field_content',array(&$this, 'gravify_apply_datepicker'), 10, 2 );
			
	
			

		}

		public function load_axip_transl()
		{
			load_plugin_textdomain('gravify-forms-booking-datepicker', FALSE, dirname(plugin_basename(__FILE__)).'/languages/');
		}
                
		

		
		public function gravify_apply_datepicker($field_content, $field ){
			
								
			if($field->type == 'date' && $field->gravify_apply_dateblock == 1){
				
				$gravify_disable_days_weekly = array();
				
				if($field->gravify_disable_days_weekly_sun == "Sun"){
					$gravify_disable_days_weekly[] = 0;
				}
				if($field->gravify_disable_days_weekly_mon == "Mon"){
					$gravify_disable_days_weekly[] = 1;
				}
				if($field->gravify_disable_days_weekly_tue == "Tue"){
					$gravify_disable_days_weekly[] = 2;
				}
				if($field->gravify_disable_days_weekly_wed == "Wed"){
					$gravify_disable_days_weekly[] = 3;
				}
				if($field->gravify_disable_days_weekly_thu == "Thu"){
					$gravify_disable_days_weekly[] = 4;
				}
				if($field->gravify_disable_days_weekly_fri == "Fri"){
					$gravify_disable_days_weekly[] = 5;
				}
				if($field->gravify_disable_days_weekly_sat == "Sat"){
					$gravify_disable_days_weekly[] = 6;
				}

				
				if(!empty($gravify_disable_days_weekly)){
					$gravify_disable_days_weekly = implode(',',$gravify_disable_days_weekly);	
				}
				else{
					$gravify_disable_days_weekly = "";
				}
				
				
				
				if(!empty($field->gravify_disable_days_manually)){				
					$gravify_disable_days_manually = implode(',',$field->gravify_disable_days_manually);				
				}
				else{
					$gravify_disable_days_manually = "";
				}
				
				
				$gravify_weekstart_firsday = ($field->gravify_weekstart_sunday ==1)?0:1;
								
				$field_content = str_replace( 'type="text"', "data-gravify_disable_days_manually='".$gravify_disable_days_manually."' data-gravify_apply_dateblock='".$field->gravify_apply_dateblock."' data-gravify_disable_days_weekly='".$gravify_disable_days_weekly."' data-gravify_weekstart_firsday='".$gravify_weekstart_firsday."' data-gravify_disable_pastdays='".$field->gravify_disable_pastdays."' type=\"text\" ", $field_content );
				$field_content = str_replace( "type='text'", "data-gravify_disable_days_manually='".$gravify_disable_days_manually."' data-gravify_apply_dateblock='".$field->gravify_apply_dateblock."' data-gravify_disable_days_weekly='".$gravify_disable_days_weekly."' data-gravify_weekstart_firsday='".$gravify_weekstart_firsday."' data-gravify_disable_pastdays='".$field->gravify_disable_pastdays."' type='text' ", $field_content );
			
				return $field_content;
								
			}
	

			
			return $field_content;
		}
		

		
        public function gravify_datepicker(){

			
	   
			       ?>
			       <script type="text/javascript">
				
					try{

  
					gform.addFilter( 'gform_datepicker_options_pre_init', function( options, formId, fieldId ) {
						
						
						
						var dateField = jQuery("#input_"+formId+'_'+fieldId);
						
						if(dateField.hasClass('datepicker')){
							
							var gravify_apply_dateblock = dateField.attr('data-gravify_apply_dateblock');
							var gravify_disable_pastdays = dateField.attr('data-gravify_disable_pastdays');
							var gravify_weekstart_firsday = dateField.attr('data-gravify_weekstart_firsday');
							var gravify_disable_days_weekly = dateField.attr('data-gravify_disable_days_weekly');
							var gravify_disable_days_manually = dateField.attr('data-gravify_disable_days_manually');
							
							if (gravify_apply_dateblock == 1) {
							
								
								
								if (gravify_disable_pastdays == 1) {
									options.minDate = new Date();
								}
								if (gravify_disable_pastdays != 'undefined') {
									options.firstDay = gravify_weekstart_firsday;
								}
							
								var maxDate = new Date(); 
								maxDate.setFullYear(maxDate.getFullYear() + 1);
								options.maxDate = maxDate;
								
								options.beforeShowDay = function(date){
									
									var day = date.getDay();
									
									var gravify_disable_days_manually_Array = gravify_disable_days_manually.split(',');										
									var pickDate = jQuery.datepicker.formatDate( options.dateFormat, date);
									
									if(jQuery.inArray(pickDate, gravify_disable_days_manually_Array ) > -1){
										return [false];
									}									
									
									if (gravify_disable_days_weekly == "") {
										return [true];
									}
									else{
										
										var gravify_disable_days_weekly_Array = gravify_disable_days_weekly.split(',').map(function(strVale){return Number(strVale);});
										
										
										if(jQuery.inArray(day, gravify_disable_days_weekly_Array ) > -1){
											
											return [false];
										}
										else{
											return [true];	
										}
																			
									}
								}
								
							}
							
								
						}
						
					
							
						return options;
						

						
					
					} );				
					
					
					}
					catch(e){
						console.log("gravify error: "+e);
					}					
			       </script>
			       <?php
			
			
		}
                
                
		
		 function gravify_advanced_settings($position, $form_id){
		 
		 $time_array = range(1,24);
		 
			 //create settings on position 50 (right after Admin Label)
			 if($position == 550){
				 ?>
				 <li class="apply_dateblock_setting field_setting">
					 <label for="field_apply_dateblock_value">
						 <?php _e("Apply date block", "gravify-forms-booking-datepicker"); ?>
						 <?php gform_tooltip("form_field_apply_dateblock_value") ?>
					 </label>
					 <input type="checkbox" id="field_apply_dateblock_value" onclick="SetFieldProperty('gravify_apply_dateblock', this.checked);" /> Apply date block settings
				 </li>
				 
				 <li class="disable_pastdays_setting field_setting">
					 <label for="field_disable_pastdays">
						 <?php _e("Disable past days", "gravify-forms-booking-datepicker"); ?>
						 <?php gform_tooltip("form_field_disable_pastdays_value") ?>
					 </label>
					 <input type="checkbox" id="form_field_disable_pastdays_value" onclick="SetFieldProperty('gravify_disable_pastdays', this.checked);" /> 
				 </li>
				 
				 <li class="weekstart_setting field_setting">
					 <label for="field_admin_label">
						 <?php _e("Week starts on", "gravify-forms-booking-datepicker"); ?>
						 <?php gform_tooltip("form_field_weekstart_value") ?>
					 </label>
					 <input type="radio" name="form_field_weekstart_value" id="form_field_weekstart_value_sunday" onclick="SetFieldProperty('gravify_weekstart_sunday', this.checked);" /> Sunday
					 <input type="radio" name="form_field_weekstart_value" id="form_field_weekstart_value_monday" onclick="SetFieldProperty('gravify_weekstart_monday', this.checked);" /> Monday
				 </li>                    

				 <li class="disable_days_weekly_pattern_setting field_setting">
					 <label for="form_field_disable_days_weekly_pattern_value">
						 <?php _e("Disable days by weekly pattern", "gravify-forms-booking-datepicker"); ?>
						 <?php gform_tooltip("form_field_disable_days_weekly_pattern_value") ?>
					 </label>
					 <input type="checkbox" name="gravify_disable_days_weekly[]" id="form_field_disable_days_weekly_pattern_sun" onclick="SetFieldProperty('gravify_disable_days_weekly_sun', this.checked);" /> <?php _e("Sun", "gravify-forms-booking-datepicker"); ?>
					 <input type="checkbox" name="gravify_disable_days_weekly[]" id="form_field_disable_days_weekly_pattern_mon" onclick="SetFieldProperty('gravify_disable_days_weekly_mon', this.checked);" /> <?php _e("Mon", "gravify-forms-booking-datepicker"); ?>
					 <input type="checkbox" name="gravify_disable_days_weekly[]" id="form_field_disable_days_weekly_pattern_tue" onclick="SetFieldProperty('gravify_disable_days_weekly_tue', this.checked);" /> <?php _e("Tue", "gravify-forms-booking-datepicker"); ?>
					 <input type="checkbox" name="gravify_disable_days_weekly[]" id="form_field_disable_days_weekly_pattern_wed" onclick="SetFieldProperty('gravify_disable_days_weekly_wed', this.checked);" /> <?php _e("Wed", "gravify-forms-booking-datepicker"); ?>
					 <input type="checkbox" name="gravify_disable_days_weekly[]" id="form_field_disable_days_weekly_pattern_thu" onclick="SetFieldProperty('gravify_disable_days_weekly_thu', this.checked);" /> <?php _e("Thu", "gravify-forms-booking-datepicker"); ?>
					 <input type="checkbox" name="gravify_disable_days_weekly[]" id="form_field_disable_days_weekly_pattern_fri" onclick="SetFieldProperty('gravify_disable_days_weekly_fri', this.checked);" /> <?php _e("Fri", "gravify-forms-booking-datepicker"); ?>
					 <input type="checkbox" name="gravify_disable_days_weekly[]" id="form_field_disable_days_weekly_pattern_sat" onclick="SetFieldProperty('gravify_disable_days_weekly_sat', this.checked);" /> <?php _e("Sat", "gravify-forms-booking-datepicker"); ?>					 
				 </li>
				 <li class="gravify_disable_days_manually_settings field_setting">
					<label for="gravify_disable_days_manually">Disable additional days manually</label>
					<div id="gravify_disable_days_manually" class="box"></div>
				 </li>
				 
				 <!-- Time block settings -->
				 <li class="apply_timeblock_setting field_setting">
					 <label for="field_apply_timeblock_value">
						 <?php _e("Apply hour block", "gravify-forms-booking-datepicker"); ?>
						 <?php gform_tooltip("form_field_apply_timeblock_value") ?>
					 </label>
					 <input type="checkbox" id="field_apply_timeblock_value" onclick="SetFieldProperty('gravify_apply_timeblock', this.checked);" /> Apply hour blocking settings
				 </li>
				 
				 
				 
				 
				 <li class="global_timeblock_setting field_setting">
					 <label for="field_global_timeblock_value">
						 <?php _e("Global time block", "gravify-forms-booking-datepicker"); ?>
						 <?php gform_tooltip("form_field_global_timeblock_value") ?>
					 </label>
					 
					 <select id="field_global_timeblock_value" onchange="SetFieldProperty('gravify_global_timeblock', jQuery(this).val());" multiple="multiple" />
					    <option value="">Choose</option>
						<?php foreach($time_array as $row){ ?>
						<option value="<?php echo $row; ?>"><?php echo $row; ?></option>
						<?php } ?>
						
					 </select>
				 </li>
				 
				 
				 
				 <li class="apply_starttime_setting field_setting">
					 <label for="field_apply_starttime_value">
						 <?php _e("Start blocking hour", "gravify-forms-booking-datepicker"); ?>
						 <?php gform_tooltip("form_field_apply_starttime_value") ?>
					 </label>
					 
					 <select id="field_apply_starttime_value" onchange="SetFieldProperty('gravify_apply_starttime', jQuery(this).val());" />
					    <option value="">Choose</option>
						<?php foreach($time_array as $row){ ?>
						<option value="<?php echo $row; ?>"><?php echo $row; ?></option>
						<?php } ?>
						
					 </select>
				 </li>
				 
				 
				 <li class="apply_endtime_setting field_setting">
					 <label for="field_apply_endtime_value">
						 <?php _e("End blocking hour", "gravify-forms-booking-datepicker"); ?>
						 <?php gform_tooltip("form_field_apply_endtime_value") ?>
					 </label>
					 
					 <select id="field_apply_endtime_value" onchange="SetFieldProperty('gravify_apply_endtime', jQuery(this).val());" />
					    <option value="">Choose</option>
						<?php foreach($time_array as $row){ ?>
						<option value="<?php echo $row; ?>"><?php echo $row; ?></option>
						<?php } ?>
						
					 </select>
				 </li>					 

				 <?php
			 }
		 }
		 
		 //Action to inject supporting script to the form editor page
		 
		 function editor_script(){
			 ?>
			 <script type='text/javascript'>
					
				
				 //adding setting to fields of type "date"
				 fieldSettings["date"] += ", .apply_dateblock_setting";
				 fieldSettings["date"] += ", .disable_pastdays_setting";
				 fieldSettings["date"] += ", .weekstart_setting";
				 fieldSettings["date"] += ", .disable_days_weekly_pattern_setting";
				 fieldSettings["date"] += ", .gravify_disable_days_manually_settings";
				 
				 fieldSettings["date"] += ", .apply_timeblock_setting";
				 fieldSettings["date"] += ", .global_timeblock_setting";
				 fieldSettings["date"] += ", .apply_starttime_setting";
				 fieldSettings["date"] += ", .apply_endtime_setting";
				 

		 
		 
				var multidatepicker_options = {};
		 
				 //binding to the load field settings event to initialize the checkbox
				 jQuery(document).bind("gform_load_field_settings", function(event, field, form){
					
					try{
					
						 jQuery("#field_apply_dateblock_value").attr("checked", field["gravify_apply_dateblock"] == true);
						 jQuery("#form_field_disable_pastdays_value").attr("checked", field["gravify_disable_pastdays"] == true);
			
						 jQuery("#form_field_weekstart_value_sunday").attr("checked", field["gravify_weekstart_sunday"] == true);
						 jQuery("#form_field_weekstart_value_monday").attr("checked", field["gravify_weekstart_monday"] == true);
	
						
	
						multidatepicker_options.onSelect=function(dateStr){
												
												
									var dates = jQuery('#gravify_disable_days_manually').multiDatesPicker('getDates');
																	
									SetFieldProperty('gravify_disable_days_manually', dates);
																		
								};
											 
						if(jQuery("#form_field_disable_pastdays_value").is(":checked")){
							multidatepicker_options.minDate = new Date();
						}											 
						
	
						 
						 if (field["gravify_disable_days_weekly_sun"] != true && field["gravify_disable_days_weekly_mon"] != true
						     && field["gravify_disable_days_weekly_tue"] != true && field["gravify_disable_days_weekly_wed"] != true
						     && field["gravify_disable_days_weekly_thu"] != true && field["gravify_disable_days_weekly_fri"] != true
						     && field["gravify_disable_days_weekly_sat"] != true) {
							
							 jQuery('input[name="gravify_disable_days_weekly[]"]').attr("checked", false);
							// jQuery("#form_field_disable_days_weekly_pattern_fri").attr("checked", true);
							// jQuery("#form_field_disable_days_weekly_pattern_sat").attr("checked", true);
							// 
							//
							//SetFieldProperty('gravify_disable_days_weekly_fri', true);
							//SetFieldProperty('gravify_disable_days_weekly_sat', true);
							//	 
							 
							
						 }
						 else{
							
							
							jQuery("#form_field_disable_days_weekly_pattern_sun").attr("checked", field["gravify_disable_days_weekly_sun"] == true);
							jQuery("#form_field_disable_days_weekly_pattern_mon").attr("checked", field["gravify_disable_days_weekly_mon"] == true);
							jQuery("#form_field_disable_days_weekly_pattern_tue").attr("checked", field["gravify_disable_days_weekly_tue"] == true);
							jQuery("#form_field_disable_days_weekly_pattern_wed").attr("checked", field["gravify_disable_days_weekly_wed"] == true);
							jQuery("#form_field_disable_days_weekly_pattern_thu").attr("checked", field["gravify_disable_days_weekly_thu"] == true);
							jQuery("#form_field_disable_days_weekly_pattern_fri").attr("checked", field["gravify_disable_days_weekly_fri"] == true);
							jQuery("#form_field_disable_days_weekly_pattern_sat").attr("checked", field["gravify_disable_days_weekly_sat"] == true);
												       
							
						 }
						 
						 var weekly_disable_array=[];
						 
						 if (field["gravify_disable_days_weekly_sun"] == true){
							
							weekly_disable_array.push(0);
						 }
						 
						 if (field["gravify_disable_days_weekly_mon"] == true){
							weekly_disable_array.push(1);
							
						 }
						 
						 if (field["gravify_disable_days_weekly_tue"] == true){
							weekly_disable_array.push(2);
						 }
						 
						 if (field["gravify_disable_days_weekly_wed"] == true){
							weekly_disable_array.push(3);
	
						 }
						 
						 
						 if (field["gravify_disable_days_weekly_thu"] == true){
							weekly_disable_array.push(4);
						 }
						 
						 if (field["gravify_disable_days_weekly_fri"] == true){
							weekly_disable_array.push(5);
							
						 }
						 
						 if (field["gravify_disable_days_weekly_sat"] == true){
							weekly_disable_array.push(6);
							
						 }						 
						 
					
						 
						 
						 
						multidatepicker_options.beforeShowDay = function(date){
							
							var day = date.getDay();
									
							
							if (weekly_disable_array.length == 0) {
								return [true];
							}
							else{
							
								if(jQuery.inArray(day, weekly_disable_array ) > -1){
									
									return [false];
								}
								else{
									return [true];	
								}
																	
							}
						};
									
					
						if (typeof field['dateFormat'] == 'undefined') {
						    multidatepicker_options.dateFormat = 'mm/dd/yy';	
						}
						else if (field['dateFormat'] == 'dmy') {
						    multidatepicker_options.dateFormat = 'dd/mm/yy';
						} else if (field['dateFormat'] == 'dmy_dash') {					
						    multidatepicker_options.dateFormat = 'dd-mm-yy';
						} else if (field['dateFormat'] == 'dmy_dot') {					    					
						    multidatepicker_options.dateFormat = 'dd.mm.yy';
						} else if (field['dateFormat'] == 'ymd_slash') {    					
						    multidatepicker_options.dateFormat = 'yy/mm/dd';
						} else if (field['dateFormat'] == 'ymd_dash') {    					
						    multidatepicker_options.dateFormat = 'yy-mm-dd';
						} else if (field['dateFormat'] == 'ymd_dot') {   					
						    multidatepicker_options.dateFormat = 'yy.mm.dd';
						}
						else{
							multidatepicker_options.dateFormat = 'mm/dd/yy';	
						}
				
		
		
						 
						  jQuery('#gravify_disable_days_manually').multiDatesPicker(multidatepicker_options);	
						 
	
					
					
					

					
						if (typeof field['gravify_disable_days_manually'] !="undefined") {
							//var gravify_disable_days_manually = jQuery.map( field['gravify_disable_days_manually'], function( val, i ) {
							//	var format = jQuery('#field_date_format option:selected').text();
							//	console.log(format+'=='+val);
							//	return jQuery.datepicker.formatDate( format, val );
							//});
							//console.log(gravify_disable_days_manually);
					
					
					
							var gravify_disable_days_manually = field['gravify_disable_days_manually'];
					
							jQuery('#gravify_disable_days_manually').multiDatesPicker('addDates',gravify_disable_days_manually);
						}
						
						
						
						jQuery("#field_apply_timeblock_value").attr("checked", field["gravify_apply_timeblock"] == true);
						jQuery("#field_global_timeblock_value").val(field["gravify_global_timeblock"]);
						jQuery("#field_apply_starttime_value").val(field["gravify_apply_starttime"]);
						jQuery("#field_apply_endtime_value").val(field["gravify_apply_endtime"]);
					
					}
					catch (e) {
						console.log("gravify datepicker error: "+e);
					}
					
					
				
					 
				 });
				 
				 
				 //
				// jQuery(function(){
				// 
				// jQuery(document).on('click','#form_field_disable_pastdays_value',function(){
				//	
				//	jQuery('#gravify_disable_days_manually').multiDatesPicker('destroy');
				//	
				//	if (jQuery(this).is(':checked')) {
				//		multidatepicker_options.minDate = new Date();
				//	}
				//	else{
				//		multidatepicker_options.minDate = null;
				//		
				//	}
				//	
				//	console.log(multidatepicker_options.minDate);
				//	
				//	jQuery('#gravify_disable_days_manually').multiDatesPicker(multidatepicker_options);
				// });
				// 
				// });
				 
				 
			 </script>
			 <?php
		 }
		 
		 //Filter to add a new tooltip
		 
		 function form_field_apply_dateblock_value($tooltips){
			$tooltips["form_field_apply_dateblock_value"] = "<h6>Apply date block settings</h6>It enables the block date settings for calendar";
			return $tooltips;
		 }

		 function form_field_disable_pastdays_value($tooltips){
			$tooltips["form_field_disable_pastdays_value"] = "<h6>Disable past days</h6> This option disables all the post days.";
			return $tooltips;
		 }
		 
		 function form_field_weekstart_value($tooltips){
			 
			 $tooltips["form_field_weekstart_value"] = "<h6>Week starts on</h6>This option helps set the week start.";
			return $tooltips;
		 }

		 function form_field_disable_days_weekly_pattern_value($tooltips){
			 
			 $tooltips["form_field_disable_days_weekly_pattern_value"] = "<h6>Week starts on</h6>";
			return $tooltips;
		 }

		 function form_field_apply_timeblock_value($tooltips){
			$tooltips["form_field_apply_timeblock_value"] = "<h6>Apply hour block settings</h6>It enables the block hour settings for calendar";
			return $tooltips;
		 }                
         
	 
	 
	 
		function form_field_global_timeblock_value($tooltips){
			$tooltips["form_field_global_timeblock_value"] = "<h6>Global time block</h6> This could be global time block for every day";
			return $tooltips;
		 }	 
	 
	 
		function form_field_apply_starttime_value($tooltips){
			$tooltips["form_field_apply_starttime_value"] = "<h6>Start blocking hour</h6> This could be start time block";
			return $tooltips;
		 }
		 
		function form_field_apply_endtime_value($tooltips){
			$tooltips["form_field_apply_endttime_value"] = "<h6>End blocking hour</h6> This could be end time block";
			return $tooltips;
		 }		 
                
		

		##
		## Loading Scripts and Styles
		##
	
		public function adminLoadStyles()
		{
			?>
			 <style type="text/css">
				.ui-datepicker .ui-datepicker-calendar .ui-state-highlight a {
					background: #C87575;
					color: white;
					text-shadow: none;
					text-decoration: line-through;
				}
			 </style>
			 <?php
		}
	
		public function adminLoadScripts()
		{
			
			if(class_exists('RGForms')){
				
			
				if(RGForms::is_gravity_page()){
					wp_enqueue_script('jquery-ui-datepicker');					
					wp_register_script( 'jquery_ui_multidatespicker', GFBDATE_PLUGIN_URL . '/js/jquery-ui.multidatespicker.js',array('jquery-ui-datepicker') );
					wp_enqueue_script( 'jquery_ui_multidatespicker');
					
					if(substr( get_locale(), 0, 2 ) == "he"){
						wp_enqueue_script( 'gravify-datepicker-localization-for-gravity-forms', GFBDATE_PLUGIN_URL . '/js/datepicker-he.js', array( 'jquery-ui-datepicker' ) );	
					}				
				}
			}
			
		}
	
	
		public function gravify_register_script($scripts){
			
			//registering my script with Gravity Forms so that it gets enqueued when running on no-conflict mode
			$scripts[] = "jquery-ui-datepicker";
			$scripts[] = "jquery_ui_multidatespicker";
			$scripts[] = "gravify-datepicker-localization-for-gravity-forms";
			
			
			return $scripts;
			
		}	
	
	
		public function siteLoadStyles(){
			
			wp_register_style('gravify-style',plugins_url('css/gravify-styles.css', __FILE__));
			wp_enqueue_style( 'gravify-style' );
					
		}
	
		public function gravify_enqueue_scripts(){
			wp_register_style('gravify-style',plugins_url('css/gravify-styles.css', __FILE__));
			wp_enqueue_style( 'gravify-style' );
			
			wp_enqueue_script('jquery-ui-core');
			wp_enqueue_script('jquery-ui-datepicker');
			if(substr( get_locale(), 0, 2 ) == "he"){
				wp_enqueue_script( 'gravify-datepicker-localization-for-gravity-forms', GFBDATE_PLUGIN_URL . '/js/datepicker-he.js', array( 'gform_datepicker_init','jquery-ui-datepicker' ) );	
			}
			
		}
	
		public function siteLoadScripts()
		{
			wp_enqueue_script('jquery-ui-core');
			wp_enqueue_script('jquery-ui-datepicker');
			
		}



		##
		## Widgets initializations
		##

		public function widgetsRegistration()
		{
		 
		 
		}


		##
		## Plugin Activation and Deactivation
		##

		/**
		* Activate plugin
		* @return void
		*/
		public function pluginActivate()
		{
		 


		}

		/**
		* Deactivate plugin
		* @return void
		*/
		public function pluginDeactivate(){
			
		}

		/**
		* Uninstall plugin
		* @return void
		*/
		static public function pluginUninstall()
		{

		}


		public function pluginLoad(){

		}
			
			


	}
}


//instantiate the class
if (class_exists('GF_Booking_Datepicker')) {
	$GF_Booking_Datepicker =  GF_Booking_Datepicker::getInstance();
}
