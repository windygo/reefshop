jQuery(document).ready(function($) {
  $(".disabled input, .disabled textarea").prop("disabled", "disabled");
  $(".readonly input, .readonly textarea").prop("readonly", "readonly");
  $(".read-only input, .read-only textarea").prop("readonly", "readonly");
  $("input:read-only").css("background-color", "#d3d3d359");
});
