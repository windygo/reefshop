<?php
/**
 * Plugin Name: Disable Gravity Forms Fields
 * Plugin URI: https://renventura.com/
 * Description: Disable form fields by adding the CSS class "disabled". Fields can also be set to the readonly attribute with a class of "readonly".
 * Author: Ren Ventura
 * Author URI: https://renventura.com/
 * Version: 1.4
 * License: GPL 2.0
 */


//* Bail if accessed directly
if ( ! defined( 'WPINC' ) ) {
	die;
}


/**
 * Enqueue JS
 */
function rv_disable_gf_fields_enqueues() {
	wp_enqueue_script( 'disable-gf-fields', plugins_url( '/disable-gf-fields.js', __FILE__ ), array( 'jquery' ) );
}
add_action( 'wp_enqueue_scripts', 'rv_disable_gf_fields_enqueues' );