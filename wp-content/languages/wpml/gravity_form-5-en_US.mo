��    �      4      L
      L
    M
  $   O     t  Z   �  Y   �     G     U     d     s     �     �     �     �     �     �     �     �     �     	          -     B     P     _     n     }     �     �     �     �     �     �     �     �               %     4     C     R     a     o     ~     �     �     �     �     �     �     �     �     
          *     9     H     V     d  
   w     �  $   �  *   �      �          '     C     Y     s     �     �     �     �     	     '     G     e     ~     �     �  %   �  %   �  %     %   :  %   `  $   �  $   �     �  %   �  %     %   6  %   \  %   �  $   �  $   �     �       "     D   B     �     �  !   �     �     �     �  A   �  %   0  )   V  /   �  "   �  &   �  ^   �     Y     `  '   n  �  �  P   \     �     �     �     �     �             
   4     ?     W     u     ~     �  
   �  
   �  
   �     �     �     �     �     �               ,     >     P     h     z  E   �     �     �     	          -     4     @  '   L     t     �     �     �  
   �     �     �     �            %  �   &  (   �  !     :   <  &   w  
   �  
   �     �     �     �     �     �     �                     $     2  0   K  	   |  �   �     Q      V      h      �      �      �      �       �   	   �      �   !   !     7!     T!  (   r!     �!     �!  	   �!     �!     �!     �!     "     $"  
   B"     M"     ["     q"     �"     �"     �"  N   �"     #  �  %#     �$  B   �$     %     	%     %     #%     <%     C%     O%     \%     i%     n%     u%     |%     �%     �%     �%     �%     �%     �%     �%     �%     �%     �%     �%     �%     �%     �%     &     &     &     *&     7&     D&     K&     X&     e&     r&     &     �&     �&     �&     �&  	   �&  &   �&     �&     �&     '     5'     B'     F'  0   T'     �'      �'     �'     �'     (  N   (     h(     m(     z(  �  �(  B   *  
   \*     g*     t*     �*     �*     �*     �*     �*     �*     �*     �*     �*     �*     +     +     +     +     .+     >+     N+     c+  	   j+     t+     �+     �+     �+     �+     �+  :   �+     ,      ,     >,     K,     X,     ],     k,     y,  	   �,  
   �,     �,     �,     �,     �,     �,  
   �,     �,   <ul class="policy">
<li>
הרשמה לפי חודש קלנדרי לא לפי מספר שבועות
</li>
<li>
במקרה של ביטול החוג יהיה החזר כספי יחסי כשהחיוב עבור כל חודש שנעשה הוא 450 ₪
</li>
</ul> [sc name="declarations-hugim-youth"] [sc name="terms-hugim-youth"] checkbox-47-choice-0-קראתי-ומאשר-תקנון-תשלום-והצהרת-בריאות checkbox-58-choice-0-אני-מעוניין/ת-לקבל-עדכונים-במייל-מריף field-1-label field-10-label field-11-label field-12-label field-13-label field-14-label field-15-label field-16-label field-17-label field-18-label field-19-label field-20-label field-23-label field-24-description field-24-label field-29-description field-3-label field-31-label field-33-label field-34-label field-36-label field-37-label field-38-label field-39-label field-4-label field-40-label field-41-content field-41-label field-43-label field-44-content field-44-label field-46-label field-47-label field-48-label field-49-label field-5-label field-53-label field-54-label field-55-label field-57-label field-58-label field-59-label field-6-label field-61-label field-62-label field-64-description field-64-label field-65-content field-65-label field-66-label field-7-label field-9-label form_submit_button form_title option-64-choice-0-בחר option-64-choice-1-פעם-בשבוע option-64-choice-2-פעמיים-בשבוע option-64-choice-3-חד-פעמי select-36-choice-0-זכר select-36-choice-1-נקבה select-36-placeholder select-53-choice-0-בחר select-53-choice-1-מתחיל select-53-choice-2-בינוני select-53-choice-3-מתקדם select-54-choice-0-בחר select-54-choice-0-בחר-value select-54-choice-1-מתחיל select-54-choice-2-בינוני select-54-choice-3-מתקדם select-6-choice-0-זכר select-6-choice-1-נקבה select-6-placeholder select-61-choice-0-בחר select-61-choice-1-יום-א-15-17:00 select-61-choice-2-יום-ב-15-17:00 select-61-choice-3-יום-ג-15-17:00 select-61-choice-4-יום-ד-15-17:00 select-61-choice-5-יום-ה-15-17:00 select-61-choice-6-שישי-11-13:00 select-61-choice-7-שישי-14-16:00 select-62-choice-0-בחר select-62-choice-1-יום-א-15-17:00 select-62-choice-2-יום-ב-15-17:00 select-62-choice-3-יום-ג-15-17:00 select-62-choice-4-יום-ד-15-17:00 select-62-choice-5-יום-ה-15-17:00 select-62-choice-6-שישי-11-13:00 select-62-choice-7-שישי-14-16:00 select-7-placeholder אישור התקנון אישור תקנון והצהרה אני מעוניין/ת לקבל עדכונים במייל מריף בחר בחר יום בשבוע בחר יום נוסף בשבוע בינוני גיל גיל אח/ות החיוב הינו עבור 9 חודשים עד 3 תשלומים הנחת אחים לפעם בשבוע הנחת אחים פעמיים בשבוע הצהרה בריאות וידיעת שחייה הצהרה/תקנון בריאות הצטרפות לרשימת דיוור התשלום הינו מראש ל 9 חודשים וניתן לשלמו עד 3 תשלומים.  זכר חד פעמי חוג גלישה ילדים ונוער חוג ימי שישי- מתחיל ב4.10.19 ומסתיים ב 26.6.20
חוג ימי ראשון- מתחיל ב 6.10.19 ומסתיים ב 21.6.20
 חוג ימי שני - מתחיל ב7.10.19 ומסתיים ב22.6.20
חוג ימי שלישי - מתחיל ב15.10.19 ומסתיים ב 23.6.20
חוג ימי רביעי- מתחיל ב16.10.19 ומסתיים ב24.6.20
חוג ימי חמישי - מתחיל ב10.10.19 ומסתיים 25.6.20
<hr>
ימים א-ה בשעות 15:00-17:00
ימי ו' בשני מחזורים:
11:00-13:00
14:00-16:00
<hr>
ישנה אופציה לפתיחת קבוצה פרטית באמצע שבוע ובכל שעה בתנאי למספר משתתפים מינימלי של 5 חניכים.
 חתימתי מטה מאשרת תנאי התקנון והצהרת הבריאות טלפון בבית יום א	15-17:00 יום ב	15-17:00 יום ג	15-17:00 יום ד	15-17:00 יום ה	15-17:00 יישוב / עיר כתובת כתובת - שורה 2 לו"ז - שעות וימים מייל מין מין אח/ות מיקוד מתחיל מתקדם נייד אח/ות נייד האב נייד האם נייד החניך נקבה סה"כ לתשלום סוג החוג סוג הרשמה פעם בשבוע פעמיים בשבוע פרטי החוג פרטי המועמד/ת קראתי ומאשר תקנון תשלום והצהרת בריאות רמת בגלישה רמת גלישה אח/ות שישי	11-13:00 שישי	14-16:00 שלח שם האב שם האם שם ההורה - פרטי ומשפחה שם משפחה שם פרטי שם פרטי אח/ות ת.ז. תאריך תאריך לידה תאריך לידה אח/ות תאריך תחילה תנאי תשלום והרשמה  <ul class="policy">
<li> Registration is by calendar month, not by weeks</li> 
<li>In case of cancellation the current month will be charged fully at 450 and the remaining months refunded</li> 
</ul> [sc name="declarations-hugim-youth-eng"] [sc name="terms-hugim-youth-eng"] I have read and confirm regulations and health declaration Please send me email updates from Reef First name Home phone Address Address - line 2 City / Town Zip code Mother's name Mother's mobile Father's name Father's mobile Email Class details Surfing Youth & Children The charge is for 9 months, up to 3 installments Total due <ul class="policy">
<li> Registration is by calendar month, not by weeks</li> 
<li>In case of cancellation the current month will be charged fully at 450 and the remaining months refunded</li> 
</ul> Date Registration Type Brothers discount - Once a week First name - Sibling Gender - sibling Age - sibling Mobile - Sibling Brothers discount - Twice a week Last name Registration & payment terms [sc name="terms-hugim-youth-eng"] Registration & payment terms Health & swimming declaration [sc name="declarations-hugim-youth-eng"] Health regulations/declaration Regulation confirmation Approvals Parent - First & Last Name ID Participant's details Surfing skill level Surfing skill level - Sibling Start date Date of birth Join our mailing list Date of birth - Sibling Gender Select a day of the week Select another day of the week The payment is in advance for 9 months and can be divided up to 3 installments Type of class FRI starts on 4.10.19 and ends 26.6.20
MON starts on 7.1.19 and ends 22.6.20
TUE starts on 15.10.19 and ends 23.6.20
WED starts 16.10.19 and ends 24.6.20
THU starts 10.10.19 and ends 25.6.20
<hr>
SUN to THU at 15:00 to 17:00
FRI has two sessions: 11:00 to 13:00 & 14:00 to 16:00
<hr>
There is an option top open a private class midweek anytime provided a min. of 5 participants.
 Schedule - Days, hours My signature below affirms health declaration and club terms above Age Mobile - Participant Send Surfing Youth & Children Select Once a week Twice a week Single visit Male Female Select Select Beginner Intermediate Advanced Select Select Beginner Intermediate Advanced Male Female Select Select SUN 15-17:00 MON 15-17:00 TUE 15-17:00 WED 15-17:00 THU 15-17:00 FRI 11-13:00 FRI 14-16:00 Select SUN 15-17:00 MON 15-17:00 TUE 15-17:00 WED 15-17:00 THU 15-17:00 FRI 11-13:00 FRI 14-16:00 Select Regulation confirmation Approvals Please send me email updates from Reef Select Select a day of the week Select another day of the week Intermediate Age Age - sibling The charge is for 9 months, up to 3 installments Brothers discount - Once a week Brothers discount - Twice a week Health & swimming declaration Health regulations/declaration Join our mailing list The payment is in advance for 9 months and can be divided up to 3 installments Male Single visit Surfing Youth & Children FRI starts on 4.10.19 and ends 26.6.20
MON starts on 7.1.19 and ends 22.6.20
TUE starts on 15.10.19 and ends 23.6.20
WED starts 16.10.19 and ends 24.6.20
THU starts 10.10.19 and ends 25.6.20
<hr>
SUN to THU at 15:00 to 17:00
FRI has two sessions: 11:00 to 13:00 & 14:00 to 16:00
<hr>
There is an option top open a private class midweek anytime provided a min. of 5 participants.
 My signature below affirms health declaration and club terms above Home phone SUN 15-17:00 MON 15-17:00 TUE 15-17:00 WED 15-17:00 THU 15-17:00 City / Town Address Address - line 2 Schedule - Days, hours Email Gender Gender - sibling Zip code Beginner Advanced Mobile - Sibling Father's mobile Mother's mobile Mobile - Participant Female Total due Type of class Registration Type Once a week Twice a week Class details Participant's details I have read and confirm regulations and health declaration Surfing skill level Surfing skill level - Sibling FRI 11-13:00 FRI 14-16:00 Send Father's name Mother's name Parent - First & Last Name Last name First name First name - Sibling ID Date Date of birth Date of birth - Sibling Start date Registration & payment terms 