<?php
/**
 * Functions.
 *
 * @package Page Builder Framework Child
 */

defined( "ABSPATH" ) || die( "Can't access directly" );

/**
 * Child theme setup.
 */
function wpbf_child_theme_setup() {

	// Textdomain.
	load_child_theme_textdomain( 'page-builder-framework-child', WPBF_CHILD_THEME_DIR . '/languages' );

}
add_action( 'after_setup_theme', 'wpbf_child_theme_setup' );

/**
 * Enqueue scripts & styles.
 */
function wpbf_child_scripts() {

	// Styles.
	wp_enqueue_style( 'wpbf-style-child', WPBF_CHILD_THEME_URI . '/style.css', false, WPBF_CHILD_VERSION );

	// Font Awesome Pro (local)
	wp_enqueue_style( 'wpbf-style-fa-pro', '/wp-content/node_modules/@fortawesome/fontawesome-pro/css/all.css', false, WPBF_CHILD_VERSION );
	

	if (is_rtl()){
		wp_enqueue_style( 'wpbf-style-child-rtl', WPBF_CHILD_THEME_URI . '/style-rtl.css', false, WPBF_CHILD_VERSION );
	}

	// Scripts (uncomment if needed).
	wp_enqueue_script( 'wpbf-site-child', WPBF_CHILD_THEME_URI . '/js/site-child.js', false, WPBF_CHILD_VERSION, true );

}
add_action( 'wp_enqueue_scripts', 'wpbf_child_scripts', 13 );

// Custom Functions - Zvi Eshel

// WooCommerce
// Remove Single Product Images
// function remove_single_product_image( $html, $thumbnail_id ) {
// 	return '';
// }

// add_filter( 'woocommerce_single_product_image_thumbnail_html', 'remove_single_product_image', 10, 2 );

// Adding a language class to the body to apply styles individually per language
add_filter('body_class', 'append_language_class');
function append_language_class($classes){
$classes[] = 'language-'.ICL_LANGUAGE_CODE; 
return $classes;
}

// Gravity Forms // 0 Decimal Currencies
add_filter( 'gform_currencies','gravity_forms_decimals_value');

function gravity_forms_decimals_value( $currencies ) {
	$currencies['ILS']['decimals'] = 0;
	return $currencies;
}

// Remove product category meta
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

// Remove category from related products
function exclude_brands_from_related( $categories ){
	// array of category id's that should be excluded
	$exclude_cats = array( '147');
	foreach( $categories as $index => $cat ){
		if( in_array( $cat->term_id, $exclude_cats ) ){
		unset($categories[$index]);
		}
	}
		return $categories;
	}	
	add_filter( 'woocommerce_get_related_product_cat_terms', 'exclude_brands_from_related' );


// Remove Product Description Tab
add_filter( 'woocommerce_product_tabs', 'reef_remove_description_tab', 11 );
 
function reef_remove_description_tab( $tabs ) {
 
	unset( $tabs['description'] );
	return $tabs;
 
}

// Remove Additional Information
add_filter( 'woocommerce_product_tabs', 'reef_remove_additional_information' );
 
function reef_remove_additional_information( $tabs ) {
 
	unset( $tabs['additional_information'] );
	return $tabs;
 
}

// Remove Cardcom Payment Gateway if coupon 'COD' applied
add_filter('woocommerce_available_payment_gateways', 'switch_gateway_if_cod');

function switch_gateway_if_cod($available_gateways)
{

    $coupons = WC()->cart->applied_coupons;
		$cod_applied = false;

    foreach ($coupons as $coupon) {

				// Compare coupon with its lower case value
        if (strtolower($coupon) == 'cod') { //here you can specific your coupon name
						$cod_applied = true;	
						break;		      
        }

    }

		?> 
		<script>		
		// Equals 'true' if coupon COD applied, othewise 'false'
		coupon_cod = <?php echo json_encode($cod_applied); ?>;
		// console.log('cod status in js', cod_applied);
		// reef_set_gateways();
		jQuery(document).ready(function($){
			jQuery("li.wc_payment_method.payment_method_cod").hide();
			if (coupon_cod) {
				jQuery("li.wc_payment_method.payment_method_cardcom").hide();
				jQuery("li.wc_payment_method.payment_method_cod").show();
			} else {
				jQuery("li.wc_payment_method.payment_method_cardcom").show();
				jQuery("li.wc_payment_method.payment_method_cod").hide();
			}
		});
		</script>
		<?php    
	return $available_gateways;
}
	
// Remove sort by dropdown
// remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

///////////////////////////////////////////////////////
// Older Snippets from Original Work -- Zvi NOV 2018 //
///////////////////////////////////////////////////////

// Return current year to Gravity Forms current_year -- Zvi Eshel -- 22 Nov 2018

add_filter( 'gform_field_value_current_year', 'my_current_year_population_function' );
function my_current_year_population_function( $value ) {
    return date('Y');
}

// Admin bar shortcut to WooCommerce Orders
// add a link to the WP Toolbar
function custom_toolbar_link($wp_admin_bar) {
	$args = array(
			'id' => 'orders',
			'title' => 'הזמנות', 
			'href' => 'https://shop.reefseacenter.co.il/wp-admin/edit.php?post_type=shop_order', 
			'meta' => array(
					'class' => 'wc-orders', 
					'title' => 'הזמנות מועדון ריף'
			),
	);
	$wp_admin_bar->add_node($args);
}

add_action('admin_bar_menu', 'custom_toolbar_link', 999);

// Admin bar shortcut to WooCommerce Reports by Profiles
// add a link to the WP Toolbar
function custom_toolbar_link2($wp_admin_bar) {
	$args = array(
			'id' => 'reports',
			'title' => 'דוחות', 
			'href' => 'https://shop.reefseacenter.co.il/wp-admin/admin.php?page=wc-order-export&tab=profiles', 
			'meta' => array(
					'class' => 'wc-reports', 
					'title' => 'דוחות'
					)
	);
	$wp_admin_bar->add_node($args);
}
add_action('admin_bar_menu', 'custom_toolbar_link2', 999);


// Add Refresh button after WC Produt Title
function action_woocommerce_before_single_product( $wc_print_notices ) { 
	// F
$classes = get_body_class();
if (in_array('product_cat_woo-forms',$classes)&& is_product()) {
	echo do_shortcode( '[sc name="refresh-page"]' );
}	
}; 


add_action( 'woocommerce_thankyou', 'ze_add_transpo_to_order', 5 );
 
function ze_add_transpo_to_order( $order_id ) { 
 
    if ( ! $order_id ) {
        return;
    }
 
// 1. Get order object
 
    $order = wc_get_order( $order_id );
 
// 2. Initialise $cat_in_order variable
   
    $cat_in_order = false;
	 
// 3. Get order items and loop through them...
// ... if product in category, edit $cat_in_order
   
    $items = $order->get_items(); 
     
    foreach ( $items as $item ) {
         
        $product_id = $item['product_id'];
      
        if ( has_term( 'transpo', 'product_cat', $product_id ) ) {
            $cat_in_order = true;
            break;
        }
     
    }
 
// 4. Echo image only if $cat_in_order == true
   
    if ( $cat_in_order ) {   
    echo do_shortcode( '[sc name="transpo-link"]' );
	$transpo_email = true;
    }
   
}


// Add Transpo link to WC Orders Completed Emails if a product with Transpo category included in the order
add_action( 'woocommerce_email_before_order_table', 'add_order_email_transpo_instructions', 10, 2 );
 
function add_order_email_transpo_instructions( $order_id ) {
  
   if ( ! $order_id ) {
        return;
    }
 
// 1. Get order object
 
    $order = wc_get_order( $order_id );
 
// 2. Initialise $cat_in_order variable
   
    $cat_in_order = false;
	 
// 3. Get order items and loop through them...
// ... if product in category, edit $cat_in_order
   
    $items = $order->get_items(); 
     
    foreach ( $items as $item ) {
         
        $product_id = $item['product_id'];
      
        if ( has_term( 'transpo', 'product_cat', $product_id ) ) {
            $cat_in_order = true;
            break;
        }
     
    }
 
// 4. Echo image only if $cat_in_order == true
   
    if ( $cat_in_order ) {   
        echo '<div style="background:yellow;margin:0 0 16px; padding:10px; border-radius:5px;" ><p>';
		echo do_shortcode( '[sc name="transpo-link"]' ); 
		echo '</p></div><br>';   
    }
}



// Santize unwanted strings from AlgoPlus Woo Export Reports
// filter out the ILS currency symbol from Woocommerce Exported Orders as XLS
add_filter( 'woe_fetch_order_row', function ( $row,$order_id ) {
	foreach($row as $k=>$v)
	$row[$k] = str_replace( "&#8362;", "", $v); 
	$row[$k] = str_replace( "<a href='mailto:", "", $v); 
	$row[$k] = str_replace( "'>", "", $v); 
	$row[$k] = str_replace( "</a>", "", $v); 
	$row[$k] = str_replace( "</li></ul>", "", $v); 
	$row[$k] = str_replace( "<ul class='bulleted'><li>", "", $v); 
  	return $row;
},10,2);

// add_filter( 'body_class','custom_body_class' );
// function custom_body_class( $classes ) {
    
//     global $post;
//     $post_content = $post->post_content;
//     $prod_id = explode('[',$post_content);
//     $pid = $prod_id[1];
//     $prod = explode('=',$pid);
//     $productid = $prod[1];
//     $product_id = trim($productid,'"]');
//     $terms = get_the_terms ( $product_id, 'product_cat' );
//     foreach ( $terms as $term ) {
//      $cat_id = $term->term_id;
//     }
    
//     if (is_page() && ($cat_id == '147')) { category.
//         $classes[] = 'woo-forms';
//     }

//     return $classes;
    
// }

// ===== Support for WooCommerce Order and API Calls - Zvi Eshel ======
/**
 * Handle a custom 'customvar' query var to get orders with the 'customvar' meta.
 * @param array $query - Args for WP_Query.
 * @param array $query_vars - Query vars from WC_Order_Query.
 * @return array modified $query
 */
function handle_custom_query_var( $query, $query_vars ) {
	if ( ! empty( $query_vars['customvar'] ) ) {
		$query['meta_query'][] = array(
			'key' => 'customvar',
			'value' => esc_attr( $query_vars['customvar'] ),
		);
	}

	return $query;
}
add_filter( 'woocommerce_order_data_store_cpt_get_orders_query', 'handle_custom_query_var', 10, 2 );

// Custom Admin Styles
function customAdmin() {
	$url = get_settings('siteurl');
	$url = $url . '/wp-content/themes/page-builder-framework-child/style-admin.css';
	echo '<!-- custom admin css -->
				<link rel="stylesheet" type="text/css" href="' . $url . '" />
				<!-- /end custom adming css -->';
}
add_action('admin_head', 'customAdmin');


